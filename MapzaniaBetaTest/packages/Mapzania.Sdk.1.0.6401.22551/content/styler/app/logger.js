﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('logger', service);

    function service() {

        return {
            error: error
        };

        ////////////

        function error(module, e) {

            if (typeof e === 'string' || e instanceof String)
            {
                console.log(e);
                return;
            }

            var newMessage = 'XHR Failed for ' + module;

            if (e.data && e.data.description) {
                newMessage = newMessage + '\n' + e.data.description;
            }

            if (!e.data)
                e.data = {};

            e.data.description = newMessage;

            console.log(newMessage);
        };
    }

})();