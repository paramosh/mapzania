﻿var gmap;

function MapController(mapDef, api) {

    if (MapController.prototype.watchedMap)
        unwatch(MapController.prototype.watchedMap);

    var map = mapDef.entity;

    MapController.prototype.watchedMap = map;

    var mzmap = new Mapzania.Map("map", mapDef.schema);

    gmap = mzmap;

    var dirty = false;

    var timeoutTime = 1000;

    var whenTimeout = function () {
        //console.log("tick");
        if (dirty) {
            api.saveMap(map);
            dirty = false;
        }
    };

    var timeoutHandle = setInterval(whenTimeout, timeoutTime);

    watch(map, function (prop, action, newvalue, oldvalue) {

        log(this, prop, action, newvalue, oldvalue);

        if (dirty) {
            clearInterval(timeoutHandle);
            timeoutHandle = setInterval(whenTimeout, timeoutTime);
        }

        dirty = true;

        switch (this.type) {

            //////
            case "Map":

                switch (prop) {

                    case "backColor":
                        setBackColor(this["backColor"]);
                        break;

                    default:

                }

                break;

                //////
            case "VectorLayer":
            case "RasterLayer":

                switch (prop) {

                    case "visible":
                        setLayerVisibility(this.key, this[prop]);
                        break;

                    default:
                        setLayerProperty(this.key, prop, this[prop]);
                }
                break;

                //////
            case "Layers":

                switch (action) {

                    case "push":
                        addLayer(newvalue);
                        break;

                    case "splice":
                        removeLayer(oldvalue[0]);
                        break;

                    default:
                        throw "Layers: Unknown action.";
                }
                break;

                //////
            case "StandardTheme":
            case "StandardLabeller":
            case "RangeTheme":
            case "ThemeValue":
            case "ThemeValues":

                updateThemes();

                break;

                //////
            default:

        }

        if (action == "splice" &&
            (oldvalue[0].type == "RangeTheme"
            || oldvalue[0].type == "StandardTheme")) {
            updateThemes();
        }

        if (action == "splice" &&
            (oldvalue[0].type == "RangeTheme"
            || oldvalue[0].type == "StandardTheme")) {
            updateThemes();
        }

    }, 10);

    function log(object, prop, action, newvalue, oldvalue) {

        console.log("-------------WATCH:Map Changed---------------------")
        console.log("PROP:" + prop);
        console.log("ACTION:" + action);
        console.log("TYPE:" + object.type);
        console.log(newvalue);
        console.log(oldvalue);
        console.log(object);
        console.log("----------------------------------------------------")
    }

    function setBackColor(color) {
        mzmap.setBackColor(color)
    }

    function setLayerVisibility(layerKey, isVisible) {
        if (isVisible)
            mzmap.showLayer(layerKey);
        else
            mzmap.hideLayer(layerKey);
    }

    function setLayerProperty(layerKey, property, value) {

        var model = mzmap._stylerInternals().model;

        var layer = model.layers.find(m => {
            return m.key == layerKey;
        });

        layer[property] = value;

        if (property == "greyScale") {
            var bbox = mzmap.getBoundingBox();

            mzmap = new Mapzania.Map("map", model.getSchema(), function () {
                mzmap.fitToBoundingBox(bbox);
            });
        }
        else {
            mzmap.updateLayer(layerKey);
        }
    }

    function updateThemes() {

        setTimeout(m=> {
            console.log("executing updatethemes...")
            var model = mzmap._stylerInternals().model;

            for (var i = 0; i < map.layers.length; i++) {
                var lyr = map.layers[i];

                if (lyr.themes) {

                    var modelLayer = model.layers.find(m => {
                        return m.key == lyr.key;
                    });

                    modelLayer.themes = lyr.themes;

                    mzmap.updateLayer(modelLayer.key);
                    mzmap.updateLayer(modelLayer.key);
                }
            }
        }, 200);
    }

    function addLayer(layer) {
        mzmap.addLayer(layer.key, layer);
        mzmap.updateLayer(layer.key);
    }

    function removeLayer(layer) {
        console.log("-------HERE -----")
        console.log(layer)
        mzmap.removeLayer(layer.key);
        mzmap.refresh();
    }
}