﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('upgrader', service);



    function service() {

        return {
            upgradeMap: upgradeMap
        };

        /////////////////////////////

        function upgradeMap(map) {

            map.toString = function () {
                return this.key;
            }

            //map.layers.type = "Layers";

            for (var i = 0; i < map.layers.length; i++) {
                var layer = map.layers[i];

                layer.toString = function () {
                    return this.key;
                }

                if (layer.themes) {
                    for (var j = 0; j < layer.themes.length; j++) {
                        layer.themes[j] = upgradeTheme(layer.themes[j]);
                    }
                }
            }

            return map;
        }

        function hexToRGBA(hex, opacity) {
            hex = hex.replace('#', '');

            var r = parseInt(hex.substring(0, 2), 16);
            var g = parseInt(hex.substring(2, 4), 16);
            var b = parseInt(hex.substring(4, 6), 16);

            var result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
            return result;
        }

        function upgradeColor(symbol, colorFieldName, opacityFieldName) {

            if (!colorFieldName)
                colorFieldName = "color";

            if (!opacityFieldName)
                opacityFieldName = "opacity";

            var color = symbol[colorFieldName];
            var opacity = symbol[opacityFieldName];

            if (color.indexOf("#") != -1) {
                symbol[colorFieldName] = hexToRGBA(color, opacity);
                symbol[opacityFieldName] = 100;
            }
        }

        function upgradeSymbol(symbol) {

            if (symbol == null) {
                return {
                    type: "GenericSymbol",
                    iconSize: 8,
                    iconPath: null,
                    fillColor: "rgba(0,0,0,0)",
                    strokeColor: "rgba(0,0,0,1)",
                    strokeWidth: 0
                };
            }

            if (symbol.type == "GenericSymbol")
                return symbol;

            if (symbol.type == "LabelSymbol")
                return symbol;

            if (symbol.type == "CompoundPointSymbol") {

                symbol = symbol.points[symbol.points.length - 1];
            }

            if (symbol.type == "PointSymbol") {

                upgradeColor(symbol);

                var res = {
                    type: "GenericSymbol",
                    iconSize: symbol.size,
                    iconPath: symbol.imageUrl,
                    fillColor: symbol.color,
                    strokeColor: "rgba(0,0,0,1)",
                    strokeWidth: 0
                };

                if (symbol.outline) {

                    if (symbol.outline.type == "CompoundLineSymbol")
                        symbol.outline = symbol.outline.lines[symbol.outline.lines.length - 1];

                    upgradeColor(symbol.outline);
                    res.strokeColor = symbol.outline.color;
                    res.strokeWidth = symbol.outline.size;
                }

                return res;
            }

            if (symbol.type == "CompoundLineSymbol") {

                symbol = symbol.lines[symbol.lines.length - 1];
            }

            if (symbol.type == "LineSymbol") {

                upgradeColor(symbol);

                return {
                    type: "GenericSymbol",
                    iconSize: null,
                    iconPath: null,
                    fillColor: null,
                    strokeColor: symbol.color,
                    strokeWidth: symbol.size
                };
            }

            if (symbol.type == "FillSymbol") {

                upgradeColor(symbol);

                var res = {
                    type: "GenericSymbol",
                    iconSize: null,
                    iconPath: null,
                    fillColor: symbol.color
                };

                if (symbol.outline) {

                    if (symbol.outline.type == "CompoundLineSymbol")
                        symbol.outline = symbol.outline.lines[symbol.outline.lines.length - 1];

                    upgradeColor(symbol.outline);
                    res.strokeColor = symbol.outline.color;
                    res.strokeWidth = symbol.outline.size;
                }

                return res;
            }

            console.log(symbol);

            throw "upgrader.upgradeSymbol: Unknown Symbol type: " + symbol.type;
        }

        function upgradeTheme(theme) {
            theme.toString = function () {
                return "ZOOM: " + this.minZoom + "-" + this.maxZoom;
            }

            theme.labeller = upgradeLabeller(theme.labeller);

            switch (theme.type) {

                case "StandardTheme":
                    theme.symbol = upgradeSymbol(theme.symbol);
                    return theme;

                case "RangeTheme":
                    theme.defaultSymbol = upgradeSymbol(theme.defaultSymbol);

                    theme.values.type = "ThemeValues";

                    for (var i = 0; i < theme.values.length; i++) {
                        var val = theme.values[i];
                        upgradeThemeValue(val);
                    }
                    return theme;

                default:
                    throw "api.initTheme: Unknown theme type";

            }
        }

        function upgradeThemeValue(val) {

            val.toString = function () {

                if (this.value)
                    return this.value;

                return this.min + " to " + this.max;
            }

            val.type = "ThemeValue";
            val.symbol = upgradeSymbol(val.symbol);
            val.labeller = upgradeLabeller(val.labeller);
        }

        function upgradeLabeller(val) {

            if (val == null) {
                return {
                    type: "StandardLabeller",
                    symbol: {
                        type:"LabelSymbol",
                        color: "#000000",
                        opacity: 100,
                        offset: { x: 0, y: 0 },
                        fontSize: 10,
                        fontFamily: "Arial",
                        haloColor: "#FFFFFF",
                        haloThickness: 0,
                        visible: false
                    }
                }
            }

            val.visible = true;
            return val;
        }
    }
})();