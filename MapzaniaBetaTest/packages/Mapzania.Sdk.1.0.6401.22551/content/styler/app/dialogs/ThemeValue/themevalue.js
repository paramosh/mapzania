﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('themeValueDialog', controller);

    function controller(data, events) {
        var vm = this;

        vm.symbol = data.value.symbol;
        vm.value = data.value;

        vm.cancel = cancel;
        vm.apply = apply;

        /////////////////////

        function cancel() {
            events.cancel();
        }

        function apply() {
            events.complete(vm.value);
        }
    }

})();