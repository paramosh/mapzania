﻿(function () {
    'use strict';

    angular
        .module("app")
        .directive("labellerField", directive);

    //////////////////////////////////

    directive.$inject = ['api'];

    function directive(api) {

        return {

            scope: {
                model: '=ngModel',
                property: '@',
                title: '@',
                toolTip: '@'
            },
            templateUrl: api.getTemplateUrl("widgets/LabellerField/LabellerField.html"),
            controller: controller,
            controllerAs: "vm",
            bindToController: true
        };
    }

    controller.$inject = ['$q', 'dialogs'];

    function controller($q, dialogs) {
        var vm = this;

        vm.showDialog = showDialog;
        vm.showHelp = showHelp;

        //////////////////////////

        function showDialog() {

            return $q((resolve, reject) => {

                var current = angular.copy(vm.model[vm.property]);

                dialogs.show('labeller',
                    { current: current })
                    .then(data => {
                        vm.model[vm.property] = data;
                    });
            });
        }

        function showHelp(ev) {
            dialogs.showHelp(ev, vm.title, vm.toolTip);
        }
    }

})();