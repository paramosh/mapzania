﻿/// <reference path="../mapzania.js"/>

var Mapzania = (function (ns) {

    function FeatureEngine(features, filters) {

        this.run = function () {
            for (var i = 0; i < filters.length; i++)
                filters[i](features);
        }
    }

    ns.FeatureEngine = FeatureEngine;

    return ns;

})(Mapzania || {});