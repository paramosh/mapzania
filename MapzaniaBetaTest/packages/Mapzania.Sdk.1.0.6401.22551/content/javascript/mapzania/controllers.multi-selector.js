﻿var Mapzania = (function (ns) {
    if (!ns.Controllers)
        ns.Controllers = {};

    var MultiSelector = function (viewport, mapper, callback, pointOnly, showFill) {

        //if (!MultiSelector.prototype.instances)
        //    MultiSelector.prototype.instances = [];

        //MultiSelector.prototype.instances.push(this);

        //this.id = MultiSelector.prototype.instances.length;

        this.viewport = viewport;
        this.mapper = mapper;
        this._reset = false;

        this.points = [];

        var render = function (line, ping) {

            var max = 20;
            var radius = 3;
            var opacity = 100;


            mapper.clearLayer("ts_interactive");

            if (line.points.length > 1) {

                if (showFill)
                    mapper.fillPolygon("ts_interactive", [line], "#000000", null, 20);
                mapper.drawLine("ts_interactive", line, "#000000", 2, 100);
                mapper.drawLine("ts_interactive", line, "#FFFF00", 4, 60);

                for (var i = 0; i < line.points.length; i++) {
                    mapper.fillCircle("ts_interactive", line.points[i], 4, "#FFFF00", 60);
                    mapper.fillCircle("ts_interactive", line.points[i], 2, "#000000", 100);
                }
            }
            else {
                mapper.clearLayer("ts_interactive");
                mapper.fillCircle("ts_interactive", line.points[0], 4, "#FFFF00", 60);
                mapper.fillCircle("ts_interactive", line.points[0], 2, "#000000", 100);
            }

            mapper.flush();

            var iter = 0;

            var id = setInterval(function () {
                mapper.clearLayer("ts_interactive_2");

                mapper.drawCircle("ts_interactive_2", ping, radius, "#FF0000", 2, opacity);
                mapper.drawCircle("ts_interactive_2", ping, radius, "#FFFFFF", 4, opacity);
                mapper.flush();

                radius *= 1.2;
                opacity *= 0.6;

                if (iter++ > 5) {
                    mapper.clearLayer("ts_interactive_2");
                    clearInterval(id);
                }
            }, 50);
        };

        var me = this;

        viewport.mouseUp.add(function (evt) {
            if (!me._enabled)
                return;

            clicked(evt);
        });

        var clicked = function (evt) {

            if (pointOnly || me._reset) {
                me.points = [];
                me._reset = false;
            }

            me.points.push(evt.data);

            line = { points: me.points };

            if (me.points.length > 2 && showFill) {
                var pnts = me.points.concat([me.points[0]]);
                line = { points: pnts };
            }

            render(line, evt.data);

            if (pointOnly && callback)
                callback(me.points);
        }

        viewport.doubleClick.add(function (evt) {
            if (!me._enabled)
                return;

            clicked(evt);

            me._reset = true;

            if (callback)
                callback(me.points);
        });        
    }

    MultiSelector.prototype.enabled = function (val) {
        if (typeof val != 'undefined') {
            this._enabled = val;
            this.mapper.clearLayer("ts_interactive");
            this.points = [];
        }
        return this._enabled;
    }

    MultiSelector.prototype.canZoomMap = function () {
        return true;
    }

    MultiSelector.prototype.isTempState = function () {
        return false;
    }

    ns.Controllers.MultiSelector = MultiSelector;

    return ns;
})(Mapzania || {})
