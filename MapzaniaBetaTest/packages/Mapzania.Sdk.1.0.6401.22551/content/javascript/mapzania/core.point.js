﻿/**
 * @doc Mapzania.Point.*ClassDescription*
 *
 * Encapsulates a single Point geometry. A point geometry defines a x- and y- coordinate. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.Point.Geometry
      *
      * Constructs a Mapzania Point Geometry.
      *
      * @param x (Number) The x-coordinate for the point.
      * @param y (Number) The y-coordinate for the point.
      */
	function Point(x, y) {
		this.x = !x ? 0 : x;
		this.y = !y ? 0 : y;
	}

    /**
      * @doc Mapzania.Point.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	Point.prototype.read = function (data) {
	    this.x = data[0][0][0];
		this.y = data[0][0][1];
		return this;
	}

    /**
      * @doc Mapzania.Point.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	Point.prototype.getExtent = function () {
		return new ns.Extent({
			minX: this.x,
			minY: this.y,
			maxX: this.x,
			maxY: this.y
		});
	}

    /**
      * @doc Mapzania.Point.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	Point.prototype.distanceTo = function (point) {
	    var dx = this.x - point.x;
	    var dy = this.y - point.y;

	    return Math.sqrt(dx * dx + dy * dy);
	}

    /**
      * @doc Mapzania.Point.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	Point.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.Point.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	Point.prototype.getPath = function () {
	    return [[[this.x, this.y]]];
	}

    /**
      * @doc Mapzania.Point.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	Point.prototype.getSize = function () {
		return 1;
	}

    /**
      * @doc Mapzania.Point.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	Point.prototype.getCentroid = function () {
	    return new Point(this.x, this.y);
	}

    /**
      * @doc Mapzania.Point.rotate
      *
      * Rotates a point around an origin, by a number of degrees (provided in radians). This method returns a new Point instance.
      *
      * @param origin (Mapzania.Point) The origin point.
      *
      * @param angleRandians (Number) The angle, in radians. A positive value indicates an anti-clockwise direction.
      *
      * @return (Mapzania.Point) A rotated point instance.
      */

	Point.prototype.rotate = function (origin, angleRandians) {
	    var _cos = Math.cos(angleRandians);
	    var _sin = Math.sin(angleRandians);

	    // Rotate the point (counter clockwise)
	    var x = (this.x - origin.x) * _cos - (this.y - origin.y) * _sin + origin.x;
	    var y = (this.x - origin.x) * _sin + (this.y - origin.y) * _cos + origin.y;

	    return new Mapzania.Point(x, y);
	}

    /**
      * @doc Mapzania.Point.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Point.prototype.getGeometryType = function () {
	    return 1;
	}

    /**
      * @doc Mapzania.Point.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Point.prototype.getType = function () {
	    return 1;
	}

    /**
      * @doc Mapzania.Point.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Point) An exact deep clone of this instance.
      */

	Point.prototype.clone = function () {
	    var result = new Mapzania.Point();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.Point.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	Point.prototype.translate = function (x, y) {
	    this.x += x;
	    this.y += y;
	}

	ns.Point = Point;

	if (!ns.FeatureTypes)
	    ns.FeatureTypes = {};

	ns.FeatureTypes["1"] = Point;

	return ns;

})(Mapzania || {})
