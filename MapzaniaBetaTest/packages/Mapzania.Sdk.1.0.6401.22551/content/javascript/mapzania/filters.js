﻿var Mapzania = (function (ns) {

    var me = this;

    var _setModel = function (model) {
        me.model = model;
    }

    var filterByText = function (statement, passthru) {
        if (!passthru)
            passthru = false;

        return {
            type: "FilterByText",
            statement: statement,
            passthru: passthru
        };
    };

    var filterByBoundingBox = function (bbox) {
        return {
            type: "FilterByBoundingBox",
            minX: bbox[0],
            minY: bbox[1],
            maxX: bbox[2],
            maxY: bbox[3]
        };
    };

    var filterByNearest = function (point, take, includeDistance) {
        var n = 1;

        if (take)
            n = take;

        return {
            type: "FilterByNearest",
            x: point.coordinates[0],
            y: point.coordinates[1],
            take: n,
            includeDistance: includeDistance
        };
    };

    var filterByFeatureCollection = function (features, operation) {
        return {
            type: "FilterByFeatureCollection",
            features: features,
            operation: operation || "intersects"
        };
    };

    var filterByLayer = function (layerKey, layerFilters, operation) {
        var key = me.model.getLayer(layerKey).sourceKey;

        var filters = [];

        if (layerFilters)
            filters = layerFilters;
        return {
            type: "FilterByLayer",
            sourceKey: key,
            operation: operation || "intersects",
            filters: filters
        };
    }

    var clipByFeatureCollection = function (features) {
        return {
            type: "ClipByFeatureCollection",
            features: features
        };
    };

    var changeProperties = function (properties) {
        //http://stackoverflow.com/questions/26195975/regex-to-match-only-commas-but-not-inside-multiple-parentheses

        var props = properties;

        if (typeof (properties) == "string")
            props = properties.split(/,(?![^()]*(?:\([^()]*\))?\))/);

        return {
            type: "ChangeProperties",
            properties: props
        };
    };

    var changeCoordinateSystem = function (srid) {
        return {
            type: "ChangeCoordinateSystem",
            srid: srid
        };
    };

    var changeGeometryAccuracy = function (figures) {
        return {
            type: "ChangeGeometryAccuracy",
            figures: figures
        };
    };

    var simplifyGeometry = function (factor) {
        return {
            type: "SimplifyGeometry",
            factor: factor
        };
    };

    var addAreaProperty = function () {
        return {
            type: "AddAreaProperty"
        };
    };

    var addLengthProperty = function () {
        return {
            type: "AddLengthProperty"
        };
    };

    var convertToCentroids = function () {
        return {
            type: "ConvertToCentroids"
        };
    }

    var bufferGeometry = function (factor, side, joins, ends, mitreLimit, srid) {
        return {
            type: "BufferGeometry",
            factor: factor,
            side: side,
            joins: joins,
            ends: ends,
            mitreLimt: mitreLimit,
            srid: srid
        };
    }

    var aggregateByFeatures = function (featuresCollection, aggregation) {
        return {
            type: "AggregateByPolygons",
            features: featuresCollection,
            aggregation: aggregation
        };
    }

    var aggregateByLayer = function (layersource, aggregation) {
        return {
            type: "AggregateByPolygons",
            layerSource: layersource,
            aggregation: aggregation
        };
    }

    var aggregateLinesByOriginAndDestination = function (gridSize, includeCount) {
        if (!includeCount)
            includeCount = true;

        return {
            type: "AggregateLinesByOriginAndDestination",
            gridSize: gridSize,
            includeCount: includeCount
        };
    }

    var clusterPoints = function (factor, properties) {

        if (typeof (properties) == "string")
            properties = properties.split(/,(?![^()]*(?:\([^()]*\))?\))/);

        return {
            type: "ClusterPoints",
            factor: factor,
            properties: properties
        };
    }

    var splitLineByFeatures = function (featureCollection, method) {
        return {
            type: "SplitLineByPolygons",
            features: featureCollection,
            method: method
        };
    }

    var splitLineByLayer = function (layerSource, method) {
        return {
            type: "SplitLineByPolygons",
            layerSource: layerSource,
            method: method
        };
    }

    ns.Filters = {
        _setModel: _setModel,
        filterByText: filterByText,
        filterByBoundingBox: filterByBoundingBox,
        filterByNearest: filterByNearest,
        filterByFeatureCollection: filterByFeatureCollection,
        filterByLayer: filterByLayer,
        clipByFeatureCollection: clipByFeatureCollection,
        changeProperties: changeProperties,
        changeCoordinateSystem: changeCoordinateSystem,
        changeGeometryAccuracy: changeGeometryAccuracy,
        simplifyGeometry: simplifyGeometry,
        addAreaProperty: addAreaProperty,
        addLengthProperty: addLengthProperty,
        convertToCentroids: convertToCentroids,
        bufferGeometry: bufferGeometry,
        aggregateLinesByOriginAndDestination: aggregateLinesByOriginAndDestination,
        clusterPoints: clusterPoints,
        splitLineByFeatures: splitLineByFeatures,
        splitLineByLayer: splitLineByLayer,
        aggregateByFeatures: aggregateByFeatures,
        aggregateByLayer: aggregateByLayer
    };

    return ns;

})(Mapzania || {});