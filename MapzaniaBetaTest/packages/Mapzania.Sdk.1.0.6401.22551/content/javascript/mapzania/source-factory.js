﻿var Mapzania = (function (ns) {

    function SourceFactory(model) {

        var sources = {};
        this.sources = sources;

        this.init = function (callback) {
            model.api.getLayerSources(function (data) {

                for (var i = 0; i < data.length; i++) {
                    var def = data[i];
                    sources[def.key] = def;
                }

                if (callback)
                    callback();
            });
        }

        this.getSource = function (layer) {

            var key = layer.sourceKey.toUpperCase();

            if (!sources[key])
                throw "Invalid sourceKey : " + layer.sourceKey;

            var def = sources[key];

            var srid = model.displaySRID;

            if (def.sourceType == "VECTOR")
                return new ns.VLS2(model, layer);

            if (def.sourceType == "LOCALVECTOR")
                return new ns.VLS2(model, layer, true);

            if (def.sourceType == "TILED_RASTER")
                return new ns.TiledRasterLayerSource(model, layer);

            throw "SourceFactory.getSource: invalid source type.";
        }
    }

    ns.SourceFactory = SourceFactory;

    return ns;
})(Mapzania || {})
