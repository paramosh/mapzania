﻿var Mapzania = (function (ns) {
    function Algo() { }

    Algo.prototype.getExtentFromPoints = function (pnts) {
        if (!pnts || pnts.length == 0)
            throw "pnts not valid";

        var minX = pnts[0].x;
        var minY = pnts[0].y;
        var maxX = pnts[0].x;
        var maxY = pnts[0].y;

        for (var i = 1; i < pnts.length; i++) {
            var p = pnts[i];

            if (p.x < minX)
                minX = p.x;

            if (p.y < minY)
                minY = p.y;

            if (p.x > maxX)
                maxX = p.x;

            if (p.y > maxY)
                maxY = p.y;
        }

        return new ns.Extent({ minX: minX, minY: minY, maxX: maxX, maxY: maxY });
    }

    Algo.prototype.getExtentFromLines = function (lines) {
        var ext = this.getExtentFromPoints(lines[0].points);

        for (var i = 1; i < lines.length; i++) {
            var lineExt = this.getExtentFromPoints(lines[i].points);

            if (lineExt.minX < ext.minX)
                ext.minX = lineExt.minX;
            if (lineExt.minY < ext.minY)
                ext.minY = lineExt.minY;
            if (lineExt.maxX > ext.maxX)
                ext.maxX = lineExt.maxX;
            if (lineExt.maxY > ext.maxY)
                ext.maxY = lineExt.maxY;
        }

        return ext;
    }

    Algo.prototype.ringContainsPoint = function (ring, p) {
        var i, i1;		// point index; i1 = i-1 mod n
        var xInt;		// x intersection of e with ray
        var crossings = 0;	// number of edge/ray crossings
        var x1, y1, x2, y2;
        var nPts = ring.length;

        // For each line edge l = (i-1, i), see if it crosses ray from test point in positive x direction.
        for (i = 1; i < nPts; i++) {
            i1 = i - 1;
            var p1 = ring[i];
            var p2 = ring[i1];
            x1 = p1.x - p.x;
            y1 = p1.y - p.y;
            x2 = p2.x - p.x;
            y2 = p2.y - p.y;

            if (((y1 > 0) && (y2 <= 0)) ||
                ((y2 > 0) && (y1 <= 0))) {
                // e straddles x axis, so compute intersection.
                xInt = (x1 * y2 - x2 * y1) / (y2 - y1);
                //xsave = xInt;
                // crosses ray if strictly positive intersection.
                if (0.0 < xInt) {
                    crossings++;
                }
            }
        }
        // p is inside if an odd number of crossings.
        if ((crossings % 2) == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    ns.Algo = Algo;

    return ns;

})(Mapzania || {});

//
// Intersections
//

var Mapzania = (function (ns) {
    function Intersections() { }

    Intersections.prototype.intersectsPolyPoly = function (poly1, poly2) {
        if (!poly1 || !poly2) throw "Both polygons need to be valid instances.";

        var algo = new Mapzania.Algo();

        var extent1 = poly1.getExtent();
        var extent2 = poly2.getExtent();
        if (!extent1.intersects(extent2)) {
            return false;
        }

        var lineSegments1 = poly1.shell.getLineSegments(null);
        var lineSegments2 = poly2.shell.getLineSegments(null);

        for (var i = 0; i < lineSegments1.length; i++) {
            for (var j = 0; j < lineSegments2.length; j++) {
                if (lineSegments1[i].getIntersectionPoint(lineSegments2[j]) != null) {
                    return true;
                }
            }
        }

        if (algo.ringContainsPoint(poly1.shell.points, lineSegments2[0].p1)) {
            return true;
        }

        if (algo.ringContainsPoint(poly2.shell.points, lineSegments1[0].p1)) {
            return true;
        }

        return false;
    }

    ns.Intersections = Intersections;

    return ns;

})(Mapzania || {});

