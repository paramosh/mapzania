﻿/**
 * @doc Mapzania.MultiLine.*ClassDescription*
 *
 * Encapsulates a single Multi-Line geometry. A multi-line geometry defines a set of line geometries. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.MultiLine.MultiLine
      *
      * Constructs a Mapzania MultiLine Geometry.
      * 
      */
	function MultiLine() {
		this.lines = []
	}

    /**
      * @doc Mapzania.MultiLine.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiLine.prototype.read = function (data) {
	    for (var i = 0; i < data[0].length; i++) {
	        var oneLine = [[[]]];
	        oneLine[0][0] = data[0][i];
	        this.lines.push(new ns.Line().read(oneLine));
	    }

		return this;
	}

    /**
      * @doc Mapzania.MultiLine.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	MultiLine.prototype.getExtent = function () {
		var algo = new ns.Algo();
		return algo.getExtentFromLines(this.lines);
	}

    /**
      * @doc Mapzania.MultiLine.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	MultiLine.prototype.distanceTo = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiLine.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	MultiLine.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiLine.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiLine.prototype.getPath = function () {
	    var lines = [];

	    for (var i = 0; i < this.lines.length; i++) {
	        var line = this.lines[i];
	        var arr = [];
	        for (var j = 0; j < line.points.length; j++) {
	            var pnt = line.points[j];
	            arr.push(pnt.x);
	            arr.push(pnt.y);
	        }
            lines.push(arr);
	    }

	    return [lines];
	}

    /**
      * @doc Mapzania.MultiLine.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	MultiLine.prototype.getSize = function () {
		var res = 0;

		for (var i = 0; i < this.lines.length; i++) {
			res += this.lines[i].getSize();
		}

		return res;
	}

    /**
      * @doc Mapzania.MultiLine.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	MultiLine.prototype.getCentroid = function () {
	    //TODO:Better centroid algorithm
	    return this.getExtent().getCenter();
	}

    /**
      * @doc Mapzania.MultiLine.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiLine.prototype.getGeometryType = function () {
	    return 5;
	}

    /**
      * @doc Mapzania.MultiLine.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiLine.prototype.getType = function () {
	    return 5;
	}

    /**
      * @doc Mapzania.MultiLine.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.MultiLine) An exact deep clone of this instance.
      */

	MultiLine.prototype.clone = function () {
	    var result = new Mapzania.MultiLine();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.MultiLine.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	MultiLine.prototype.translate = function (x, y) {
	    this.lines.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	}

	ns.MultiLine = MultiLine;

	if (!ns.FeatureTypes)
	    ns.FeatureTypes = {};

	ns.FeatureTypes["5"] = MultiLine;

	return ns;

})(Mapzania || {})
