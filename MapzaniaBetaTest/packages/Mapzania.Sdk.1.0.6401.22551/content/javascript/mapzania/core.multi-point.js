﻿/**
 * @doc Mapzania.MultiPoint.*ClassDescription*
 *
 * Encapsulates a single Multi-Point geometry. A multi-point geometry defines a set of point geometries. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.MultiPoint.MultiPoint
      *
      * Constructs a Mapzania MultiPoint Geometry.
      * 
      */
	function MultiPoint() {
		this.points = []
	}

    /**
      * @doc Mapzania.MultiPoint.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPoint.prototype.read = function (data) {
		for (var i = 1; i < data[0][0].length; i += 2)
			this.points.push(new ns.Point().read([[[data[0][0][i - 1], data[0][0][i]]]]))

		return this
	}

    /**
      * @doc Mapzania.MultiPoint.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	MultiPoint.prototype.getExtent = function () {
		var algo = new ns.Algo();
		return algo.getExtentFromPoints(this.points);
	}

    /**
      * @doc Mapzania.MultiPoint.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	MultiPoint.prototype.distanceTo = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPoint.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	MultiPoint.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPoint.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPoint.prototype.getPath = function () {
	    var points = [];
	    for (var i = 0; i < this.points.length; i++) {
	        var pnt = this.points[i];
	        points.push(pnt.x);
	        points.push(pnt.y);
	    }

	    return [[points]];
	}

    /**
      * @doc Mapzania.MultiPoint.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	MultiPoint.prototype.getSize = function () {
		return this.points.length;
	}

    /**
      * @doc Mapzania.MultiPoint.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	MultiPoint.prototype.getCentroid = function () {
	    //TODO:Better centroid algorithm
	    return this.getExtent().getCenter();
	}

    /**
      * @doc Mapzania.MultiPoint.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPoint.prototype.getGeometryType = function () {
	    return 4;
	}

    /**
      * @doc Mapzania.MultiPoint.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPoint.prototype.getType = function () {
	    return 4;
	}

    /**
      * @doc Mapzania.MultiPoint.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.MultiPoint) An exact deep clone of this instance.
      */

	MultiPoint.prototype.clone = function () {
	    var result = new Mapzania.MultiPoint();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.MultiPoint.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	MultiPoint.prototype.translate = function (x, y) {
	    this.points.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	}

	ns.MultiPoint = MultiPoint;

	if (!ns.FeatureTypes)
	    ns.FeatureTypes = {};

	ns.FeatureTypes["4"] = MultiPoint;

	return ns;

})(Mapzania || {})
