﻿/// <reference path="../mapzania.js"/>
var Mapzania = (function (ns) {

    var createPseudoGuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    var truncate = function (x) {
        return x < 0 ? Math.ceil(x) : Math.floor(x);
    };

    var startsWith = function (str, subStr) {
        return str.slice(0, subStr.length) == subStr;
    };

    var endsWith = function (str, subStr) {
        return str.slice(-subStr.length) == subStr;
    };

    var isFunction = function (item) {
        return item instanceof Function;
    };

    var isString = function (item) {
        return typeof item === 'string' || item instanceof String;
    };

    var isObject = function (item) {
        if (item === null)
            return false;

        return ((typeof item === 'function') || (typeof item === 'object'));
    }

    var isArray = function (item) {
        if (Array.isArray)
            return Array.isArray(item);

        return item instanceof Array;
    };

    var isNull = function (item) {
        return item === null;
    }

    var isUndefined = function (item) {
        return typeof item === 'undefined';
    }

    var isNullOrUndefined = function (item) {
        return isNull(item) || isUndefined(item);
    }

    var clone = function (object) {
        return JSON.parse(JSON.stringify(object));
    }

    ns.Util = {
        createPseudoGuid: createPseudoGuid,
        truncate: truncate,
        startsWith: startsWith,
        endsWith: endsWith,
        isFunction: isFunction,
        isString: isString,
        isObject: isObject,
        isArray: isArray,
        isNull: isNull,
        isUndefined: isUndefined,
        isNullOrUndefined: isNullOrUndefined,
        clone: clone,
    };

    return ns;

})(Mapzania || {});
