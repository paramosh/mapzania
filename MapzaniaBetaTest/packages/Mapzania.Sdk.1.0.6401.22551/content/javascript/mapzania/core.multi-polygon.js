﻿/**
 * @doc Mapzania.MultiPolygon.*ClassDescription*
 *
 * Encapsulates a single Multi-Polygon geometry. A multi-polygon geometry defines a set of polygon geometries. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.MultiPolygon.MultiPolygon
      *
      * Constructs a Mapzania Geometry.
      * 
      */
	function MultiPolygon() {
		this.polygons = [];
	}

    /**
      * @doc Mapzania.MultiPolygon.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPolygon.prototype.read = function (data) {
	    for (var i = 0; i < data.length; i++) {
	        var onePoly = [[[]]];
	        onePoly[0] = data[i];
	        this.polygons.push(new ns.Polygon().read(onePoly));
	    }
		return this;
	}

    /**
      * @doc Mapzania.MultiPolygon.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	MultiPolygon.prototype.getExtent = function () {
		var algo = new ns.Algo();
		var ext = algo.getExtentFromLines([this.polygons[0].shell]);

		for (var i = 1; i < this.polygons.length; i++) {
			var polyExt = algo.getExtentFromLines([this.polygons[i].shell]);

			if (polyExt.minX < ext.minX)
				ext.minX = polyExt.minX;
			if (polyExt.minY < ext.minY)
				ext.minY = polyExt.minY;
			if (polyExt.maxX > ext.maxX)
				ext.maxX = polyExt.maxX;
			if (polyExt.maxY > ext.maxY)
				ext.maxY = polyExt.maxY;
		}

		return ext;
	}

    /**
      * @doc Mapzania.MultiPolygon.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	MultiPolygon.prototype.distanceTo = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPolygon.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	MultiPolygon.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPolygon.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPolygon.prototype.getPath = function () {
	    var res = [];
	    for (var i = 0; i < this.polygons.length; i++) 
	        res.push(this.polygons[i].getPath());

	    return res;
	}

    /**
      * @doc Mapzania.MultiPolygon.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	MultiPolygon.prototype.getSize = function () {
		var res = 0;

		for (var i = 0; i < this.polygons.length; i++) {
			res += this.polygons[i].getSize();
		}

		return res;
	}

    /**
      * @doc Mapzania.MultiPolygon.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	MultiPolygon.prototype.getCentroid = function () {
	    //TODO:Better centroid algorithm
	    return this.getExtent().getCenter();
	}

    /**
      * @doc Mapzania.MultiPolygon.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPolygon.prototype.getGeometryType = function () {
	    return 6;
	}

    /**
      * @doc Mapzania.MultiPolygon.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPolygon.prototype.getType = function () {
	    return 6;
	}

    /**
      * @doc Mapzania.MultiPolygon.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.MultiPolygon) An exact deep clone of this instance.
      */

	MultiPolygon.prototype.clone = function () {
	    var result = new Mapzania.MultiPolygon();
	    for (var i = 0; i < this.polygons.length; i++) {
	        result.polygons.push(this.polygons[i].clone());
	    }
	    //result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.MultiPolygon.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	MultiPolygon.prototype.translate = function (x, y) {
	    this.polygons.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	}

    /**
      * @doc Mapzania.MultiPolygon.getArea
      *
      * Calculates the area of this multi polygon.
      *
      * @return (Number) The area of this multi polygon (excluding the area taken up by holes).
      */

	MultiPolygon.prototype.getArea = function () {
	    var sum = 0;
	    this.polygons.forEach(function (cur) {
	        sum += cur.getArea();
	    });
	    return sum;
	}

	ns.MultiPolygon = MultiPolygon;

	if (!ns.FeatureTypes)
	    ns.FeatureTypes = {};

	ns.FeatureTypes["6"] = MultiPolygon;

	return ns;

})(Mapzania || {})
