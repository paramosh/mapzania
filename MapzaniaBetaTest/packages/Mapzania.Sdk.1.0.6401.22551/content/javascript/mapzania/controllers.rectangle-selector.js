﻿var Mapzania = (function (ns) {
	if (!ns.Controllers)
		ns.Controllers = {};

	function RectangleSelector(viewport, mapper, callback) {

		this._enabled = false;

		var me = this;

		viewport.mouseDown.add(function (evt) {
			if (!me._enabled)
				return;

			//console.log("zoom down");
			me.zooming = true;
			me.start = evt.data;
		});

		viewport.mouseUp.add(function (evt) {
			if (!me._enabled)
				return;
			me.zooming = false;

			var ext = new ns.Extent({
				minX: Math.min(me.start.x, evt.data.x),
				minY: Math.min(me.start.y, evt.data.y),
				maxX: Math.max(me.start.x, evt.data.x),
				maxY: Math.max(me.start.y, evt.data.y)
			});

			mapper.clearLayer("ts_interactive");

			if (callback)
			    callback(ext);
		});

		viewport.mouseMove.add(function (evt) {
			if (!me._enabled)
				return;

			if (me.zooming) {

				mapper.clearLayer("ts_interactive");
				
				var minX = Math.min(me.start.x, evt.data.x);
				var minY =	Math.min(me.start.y, evt.data.y);
				var maxX =	Math.max(me.start.x, evt.data.x);
				var maxY = Math.max(me.start.y, evt.data.y);

				var line = {
					points:
						[{ x: minX, y: minY },
						{ x: minX, y: maxY },
						{ x: maxX, y: maxY },
						{ x: maxX, y: minY },
						{ x: minX, y: minY }
						]
				};
				
				//console.log(line);
				mapper.fillPolygon("ts_interactive", [line], "#000000", null, 20, 1);
				mapper.drawLine("ts_interactive", line, "#000000", 1, 100);
				mapper.flush();
			}
		});
	}

	RectangleSelector.prototype.enabled = function (val) {
		if (typeof val != 'undefined')
			this._enabled = val;

		return this._enabled;
	}

	RectangleSelector.prototype.canZoomMap = function () {
	    return false;
	}

	RectangleSelector.prototype.isTempState = function () {
	    return false;
	}

	ns.Controllers.RectangleSelector = RectangleSelector;

	return ns;
})(Mapzania || {})
