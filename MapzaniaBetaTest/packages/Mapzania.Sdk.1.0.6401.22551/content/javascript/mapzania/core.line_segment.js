﻿/**
 * @doc Mapzania.LineSegment.*ClassDescription*
 *
 * Encapsulates a single line segment. A line segment defines two connecting points. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.LineSegment.LineSegment
      *
      * Constructor. Constructs a Mapzania LineSegment.
      */
    function LineSegment(p1, p2) {
        this.p1 = p1;
        this.p2 = p2;
    }


    /**
      * @doc Mapzania.LineSegment.getExtent
      *
      * Determines the extent for this LineSegment.
      *
      * @return (Mapzania.Extent) The extent for this LineSegment.
      */
    LineSegment.prototype.getExtent = function () {
        if (!p1 | !p2) return null;

        var res = new Mapzania.Extent(
                {
                    minX: Math.min(p1.x, p2.x),
                    minY: Math.min(p1.y, p2.y),
                    maxX: Math.max(p1.x, p2.x),
                    maxY: Math.max(p1.y, p2.y)
                }
            );
        return res;
    }

    /**
      * @doc Mapzania.LineSegment.distanceTo
      *
      * Determines the closest distance from this LineSegment to another geometry. NB: This method is 
      * not implemented from all geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this and LineSegment provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    LineSegment.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.LineSegment.getSize
      *
      * Determines the total number of points in this LineSegment.
      *
      * @return (Number) The number of points in this LineSegment.
      */
    LineSegment.prototype.getSize = function () {
        return 2;
    }

    /**
      * @doc Mapzania.Geometry.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
    LineSegment.prototype.getCentroid = function () {
        return new Mapzania.Point((this.p1.x + this.p2.x) / 2, (this.p1.y + this.p2.y) / 2);
    }

    LineSegment.prototype.getIntersectionPoint = function (lineSegment) {
        // http://jsfiddle.net/justin_c_rounds/Gd2S2/light/
        var p1 = this.p1;
        var p2 = this.p2;
        var p3 = lineSegment.p1;
        var p4 = lineSegment.p2;

        var denominator, a, b, numerator1, numerator2, result = {
            x: null,
            y: null,
            onLine1: false,
            onLine2: false
        };
        denominator = ((p4.y - p3.y) * (p2.x - p1.x)) - ((p4.x - p3.x) * (p2.y - p1.y));
        if (denominator == 0) {
            return null;
        }
        a = p1.y - p3.y;
        b = p1.x - p3.x;
        numerator1 = ((p4.x - p3.x) * a) - ((p4.y - p3.y) * b);
        numerator2 = ((p2.x - p1.x) * a) - ((p2.y - p1.y) * b);
        a = numerator1 / denominator;
        b = numerator2 / denominator;

        // if we cast these lines infinitely in both directions, they intersect here:
        result.x = p1.x + (a * (p2.x - p1.x));
        result.y = p1.y + (a * (p2.y - p1.y));
        /*
            // it is worth noting that this should be the same as:
            x = p3.x + (b * (p4.x - p3.x));
            y = p3.x + (b * (p4.y - p3.y));
        */
        // if line1 is a segment and line2 is infinite, they intersect if:
        if (a > 0 && a < 1) {
            result.onLine1 = true;
        }
        // if line2 is a segment and line1 is infinite, they intersect if:
        if (b > 0 && b < 1) {
            result.onLine2 = true;
        }

        if (result.onLine1 && result.onLine2)
            return new Mapzania.Point(result.x, result.y);
        else
            return null;
    }

    LineSegment.prototype.isVisibleInExtent = function (extent) {
        if (extent.containsPoint(this.p1)) return true;
        if (extent.containsPoint(this.p2)) return true;

        var ls1 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.minY), new Mapzania.Point(extent.maxX, extent.minY));
        var ls2 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.minY), new Mapzania.Point(extent.maxX, extent.maxY));
        var ls3 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.maxY), new Mapzania.Point(extent.minX, extent.maxY));
        var ls4 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.maxY), new Mapzania.Point(extent.minX, extent.minY));

        return (this.getIntersectionPoint(ls1) != null || this.getIntersectionPoint(ls2) != null || this.getIntersectionPoint(ls3) != null || this.getIntersectionPoint(ls4) != null);
    }

    LineSegment.prototype.getVisibleIntersectionInExtent = function (extent) {
        // Both points inside the polygon
        if (extent.containsPoint(this.p1) && extent.containsPoint(this.p2)) {
            return this;
        }

        // Both points outside the polygon
        if (!extent.containsPoint(this.p1) && !extent.containsPoint(this.p2)) {
            return null;
        }
        
        // Now we know that one point is inside and the other outside the extent
        var ls1 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.minY), new Mapzania.Point(extent.maxX, extent.minY));
        var ls2 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.minY), new Mapzania.Point(extent.maxX, extent.maxY));
        var ls3 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.maxY), new Mapzania.Point(extent.minX, extent.maxY));
        var ls4 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.maxY), new Mapzania.Point(extent.minX, extent.minY));

        // Find intersection point with the extent
        var p = this.getIntersectionPoint(ls1);
        if (!p) p = this.getIntersectionPoint(ls2);
        if (!p) p = this.getIntersectionPoint(ls3);
        if (!p) p = this.getIntersectionPoint(ls4);
        if (!p) console.error("Unexpected TS error: No point intersection with extent. Not expected.");

        if (extent.containsPoint(this.p1)) {
            return new Mapzania.LineSegment(this.p1, p);
        } else {
            return new Mapzania.LineSegment(p, this.p2);
        }
    }

    LineSegment.prototype.length = function () {
        return this.p1.distanceTo(this.p2);
    }

    /**
	 * @return 
	 * 		Computes and returns the angle of the line in radians, when measured 
	 * 		counterclockwise from the x axis. The return value is between positive pi and negative pi. 
	 */
    LineSegment.prototype.getAngleRadians = function () {
        var x = this.p2.x - this.p1.x;
        var y = this.p2.y - this.p1.y;
        return Math.atan2(y, x);
    }

    ns.LineSegment = LineSegment;

    return ns;

})(Mapzania || {})
