﻿/// <reference path="../mapzania.js"/>

var Mapzania = (function (ns) {

    function VLS2(model, layer) {

        var cacheData
        var cacheKey

        var me = this;

        this.gotFeatures = new ns.Event();

        this.getFeatures = function (filters, options) {

            var key = JSON.stringify({ filters: filters, options: options });


            if (key == cacheKey) {

                //var res = JSON.parse(cacheData);

                var res = cacheData.map(function (ftr) {
                    return ftr.clone();
                });

                this.gotFeatures.fire(this, { seqNo: 0, features: res, isFinal: true });

            } else {

                var callback = function (data) {
                    var res = data;

                    //cacheData = JSON.stringify(data);
                    cacheData = data;
                    cacheKey = key;

                    me.gotFeatures.fire(me, { seqNo: 0, features: res.slice(), isFinal: !options.tiled });
                };

                model.loadQueue.getFeatures(layer, filters, options, callback);
            }
        }
    }

    ns.VLS2 = VLS2;

    return ns;

})(Mapzania || {});
