﻿/// <reference path="../mapzania.js"/>
var Mapzania = (function (ns) {

    ns.MouseModes = {
        dragToZoomIn: 1,
        clickToZoomOut: 2,
        dragToPanAndWheelToZoom: 3,
        clickToDraw: 4
    };

    ns.DrawingModes = {
        drawPoint: 1,
        drawLine: 2,
        drawPolygon: 3,
        // TODO: Add functionality for drawing modes below
        //drawRectangle: 4,
        //drawCircle: 5,
        //drawMarker: 6
    }

    function Map(elementId, param0, param1, param2) {

        var settings, api, model, mapper, viewport, renderer,
            toolsManager, highlightManager;

        var drawingMode = ns.DrawingModes.drawPoint;

        var displayWkt;
        var mouseMode;

        var me = this;

        this.init = function (elementId, param0, param1, param2) {

            var options = null;
            var callback = null;

            if (Mapzania.Util.isFunction(param1) && param2 != null)
                throw "Map:Invalid instantation parameters";

            if (Mapzania.Util.isFunction(param1)) {
                callback = param1;
            }
            else {
                options = param1;
                if (param2 !== undefined)
                    callback = param2;
            }

            //TODO: Flesh out defaults
            var defaults = {
                displaySrid: "GOOGLE",
                apiRoot: "mz",
                isCompact: false,
                debug: false,
                renderer: "CANVAS"
            };

            var settings = Object.assign(defaults, options);

            Mapzania.Log.debugEnabled = settings.debug;

            var l = window.location;
            var path = l.protocol + "//" + l.host + "/" + settings.apiRoot + "/";

            api = new Mapzania.Api(path, settings.isCompact);

            model = new Mapzania.Model(api);

            Mapzania.Filters._setModel(model);

            model.store.changed.add(function (evt) {
                //return;
                var layerKey = evt.data;

                var ftrs = model.store.get(layerKey);

                var res = ftrs.map(function (ftr) {
                    return ftr.clone();
                });

                Mapzania.Projection.projectFeatureArray(res, "GOOGLE", "LATLONG");
                res = Mapzania.GeoJson.toFeatureCollection(res);

                me.fire(layerKey.toLowerCase() + "_layer_changed", res);
            });

            //model.stateChanged.add(function (evt) {
            //    me.mouseModeChanged.fire(me, evt);
            //})

            model.queried.add(function (evt) {
                var geom = evt.data.geom;
                Mapzania.Projection.projectGeometry(geom, "GOOGLE", "LATLONG");
                var res = Mapzania.GeoJson.toGeometry(geom);

                //These events all fire on timeouts to allow other events to complete (such as dblclick).
                //Otherwise it can lead to strange behaviours if the MapMode is set in the event listener.
                if (res.type == "Point")
                    setTimeout(function () {
                        me.fire("point_drawn", res);
                    }, 0);

                if (res.type == "LineString")
                    setTimeout(function () {
                        me.fire("line_drawn", res);
                    }, 0);

                if (res.type == "Polygon")
                    setTimeout(function () {
                        me.fire("polygon_drawn", res);
                    }, 0);

                setTimeout(function () {
                    me.fire("drawing_done", {
                        type: "FeatureCollection",
                        features: [{ type: "Feature", geometry: res }]
                    });
                }, 0);
            });

            switch (settings.renderer) {
                case "CANVAS":
                    mapper = new Canvas.Mapper();
                    break;
                case "SVG":
                    mapper = new TRaphael.Mapper();
                    break;
                default:
                    throw new "Unknown renderer key.";
            }

            viewport = new Mapzania.Viewport("#" + elementId, mapper);

            viewport.mouseUp.add(function (evt) {
                me.fire("mouseup", evt);
            })

            viewport.init(function () {

                renderer = new Mapzania.Renderer(model, viewport, mapper);
                toolsManager = new Mapzania.ToolsManager(model, viewport, mapper);
                //highlightManager = new Mapzania.HighlightManager(model, viewport, renderer);

                model.schemaLoaded.add(function () {

                    var wkt = model.srids.find(function (itm) {
                        return itm.key == model.displaySRID;
                    }).val;

                    displayWkt = wkt;

                    //HACK - API should not need wkt - remove when loadQueue is fixed.
                    api.wkt = wkt;
                    model.wkt = wkt;

                    model.load();

                    me.setMouseMode(ns.MouseModes.dragToPanAndWheelToZoom);
                    me.setDrawingMode(ns.DrawingModes.drawPoint);

                    if (callback)
                        callback();
                });

                if (Mapzania.Util.isObject(param0))
                    model.applySchema(param0, settings.displaySrid);
                else
                    model.loadSchema(param0, settings.displaySrid);
                    
            });
        }

        this._dump = function () {

            var lyrs = this.getLayers();

            var store = [];
            var styles = [];

            for (var i = 0; i < lyrs.length; i++) {
                var key = lyrs[i].key;
                store.push({
                    key: key,
                    data: model.store.get(key)
                });

                var lyr = model.getLayer(key);

                styles.push({
                    themes: lyr.themes,
                    styles: lyr.styles,
                    featureStyles:lyr.featureStyles
                });
            }

            var displayExtent = function (ext) {
                return {
                    minX: ext.minX,
                    minY: ext.minY,
                    maxX: ext.maxX,
                    maxY: ext.maxY,
                    width: ext.width(),
                    height: ext.height()
                }
            }

            var displayBBox = function (bbox) {
                return {
                    minX: bbox[0],
                    minY: bbox[1],
                    maxX: bbox[2],
                    maxY: bbox[3],
                    width: bbox[2]-bbox[0],
                    height: bbox[3] - bbox[1],
                }
            }

            var dump = {
                extents: {
                    model_extent: displayExtent(model.extent),
                    model_startExtent: displayExtent(model.startExtent),
                    bbox: displayBBox(this.getBoundingBox()),
                    bbox_initial: displayBBox(this.getInitialBoundingBox())
                },
                styles: styles,
                viewportSize: model._viewportSize,
                layers: lyrs,
                schema: model._schema,
                store: store,
                model: model,
                modelLayers: model.layers,
                listeners: listeners
            };

            //dump.thinFactor = (dump.extent.width * dump.extent.height) / (dump.viewportSize.width * dump.viewportSize.height);
            //dump.thinFactor2 = (dump.extent.width / dump.viewportSize.width + dump.extent.height / dump.viewportSize.height) / 2;

            return dump;
        }

        this._stylerInternals = function () {
            var res = {
                schema: model._schema,
                model: model,
            }

            return res;
        }

        //-------------------- FLUENT ---------------------------------------
        this.usingLayer = function (layerKey) {
            return new ns.Layer(layerKey, this);
        }

        this.usingNewLayer = function (layerKey, options) {
            this.addLayer(layerKey, options);
            return new ns.Layer(layerKey, this);
        }

        //-------------------- MAP MANIPULATION ---------------------------------------

        this.setMouseMode = function (mode) {

            if (mode > 4)
                throw "Mapzania.Map.setMouseMode: Invalid MouseMode value.";

            if (mode == ns.MouseModes.clickToDraw) {
                model.changeState(drawingMode + 5);
            }
            else {
                model.changeState(mode);
            }

            mouseMode = mode;

            me.fire("mouse_mode_set", mouseMode);

            return this;
        }

        this.getMouseMode = function () {
            return mouseMode;
        }

        this.setDrawingMode = function (mode) {
            drawingMode = mode;
            me.setMouseMode(mouseMode);
            me.fire("drawing_mode_set", mode);

            return this;
        }

        this.getDrawingMode = function () {
            return drawingMode;
        }

        this.refresh = function () {
            //HACK: must fix
            model.extent.minX++;
            model.zoomTo(model.extent);

            return this;
        }

        this.reset = function () {
            model.reset();
            me.setMouseMode(ns.MouseModes.dragToPanAndWheelToZoom);
            me.setDrawingMode(ns.DrawingModes.drawPoint);

            return this;
        };

        this.fitToBoundingBox = function (bbox, buffer) {

            var extent = Mapzania.GeoJson.fromBoundingBox(bbox);
            if (buffer)
                extent = extent.grow(buffer, buffer);
            Mapzania.Projection.projectExtent(extent, "LATLONG", "GOOGLE");
            model.zoomTo(extent);

            return this;
        };

        this._zoomOut = function () {
            model.zoomOut();
        }

        this.setSchema = function (schema) {
            model.setSchema(schema);
        }

        this.getSchema = function () {
            return model.getSchema();
        }

        this.setBackColor = function (color) {
            renderer.setBackColor(color);
        }

        // --------------------------- LAYERS --------------------------------------
        this.getLayers = function () {
            return model.getLayers().map(function (lyr) {
                return {
                    key: lyr.key,
                    name: lyr.name,
                    sourceKey: lyr.sourceKey,
                    ordinal: lyr.ordinal,
                    visible: lyr.visible
                };
            });
        };

        this.getLayer = function (layerKey) {
            return me.getLayers().find(function (m) {
                return m.key == layerKey;
            });
        };

        this.addLayer = function (layerKey, options) {

            if (this.getLayer(layerKey))
                throw "addLayer: a layer with that layerKey already exists.";

            var srcId = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });

            var defaults = {
                isTemp: true,
                ordinal: 0,
                visible: true,
                name: "(not set)",
                sourceKey: (layerKey + "_SRC_" + srcId).toUpperCase(),
                scaleType: 0,
                allowDuplicateLabels: false,
                themes: []
            };

            var settings = Object.assign(defaults, options);

            var sources = model.sourceFactory.sources;

            var isNewSource = !sources[settings.sourceKey];

            if (isNewSource) {
                sources[settings.sourceKey] = {
                    key: settings.sourceKey,
                    sourceType: "LOCALVECTOR"
                };
            }

            var layer = {
                key: layerKey,
                ordinal: settings.ordinal,
                name: settings.name,
                visible: settings.visible,
                sourceKey: settings.sourceKey,
                backgroundColor: settings.backgroundColor,
                themes: settings.themes,
                allowDuplicateLabels: settings.allowDuplicateLabels,
                isLocal: isNewSource
            };

            var layers = this.getLayers();

            for (var i = 0; i < layers.length; i++) {
                var lyr = layers[i];
                if (settings.ordinal < lyr.ordinal) {
                    model.addLayer(layer, i);
                    return;
                }
            }

            model.addLayer(layer, layers.length);
        }

        this.removeLayer = function (layerKey) {
            model.removeLayer(layerKey);
        }

        this.updateLayer = function (layerKey) {

            var layer = model.getLayer(layerKey);

            //if (layer.src.source)
            //    layer.src.source.cacheKey = null;

            model.refreshLayer(layerKey);
        }

        this._updateLayer = function (layerKey) {

        }

        this._clearLayer = function (layerKey) {
            mapper.clearLayer(layerKey);
        }


        //---------------------LAYER MANIPULATION--------------------------------

        this.showLayer = function (layerKey) {
            model.showLayer(layerKey);
        };

        this.hideLayer = function (layerKey) {
            model.hideLayer(layerKey);
        };

        //Deprecated
        this.addFeatures = function (layerKey, features) {
            console.log("addFeatures is deprecated. Use addFeatureCollection (or addFeature)");
            this.addFeatureCollection(layerKey, features);
        }

        this.addFeatureCollection = function (layerKey, featureCollection) {
            var layer = model.getLayer(layerKey);
            layer.clientFeatures = featureCollection;
            //layer.src.addFeatures(featureCollection);

            //var ftrs = [];

            //if (featureCollection)
            //    ftrs = Mapzania.GeoJson.fromFeatureCollection(featureCollection);

            //Mapzania.Projection.projectFeatureArray(ftrs, "LATLONG", "GOOGLE");

            //var layer = model.getLayer(layerKey);
            //layer.src.addFeatures(ftrs);
        }

        this.addFeature = function (layerKey, feature) {
            var col = {
                type: "FeatureCollection",
                features: []
            };

            if (feature)
                col.features.push(feature);

            me.addFeatureCollection(layerKey, col);
        }

        this.clearFeatures = function (layerKey) {
            //model.store.set(layerKey, []);
            //renderer.updateLayer(layerKey);
            var layer = model.getLayer(layerKey);
            layer.clientFeatures = { features: [] };
        }

        //-------------------- FILTERS ------------------------------------------

        this._setFilter = function (layerKey, filter) {
            var layer = model.getLayer(layerKey);
            layer.filter = filter;
        }

        this.getStateFilters = function (layerKey) {
            var layer = model.getLayer(layerKey);
            var filters = new ns.FilterBuilder(model).getStateFilters(layer);
            return filters;
        }

        this.getFilters = function (layerKey) {
            var layer = model.getLayer(layerKey);
            return layer.requestFilters;
        }

        this.addFilter = function (layerKey, filter, index) {
            var layer = model.getLayer(layerKey);

            if (index)
                layer.requestFilters.splice(index, 0, filter)
            else
                layer.requestFilters.push(filter);
        }

        this.removeFilter = function (index) {
            var layer = model.getLayer(layerKey);
            layer.requestFilters.splice(index, 1);
        }

        this.clearFilters = function (layerKey) {
            var layer = model.getLayer(layerKey);
            layer.requestFilters.length = 0;
        }

        //-------------------------TOOLBAR----------------------------------------

        function buildToolbar($el, options) {

            for (var i = 0; i < options.layout.length; i++) {
                var $btn = $("<button></button");
                var btnKey = options.layout[i];
                var btn = ns.Toolbar.buttons[btnKey];

                if (!btn)
                    continue;

                $btn.addClass(btn.key);
                $btn.addClass("btn");

                if (options.showIcons) {
                    //if (typeof $.fn.button == "function") { }
                    //else {
                    $btn.append("<img src='" + options.iconPath + "/" + btn.key + ".png' />");
                    //}
                }

                if (options.showText)
                    $btn.append("<span>" + btn.text + "</span>");
                $el.append($btn);
                //$el.append("<button class='" + btn.key + "'>" + btn.text + "</button>");
            }
        }

        this.createToolbar = function (elementId, notificationElementId, options) {

            var map = this;

            var defaults = {
                layout: ns.Toolbar.Layouts.simple,
                showText: true,
                showIcons: false
            };

            var settings = Object.assign(defaults, options);

            var $el = $("#" + elementId);

            if ($el.children().length == 0)
                buildToolbar($el, settings);

            var tb = new Mapzania.Toolbar($el, this);

            var update = function () {
                var msg = $("#" + notificationElementId);

                switch (map.getMouseMode()) {

                    case Mapzania.MouseModes.dragToZoomIn:
                        msg.html("Click & drag to zoom in");
                        break;

                    case Mapzania.MouseModes.clickToZoomOut:
                        msg.html("Click to zoom out");
                        break;

                    case Mapzania.MouseModes.dragToPanAndWheelToZoom:
                        msg.html("Click & drag to pan");
                        break;

                    case Mapzania.MouseModes.clickToDraw:

                        switch (map.getDrawingMode()) {
                            case Mapzania.DrawingModes.drawPoint:
                                msg.html("Click a point on the map to query");
                                break;

                            case Mapzania.DrawingModes.drawLine:
                                msg.html("Click line points on the map to query");
                                break;

                            case Mapzania.DrawingModes.drawPolygon:
                                msg.html("Click polygon points on the map to query");
                                break;
                        }

                        break;

                    default:
                        Mapzania.Log.error("Unknown Map State", "Mapzania.Map");
                        break;
                }
            }

            this.on("mouse_mode_set", function () {
                update();
            });

            update();

            return this;
        }

        // -------------------------- QUERY MAP   -----------------------------------

        this.query = function (layerKey, filters, options) {
            var defaults = {
                simplified: false,
                //ignoreLayerFilter: false,
                ignoreAppliedFilters: false
            };

            var settings = Object.assign(defaults, options);

            filters = [].concat(filters);

            var fact = Mapzania.Filters;

            var layer = model.getLayer(layerKey);

            var allFilters = [];

            //if (!settings.ignoreMapFilters) {
            //TODO: Bring in the theme filter
            if (layer.filter && layer.filter != "")
                allFilters.push(fact.filterByText(layer.filter));
            //}

            if (!settings.ignoreAppliedFilters) {
                allFilters = allFilters.concat(me.getFilters(layerKey));
            }

            if (settings.simplified) {
                //TODO: Implement options.simplified
                throw new "Mapzania.map.query: options.simplified is not yet implemented."
            }

            if (filters)
                allFilters = allFilters.concat(filters);

            api.getFeatures(layer.src.key, allFilters, null, function (data) {
                var res = Mapzania.GeoJson.toFeatureCollection(data);
                me.fire(layerKey.toLowerCase() + "_layer_queried", res);
            }, { isProjected: false });
        }
	
        //Deprecated
        this.getFeatures = function (layerKey) {
            console.log("getFeatures is deprecated. Use getFeatureCollection (or getFeature)");
            this.getFeatureCollection(layerKey);
        }	

        this.getFeatureCollection = function (layerKey) {
            var ftrs = model.store.get(layerKey) || [];

            var clonedFeatures = ftrs.map(function (m) {
                return m.clone();
            });

            Mapzania.Projection.projectFeatureArray(clonedFeatures, "GOOGLE", "LATLONG");
            var res = Mapzania.GeoJson.toFeatureCollection(clonedFeatures);
            return res;
        }

        this.getFeature = function (layerKey, id) {
		return this.getFeatureCollection(layerKey).find(function(obj){
			return id == obj.id;
		});
        }

        this.getBoundingBox = function () {
            var ext = new Mapzania.Extent(model.extent);
            Mapzania.Projection.projectExtent(ext, "GOOGLE", "LATLONG");
            return [ext.minX, ext.minY, ext.maxX, ext.maxY];
        };

        this.getInitialBoundingBox = function () {
            var ext = new Mapzania.Extent(model.startExtent);
            Mapzania.Projection.projectExtent(ext, "GOOGLE", "LATLONG");
            return [ext.minX, ext.minY, ext.maxX, ext.maxY];
        };

        // -------------------FEATURE MANIPULATION -------------------------------

        this.hideFeature = function (layerKey, id) {
            var layer = model.getLayer(layerKey);

            if (layer.hidden.indexOf(id) == -1)
                layer.hidden.push(id);
        }

        this.showFeature = function (layerKey, id) {
            var layer = model.getLayer(layerKey);

            var idx = layer.hidden.indexOf(id);

            if (idx > -1)
                layer.hidden.splice(idx, 1);
        }

        // ------------------------ STYLING --------------------------------------

        var validateStyle = function (style) {

            if (!style)
                throw "Style cannot be undefined or null.";

            var sc = style.strokeColor;
            var so = style.strokeOpacity;
            var sw = style.strokeWidth;

            var isArray = ns.Util.isArray;

            if (isArray(sc) || isArray(so) || isArray(sw)) {

                if (!(isArray(sc) && isArray(so) && isArray(sw)))
                    throw "If one stroke property is an array, all stroke properties must be arrays.";

                if (sc.length != so.length || sc.length != sw.length)
                    throw "All stroke property arrays must be the same length";
            }
        }

        var setStyleDefaults = function (style) {

            if (ns.Util.isNullOrUndefined(style.size))
                style.size = 10;

            if (ns.Util.isNullOrUndefined(style.strokeColor))
                style.strokeColor = "#000000";

            if (ns.Util.isNullOrUndefined(style.strokeOpacity))
                style.strokeOpacity = 1;

            if (ns.Util.isNullOrUndefined(style.strokeWidth))
                style.strokeWidth = 1;

            if (style.fillColor && ns.Util.isNullOrUndefined(style.fillOpacity))
                style.fillOpacity = 1;

            if (ns.Util.isNullOrUndefined(style.fillColor)) {
                style.fillColor = "#000000";
                style.fillOpacity = 0;
            }
        }

        //Deprecated
        this.addStyle = function (layerKey, style) {
            console.log("addStyle is deprecated. Use setLayerStyle");
            this.setLayerStyle(layerKey, style);
        }

        //Deprecated
        this.clearStyles = function (layerKey, featureId) {
            console.log("clearStyle is deprecated. Use clearLayerStyles");
            this.clearLayerStyles(layerKey);
        }

        this.setLayerStyle = function (layerKey, style) {
            validateStyle(style);
            setStyleDefaults(style);

            var layer = model.getLayer(layerKey);
            layer.styles = [style];
        }

        this.addLayerStyle = function (layerKey, style) {
            validateStyle(style);
            setStyleDefaults(style);

            var layer = model.getLayer(layerKey);

            if (!layer.styles)
                layer.styles = [];

            layer.styles.push(style);
        }

        this.clearLayerStyles = function (layerKey) {
            var layer = model.getLayer(layerKey);
            layer.styles = [];
        }

        this.setFeatureStyle = function (layerKey, id, style) {
            validateStyle(style);
            setStyleDefaults(style);

            var layer = model.getLayer(layerKey);

            if (!layer.featureStyles)
                layer.featureStyles = {};

            layer.featureStyles[id] = [style];
        }

        this.addFeatureStyle = function (layerKey, id, style) {
            validateStyle(style);
            setStyleDefaults(style);

            var layer = model.getLayer(layerKey);

            if (!layer.featureStyles)
                layer.featureStyles = {};

            if (!layer.featureStyles[id])
                layer.featureStyles[id] = [];

            layer.featureStyles[id].push(style);
        }

        this.clearFeatureStyles = function (layerKey, id) {

            var layer = model.getLayer(layerKey);

            if (!id)
                layer.featureStyles = {};
            else
                delete layer.featureStyles[id];
        }


        // -------------------------- LISTENERS  -----------------------------

        var listeners = [];

        this.on = function (type, fn) {
            listeners.push({
                type: type,
                once: false,
                fn: fn
            });
        }

        this.once = function (type, fn) {
            listeners.push({
                type: type,
                once: true,
                fn: fn
            });
        }

        this.off = function (type) {

            for (var i = listeners.length - 1; i >= 0 ; i--) {

                var l = listeners[i];

                if (l.type == type)
                    listeners.splice(i, 1);
            }
        }

        this.fire = function (type, data) {
            Mapzania.Log.debug(type + " fired. (" + listeners.length + " listeners)", "Mapzania.Map");

            for (var i = listeners.length - 1; i >= 0 ; i--) {

                var l = listeners[i];

                if (l.type == type) {
                    l.fn(data);
                    if (l.once)
                        listeners.splice(i, 1);
                }
            }
        }

        // -----------------------------------------------------------------------

        this.init(elementId, param0, param1, param2);
    };

    ns.Map = Map;

    return ns;

})(Mapzania || {});