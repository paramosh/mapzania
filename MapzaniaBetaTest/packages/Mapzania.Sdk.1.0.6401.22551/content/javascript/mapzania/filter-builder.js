﻿/// <reference path="../mapzania.js"/>

var Mapzania = (function (ns) {

    function FilterBuilder(model) {

        this.getStateFilters = function (layer) {

            var filters = [];

            addLocalFeatures(layer, filters);
            addFilterByText(layer, filters);
            addFilterByBoundingBox(filters);
            addChangeProperties(layer, filters);
            addLabelVisibilityClause(filters);

            if (!layer.tiled)
                addSimplifyGeometry(filters);

            return filters;
        }

        this.getRequestFilters = function (layer) {

            var filters = [];

            addLocalFeatures(layer, filters);
            addFilterByText(layer, filters);
            addFilterByBoundingBox(filters);
            addChangeProperties(layer, filters);
            addLabelVisibilityClause(filters);

            if (layer.requestFilters)
                filters = filters.concat(layer.requestFilters);

            if (!layer.tiled)
                addSimplifyGeometry(filters);

            return filters;
        }

        this.getResponseFilters = function (layer) {

            var filters = [];

            //addLocalFeatures
            if (layer.clientFeatures
                && layer.clientFeatures.features
                && layer.clientFeatures.features.length > 0
                && layer.requestFilters.length == 0) {

                filters.push(function (features) {
                    var res = Mapzania.GeoJson.fromFeatureCollection(layer.clientFeatures);
                    Mapzania.Projection.projectFeatureArray(res, "LATLONG", "GOOGLE");

                    //TODO: Replace based on ids
                    [].push.apply(features, res);
                });
            }

            //removeHidden
            filters.push(function (features) {

                for (var i = features.length - 1; i >= 0; i--) {
                    if (layer.hidden.indexOf(features[i].id) > -1)
                        features.splice(i, 1);
                }
            });

            filters.push(function (features) {

                for (var i = features.length - 1; i >= 0; i--) {
                    if (layer.hidden.indexOf(features[i].id) > -1)
                        features.splice(i, 1);
                }
            });

            //add custom
            for (var i = 0; i < layer.responseFilters.length; i++) {
                filters.push(layer.requestFilters[i]);
            }

            return filters;
        }

        var addLocalFeatures = function (layer, filters) {

            if (layer.clientFeatures.features.length > 0 && layer.requestFilters.length != 0)
                filters.push({ type: "AddFeatureCollection", features: layer.clientFeatures })
        }

        var addFilterByText = function (layer, filters) {
            var theme = model.getTheme(layer);

            var text = model.getFilter(layer, theme);

            if (text) {
                var filter = ns.Filters.filterByText(text, false);
                filters.push(filter);
            }
        }

        var addFilterByBoundingBox = function (filters) {
            var ext = model._getExtentToLoad(model.extent).clone();
            ns.Projection.projectExtent(ext, "GOOGLE", "LATLONG");

            var bbox = [ext.minX, ext.minY, ext.maxX, ext.maxY];
            var filter = ns.Filters.filterByBoundingBox(bbox);

            filters.push(filter);
        }

        var addChangeProperties = function (layer, filters) {
            var fields = [];

            if (layer.fields)
                layer.fields.forEach(function (cur) {
                    if (!cur.expression)
                        fields.push(cur.key);
                });

            var fieldsStr = null;

            if (fields.length > 0)
                fieldsStr = fields.join(",");

            if (fieldsStr)
                filters.push(ns.Filters.changeProperties(fieldsStr));
        }

        var addLabelVisibilityClause = function (filters) {
            //TODO:labelVisibilityClause
            //var theme = model.getTheme(layer);
            //var labelVisibilityClause = null;
            //if (theme && theme.labeller && theme.labeller.visibilityClause)
            //    labelVisibilityClause = theme.labeller.visibilityClause;
        }

        var addSimplifyGeometry = function (filters) {
            var ext = model._getExtentToLoad(model.extent).clone();
            ns.Projection.projectExtent(ext, "GOOGLE", "LATLONG");

            var size = model._viewportSize;

            var pixelWidth = size.width;
            var pixelHeight = size.height;
            var meterWidth = model.extent.width();
            var meterHeight = model.extent.height();

            var thinX = (ext.maxX - ext.minX) / pixelWidth;
            var thinY = (ext.maxY - ext.minY) / pixelHeight;
            thinFactor = Math.min(thinX, thinY);

            var filter = ns.Filters.simplifyGeometry(thinFactor);

            filters.push(filter);

            //TODO: changePrecision
            //var sigFigs = function (n, sig) {
            //    var mult = Math.pow(10, sig - Math.floor(Math.log(n) / Math.LN10) - 1);
            //    return Math.round(n * mult) / mult;
            //}
            //filters.push({ type: "ChangePrecision", accuracy: sigFigs(thinFactor, 1) });
        }
    }

    ns.FilterBuilder = FilterBuilder;

    return ns;

})(Mapzania || {})