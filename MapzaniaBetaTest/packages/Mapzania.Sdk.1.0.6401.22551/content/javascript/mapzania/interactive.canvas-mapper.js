﻿/// <reference path="../lib/swfobject-1.5.js" />

/**
 * @doc Canvas.Mapper.*ClassDescription*
 *
 * An interactive map drawer implementation, using the HTML Canvas component. 
 * Applications typically instantiate this class once, and does not make direct use of any functions defined in this class.
 */
var Canvas = (function (ns) {

	var ts = Mapzania;

    /**
      * @doc Canvas.Mapper.Mapper
      *
      * Constructor. The properties and functions defined in this class are not public, and reserved for the framework and SDK.
      *
      */
	function Mapper() {
		this.contexts = {};
		this.canvasses = {};

		this.viewChanged = new ts.Event();
	}

	Mapper.prototype.init = function ($el, callback) {
	    this.$el = $el;

	    var html = "<canvas id='invisible_canvas_element_text_measure" + "'></canvas>";
	    this.$el.append(html);
	    this._canvasTextMeasure = $("#invisible_canvas_element_text_measure")[0];

	    if (callback)
	        callback();
	}

	Mapper.prototype.zoom = function (factor) {
		//this.swf.zoom(factor);
	}

	Mapper.prototype.onResized = function () {
	    for (cur in this.canvasses) {
	        this.canvasses[cur][0].width = this.$el.width();
	        this.canvasses[cur][0].height = this.$el.height();
	    }
	}

	Mapper.prototype.setBackColor = function (color) {
		this.$el.css("background-color", color);
	}

	Mapper.prototype.setBackImageUrl = function (url) {
		this.$el.css("background-image", "url('" + url + "')");
		this.$el.css("background-repeat", "repeat");
	}

	Mapper.prototype.addLayer = function (key, index, greyScale) {
	    var $canvas;
	    var html = "<canvas id='canvas_mapper_layer_id_"+key+"'></canvas>";   

	    if (typeof index != 'undefined') {
	        if (index > this.$el.children().length) {
	            //console.log("NB!: index > this.$el.children().length");
	            index = this.$el.children().length - 1;
	        }

	        if (this.$el.children().length == 0) {
	            this.$el.append(html);
	        } else {
	            this.$el.children().eq(index).before(html);
	        }
	        
	        $canvas = this.$el.children().eq(index);
	    }
	    else {
	        this.$el.append(html);
	        $canvas = this.$el.children().last();
	    }

		$canvas.css("position", 'absolute');
		$canvas.css("top", '0');
		$canvas.css("left", '0');
		$canvas.css("bottom", '0');
		$canvas.css("right", '0');
		$canvas.css("overflow", 'none');

		if (greyScale) {
		    //$canvas.css("filter", "url('data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale')");
		    $canvas.css("filter", "grayscale(100%)");
		    $canvas.css("-webkit-filter", "grayscale(100%)");
		    $canvas.css("-moz-filter", "grayscale(100%)");
		    $canvas.css("-ms-filter", "grayscale(100%)");
		    $canvas.css("-o-filter", "grayscale(100%)");
		    //$canvas.addClass("ts-grey-scale-layer");
		}

		$canvas[0].width = this.$el.width();
		$canvas[0].height = this.$el.height();

		this.canvasses[key] = $canvas;
		this.contexts[key] = $canvas[0].getContext('2d');
		this.contexts[key].imageSmoothingEnabled = true;
		this.contexts[key].greyScale = greyScale;
		this.contexts[key].layerSessionId = Mapzania.Util.createPseudoGuid();

        // A hack to get maps to show correctly on Google Chrome. Not sure if this is 100% reliable, but it does seem to work for the demos.
		this.drawCircle(key, { x: 0, y: 0 }, 1, "#000000", 1, 0);
	}

	Mapper.prototype.removeLayer = function (key) {
	    var id = "canvas_mapper_layer_id_" + key;
	    delete this.contexts[key];
	    delete this.canvasses[key];
	    $("#" + id).remove();
	}

	Mapper.prototype.removeAllLayers = function () {
	    this.contexts = {};
	    this.canvasses = {};
	    this.$el.html("");
	}

	Mapper.prototype.hideLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeOut();
	}

	Mapper.prototype.showLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeIn();
	}

	Mapper.prototype.clearLayer = function (key) {
		var ctx = this._getContext(key);
		var size = this._getSize();
		ctx.layerSessionId = Mapzania.Util.createPseudoGuid();
		ctx.clearRect(0, 0, size.width, size.height);
	}

	Mapper.prototype.drawCircle = function (layerKey, center, radius, color, size, opacity) {
	    var ctx = this._getContext(layerKey);

	    ctx.strokeStyle = color;
	    ctx.lineWidth = size;
	    ctx.globalAlpha = opacity / 100;

	    ctx.beginPath();
	    ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI, false);
	    ctx.closePath();
	    ctx.stroke();
	}

	Mapper.prototype.fillCircle = function (layerKey, center, radius, color, opacity) {
		var ctx = this._getContext(layerKey);

		ctx.fillStyle = color;
		ctx.globalAlpha = opacity / 100;

		ctx.beginPath();
		ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI, false);
		ctx.closePath();
		ctx.fill();
	}

	Mapper.prototype.drawLine = function (layerKey, line, color, size, opacity, endCap, joinType, dashArray) {
		opacity = opacity / 100.0;

		if (!dashArray)
			dashArray = [];

		if (!endCap || dashArray.length > 0)
			endCap = 0;

		var caps = ["round", "butt", "square"];
		var cap = caps[endCap];

		if (!joinType)
		    joinType = 0;

		var joins = ["round", "bevel", "miter"];
		var join = joins[joinType];

		var ctx = this._getContext(layerKey);

		ctx.beginPath();
		ctx.strokeStyle = color;
		ctx.lineWidth = size < 0.7 ? 0.7 : size;
		ctx.globalAlpha = opacity;
		ctx.lineCap = cap;
		ctx.lineJoin = join;
		ctx.setLineDash(dashArray);

		var pnts = line.points;
		ctx.moveTo(pnts[0].x, pnts[0].y);

		for (var i = 1; i < pnts.length; i++)
			ctx.lineTo(pnts[i].x, pnts[i].y);

		ctx.stroke();

	}

	Mapper.prototype.fillPolygon = function (layerKey, lines, color, color2, opacity, type) {
	    if (type == 1) {
	        var ctx = this._getContext(layerKey);

	        ctx.fillStyle = color;
	        ctx.globalAlpha = opacity / 100;

	        ctx.beginPath();

	        for (var i = 0; i < lines.length; i++) {
	            var pnts = lines[i].points;

	            ctx.moveTo(pnts[0].x, pnts[0].y);

	            for (var j = 1; j < pnts.length; j++)
	                ctx.lineTo(pnts[j].x, pnts[j].y);

	            ctx.closePath();
	        }

	        ctx.fill();
	    }
	}

	Mapper.prototype.drawText = function (layerKey, location, text, font, size, color, backColor,
				opacity, align, haloSize, haloColor, bold, italic, underline, addMark, angleRadiansCounterClockwise, offset) {

		var al;
		switch (align) {
			case 0: al = "left"; break;
			case 1: al = "center"; break;
			case 2: al = "right"; break;
			case 3: al = "justify"; break;
			default: al = "center";
		}

		var fontTxt = (size * 1.386) + "px " + font // We use 1.386 to get capitals of the font to match the size. (Used Verdana to get this size)

        if (italic)
            fontTxt = "italic " + fontTxt;

		if (bold)
		    fontTxt = "bold " + fontTxt;

		var ctx = this._getContext(layerKey);
		
		ctx.fillStyle = color;
		ctx.globalAlpha = opacity / 100.0;
		ctx.lineStyle = color;
		ctx.font = fontTxt;
		ctx.textAlign = al;
		ctx.textBaseline = "middle";

		if (angleRadiansCounterClockwise && angleRadiansCounterClockwise != 0) {
		    var p = new Mapzania.Point(location.x + offset.x, location.y - offset.y);
		    var p2 = p.rotate(new Mapzania.Point(location.x, location.y), -angleRadiansCounterClockwise);
		    ctx.save();
		    ctx.translate(p2.x, p2.y);
		    ctx.rotate(-angleRadiansCounterClockwise);
		    this._drawTextRaw(ctx, text, 0, 0, haloSize, haloColor);
		    ctx.restore();
		}
		else {
		    this._drawTextRaw(ctx, text, location.x + offset.x, location.y - offset.y, haloSize, haloColor);
		}
	}

	Mapper.prototype._drawTextRaw = function (ctx, text, x, y, haloSize, haloColor) {
	    if (haloColor) {
	        ctx.shadowBlur = haloSize;
	        ctx.shadowColor = haloColor;

	        ctx.shadowOffsetX = haloSize;
	        ctx.shadowOffsetY = haloSize;
	        ctx.fillText(text, x, y);

	        ctx.shadowOffsetX = haloSize;
	        ctx.shadowOffsetY = -haloSize;
	        ctx.fillText(text, x, y);

	        ctx.shadowOffsetX = -haloSize;
	        ctx.shadowOffsetY = haloSize;
	        ctx.fillText(text, x, y);

	        ctx.shadowOffsetX = -haloSize;
	        ctx.shadowOffsetY = -haloSize;
	        ctx.fillText(text, x, y);
	    } else {
	        ctx.fillText(text, x, y);
	    }
	}

	Mapper.prototype.fillBitmapRotated = function (layerKey, pTopLeft, width, height, src, opacity, angleRadiansCounterClockwise) {
	    opacity = opacity / 100.0;

	    var ctx = this._getContext(layerKey);

	    ctx.globalAlpha = opacity;

	    var img = new Image();
	    img.layerSessionId = ctx.layerSessionId;

	    img.onload = function () {
	        if (img.layerSessionId == ctx.layerSessionId) {
	            ctx.save();
	            ctx.translate(pTopLeft.x, pTopLeft.y);
	            ctx.rotate(-angleRadiansCounterClockwise);
	            ctx.drawImage(img, 0, 0, width, height);
	            ctx.restore();
	        }
	    }

	    img.src = this._mapTileSource(layerKey, src);
	}

	Mapper.prototype.fillBitmap = function (layerKey, extent, src, opacity) {

	    opacity = opacity / 100.0;
	    var me = this;
	    var ctx = this._getContext(layerKey);

	    ctx.globalAlpha = opacity;

	    var img = new Image();
	    img.layerSessionId = ctx.layerSessionId;
	    img.onload = function () {
	        if (img.layerSessionId == ctx.layerSessionId) {
	            ctx.drawImage(img, extent.minX, extent.minY, extent.width(), extent.height());
	        }
	    }

	    img.src = this._mapTileSource(layerKey, src);
	}

	Mapper.prototype.drawImage = function (layerKey, extent, src, opacity) {
	    opacity = opacity / 100.0;
	    var ctx = this._getContext(layerKey);
	    ctx.globalAlpha = opacity;

	    var img = new Image();
	    img.onload = function () {
	        ctx.drawImage(img, extent.minX, extent.minY, extent.width(), extent.height());
	    }

	    img.src = src;
	}

	Mapper.prototype.flush = function () { }

	Mapper.prototype._getContext = function (key) {

		var ctx = this.contexts[key];

		if (!ctx)
			throw "No canvas matches key=" + key;

		return ctx;
	}

	Mapper.prototype._mapTileSource = function (layerKey, src) {
	    //// A function to detect IE 10 and 11
	    //var isIE = function () {
	    //    return ("" + navigator.userAgent).indexOf("MSIE 10.0") >= 0 || ("" + navigator.userAgent).indexOf("Trident/7.0") >= 0;
	    //};

	    //// IE 10 and 11 has no (CSS) filters to map images to gray scale. So, for these browsers, we simply let the server do the conversion for us.
	    ////  For other browsers, we let the filters do the work for us
	    //if (this._getContext(layerKey).greyScale && isIE()) {
	    //    return "/api/temp/osmgrayscale/?url=" + encodeURIComponent(src);
	    //}

	    return src;
	}

	Mapper.prototype._getSize = function () {
	    return {
	        height: this.$el.height(),
	        width: this.$el.width()
	    };
	}

	Mapper.prototype.measureText = function (string, font, fontSize) {
	    var ctx = this._canvasTextMeasure.getContext("2d");
	    ctx.font = "" + (fontSize*1.386) + "px" + " " + font;
	    return {
	        height: (fontSize * 1.386),
	        width: ctx.measureText(string).width
	    };
	}

	ns.Mapper = Mapper;

	return ns;

})(Canvas || {})