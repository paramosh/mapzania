﻿/// <reference path="../mapzania.js"/>
var Mapzania = (function (ns) {

    var toPointSymbol = function (style) {

        return {
            type:"PointSymbol",
            size: style.size,
            color: style.fillColor,
            opacity: style.fillOpacity * 100,
            border: toLineSymbol(style)
        }
    }

    //var toCompoundPointSymbol = function (style) {

    //    var points = style.map(function (m) {
    //        return toPointSymbol(m);
    //    });

    //    return {
    //        drawByLayer: true,
    //        points: points
    //    }
    //}

    var toLineSymbol = function (style) {

        if (ns.Util.isArray(style.strokeColor)) {
            var res = {
                type:"CompoundLineSymbol",
                drawByLayer: true,
                lines: []
            };

            for (var i = 0; i < style.strokeColor.length; i++) {
                res.lines.push({
                    color: style.strokeColor[i],
                    opacity: style.strokeOpacity[i] * 100,
                    size: style.strokeWidth[i]
                });
            }

            return res;
        }

        return {
            type:"LineSymbol",
            color: style.strokeColor,
            opacity: style.strokeOpacity * 100,
            size: style.strokeWidth
        };
    }

    //var toCompoundLineSymbol = function (style) {

    //    if (style.lines) {

    //        var drawByLayer = style.drawByLayer;

    //        if (typeof style.drawByLayer === "undefined")
    //            drawByLayer = true;

    //        var lines = style.lines.map(function (m) {
    //            return toLineSymbol(m);
    //        });

    //        return {
    //            drawByLayer: drawByLayer,
    //            lines: lines
    //        }
    //    }
    //    else {
    //        var lines = style.map(function (m) {
    //            return toLineSymbol(m);
    //        });

    //        return {
    //            drawByLayer: true,
    //            lines: lines
    //        }
    //    }
    //}

    var toFillSymbol = function (style) {

        return {
            type: "FillSymbol",
            fillType: 1,
            color: style.fillColor,
            opacity: style.fillOpacity * 100,
            outline: toLineSymbol(style)
        }
    }

    var toLabelSymbol = function (label) {

        var isBold = label.fontWeight && label.fontWeight.toLowerCase() == "bold";
        var res = {
            type:"LabelSymbol",
            color: label.fill,
            bold: isBold,
            fontSize: 10,
            rotation: 0,
            fontFamily: "arial",
            offset: { x: 0, y: 0 }
        };

        return res;
    }

    //var pointToGeneric = function (symbol) {
    //    var res = {
    //        sizes: [symbol.size],
    //        strokes: [],
    //        fills: [{
    //            color: symbol.color
    //        }],
    //        options: {
    //            drawByLayer: false
    //        }
    //    }

    //    if (symbol.border) {
    //        sym.strokes.push({
    //            width: symbol.border.size,
    //            color: symbol.border.color
    //        })
    //    }

    //    return res;
    //}

    //var lineToGeneric = function (symbol) {
    //}

    //var fillToGeneric = function (symbol) {
    //}

    ns.Symbols = {
        //pointToGeneric: pointToGeneric,
        //lineToGeneric: lineToGeneric,
        //fillToGeneric: fillToGeneric,
        toPointSymbol: toPointSymbol,
        //toCompoundPointSymbol: toCompoundPointSymbol,
        toLineSymbol: toLineSymbol,
        //toCompoundLineSymbol: toCompoundLineSymbol,
        toFillSymbol: toFillSymbol,
        toLabelSymbol: toLabelSymbol
    };

    return ns;

})(Mapzania || {});