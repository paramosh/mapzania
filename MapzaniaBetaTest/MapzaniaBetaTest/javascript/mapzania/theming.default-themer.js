﻿var Mapzania = (function (ns) {

	function DefaultThemer() {}

	DefaultThemer.prototype.getSymbol = function (feature) {
	    var type = feature.geometry.getType();

	    if (type == 1 || type == 4) {
	        return {
	            type: "PointSymbol",
	            size: 6,
	            color: "#000000",
	            opacity: 100
	        };
	    }

	    if (type == 2 || type == 5) {
	        return {
	            type: "LineSymbol",
	            size: 2,
	            color: "#000000",
	            opacity: 100
	        };
	    }

	    if (type == 3 || type == 6) {
	        return {
	            type: "FillSymbol",
	            outline: {
	                size: 2,
	                color: "#000000",
	                opacity: 100
	            }
	        };
	    }

	    throw "DefaultThemer: unsupported geometry type.";
	}

	DefaultThemer.prototype.getLabelText = function (feature) {
	    return null;
	}

	DefaultThemer.prototype.getLabelSymbol = function (feature) {
	    return {
	        color: "#000000",
	        bold: false,
	        fontSize: 10,
	        fontFamily: "arial",
	        rotation: 0,
	        "haloColor": "#FFFFFF",
	        "haloThickness": 1.6,
	        offset: { x: 0, y: -12 },
	        visible: false,
	        opacity: 100
	    };
	}

	ns.DefaultThemer = DefaultThemer;

	return ns;

})(Mapzania || {})
