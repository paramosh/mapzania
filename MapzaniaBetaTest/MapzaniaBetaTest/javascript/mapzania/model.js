﻿/// <reference path="../mapzania.js"/>

var Mapzania = (function (ns) {
    ns.MapState = {
        zoomIn: 1,
        zoomOut: 2,
        pan: 3,
        length: 4,
        area: 5,
        queryPoint: 6,
        queryLine: 7,
        queryPolygon: 8,
        printMap: 9,
        exportMap: 10,
        queryPolygonEnclosed: 11
    }

    function Model(api) {
        var me = this;

        this.store = new ns.FeatureStore();

        //this.filterBuilder = new ns.FilterBuilder(this);

        Mapzania.Debug = Mapzania.Debug || {};
        Mapzania.Debug.store = this.store;
        Mapzania.Debug.model = this;

		this.api = api;
		this.schemaLoaded = new ns.Event();
		this.loading = new ns.Event();
		this.loadingProgress = new ns.Event();
		this.loaded = new ns.Event();
		this.zoomed = new ns.Event();
		this.stateChanged = new ns.Event();
		this.layerShown = new ns.Event();
		this.layerHidden = new ns.Event();
		this.layerCleared = new ns.Event();
		this.layerAdded = new ns.Event();
		this.layerRemoved = new ns.Event();
		this.gotLength = new ns.Event();
		this.gotArea = new ns.Event();
		this.queried = new ns.Event();

		this._viewportSize = null; // A hint for the viewport size. A value might not always be available.

		this.sourceFactory = null; // A handle to the source factory. 

		this.loadQueue = new ns.LayerSourceLoadQueue(this);

		this.loadQueue.progress.add(function (e) {
		    me.loadingProgress.fire(e.source, e.data);
		});

        // Private properties only used by Leaflet 
		this._disableLoadOnZoomTo = false;
		this._ignorePanInZoomTo = false;
	}

	Model.prototype.loadSchema = function (mapKey, srid) {
		var me = this;

		this.api.getMap(mapKey, srid, function (schema) {
			if (srid)
				schema.displaySRID = srid;

			me.api.getCoordinateSystems(function (data) {
			    me.srids = data;
			    me.setSchema(schema);
			});
		});
	}

	Model.prototype.applySchema = function (schema, srid) {
	    var me = this;

	    if (srid)
	        schema.displaySRID = srid;

	    me.api.getCoordinateSystems(function (data) {
	        me.srids = data;
	        me.setSchema(schema);
	    });
	}

	Model.prototype.setSchema = function (schema) {
		$.extend(true, this, schema);

		this.startExtent = new ns.Extent(this.startExtent);
		this._setCurrentExtentTo(this.startExtent);
		
		var sourceFactory = new Mapzania.SourceFactory(this);
		var me = this;

		sourceFactory.init(function () {
			for (var i = 0; i < me.layers.length; i++) {
				var layer = me.layers[i];
				me._initLayer(sourceFactory, layer);
			}
			me.schemaLoaded.fire(me, schema);
		});

		this.sourceFactory = sourceFactory;
	}

	Model.prototype.getSchema = function () {
	    var schema =
            {
                backColor: this.backColor,
                backImageUrl: this.backImageUrl,
                backOpacity: this.backOpacity,
                displaySRID: this.displaySRID,
                key: this.key,
                layers: this.layers,
                name: this.name,
                namedZooms: this.namedZooms,
                rotation: this.rotation,
                sRID: this.displaySRID,
                scaleType: this.scaleType,
                startExtent: new ns.Extent(this.startExtent),
                tags: this.tags,
                useUnifiedZoomSizes:this.useUnifiedZoomSizes,
                unifiedZoomSizes: this.unifiedZoomSizes,
                snapUIToUnifiedZoomSizes: this.snapUIToUnifiedZoomSizes,
                removeConflictingLabels: this.removeConflictingLabels
            };
	    var schemaDeep = {};
	    $.extend(true, schemaDeep, schema);
	    schemaDeep.layers.forEach(function (cur) { cur.src = null;});
	    return schemaDeep;
	}

	Model.prototype.addLayer = function (layer, indexSchema) {
	    if (!indexSchema) {
	        indexSchema = 0;
	    }

	    layer.src = undefined;
	    this.layers.splice(indexSchema, 0, layer);
	    this._initLayer(this.sourceFactory, layer);
	    this.layerAdded.fire(this, { layer: layer, indexSchema: indexSchema, layers: this.layers });

	    if (layer.visible) {
	        this.loading.fire(this, this.extent);
	        this.showLayer(layer.key);
	    }
	}
    
	Model.prototype.removeLayer = function (key) {
	    var layer = this.getLayer(key);
	    var index = -1;
	    $.each(this.layers, function (i, cur) {
	        if (cur.key == key) index = i;
	    });

	    this.layers.splice(index, 1);
	    this.layerRemoved.fire(this, { layer: layer});
	}

	Model.prototype._initLayer = function (sourceFactory, layer) {

	    var src = sourceFactory.getSource(layer);
	    layer.src = src;
	    src.layer = layer;
	    layer.hidden = [];
	    layer.requestFilters = [];
	    layer.responseFilters = [];
	    layer.clientFeatures = { features: [] };

	    if (layer.src.gotFeatures) {
	        layer.src.gotFeatures.add(function (evt) {

	            var filters = new ns.FilterBuilder(this).getResponseFilters(evt.source.layer);

	            var features = evt.data.features;

	            var engine = new ns.FeatureEngine(features, filters);
	            engine.run();

	            //---ANDRE----
	            this.store.set(evt.source.layer.key, features);
                //------------
	            if (this.noOfLoaded > 0) {
	                this.loadedData.push({ layer: evt.source.layer, data: evt.data });

	                if (this.loadedData.length == this.noOfLoaded) {
	                    //console.log("loadedData=" + this.loadedData.length);
	                    //console.log(this.loadedData);
	                    this.noOfLoaded = -1;
	                    this.loaded.fire(this, this.loadedData);
	                }
	            }
	            else {
	                var data = [{ layer: evt.source.layer, data: evt.data }];
	                this.loaded.fire(this, data);
	            }
	        }.bind(this))
	    }

	    if (layer.src.gotTiles) {
	        layer.src.gotTiles.add(function (evt) {
	            var data = [{ layer: evt.source.layer, data: evt.data }];
	            this.loaded.fire(this, data);
	        }.bind (this));
	    }
	}

	Model.prototype.load = function (toDelayVectorData) {
		this.noOfLoaded = 0;
		this.loadedData = [];

		toDelayVectorData = !(!toDelayVectorData);
		this.loading.fire(this, this.extent);
		var me = this;
		for (var i = 0; i < this.layers.length; i++) {
		    var layer = this.layers[i];

		    if (layer.src.getFeatures) {
		        setTimeout(function (layer) {
		            me._getFeatures(layer);
		        }, toDelayVectorData ? 1000 : 0, layer);
		    }

		    if (layer.src.getTiles) {
		        this._getTiles(layer);
		    }
		}
	}

	Model.prototype.getTheme = function (layer) {
	    var zoom = Math.max(this.extent.width(), this.extent.height());
	    if (this.currentZoomSize) {
	        zoom = this.currentZoomSize.zoom;
	    }

	    for (var i = 0; i < layer.themes.length; i++) {
	        var theme = layer.themes[i];
	        if (zoom >= theme.minZoom && zoom <= theme.maxZoom)
	            return theme;
	    }
	    return null;
	}

	Model.prototype.updateLayer = function (key) {
	}

	Model.prototype.getFilter = function (layer, theme) {
		var filter;

		if (layer.filter && theme && theme.filter)
			filter = "(" + theme.filter + ") AND (" + layer.filter + ")";
		else
			if (theme && theme.filter)
				filter = theme.filter;
			else
				filter = layer.filter;

		return filter;
	}

	Model.prototype.getLayers = function () {
		return this.layers;
	}

	Model.prototype.getLayer = function (key) {
		for (var i = 0; i < this.layers.length; i++) {
			var lyr = this.layers[i];
			if (lyr.key == key)
				return lyr;
		}

		throw "Model.getLayer: Invalid layerKey:" + key;
	}

	Model.prototype.getLayerSource = function (key) {
	    for (var i = 0; i < this.layers.length; i++) {
	        var lyr = this.layers[i];
	        if (lyr.key == key)
	            return lyr.src;
	    }

	    throw "Model.getLayerSource: Invalid layerKey:" + key;
	}

	Model.prototype.hasLayer = function (key) {
	    for (var i = 0; i < this.layers.length; i++) {
	        var lyr = this.layers[i];
	        if (lyr.key == key)
	            return true;
	    }
	    return false;
	}

	Model.prototype.filterLayer = function (key, filter, fence) {
	    var lyr = this.getLayer(key);
	    lyr.filter = filter;
	    lyr.fence = fence;

		if (this.updating)
			this.noOfLoaded++;

		this.loading.fire(this, this.extent);
		this.clearLayer(key);
		this._getFeatures(lyr);
	}

	Model.prototype.refreshLayer = function (key) {
	    var layer = this.getLayer(key);
	    this.clearLayer(key);

	    if (layer.src.getFeatures) {
            this._getFeatures(layer);
	    }

	    if (layer.src.getTiles) {
	        this._getTiles(layer);
	    }
	}

	Model.prototype.setFeaturesOnSchema = function (layerKey, features) {
	    var lyr = this.getLayer(layerKey);
	    if (!lyr.src.setFeatures)
	        throw "The layer [" + layerKey + "] does not seem to be a 'schema source' (SCHEMASOURCE-*) layer.";
	    lyr.src.setFeatures(features);
	    this.clearLayer(layerKey);
	    this._getFeatures(lyr);
	}

	Model.prototype.changeState = function (state) {
		this.stateChanged.fire(this, state);
	}

	Model.prototype.zoomTo = function (extent, isZoomOut, isZoomIn, toDelayVectorData, isPan) {
	    if (this._ignorePanInZoomTo && isPan) {
	        return;
	    }

	    this._setCurrentExtentTo(extent, isZoomOut, isZoomIn);

	    for (var i = 0; i < this.layers.length; i++) {
	        var key = this.layers[i].key;
	        this.clearLayer(key);
	    }

	    this.zoomed.fire(this, this.extent);

	    if (!this._disableLoadOnZoomTo) {
	        this.load(toDelayVectorData);
	    }
	}

	Model.prototype._zoomToLeaflet = function (extent, zoom) {
	    this.extent = extent;
	    this.currentZoomSize = null;
	    for (var i = 0; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];
	        if (zoom == cur.zoom) {
	            this.currentZoomSize = cur;
	            break;
	        }
	    }
	    
	    for (var i = 0; i < this.layers.length; i++) {
	        var key = this.layers[i].key;
	        this.clearLayer(key);
	    }

	    this.load(true);
	}

	Model.prototype.zoomIn = function (point) {
	    this._zoomWithFactorAroundPoint(0.5, point, false, true);
	}

	Model.prototype.zoomOut = function (point) {
	    this._zoomWithFactorAroundPoint(1.8, point, true, false);
	}

    // Internal utility method used when zooming in or out
	Model.prototype._zoomWithFactorAroundPoint = function (factor, point, isZoomOut, isZoomIn) {

	    var curExtent = new Mapzania.Extent(this.extent);
	    if (!point) {
	        point = curExtent.getCenter();
	    }
	    var curCenter = curExtent.getCenter();
	    var dx = point.x - curCenter.x;
	    var dy = point.y - curCenter.y;
	    var w = curExtent.width();
	    var h = curExtent.height();
	    var pdx = dx / (w / 2);
	    var pdy = dy / (h / 2);

	    var newExtent = curExtent.grow(factor);
	    var result = this._calculateBestFitExtentAndZoomSize(newExtent, isZoomOut, isZoomIn);
	    newExtent = result.extent;
	    var w2 = newExtent.width();
	    var h2 = newExtent.height();

	    var pointNew = new Mapzania.Point(curCenter.x + (w2 / 2) * pdx, curCenter.y + (h2 / 2) * pdy);
	    newExtent.translate(-(pointNew.x - point.x), -(pointNew.y - point.y));

	    this.zoomTo(newExtent, null, null, true);
	}

	Model.prototype.refresh = function () {
	    this.zoomTo(this.extent);
	}

	Model.prototype._setCurrentExtentTo = function (extent, isZoomOut, isZoomIn) {
	    var x = this._calculateBestFitExtentAndZoomSize(extent, isZoomOut, isZoomIn);
	    this.extent = x.extent;
	    this.currentZoomSize = x.currentZoomSize;
	    if (this.currentZoomSize) {
	        var msg = "Map display is set to the closest unified zoom size " + this.currentZoomSize.size + " (zoom " + this.currentZoomSize.zoom + "). ";
	        msg += "Current map display zoom range is " + (Math.max(this.extent.width(), this.extent.height()));
	        Mapzania.Log.debug(msg, "Mapzania.Model");
	    } else {
	        Mapzania.Log.debug("Map display is set to zoom range " + (Math.max(this.extent.width(), this.extent.height())), "Mapzania.Model");
	    }
	}

    // Internal utility method to determine the best fitting extent and zoom Size
	Model.prototype._calculateBestFitExtentAndZoomSize = function (extent, isZoomOut, isZoomIn) {
	    isZoomOut = !(!isZoomOut);
	    isZoomIn = !(!isZoomIn);
	    var oldExtent = this.extent;
	    var extent = this.resizeExtentToRatioDisplay(extent);
	    var currentZoomSize = null;

	    if (this.useUnifiedZoomSizes && this.unifiedZoomSizes) {
	        var oldZoomSize = this.currentZoomSize;

	        var zoomRange = Math.max(extent.width(), extent.height());
	        var usedWidth = (zoomRange == extent.width());
	        currentZoomSize = this.findSuitableZoomSize(zoomRange);

	        if (this.snapUIToUnifiedZoomSizes) {
	            if (oldZoomSize && oldExtent && oldZoomSize.zoom >= currentZoomSize.zoom && isZoomIn) {
	                currentZoomSize = this.findZoomSizeAfter(oldZoomSize.zoom);
	            }

	            var max = (usedWidth ? extent.width() : extent.height());
	            var factor = currentZoomSize.size / max;
	            extent = extent.grow(factor);
	        }
	    }

	    return { extent: extent, currentZoomSize: currentZoomSize };
	}

	Model.prototype.resizeExtentToRatioDisplay = function (extent) {
	    var e = new Mapzania.Extent(extent);
	    if (this._viewportSize)
	        e.normalise(this._viewportSize);
	    return e;
	}

	Model.prototype.findSuitableZoomSize = function (size) {
	    var res = null;
	    for (var i = 0; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];
	        if ((size < cur.size) || (Math.abs(size - cur.size) / 100 < 0.05)) {
	            res = cur;
	        }
	    }
	    if (!res)
	        res = this.unifiedZoomSizes[0];
	    return res;
	}

	Model.prototype.findZoomSizeBefore = function (zoom) {
	    var res = null;
	    for (var i = 1; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];
	        
	        if (zoom > cur.zoom) {
	            res = cur;
	        }
	    }
	    if (!res)
	        res = this.unifiedZoomSizes[0];
	    return res;
	}

	Model.prototype.findZoomSizeAfter = function (zoom) {
	    var res = null;
	    for (var i = 0; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];

	        if (zoom < cur.zoom) {
	            return cur;
	        }
	    }

	    return this.unifiedZoomSizes[this.unifiedZoomSizes.length - 1];
	}

	Model.prototype.reset = function () {
		this.zoomTo(this.startExtent);
	}

	Model.prototype.showLayer = function (key) {
		var layer = this.getLayer(key);
		layer.visible = true;

		if (this.updating)
		    this.noOfLoaded++;

		this.layerShown.fire(this, key);

		if (layer.src.gotFeatures) {
		    this.clearLayer(key);
		    this._getFeatures(layer);
		}

		if (layer.src.gotTiles) {
		    this._getTiles(layer);
		}
	}

	Model.prototype.hideLayer = function (key) {
		var layer = this.getLayer(key);
		layer.visible = false;

		this.layerHidden.fire(this, key);
	}

	Model.prototype.clearLayer = function (key) {
		this.layerCleared.fire(this, key);
	}

	Model.prototype.getLength = function (points) {
		var me = this;

		var coords = [];

		for (var i = 0; i < points.length; i++) {
			var pnt = points[i];
			coords.push(pnt.x);
			coords.push(pnt.y);
		}
		this.api.getLength(this.key, this.displaySRID, coords, function (val) {
			me.gotLength.fire(me, val);
		});
	}

	Model.prototype.getArea = function (points) {
		var me = this;

		var coords = [];

		for (var i = 0; i < points.length; i++) {
			var pnt = points[i];
			coords.push(pnt.x);
			coords.push(pnt.y);
		}
		this.api.getArea(this.key, this.displaySRID, coords, function (val) {
			me.gotArea.fire(me, val);
		});
	}

	Model.prototype.queryAt = function (data) {
		this.queried.fire(this, data);
	}

	Model.prototype.setViewportSize = function (size) {
	    this._viewportSize = size;
	}

	Model.prototype.getTagValue = function (tagName) {
	    var value = null;
	    if (this.tags) {
	        this.tags.forEach(function (cur) {
	            if (cur.key == tagName) {
	                value = cur.value;
	            }
	        });
	    }
	    return value;
	}

    // Starts loading the layer data, or clear the layer when it is not visible
	Model.prototype._getFeatures = function (layer) {
	    if (layer.visible) {

	        var src = layer.src;

	        if(layer.isLocal && layer.requestFilters.length == 0)
	        {
	            src.gotFeatures.fire(src, { seqNo: 0, features: [], isFinal: true });
	        }
	        else{

	            var zoom = this.currentZoomSize.zoom;
	            var extent = this._getExtentToLoad(this.extent);

	            var options = { tiled: layer.tiled, extent: extent, zoom: zoom};

	            var filters = new ns.FilterBuilder(this).getRequestFilters(layer);

	            src.getFeatures(filters, options);
	        }
	    }
	    else {
	        //TODO: Review this
	        this.clearLayer(layer.key);
	        // Sometimes, the loading event is fired, but no data is actually loaded, leaving the renderer in a loading state. This workaround fixes that issue. 
	        var me = this;
	        setTimeout(function (cur) {
	            me.loaded.fire(me, [{ layer: layer, data: { seqNo: 0, features: [], isFinal: true } }]);
	        }, 100);
	    }
	}

	Model.prototype._getTiles = function (layer) {
	    if (layer.visible) {
	        layer.src.getTiles(this._getExtentToLoad(this.extent), this.extent, layer.filter);
	    }
	}

	Model.prototype._getExtentToLoad = function (extent) {
	    var rotateZ = (this.rotation && this.rotation.rotateZ ? this.rotation.rotateZ : 0.0);
	    return this.extent.grow( (1 + Math.abs( Math.sin(rotateZ) ) ) * 1.5);
	}

	Model.prototype._getSmallestLayerOrdinal = function () {
	    var MAX = 99999999999;
	    var ordinal = MAX;
	    this.getLayers().forEach(function (cur) {
	        if (cur.ordinal < ordinal) ordinal = cur.ordinal;
	    });
	    if (ordinal == MAX) ordinal = 0;
	    return ordinal;
	}

	ns.Model = Model;

	return ns;

})(Mapzania || {})
