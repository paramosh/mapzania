﻿/**
 * @doc TRaphael.Mapper.*ClassDescription*
 *
 * An interactive map drawer implementation, using the Raphael toolkit. See <a href="http://raphaeljs.com/" target="_blank">http://raphaeljs.com/</a> for more information.
 * Applications typically instantiate this class once, and does not make direct use of any functions defined in this class.
 */
var TRaphael = (function (ns) {

	var ts = Mapzania;

    /**
      * @doc TRaphael.Mapper.Mapper
      *
      * Constructor. The properties and functions defined in this class are not public, and reserved for the framework and SDK.
      */
	function Mapper() {
		this.papers = {};
		this.canvasses = {};
		this.viewChanged = new ts.Event();
        
		this.layerParams = [];
	}

	Mapper.prototype.init = function ($el, callback) {
	    this.$el = $el;

	    this._measureTextPaper = Raphael(0, 0, 0, 0);
	    this._measureTextPaper.canvas.style.visibility = 'hidden';

	    if (callback)
	        callback();
	}

	Mapper.prototype.zoom = function (factor) {
		//this.swf.zoom(factor);
	}

	Mapper.prototype.onResized = function () {
        // We implement a bit of a work-around to get map resizes to work on the Raphael renderer. We remove all the layers and re-add them all.
	    var oldLayerParams = this.layerParams;
	    this.layerParams = [];
	    this.papers = [];
	    this.canvasses = [];
	    this.$el.html("");
	    var me = this;
	    oldLayerParams.forEach(function (cur) {
	        me.addLayer(cur.key, cur.index, cur.greyScale);
	    });
	}

	Mapper.prototype.setBackColor = function (color) {
		this.$el.css("background-color", color);
	}

	Mapper.prototype.setBackImageUrl = function (url) {
		this.$el.css("background-image", "url('" + url + "')");
		this.$el.css("background-repeat", "repeat");
	}

	Mapper.prototype.zoomTo = function (extent) {
	}

	Mapper.prototype.addLayer = function (key, index, greyScale) {
	    this.layerParams.push({ key: key, index: index, greyScale: greyScale });

	    var $canvas;

	    var html = "<div id='raphael_mapper_layer_id_" + key + "'></div>";

	    if (typeof index != 'undefined') {
	        if (index > this.$el.children().length) {
	            Mapzania.Log.error("NB!: index > this.$el.children().length", "Mapzania.Mapper (Raphael)");
	            index = this.$el.children().length - 1;
	        }

	        if (this.$el.children().length == 0) {
	            this.$el.append(html);
	        } else {
	            this.$el.children().eq(index).before(html);
	        }

	        $canvas = this.$el.children().eq(index);
	    }
        else {
	        this.$el.append(html);
	        $canvas = this.$el.children().last();
	    }

		$canvas.addClass("layername-" + key);

		$canvas.css("position", 'absolute');
		$canvas.css("top", '0');
		$canvas.css("left", '0');
		$canvas.css("bottom", '0');
		$canvas.css("right", '0');
		$canvas.css("overflow", 'none');

		if (greyScale) {
		    //$canvas.css("filter", "url('data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale')");
		    $canvas.css("filter", "grayscale(100%)");
		    $canvas.css("-webkit-filter", "grayscale(100%)");
		    $canvas.css("-moz-filter", "grayscale(100%)");
		    $canvas.css("-ms-filter", "grayscale(100%)");
		    $canvas.css("-o-filter", "grayscale(100%)");
		    //$canvas.addClass("ts-grey-scale-layer");
		}

		$canvas[0].width = this.$el.width();
		$canvas[0].height = this.$el.height();

		this.canvasses[key] = $canvas;

		var size = this._getSize();

		this.papers[key] = Raphael($canvas[0], size.width, size.height);

		this.papers[key].greyScale = greyScale;
	}

	Mapper.prototype.removeLayer = function (key) {
	    var id = "raphael_mapper_layer_id_" + key;
	    delete this.papers[key];
	    delete this.canvasses[key];
	    $("#" + id).remove();

	    for (var index = 0; index < this.layerParams.length; index++) {
	        if (this.layerParams[index].key == key) {
	            this.layerParams.splice(index, 1);
	            break;
	        }
	    }
	}

	Mapper.prototype.removeAllLayers = function () {
	    this.papers = {};
	    this.canvasses = {};
	    this.layerParams = [];
	    this.$el.html("");
	}

	Mapper.prototype.hideLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeOut();
	}

	Mapper.prototype.showLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeIn();
	}

	Mapper.prototype.clearLayer = function (key) {
	    var paper = this._getPaper(key);
	    paper.clear();
	}

	Mapper.prototype._clearAllLayers = function (key) {
	    for (var cur in this.papers) {
	        this.clearLayer(cur);
	    }
	}

	Mapper.prototype.drawCircle = function (layerKey, center, radius, color, size, opacity) {

	    var paper = this._getPaper(layerKey);

		var circle = paper.circle(center.x, center.y, radius);

		circle.attr({
		    'fill-opacity': 0,
		    'stroke': color,
		    'stroke-width': size,
		    'stroke-opacity': opacity / 100.0
		});
	}

	Mapper.prototype.fillCircle = function (layerKey, center, radius, color, opacity) {

		var paper = this._getPaper(layerKey);

		var circle = paper.circle(center.x, center.y, radius);
		circle.attr({ fill: color, 'stroke-width': 0, opacity: opacity / 100.0 });
	}

	Mapper.prototype.drawLine = function (layerKey, line, color, size, opacity, endCap, joinType, dashArray) {


	    if (!dashArray)
	        dashArray = [];

	    var da = [""];

	    if (dashArray.length > 1) {
	        if (dashArray[0] == dashArray[1])
	            da = ["."];

	        if (dashArray[0] > dashArray[1])
	            da = ["--"];
	    }

	    if (!endCap || dashArray.length > 0)
	        endCap = 0;

	    var caps = ["round", "butt", "square"];
	    var cap = caps[endCap];

	    if (!joinType)
	        joinType = 0;

	    var joins = ["round", "bevel", "miter"];
	    var join = joins[joinType];

	    var paper = this._getPaper(layerKey);

	    var str = "";

	    var pnts = line.points;

	    str += this._moveTo(pnts[0]);
	    for (var i = 1; i < pnts.length; i++) {
	        str += this._lineTo(pnts[i]);
	    }


	    var path = paper.path(str);

	    path.attr({
	        'stroke': color,
	        'stroke-width': size,
	        'stroke-opacity': opacity / 100,
	        'stroke-linecap': cap,
	        'stroke-linejoin': join,
	        'stroke-dasharray': da
	    });
	}

	Mapper.prototype.fillPolygon = function (layerKey, lines, color, color2, opacity, type) {
	    if (type == 1) {
	        var paper = this._getPaper(layerKey);

	        var str = "";

	        for (var lineNo = 0; lineNo < lines.length; lineNo++) {
	            var pnts = lines[lineNo].points;

	            str += this._moveTo(pnts[0]);
	            for (var i = 1; i < pnts.length; i++) {
	                str += this._lineTo(pnts[i]);
	            }

	            str += " Z ";
	        }

	        var path = paper.path(str);

	        path.attr({ fill: color, 'stroke-width': 0, opacity: opacity / 100.0 });
	    }
	}

	Mapper.prototype.drawText = function (layerKey, location, text, font, size, color, backColor,
				opacity, align, haloSize, haloColor, bold, italic, underline, addMark, angleRadiansCounterClockwise, offset) {
		var al;
		switch (align) {
			case 0: al = "start"; break;
			case 1: al = "middle"; break;
			case 2: al = "end"; break;
			case 3: al = "middle"; break;
			default: al = "middle";
		}

		if (!angleRadiansCounterClockwise) angleRadiansCounterClockwise = 0.0;
		var p = new Mapzania.Point(location.x + offset.x, location.y - offset.y);
		var p2 = p.rotate(new Mapzania.Point(location.x, location.y), -angleRadiansCounterClockwise);

		var paper = this._getPaper(layerKey);

		var text = paper.text(p2.x, p2.y, text);

		text.attr({
			"fill": color,
			"font-family": font,
			"font-size": ( size * 1.386 ) + "px", // We use 1.386 to get capitals of the font to match the size. (Used Verdana to get this size) 
			"text-anchor": al
		});

		if (italic)
		    text.attr("font-style", italic ? "italic" : "");

		if(bold)
		    text.attr("font-weight", bold ? "bold" : "");

		var bbox = text.getBBox(); // Get the text bounding box before it is rotated

		if (angleRadiansCounterClockwise && angleRadiansCounterClockwise != 0) {
		    var degrees = -angleRadiansCounterClockwise * (180 / Math.PI);
		    text.transform("r" + degrees+","+p2.x+","+p2.y+"");
		}
		
		if (haloColor) {
		    var halo = this._backHalo(paper, bbox, haloSize, haloColor, angleRadiansCounterClockwise);
		    text.insertBefore(halo);
		}
	}

	Mapper.prototype._backHalo = function (paper, bbox, haloSize, haloColor, angleRadiansCounterClockwise) {
	    var rect = paper.rect(bbox.x, bbox.y + bbox.height * 0.42, bbox.width, bbox.height / 6).attr('stroke', 'none');
	    if (angleRadiansCounterClockwise && angleRadiansCounterClockwise != 0) {
	        var degrees = -angleRadiansCounterClockwise * (180 / Math.PI);
	        rect.transform("r" + degrees);
	    }
	    rect.glow({
	        color: haloColor,
	        width: haloSize * 12,
	        opacity: 1,
	    });

	    return rect;
	}

	Mapper.prototype.fillBitmap = function (layerKey, extent, src, opacity) {
	    var paper = this._getPaper(layerKey);

	    var img = paper.image(this._mapTileSource(layerKey, src), extent.minX, extent.minY, extent.width(), extent.height());

	    img.attr({ opacity: opacity / 100.0 });
	}

	Mapper.prototype.fillBitmapRotated = function (layerKey, pTopLeft, width, height, src, opacity, angleRadiansCounterClockwise) {
	    var paper = this._getPaper(layerKey);
	    var degrees = -angleRadiansCounterClockwise * (180 / Math.PI);

	    var img = paper.image(this._mapTileSource(layerKey, src), 0, 0, width + 1, height + 1);

	    img.translate(pTopLeft.x, pTopLeft.y);
	    img.rotate(degrees, 0, 0);

	    img.attr({ opacity: opacity / 100.0 });
	}

	Mapper.prototype.drawImage = function (layerKey, extent, src, opacity) {
	    var paper = this._getPaper(layerKey);

	    var img = paper.image(src, extent.minX, extent.minY, extent.width(), extent.height());

	    img.attr({ opacity: opacity / 100.0 });
	}

	Mapper.prototype.flush = function () { }

	Mapper.prototype._getPaper = function (key) {

		var ctx = this.papers[key];

		if (!ctx)
			throw "No paper matches key=" + key;

		return ctx;
	}

	Mapper.prototype._mapTileSource = function (layerKey, src) {
        //// A function to detect IE 10 and 11
	    //var isIE = function () {
	    //    return ("" + navigator.userAgent).indexOf("MSIE 10.0") >= 0 || ("" + navigator.userAgent).indexOf("Trident/7.0") >= 0;
	    //};

	    //// IE 10 and 11 has no (CSS) filters to map images to gray scale. So, for these browsers, we simply let the server do the conversion for us.
        ////  For other browsers, we let the filters do the work for us
	    //if (this._getPaper(layerKey).greyScale && isIE()) {
	    //    return "/api/temp/osmgrayscale/?url=" + encodeURIComponent(src);
	    //}

	    return src;
	}

	Mapper.prototype._getSize = function () {
	    return {
	        height: this.$el.height(),
	        width: this.$el.width()
	    };
	}

	Mapper.prototype._moveTo = function (point) {
		return " M " + point.x + " " + point.y;
	}

	Mapper.prototype._lineTo = function (point) {
		return " L " + point.x + " " + point.y;
	}

	Mapper.prototype.measureText = function (string, font, fontSize) {
	    var el = this._measureTextPaper.text(0, 0, string);
	    el.attr('font-family', font);
	    el.attr('font-size', (fontSize  * 1.386) + "px");
	    var bBox = el.getBBox();
	    el.remove();
	    return {
	        width: bBox.width,
	        height: bBox.height
	    };
	}

	ns.Mapper = Mapper;

	return ns;

})(TRaphael || {})