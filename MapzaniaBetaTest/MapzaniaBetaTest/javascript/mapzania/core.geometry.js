﻿/**
 * @doc Mapzania.Geometry.*ClassDescription*
 *
 * Provides an interface specification for all Mapzania Geometry classes. Applications do not make use of this class directly, but will use the following implementations to represent the appropriate geometry:
 * <ul>
 *   <li>Mapzania.Point</li>
 *   <li>Mapzania.Line</li>
 *   <li>Mapzania.Polygon</li>
 *   <li>Mapzania.MultiPoint</li>
 *   <li>Mapzania.MultiLine</li>
 *   <li>Mapzania.MultiPolygon</li>
 * </ul>
 *
 */
var Mapzania = (function (ns) {
    /**
      * @doc Mapzania.Geometry.Geometry
      *
      * Constructor.
      *
      * This class is provided here only as an interface specification for all Mapzania Geometry classes. The supported geometry classes are:
      * <ul>
      *   <li>Mapzania.Point</li>
      *   <li>Mapzania.Line</li>
      *   <li>Mapzania.Polygon</li>
      *   <li>Mapzania.MultiPoint</li>
      *   <li>Mapzania.MultiLine</li>
      *   <li>Mapzania.MultiPolygon</li>
      * </ul>
      */
    function Geometry() { 
    }

    /**
      * @doc Mapzania.Geometry.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Geometry.prototype.read = function (data) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
    Geometry.prototype.getExtent = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    Geometry.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
    Geometry.prototype.intersects = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Geometry.prototype.getPath = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
    Geometry.prototype.getSize = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
    Geometry.prototype.getCentroid = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Geometry.prototype.getGeometryType = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Geometry.prototype.getType = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Geometry) A deep clone of this instance.
      */

    Geometry.prototype.clone = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */
    Geometry.prototype.translate = function (x, y) {
        throw "Not implemented";
    }

    ns.Geometry = Geometry;

    if (!ns.FeatureTypes)
        ns.FeatureTypes = {};

    ns.FeatureTypes["-1"] = Geometry;
    ns.FeatureTypes["1000"] = Geometry;

    return ns;

})(Mapzania || {})
