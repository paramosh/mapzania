﻿var Mapzania = (function (ns) {

    function TiledRasterLayerSource(model, layer) {

        this.gotTiles = new ns.Event();

        var me = this;

        this.getTiles = function (extentToLoad, extentVisible, filter) {

            if (!extentVisible)
                extentVisible = extentToLoad;

            me.lastRequestId = Mapzania.Util.createPseudoGuid();

            var callback = function (data, responseId) {
                if (me.lastRequestId == responseId) {
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        d.extent = new ns.Extent({
                            minX: d.minX,
                            minY: d.minY,
                            maxX: d.maxX,
                            maxY: d.maxY
                        });
                    }
                    me.gotTiles.fire(me, data);
                }
            };

            model.loadQueue.getTiles(
                layer.sourceKey,
                extentToLoad,
                extentVisible,
                model.displaySRID,
                filter,
                callback,
                this.lastRequestId);
        }
    }

    ns.TiledRasterLayerSource = TiledRasterLayerSource;

    return ns;

})(Mapzania || {})
