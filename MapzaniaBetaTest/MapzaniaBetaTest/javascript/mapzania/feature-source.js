﻿/// <reference path="../mapzania.js"/>
var Mapzania = (function (ns) {

    function FeatureSource(sourceKey, api, filterBuilder) {

        var filters = [];

        this.getFeatureCollection = function (callback) {
            //TODO: Cancellations / Queuing
            var callFilters = filterBuilder.getFilters(sourceKey).concat(filters);

            api.getFeatures(sourceKey, callFilters, null, function (data) {
                if (callback)
                    callback(data);
            })
        }

        this.addFeatureCollection = function (featureCollection) {
            api.addFeatures(sourceKey, featureCollection);
        }

        this.addFeature = function (feature) {

            var fc = {
                type: "FeatureCollection",
                features:[feature]
            }

            api.addFeatures(sourcKey, fc);
        }

        this.clearFeatures = function () {
            api.clearFeatures(sourceKey);
        }

        this.addFilter = function (filter, index) {
            if (index)
                filters.splice(index, 0, filter)
            else
                filters.push(filter);
        }

        this.removeFilter = function (index) {
            filters.splice(index, 1);
        }

        this.clearFilters = function () {
            filter = [];
        }
    }

    ns.FeatureSource = FeatureSource;

    return ns;
})(Mapzania || {});