﻿var Mapzania = (function (ns) {
	function TextMapper(elementId, map, onReady) {

		var $el = $(elementId)

		var me = this

		if (onReady) onReady();

		map.loaded.add(function () {
			me.writeLine($el, "Map loaded. [StartExtent=" + JSON.stringify(map.startExtent) + "]")

			var layers = map.getLayers()

			for (var i = 0; i < layers.length; i++) {
				var layer = layers[i];
				layer.updated.add(function (data) {
					me.writeLine($el, "Layer updated. [Info=" + JSON.stringify(data) + "]")
				})
			}
		})
	}

	TextMapper.prototype.writeLine = function (el, line) {
		el.append("<div>" + line + "</div>")
	}

	ns.TextMapper = TextMapper

	return ns;

})(Mapzania || {})
