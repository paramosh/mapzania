﻿/// <reference path="../lib/swfobject-1.5.js" />

//
// We add sheets to a mapper/drawer in a lazy fashion - i.e. a sheet is only added to the map UI if it contains visual data (i.e. features / labels). The LayerSheetManager class is responsible
//  for mapping between map layers and these "sheets" on the drawers. 
//
//  This is an internal utility class, and should not be used by applications.
//

var Mapzania = (function (ns) {
    function LayerSheetManager() {
        // Array of layer data objects, where each object has a key, hasFeatureSheet, hasLabelSheet and greyScale property. 
        //  Index 0 is closest to the user - i.e. on top of the map. This indexing-scheme matches layers on the schema layer list.
        this.layerData = [];

        // A dictionary of sheets names - used for fast lookups
        this.sheetNames = {};
    }

    LayerSheetManager.prototype.removeAllLayers = function () {
        this.layerData = [];
        this.sheetNames = {};
    }

    LayerSheetManager.prototype.addLayer = function (layerKey, indexSchema, greyScale, layerSource) {
        this.layerData.splice(
            indexSchema,
            0,
            {
                key: layerKey,
                greyScale: greyScale,
                hasFeatureSheet: false,
                hasLabelSheet: false,
                layerSource: layerSource
            });

        this.sheetNames[layerKey] = false;
        this.sheetNames[layerKey + "_label"] = false;
    }

    LayerSheetManager.prototype.removeLayer = function (layerKey) {
        var index = this.layerKeyIndex(layerKey);
        if (index >= 0) {
            this.layerData.splice(index, 1);
            this.sheetNames[layerKey] = false;
            this.sheetNames[layerKey + "_label"] = false;
        }
    }

    LayerSheetManager.prototype.getSheetNames = function (layerKey) {
        var sheetNames = [];
        var index = this.layerKeyIndex(layerKey);
        if (index >= 0) {
            var layerObj = this.layerData[index];
            if (layerObj.hasFeatureSheet) {
                sheetNames.push(layerKey);
            }
            if (layerObj.hasLabelSheet) {
                sheetNames.push(layerKey + "_label");
            }
        }
        return sheetNames;
    }

    LayerSheetManager.prototype.layerKeyIndex = function (layerKey) {
        for (var i = 0; i < this.layerData.length; i++) {
            if (this.layerData[i].key == layerKey) {
                return i;
            }
        }
        return -1;
    }

    LayerSheetManager.prototype.hasLayerKey = function (key) {
        return this.layerKeyIndex(key) >= 0;
    }

    LayerSheetManager.prototype.getLayerCount = function () {
        return this.layerData.length;
    }

    LayerSheetManager.prototype.hasSheet = function (layerKey, sheetType) {
        var sheetName = layerKey;
        if (sheetType == 1) {
            sheetName = sheetName + "_label";
        }
        return this.sheetNames[sheetName];
    }

    LayerSheetManager.prototype.addSheet = function (layerKey, sheetType) {
        var uiIndex = 0;

        // Evaluate feature sheets
        for (var i = this.layerData.length - 1; i >= 0; i--) {
            var obj = this.layerData[i];
            if (obj.key == layerKey && sheetType == 0) {
                obj.hasFeatureSheet = true;
                this.sheetNames[layerKey] = true;
                return uiIndex;
            }

            if (obj.hasFeatureSheet) {
                uiIndex++;
            }
        }

        // Evaluate label sheets
        for (var i = this.layerData.length - 1; i >= 0; i--) {
            var obj = this.layerData[i];
            if (obj.key == layerKey && sheetType == 1) {
                obj.hasLabelSheet = true;
                this.sheetNames[layerKey + "_label"] = true;
                return uiIndex;
            }

            if (obj.hasLabelSheet) {
                uiIndex++;
            }
        }

        throw "Layer is not known: " + layerKey;
    }

    LayerSheetManager.prototype.isGreyScale = function (layerKey) {
        return this.layerData[this.layerKeyIndex(layerKey)].greyScale;
    }

    LayerSheetManager.prototype.layerSource = function (layerKey) {
        return this.layerData[this.layerKeyIndex(layerKey)].layerSource;
    }

    ns.LayerSheetManager = LayerSheetManager;

    return ns;

})(Mapzania || {});

/**
 * @doc Mapzania.Renderer.*ClassDescription*
 *
 * A class responsible for coordinating the rendering of the interactive map, using the Model, Viewport and Mapper (also known as the drawer, such as Canvas or Raphael).
 * Applications typically instantiate this class once, and is not making direct use of any functionality provided in this class.
 */

var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.Renderer.Renderer
      *
      * Constructor.
      *
      * @param model (Mapzania.Model) A handle to the Mapzania Model instance. 
      *
      * @param viewport (Mapzania.Viewport) A handle to the viewport for this map
      *
      * @param drawer (Mapzania.Mapper) The map drawer instance to use. Valid class instances are Canvas.Mapper, Raphael.Mapper or Flash.Mapper.
      */
    function Renderer(model, viewport, drawer) {
        this.model = model;
        this.viewport = viewport;
        this.drawer = drawer;

        if (this.viewport.setRendererModel) {
            this.viewport.setRendererModel(this, this.model);
        }

        this.model.setViewportSize(viewport.getSize());

        this.rendered = new ns.Event();

        this.layerSheetManager = new Mapzania.LayerSheetManager();

        // Keys are layer names, values are arrays of drawing instructions for the labels
        this.labelsAll = {};  // All labels
        this.labelsVisible = {}; // Ones chosen to be visible

        var me = this;
        model.schemaLoaded.add(function (e) {
            me.removeAllLayers();

            me.drawer.setBackColor(model.backColor);

            if (model.backImageUrl)
                me.drawer.setBackImageUrl(model.backImageUrl);

            for (var i = 0; i < model.layers.length; i++) {
                var layer = model.layers[i];
                me.addLayer(layer.key, i, layer.greyScale, layer.src);
            }

            me.drawer.addLayer("ts_interactive");
            me.drawer.addLayer("ts_interactive_2");
            me.drawer.addLayer("ts_selected_feature");

            if (me.model.rotation) {
                me.viewport.rotation = me.model.rotation;
            }
        });

        model.loading.add(function (e) {
            me.viewport.setIsLoading(true);
        });

        model.loadingProgress.add(function (e) {
            me.viewport.setLoadingProgress(e.data.total, e.data.outstanding);
        });

        model.loaded.add(function (e) {

            Mapzania.Log.debug("Model loaded. Looping through layers to draw.", "Model", e.data);

            for (var j = 0; j < e.data.length; j++) {
                var itm = e.data[j];
                me.drawLayer(itm.layer, itm.layer.src, itm.data);
            }

            me.viewport.reset();
            me.viewport.setIsLoading(false);
            me.rendered.fire(me);
        });

        model.layerShown.add(function (e) {
            me.showLayer(e.data);
        });

        model.layerHidden.add(function (e) {
            me.hideLayer(e.data);
        });

        model.layerCleared.add(function (e) {
            me.clearLayer(e.data);
        });

        model.layerAdded.add(function (e) {
            me.addLayer(e.data.layer.key, e.data.indexSchema, e.data.layer.greyScale, e.data.layer.src);
        });

        model.layerRemoved.add(function (e) {
            me.removeLayer(e.data.layer.key);
        });

        model.zoomed.add(function (e) {
            var extent = e.data;
            var ext;

            if (extent == null)
                ext = $.extend({}, model.startExtent);
            else
                var ext = $.extend({}, extent);

            ext.normalise(me.getSize());

            me.viewport.zoomTo(me._trf().to(-1, ext), ext);
        });

        drawer.viewChanged.add(function () {
            //console.log("view changed");
        });

        viewport.resized.add(function () {
            model.setViewportSize(viewport.getSize());
            model.refresh();
        });
    }

    Renderer.prototype.removeAllLayers = function () {
        this.layerSheetManager.removeAllLayers();
        this.drawer.removeAllLayers();
    }

    Renderer.prototype.setBackColor = function (color) {
        this.drawer.setBackColor(color);
    }

    // -------AS 2016-08-27----------
    Renderer.prototype.updateLayer = function (layerKey) {

        var layer = model.getLayer(layerKey);
        var data = model.store.get(layerKey);

        // AS: HACK to ensure src is complete
        if (!layer.src.gotFeatures)
            layer.src.gotFeatures = new Mapzania.Event();

        Mapzania.Log.debug("Renderer updateLayer called [" + layerKey + "]", "Renderer", { layer: layer, data: data });
        this.drawLayer(layer, layer.src, { features: data });

        this.flush();
    };
    //-------------------------------

    // The entry point for when a layer is drawn
    Renderer.prototype.drawLayer = function (layer, src, data) {

        Mapzania.Log.debug("drawLayer called. [" + layer.key + "]", "Renderer", { layer: layer, src: src, data: data });

        if (!this.model.hasLayer(layer.key))
            return;

        if (src.gotFeatures) {

            var engine = new Mapzania.StyleEngine(this.model, layer.key);

            var processed = engine.process(data.features);

            // Filter out labels
            var labelFilter = new Mapzania.StandardLabelFilterStrategy();
            labelFilter.filterLabels(layer, processed.symbolizedGeometry);

            this._drawSymbolisedGeometry(layer, processed.scaleType,
                processed.symbolizedGeometry);

            this.drawer.flush();

            if (this.model.removeConflictingLabels) {
                this._startLabelTimer();
            }
        }

        if (src.gotTiles) {
            this.ensureSheetExist(layer.key, 0);
            this._sortTilesFromCenter(data);
            for (var i = 0; i < data.length; i++) {
                this.drawTile(layer.key, data[i], layer.opacity);
            }
            this.drawer.flush();
        }
    }

    Renderer.prototype._sortTilesFromCenter = function (data) {
        var pCenter = this.model.extent.getCenter();
        data.forEach(function (cur) {
            var e = new Mapzania.Extent(cur);
            cur.distance = pCenter.distanceTo(e.getCenter());
        });

        data.sort(function (a, b) {
            if (a.distance > b.distance) return 1;
            if (a.distance < b.distance) return -1;
            return 0;
        });
    }

    Renderer.prototype._drawSymbolisedGeometry = function (layer, scaleType, sgs) {

        var engine = new ns.DrawingInstructionEngine(this._trf(), scaleType);

        for (var i = 0; i < sgs.length; i++) {
            var sg = sgs[i];

            if (sg.label)
                engine.addLabel(sg.label);

            if (sg.geometry)
                engine.addGeometry(sg.geometry, sg.symbol)
        }

        var instructions = engine.getInstructions();

        var dr = this.drawer;

        var checkedFeatureSheet = false;
        var checkedLabelSheet = false;

        if (!this.labelsAll[layer.key])
            this.labelsAll[layer.key] = [];
        
        for (var i = 0; i < instructions.length; i++) {
            if (!checkedFeatureSheet) {
                this.ensureSheetExist(layer.key, 0);
                checkedFeatureSheet = true;
            }

            var ist = instructions[i];

            switch (ist.type) {
                case "draw-circle":
                    dr.drawCircle(layer.key, ist.center, ist.radius, ist.color, ist.size, ist.opacity);
                    break;
                case "fill-circle":
                    dr.fillCircle(layer.key, ist.center, ist.radius, ist.color, ist.opacity);
                    break;
                case "draw-line":
                    dr.drawLine(layer.key, ist.line, ist.color, ist.size, ist.opacity, ist.endCap, ist.join, ist.dashArray);
                    break;
                case "fill-polygon":
                    dr.fillPolygon(layer.key, ist.lines, ist.color, ist.color2, ist.opacity, ist.fillType);
                    break;
                case "draw-text":
                    var labelLines = this._measureLabelLines(ist.text, ist.font, ist.size);
                    ist.labelLines = labelLines;

                    if (this.model.removeConflictingLabels) {
                        // We don't draw the labels yet. We siply calculate the label bounds - they are filtered and drawn later
                        var P = ist.location.clone();
                        var Pcenter = P.clone();

                        P.translate(ist.offset.x, -ist.offset.y);
                        if (ist.align == 0) P.translate(labelLines.width / 2, 0);
                        if (ist.align == 1) P.translate(0, 0);
                        if (ist.align == 2) P.translate(-labelLines.width / 2, 0);

                        var W = labelLines.width / 2;
                        var H = labelLines.height / 2;
                        var line = new Mapzania.Line();
                        line.points = [
                            new Mapzania.Point(P.x - W, P.y - H),
                            new Mapzania.Point(P.x - W, P.y + H),
                            new Mapzania.Point(P.x + W, P.y + H),
                            new Mapzania.Point(P.x + W, P.y - H),
                            new Mapzania.Point(P.x - W, P.y - H)
                        ];
                        line = line.rotate(Pcenter, -ist.rotation);
                        var labelPolygon = new Mapzania.Polygon();
                        labelPolygon.shell = line;
                        ist.labelPolygon = labelPolygon;

                        this.labelsAll[layer.key].push(ist);
                    } else {
                        if (!checkedLabelSheet) {
                            this.ensureSheetExist(layer.key, 1);
                            checkedLabelSheet = true;
                        }
                        
                        this._drawOneLabelInstruction(layer.key+"_label", ist);
                    }
                    break;
                case "fill-bitmap":
                    dr.fillBitmap(layer.key, ist.extent, ist.src, ist.opacity);
                    break;
                case "image":
                    dr.drawImage(layer.key, ist.extent, ist.src, ist.opacity);
                    break;
                default:
                    throw "Invalid instruction type=" + ist.type;
            };
        }

        //
        // Sort labels according to weight. This is done once only, after the layer is loaded and drawn.
        //
        if (this.labelsAll[layer.key]) {
            this.labelsAll[layer.key].sort(function (x, y) {
                return (-1 * (x.labelWeight - y.labelWeight)); // We want the bigger polygons to have a higher priority
            });
        }
    }

    Renderer.prototype._startLabelTimer = function () {
        if (this._labelTimerRef) {
            clearTimeout(this._labelTimerRef);
            this._labelTimerRef = null;
        }

        var me = this;
        this._labelTimerRef = setTimeout(function () {
            me._filterAndDrawLabelInstructionsAllLayers();
            me._labelTimerRef = null;
        }, 100);
    }

    Renderer.prototype._filterAndDrawLabelInstructionsAllLayers = function () {
        var startTime = (new Date()).getTime();
        this.labelsVisible = {};
        for (var i = 0; i < this.model.layers.length; i++) {
            var layer = this.model.layers[i];
            if (layer.visible) {
                this._filterLabelInstructions(layer);
            }
        }
        var endTime = (new Date()).getTime();
        Mapzania.Log.debug("Eliminating duplicate labels for all layers (time in ms): " + (endTime - startTime), "Mapzania.Renderer");

        this._drawLabelInstructions();
    }

    Renderer.prototype._filterLabelInstructions = function (layer) {
        var me = this;

        //
        // Eliminate the duplicates
        //
        this.labelsVisible[layer.key] = [];
        if (this.labelsAll[layer.key]) {
            this.labelsAll[layer.key].forEach(function (cur) {
                var hasConflict = false;

                for (var cur2 in me.labelsVisible) {
                    var visibleList = me.labelsVisible[cur2];
                    for (var i = 0; i < visibleList.length && !hasConflict; i++) {
                        if (visibleList[i].labelPolygon.intersects(cur.labelPolygon)) {
                            hasConflict = true;
                        }
                    }
                    if (hasConflict) break;
                }

                if (!hasConflict) {
                    visibleList.push(cur);
                }
            });
        }
    }

    Renderer.prototype._drawLabelInstructions = function () {
        //
        // Draw the labels
        //
        var me = this;
        for (var curLayerKey in this.labelsVisible) {
            if (this.layerSheetManager.hasSheet(curLayerKey, 1)) {
                this.drawer.clearLayer(curLayerKey + "_label");
            }
            if (this.labelsVisible[curLayerKey].length > 0) {
                me.ensureSheetExist(curLayerKey, 1);
            }
            this.labelsVisible[curLayerKey].forEach(function (ist) {
                var line = ist.labelPolygon.shell;
                me._drawOneLabelInstruction(curLayerKey + "_label", ist);
                //me.drawer.fillPolygon(curLayerKey + "_label", [line], "#000000", null, 30, 1);
            });
            this.drawer.flush();
        }
    }

    Renderer.prototype.drawTile = function (layerKey, tile, opacity) {
        var trf = this._trf();

        if (this.viewport.rotation.rotateZ == 0) {
            var ext = trf.to(-1, tile.extent);
            this.drawer.fillBitmap(layerKey, ext, tile.url, opacity);
        }
        else {
            if (this.drawer.fillBitmapRotated) {
                var pTopLeft = new Mapzania.Point(tile.extent.minX, tile.extent.maxY);
                var pTopRight = new Mapzania.Point(tile.extent.maxX, tile.extent.maxY);
                var pBottomLeft = new Mapzania.Point(tile.extent.minX, tile.extent.minY);

                var pTopLeftRotated = trf.to(1, pTopLeft);
                var pTopRightRotated = trf.to(1, pTopRight);
                var pBottomLeftRotated = trf.to(1, pBottomLeft);

                var width = pTopLeftRotated.distanceTo(pTopRightRotated);
                var height = pTopLeftRotated.distanceTo(pBottomLeftRotated);

                this.drawer.fillBitmapRotated(layerKey, pTopLeftRotated, width, height, tile.url, opacity, this.viewport.rotation.rotateZ);
            } else {
                Mapzania.Log.error("Drawer/Mapper does not support rotated tiles.", "Mapzania.Renderer");
            }
        }
    }

    // indexSchema = 0 for closest to the user
    Renderer.prototype.addLayer = function (layerKey, indexSchema, greyScale, source) {
        if (this.layerSheetManager.hasLayerKey(layerKey)) {
            throw "Layer key already exist: " + layerKey;
        }

        if (typeof indexSchema == 'undefined') {
            indexSchema = this.layerSheetManager.getLayerCount();
        }

        this.layerSheetManager.addLayer(layerKey, indexSchema, greyScale, source);
    }

    //sheetType: 0 = Data, 1 = Label
    Renderer.prototype.ensureSheetExist = function (layerKey, sheetType) {
        if (!this.layerSheetManager.hasSheet(layerKey, sheetType)) {
            var indexUI = this.layerSheetManager.addSheet(layerKey, sheetType);

            var sheetName = layerKey;
            if (sheetType == 1) {
                sheetName = sheetName + "_label";
            }

            this.drawer.addLayer(sheetName, indexUI, this.layerSheetManager.isGreyScale(layerKey), this.layerSheetManager.layerSource(layerKey));
        }
    }

    Renderer.prototype.removeLayer = function (layerKey) {
        var me = this;
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.removeLayer(cur);
        });
        this.layerSheetManager.removeLayer(layerKey);
    }

    Renderer.prototype.showLayer = function (layerKey) {
        var me = this;
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.showLayer(cur);
        });
    }

    Renderer.prototype.hideLayer = function (layerKey) {
        var me = this;
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.hideLayer(cur);
        });

        if (this.model.removeConflictingLabels) {
            this._startLabelTimer();
        }
    }

    Renderer.prototype.clearLayer = function (layerKey) {
        var me = this;
        this.labelsAll[layerKey] = [];
        this.labelsVisible[layerKey] = [];
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.clearLayer(cur);
        });
    }

    Renderer.prototype.getSize = function (extent) {
        return this.viewport.getSize();
    }

    Renderer.prototype.flush = function () {
        this.drawer.flush();
    }

    Renderer.prototype._trf = function () {
        return this.viewport.getTransformer(this.model.extent);
    }

    Renderer.prototype._measureLabelLines = function (text, font, size) {
        var me = this;
        var result = { lines: [], width: 0, height:0 };
        if (text) {
            text = new String(text);
            var lines = text.split("\n");
            lines.forEach(function (curLine) {
                curLine = curLine.replace("\r", "");
                curLine = curLine.trim();
                if (curLine.length > 0) {
                    var dimensions = me.drawer.measureText(curLine, font, size);
                    result.lines.push({
                        text: curLine,
                        width: dimensions.width,
                        height: dimensions.height
                    });
                    result.height += dimensions.height;
                    result.width = Math.max(result.width, dimensions.width);
                }
            });
        }
        return result;
    }

    Renderer.prototype._drawOneLabelInstruction = function (sheetKey, ist) {
        var curOffset = {};
        $.extend(curOffset, ist.offset);

        var Y = 0;
        var lines = ist.labelLines.lines;
        // TODO : We need to find a formula for this
        if (lines.length == 2) Y = 0.5 * (lines[0].height);
        if (lines.length == 3) Y = 0.5 * (lines[0].height) + 0.5 * (lines[1].height);
        if (lines.length == 4) Y = 0.5 * (lines[0].height) + (lines[1].height);
        if (lines.length == 5) Y = 0.5 * (lines[0].height) + (lines[1].height) + 0.5 * (lines[2].height);
        curOffset.y += Y;

        for (var i = 0; i < ist.labelLines.lines.length; i++) {
            var cur = ist.labelLines.lines[i];

            this.drawer.drawText(sheetKey, ist.location, cur.text, ist.font, ist.size, ist.color,
                ist.backColor, ist.opacity, ist.align, ist.haloSize, ist.haloColor, ist.bold,
                ist.italic, ist.underline, ist.addMark, ist.rotation, curOffset);

            curOffset.y -= (cur.height);
        }
    }

    ns.Renderer = Renderer;

    return ns;

})(Mapzania || {});

//
//
//

var Mapzania = (function (ns) {
    function StandardLabelFormattingStrategy(visibleExtent) {
        this.visibleExtent = visibleExtent;
    }

    StandardLabelFormattingStrategy.prototype.getLabelInstance = function (layer, themer, feature) {
        if (this.shouldDiscardLabel(layer, themer, feature)) {
            return null;
        }

        var centerPnt = null;
        var rotation = 0;
        var symbol = themer.getLabelSymbol(feature);
        if (symbol.rotation != 0) rotation = symbol.rotation;
        var text = themer.getLabelText(feature);
        var rotationFollowGeometry = false;
        var weight = 0.0;

        if (this.isLineGeometry(feature.geometry)) {
            var x = this.calculateLineLabelDetails(symbol, feature.geometry);
            if (!x) {
                return null;
            } else {
                rotation = x.rotation;
                centerPnt = x.centerPnt;
                rotationFollowGeometry = x.rotationFollowGeometry;
                weight = x.weight;
            }
        } else {
            if (this.isPolygonGeometry(feature.geometry)) {
                weight = feature.geometry.getArea();
            }

            centerPnt = feature.geometry.getCentroid();
        }

        return {
            center: centerPnt,
            text: text,
            symbol: symbol,
            rotation: rotation,
            rotationFollowGeometry: rotationFollowGeometry,
            weight: weight
        };
    }

    StandardLabelFormattingStrategy.prototype.shouldDiscardLabel = function (layer, themer, feature) {
        if (feature.dynamicProperties && !feature.dynamicProperties.labelIsVisible) return true;
        if (themer == null) return true;
        if (themer.getLabelSymbol == null) return true;

        var text = themer.getLabelText(feature);
        if (!text) return true;

        return false;
    }

    StandardLabelFormattingStrategy.prototype.calculateLineLabelDetails = function (symbol, geometry) {
        var lines = this.flattenLine(geometry);
        var linesVisible = this.calculateVisibleLinesInExtent(lines, this.visibleExtent);

        var maxLengthLine = null;
        var maxLength = 0;
        linesVisible.forEach(function (cur) {
            var l = cur.getLength();
            if (l > maxLength) {
                maxLength = l;
                maxLengthLine = cur;
            }
        });

        if (maxLengthLine) {
            var r = maxLengthLine.getPointAndSegmentAtLength(maxLength / 2.0);
            if (r) {
                var centerPnt = r.point;
                var rotation = 0;
                var rotationFollowGeometry = false;
                if (symbol.rotation != 0) rotation = symbol.rotation;
                if (symbol.autoRotate) {
                    rotation = r.segment.getAngleRadians();
                    // Ensure the labels does not show upside down
                    if (rotation > Math.PI / 2) rotation -= (Math.PI);
                    if (rotation < -Math.PI / 2) rotation += (Math.PI);
                    rotationFollowGeometry = true;
                }
                return { centerPnt: centerPnt, rotation: rotation, rotationFollowGeometry: rotationFollowGeometry, weight: maxLength };
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    StandardLabelFormattingStrategy.prototype.calculateVisibleLinesInExtent = function (lines, extent) {
        var linesResult = [];
        lines.forEach(function (curLine) {
            var segments = curLine.getLineSegments(null);
            var r = [];
            segments.forEach(function (curSegment) {
                var t = curSegment.getVisibleIntersectionInExtent(extent);
                if (t) {
                    if (t == curSegment) {
                        r.push(t);
                    } else {
                        if (r.length > 0) {
                            r.push(t);
                            var lineResult = new Mapzania.Line();
                            lineResult.fromLineSegments(r);
                            linesResult.push(lineResult);
                            r = [];
                        } else {
                            r.push(t);
                        }
                    }
                }
            });
            if (r.length > 0) {
                var lineResult = new Mapzania.Line();
                lineResult.fromLineSegments(r);
                linesResult.push(lineResult);
            }
        });

        return linesResult;
    }

    StandardLabelFormattingStrategy.prototype.flattenLine = function (geom) {
        var result = [];
        if (geom instanceof Mapzania.Line) {
            result.push(geom);
        }
        if (geom instanceof Mapzania.MultiLine) {
            geom.lines.forEach(function (cur) {
                result.push(cur);
            });
        }
        return result;
    }

    StandardLabelFormattingStrategy.prototype.isLineGeometry = function (geom) {
        return (geom instanceof Mapzania.Line || geom instanceof Mapzania.MultiLine);
    }

    StandardLabelFormattingStrategy.prototype.isPolygonGeometry = function (geom) {
        return (geom instanceof Mapzania.Polygon || geom instanceof Mapzania.MultiPolygon);
    }

    ns.StandardLabelFormattingStrategy = StandardLabelFormattingStrategy;

    return ns;

})(Mapzania || {});

//
// StandardLabelFilterStrategy
//

var Mapzania = (function (ns) {
    function StandardLabelFilterStrategy() {
    }

    StandardLabelFilterStrategy.prototype.filterLabels = function (layer, symbolisedGeometry) {
        if (!layer.allowDuplicateLabels) {
            this.filterDuplicateLabels(layer, symbolisedGeometry);
        }
    }

    StandardLabelFilterStrategy.prototype.filterDuplicateLabels = function (layer, symbolisedGeometry) {
        var withLabels = [];
        var withoutLabels = [];
        symbolisedGeometry.forEach(function (cur) {
            if (cur.label) {
                withLabels.push(cur);
            } else {
                withoutLabels.push(cur);
            }
        });

        // Sort in descending order
        withLabels.sort(function (x, y) {
            if (x.weight < y.weight) return +1;
            if (x.weight > y.weight) return -1;
            return 0;
        });

        var labelsToRemove = {};
        withLabels.forEach(function (cur) {
            if (labelsToRemove.hasOwnProperty(cur.label.text)) {
                delete cur.label;
            } else {
                labelsToRemove[cur.label.text] = true;
            }
        });

        symbolisedGeometry.splice(0, symbolisedGeometry.length);
        withLabels.forEach(function (cur) {
            symbolisedGeometry.push(cur);
        });
        withoutLabels.forEach(function (cur) {
            symbolisedGeometry.push(cur);
        });
    }

    ns.StandardLabelFilterStrategy = StandardLabelFilterStrategy;

    return ns;

})(Mapzania || {});
