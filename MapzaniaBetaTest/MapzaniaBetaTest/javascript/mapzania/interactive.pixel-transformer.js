﻿/// <reference path="../lib/swfobject-1.5.js" />

var Mapzania = (function (ns) {

	function PixelTransformer(params) {

		this.params = {
			originX: 0, originY: 0, scaleX: 1, scaleY: 1, translateX: 0, translateY: 0, rotateZ: 0, skewX: 0, skewY: 0
		};

		this.params = Object.assign(this.params, params);

		this.params._cos = Math.cos(this.params.rotateZ);
		this.params._sin = Math.sin(this.params.rotateZ);
	}

	PixelTransformer.prototype.to = function (geomType, geom) {
		var me = this;

		var conv = function (point) {
		    var pointOrigin = new Mapzania.Point(me.params.originX, me.params.originY);

            // Translate the point to the rotated world coordinate system
			var res = point.rotate(pointOrigin, me.params.rotateZ);
			
            // Then translate it for the drawer
			res.x = (res.x - me.params.translateX) * me.params.scaleX;
			res.y = (res.y - me.params.translateY) * me.params.scaleY;

			return res;
		}

		switch (geomType) {
			case -1: return this.transformExtent(geom, conv);
			case 1: return conv(geom);//point
			case 2: return this.transformLine(geom, conv);
			case 3: return this.transformPolygon(geom, conv);
			case 4: return this.transformMultiPoint(geom, conv);
			case 5: return this.transformMultiLine(geom, conv);
			case 6: return this.transformMultiPolygon(geom, conv);
			default:
				throw "unknown geometry type";
		}
	}

	PixelTransformer.prototype.labelRotationTo = function (rotation) {
	    return rotation + this.params.rotateZ;
	}

	PixelTransformer.prototype.from = function (geomType, geom) {
		var me = this;

		var conv = function (point) {
		    var pointOrigin = new Mapzania.Point(me.params.originX, me.params.originY);

		    var res = new ns.Point(point.x, point.y);

		    // Translater from the drawer to world coordinates
		    res.x = res.x / me.params.scaleX + me.params.translateX;
		    res.y = res.y / me.params.scaleY + me.params.translateY;

		    // Rotate the point back to the Model's coordinate system(clockwise)
		    res = res.rotate(pointOrigin, -me.params.rotateZ);

		    return res;
		}

		switch (geomType) {
			case -1: return this.transformExtent(geom, conv);
			case 1: return conv(geom);//point
			case 2: return this.transformLine(geom, conv);
			case 3: return this.transformPolygon(geom, conv);
			case 4: return this.transformMultiPoint(geom, conv);
			case 5: return this.transformMultiLine(geom, conv);
			case 6: return this.transformMultiPolygon(geom, conv);
			default:
				throw "unknown geometry type";
		}
	}

	PixelTransformer.prototype.toSize = function (size) {
	    var avgScale = (Math.abs((this.params.scaleX) + Math.abs(this.params.scaleY)) / 2.0);
	    //console.log(avgScale);
	    return avgScale *size;
	    var origin = new Mapzania.Point(this.params.translateX, this.params.translateY);

	    var pnt1 = this.to(1, origin);
	    var pnt2 = this.to(1, new Mapzania.Point(origin.x + size, origin.y));

		var width = Math.abs(pnt1.x - pnt2.x);

		return width;
	}

	PixelTransformer.prototype.fromSize = function (size) {
	    var avgScale = (Math.abs((this.params.scaleX) + Math.abs(this.params.scaleY)) / 2.0);
	    return size / avgScale;
	    var origin = new Mapzania.Point(0, 0);

	    var pnt1 = this.from(1, origin);
	    var pnt2 = this.from(1, new Mapzania.Point(origin.x + size, origin.y));

	    var width = Math.abs(pnt1.x - pnt2.x);

	    return width;
	}

	PixelTransformer.prototype.transformExtent = function (extent, pointFunc) {

		var min = pointFunc(new ns.Point(extent.minX, extent.minY));
		var max = pointFunc(new ns.Point(extent.maxX, extent.maxY));

		var minX = Math.min(min.x, max.x);
		var minY = Math.min(min.y, max.y);
		var maxX = Math.max(min.x, max.x);
		var maxY = Math.max(min.y, max.y);

		var res = new ns.Extent({ minX: minX, minY: minY, maxX: maxX, maxY: maxY });

		return res;
	}

	PixelTransformer.prototype.transformMultiPoint = function (mpoint, pointFunc) {
		var res = new ns.MultiPoint();
		for (var i = 0; i < mpoint.points.length; i++)
			res.points.push(pointFunc(mpoint.points[i]));

		return res;
	}

	PixelTransformer.prototype.transformLine = function (line, pointFunc) {
		var res = new ns.Line();
		for (var i = 0; i < line.points.length; i++)
			res.points.push(pointFunc(line.points[i]));

		return res;
	}

	PixelTransformer.prototype.transformMultiLine = function (mline, pointFunc) {
		var res = new ns.MultiLine();

		for (var i = 0; i < mline.lines.length; i++)
			res.lines.push(this.transformLine(mline.lines[i], pointFunc));

		return res;
	}

	PixelTransformer.prototype.transformPolygon = function (polygon, pointFunc) {
		var res = new ns.Polygon();
		res.shell = this.transformLine(polygon.shell, pointFunc);

		for (var i = 0; i < polygon.holes.length; i++)
			res.holes.push(this.transformLine(polygon.holes[i], pointFunc));

		return res;
	}

	PixelTransformer.prototype.transformMultiPolygon = function (mpoly, pointFunc) {
		var res = new ns.MultiPolygon();
		for (var i = 0; i < mpoly.polygons.length; i++)
			res.polygons.push(this.transformPolygon(mpoly.polygons[i], pointFunc));

		return res;
	}

	ns.PixelTransformer = PixelTransformer;

	return ns;

})(Mapzania || {})
