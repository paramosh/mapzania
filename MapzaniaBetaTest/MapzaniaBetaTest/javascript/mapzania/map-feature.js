﻿/// <reference path="../mapzania.js"/>

var Mapzania = (function (ns) {

    function MapFeature(id, layerKey, map) {

        // --------------------------- LAYERS --------------------------------------

        this.update = function () {
            map.updateLayer(layerKey);
            return this;
        }

        //---------------------FEATURE MANIPULATION--------------------------------
        this.show = function () {
            map.showFeature(layerKey, id);
            return this;
        };

        this.hide = function () {
            map.hideFeature(layerKey, id);
            return this;
        };

        // ------------------------ STYLING --------------------------------------

        this.style = function (style) {
            map.setFeatureStyle(layerKey, id, style);
            return this;
        };
    }

    ns.MapFeature = MapFeature;

    return ns;

})(Mapzania || {});