﻿//require(["proj4"]);

Mapzania = (function (Mapzania) {

    proj4.defs("LATLONG", proj4.defs("WGS84"));
    proj4.defs("GoogleCoordinateSystem", proj4.defs("EPSG:900913"));
    proj4.defs("LO25", "PROJCS[\"LO25WGS84\",GEOGCS[\"GCS_Hartebeesthoek_1994\",DATUM[\"D_Hartebeesthoek_1994\",SPHEROID[\"WGS_1984\",6378137.0,298.257223563]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",25.0],PARAMETER[\"Scale_Factor\",1.0],PARAMETER[\"Latitude_Of_Origin\",0.0],UNIT[\"Meter\",1.0]]");

    var projectExtent = function (extent, fromSrid, toSrid ) {

        var min = { x: extent.minX, y: extent.minY };
        var max = { x: extent.maxX, y: extent.maxY };

        proj4(fromSrid, toSrid, min);
        proj4(fromSrid, toSrid, max);

        extent.minX = min.x;
        extent.minY = min.y;
        extent.maxX = max.x;
        extent.maxY = max.y;
    };

    var projectFeature = function (feature, fromSrid, toSrid) {
        projectGeometry(feature.geometry, fromSrid, toSrid)
    }

    var projectFeatureArray = function (features, fromSrid, toSrid) {
        
        for (var i = 0; i < features.length; i++) 
            projectFeature(features[i], fromSrid, toSrid);
    }

    var projectGeometry = function (geometry, fromSrid, toSrid) {
        
        switch (geometry.getType()) {
            case 1:
                projectPoint(geometry, fromSrid, toSrid);
                break;
            case 2:
                projectLine(geometry, fromSrid, toSrid);
                break;
            case 3:
                projectPolygon(geometry, fromSrid, toSrid);
                break;
            case 4:
                projectMultiPoint(geometry, fromSrid, toSrid);
                break;
            case 5:
                projectMultiLine(geometry, fromSrid, toSrid);
                break;
            case 6:
                projectMultiPolygon(geometry, fromSrid, toSrid);
                break;
            default:
                throw "Cannot project unknown geometry type."
        
        }
    }

    var projectPoint = function (geom, fromSrid, toSrid) {
        //NOTE: This function changed from cloning and then projecting to projecting passed object
        proj4(fromSrid, toSrid, geom);
    };

    var projectLine = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.points.length; i++)
            projectPoint(geom.points[i], fromSrid, toSrid);
    }

    var projectPolygon = function (geom, fromSrid, toSrid) {

        projectLine(geom.shell, fromSrid, toSrid);

        for (var i = 0; i < geom.holes.length; i++)
            projectLine(geom.holes[i], fromSrid, toSrid);
    }

    var projectMultiPoint = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.points.length; i++)
            projectPoint(geom.points[i], fromSrid, toSrid);
    }

    var projectMultiLine = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.lines.length; i++)
            projectLine(geom.lines[i], fromSrid, toSrid);
    }

    var projectMultiPolygon = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.polygons.length; i++)
            projectPolygon(geom.polygons[i], fromSrid, toSrid);
    }


    Mapzania.Projection = {
        projectExtent: projectExtent,
        projectFeature: projectFeature,
        projectFeatureArray: projectFeatureArray,
        projectGeometry:projectGeometry,
        projectPoint: projectPoint,
        projectLine: projectLine,
        projectPolygon: projectPolygon,
        projectMultiPoint: projectMultiPoint,
        projectMultiLine: projectMultiLine,
        projectMultiPolygon: projectMultiPolygon,
    };

    return Mapzania;

})(Mapzania || {});