﻿var Mapzania = (function (ns) {
    function Logger() {

        this.debugEnabled = false;

        this.message = function (msg, module, obj) {
            if (obj)
                console.log("MSG:" + (module ? module + ":" : "") + msg, obj)
            else
                console.log("MSG:" + (module ? module + ":" : "") + msg);
        }

        this.error = function (msg, module, obj) {
            if (obj)
                console.log("ERR:" + (module ? module + ":" : "") + msg, obj)
            else
                console.log("ERR:" + (module ? module + ":" : "") + msg);
        }

        this.debug = function (msg, module, obj) {
            if (!this.debugEnabled)
                return;

            if (obj)
                console.log("DBG:" + (module ? module + ":" : "") + msg, obj)
            else
                console.log("DBG:" + (module ? module + ":" : "") + msg);
        }

        this.custom = function (name, msg, module, obj) {
            if (obj)
                console.log(name.toUpperCase() + ":" + (module ? module + ":" : "") + msg, obj)
            else
                console.log(name.toUpperCase() + ":" + (module ? module + ":" : "") + msg);
        }

    }

    ns.Log = new Logger();

    return ns;

})(Mapzania || {});

var Mapzania = (function (ns) {
    function Timer() {
        this.count = 0;
        this.time_ms = 0;
    }

    Timer.prototype.start = function () {
        this.count++;
        this.start_time = new Date();
    }

    Timer.prototype.stop = function () {
        this.time_ms += ((new Date()) - this.start_time);
    }

    Timer.prototype.describe = function () {
        if (this.count == 0)
            return "0"
        else
            if (this.count == 1)
                return "Time (ms) = " + (this.time_ms)
            else
                return "[Count = " + this.count + ", Avg Time (ms) = " + (this.time_ms / this.count) + "]";
    }

    ns.Timer = Timer;

    return ns;

})(Mapzania || {});

