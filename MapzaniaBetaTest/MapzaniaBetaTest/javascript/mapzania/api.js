﻿/// <reference path="mapzania.js"/>

var Mapzania = (function (ns) {
    var me;

    function Api(url, isCompact) {
        this.isCompact = isCompact;
        this.url = url;
        this.resourceLimits = null;
        me = this;

        this.getCoordinateSystems = function (callback) {
            get("srids", null, callback);
        }

        this.getLayerSources = function (callback) {
            get("sources", null, callback);
        }

        this.getMaps = function (callback) {
            get("maps", null, callback);
        }

        this.getMap = function (mapKey, srid, callback) {
            var params = [];

            if (typeof srid !== "undefined")
                params.push(["srid", srid]);

            get("maps/" + mapKey, params, callback);
        }

        this.getFeatures = function (sourceKey, filters, options, callback) {
            var defaults = {
                isCompact: this.isCompact
            };

            settings = Object.assign(defaults, options);

            var path = "sources/" + sourceKey + "/features";

            var body = {
                filters: filters,
                options: settings
            };

            var requestContext = { type: "FeaturesRequest", requests: [], request: sourceKey };

            requestContext.requests.push(
             post(path, body, function (data) {

                 var res = [];

                 if (isCompact) {
                     for (var i = 0; i < data.geometries.length; i++) {

                         var ftr = new ns.Feature();
                         ftr.read(null, data, i);
                         res.push(ftr);
                     }
                 }
                 else
                     res = Mapzania.GeoJson.fromFeatureCollection(data);

                 Mapzania.Projection.projectFeatureArray(res, "LATLONG", "GOOGLE");

                 callback(res);
             }));

            return requestContext;
        }

        this.getFeaturesDirect = function (sourceKey, filters, options, callback) {
            var defaults = {
                isCompact: this.isCompact
            };

            settings = Object.assign(defaults, options);

            var path = "sources/" + sourceKey + "/features";

            var body = {
                filters: filters,
                options: settings
            };

            post(path, body, function (data) {

                if (!settings.isCompact)
                    callback(data);

                var res = [];

                for (var i = 0; i < data.geometries.length; i++) {
                    var ftr = new ns.Feature();
                    ftr.read(null, data, i);
                    res.push(ftr);
                }

                callback(Mapzania.GeoJson.toFeatureCollection(res));
            })
        }

        this.setFeatures = function (sourceKey, featureCollection) {
            var path = createUrl("sources/" + sourceKey + "/features");

            //TODO: Handle unsuccessful PUT
            $.ajax({
                url: path,
                type: 'PUT',
                data: JSON.stringify(featureCollection),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    Mapzania.Log.debug("PUT successful.");
                }
            });
        }

        this.getFeatureTiles = function (sourceKey, filters, options, callback){

            var defaults = {
                isCompact: this.isCompact
            };

            var settings = Object.assign(defaults, options);

            var zoom = settings.zoom;
            var extent = settings.extent;

            var tiles = []

            if (extent) {
                var ext = extent.clone();
                Mapzania.Projection.projectExtent(ext, me.wkt, "LATLONG");
                var tileMin = _coordsToIndices(ext.minX, ext.minY, zoom);
                var tileMax = _coordsToIndices(ext.maxX, ext.maxY, zoom);
                // get all the tiles for the extent
                var minx = tileMin[0] < tileMax[0] ? tileMin[0] : tileMax[0];
                var maxx = tileMin[0] < tileMax[0] ? tileMax[0] : tileMin[0];

                var miny = tileMin[1] < tileMax[1] ? tileMin[1] : tileMax[1];
                var maxy = tileMin[1] < tileMax[1] ? tileMax[1] : tileMin[1];


                for (var x = minx; x <= maxx; x++)
                    for (var y = miny; y <= maxy; y++)
                        tiles.push({ z: zoom, x: x, y: y });

            }

            // number of oustanding tiles
            var tilesOutstanding = tiles.length;
            // features in view
            var features = [];
            // unique ids seen so far
            var ids = [];
            var tileCallback = function (data) {
                // to deduplicate the features received as part of this view
                var idsAdded = [];
                //  unique features for this tile
                var partFeatures = [];
                // features received in tile
                var responseFeatures = [];

                if (isCompact) {
                    for (var i = 0; i < data.geometries.length; i++) {
                        // reassemble features
                        var ftr = new ns.Feature();
                        ftr.read(null, data, i);
                        responseFeatures.push(ftr);
                    }
                }
                else {
                    responseFeatures = data.features;
                }

                for (var i = 0; i < responseFeatures.length; i++) {
                    if (ids.indexOf(responseFeatures[i].id) == -1) {
                        features.push(responseFeatures[i]);
                        partFeatures.push(responseFeatures[i]);
                        idsAdded.push(responseFeatures[i].id);
                    }
                }

                var res = [];
                if (isCompact)
                    res = partFeatures;
                else
                    res = Mapzania.GeoJson.fromFeatureCollection({ type: "FeatureCollection", features: partFeatures });

                Mapzania.Projection.projectFeatureArray(res, "LATLONG", "GOOGLE");

                // Render the features for this tile
                callback(res);
                // add unique ids added in this tile
                ids.push(idsAdded);

                tilesOutstanding--;
                // Render everything at once.
                //features.push(data.features);
                //if (tilesOutstanding == 0) {

                //    var res = Mapzania.GeoJson.fromFeatureCollection({ type: "FeatureCollection", features: features });
                //    if (settings.isProjected)
                //        Mapzania.Projection.projectFeatureArray(res, "LATLONG", "GOOGLE");
                //    callback(res);
                //}
            }
            var body = {
                filters: filters,
                options: settings
            };

            var requestContext = { type: "TiledRequest", requests: [], request: sourceKey };
            var maxRequestLength = me.resourceLimits && me.resourceLimits.maxQueryLength ? me.resourceLimits.maxQueryLength : 2048;
            //request all the tiles asynchronously
            tiles.forEach(function (tile) {

                var queryString = JSON.stringify(body);
                if (queryString.length < maxRequestLength)   // Default IIS limit
                {
                    requestContext.requests.push(
                        get("sources/" + sourceKey + "/features/tiles/" + tile.z + "/" + tile.x + "/" + tile.y + "?parameters=" + queryString, null, tileCallback)
                    );
                } else {
                    requestContext.requests.push(
                        post("sources/" + sourceKey + "/features/tiles/" + tile.z + "/" + tile.x + "/" + tile.y, body, tileCallback)
                    );
                }
            });

            return requestContext;
        };

        this.cancelRequests = function (requestContext) {
            if (requestContext && requestContext.requests) {
                requestContext.requests.forEach(function (req) {
                    if (req)
                        req.abort();
                });
                requestContext.requests = [];
            }
        }

        this.getTiles = function (sourceKey, extentToLoad, extentVisible, srid, filter, callback, _requestId) {

            var params = [
                ["srid", srid]
            ];

            if (extentToLoad) {
                params.push(["minx", extentToLoad.minX]);
                params.push(["miny", extentToLoad.minY]);
                params.push(["maxx", extentToLoad.maxX]);
                params.push(["maxy", extentToLoad.maxY]);
            }

            if (extentVisible) {
                params.push(["vminx", extentVisible.minX]);
                params.push(["vminy", extentVisible.minY]);
                params.push(["vmaxx", extentVisible.maxX]);
                params.push(["vmaxy", extentVisible.maxY]);
            }

            if (filter)
                params.push(["filter", filter]);

            var path = "sources/" + sourceKey + "/tiles";

            get(path, params, callback, _requestId);
        };

        this.getImageUrl = function (mapKey, layerKeys, filters, extent, size, srid, showBackground, format, options) {
            if (!layerKeys)
                layerKeys = [];

            if (!filters)
                filters = [];

            if (!options)
                options = {};

            var params = [
                ["layerKeys", layerKeys.join(",")],
                ["filters", filters.join("|")],
                ["srid", srid],
                ["showBackground", showBackground],
                ["format", format]
            ];

            if (extent) {
                params.push(["minx", extent.minX]);
                params.push(["miny", extent.minY]);
                params.push(["maxx", extent.maxX]);
                params.push(["maxy", extent.maxY]);
            }

            if (size) {
                params.push(["width", size.width]);
                params.push(["height", size.height]);
            }

            if (options.originX) params.push(["originX", options.originX]);
            if (options.originY) params.push(["originY", options.originY]);
            if (options.rotateZ) params.push(["rotateZ", options.rotateZ]);

            var path = createUrl("maps/" + mapKey + "/image", params);

            return path;
        }

        //
        // Private Methods
        //

        var _coordsToIndices = function (lon, lat, zoom) {
            var x = (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
            var y = (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));

            return [x, y];
        }

        var get = function (path, parameters, callback, requestId) {

            var timer = new Mapzania.Timer();
            timer.start();

            return $.get(createUrl(path, parameters), function (data, testStatus, jqXHR) {

                Mapzania.Log.debug("[" + path + "] returned. Processing...", "Api.get()", data);

                timer.stop();
                var logBag = {};
                logBag.URL = path;
                logBag.data = data;
                logBag["Time: Fetching feed"] = timer.describe();
                logBag.bytes = jqXHR.responseText.length;

                timer = new Mapzania.Timer();
                timer.start();

                if (callback)
                    callback(data, requestId);

                timer.stop();
                logBag["Time: Processing feed"] = timer.describe();
                Mapzania.Log.debug("GET RESULT:" + path, "Api", logBag);
            });
        };

        var post = function (path, parameters, callback, requestId) {

            var timer = new Mapzania.Timer();
            timer.start();

            return $.ajax({
                url: me.url + path,
                type: "POST",
                data: JSON.stringify(parameters),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, testStatus, jqXHR) {

                    Mapzania.Log.debug("[" + path + "] returned. Processing...", "Api.post()", data);

                    timer.stop();
                    var logBag = {};
                    logBag.URL = path;
                    logBag.data = data;
                    logBag["Time: Fetching feed"] = timer.describe();
                    logBag.bytes = jqXHR.responseText.length;

                    timer = new Mapzania.Timer();
                    timer.start();

                    if (callback)
                        callback(data, requestId);

                    timer.stop();
                    logBag["Time: Processing feed"] = timer.describe();
                    Mapzania.Log.debug("POST RESULT:" + path, "Api", logBag);
                }
            })
        }

        var createUrl = function (path, params) {

            var paramString = "";

            if (params) {
                var terms = [];

                for (var i = 0; i < params.length; i++) {
                    var param = params[i];

                    var hasValue = param[1] != null && String(param[1]).length > 0;
                    if (hasValue) {
                        terms.push(param[0] + "=" + param[1]);
                    }
                }

                if (terms.length == 0)
                    return "";

                paramString = "?" + terms.join("&");
            }

            var url = me.url + path + paramString;
            Mapzania.Log.debug("The following url was called: " + url, "Mapzania.Api");

            return url;
        };

        var getResourceLimits = function()
        {
            get("resources/limits", null, function (data) {
                me.resourceLimits = data;
            });
        }

        getResourceLimits();
    }

    ns.Api = Api;

    return ns;

})(Mapzania || {});

