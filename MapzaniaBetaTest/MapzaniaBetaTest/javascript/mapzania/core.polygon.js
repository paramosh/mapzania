﻿/**
 * @doc Mapzania.Polygon.*ClassDescription*
 *
 * Encapsulates a single Polygon geometry. A polygon geometry defines a set of non-intersecting lines in a closed loop. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.Polygon.Polygon
      *
      * Constructs a Mapzania Polygon Geometry.
      * 
      */
    function Polygon() {
        this.holes = [];
        this.shell = new ns.Line();
    }

    /**
      * @doc Mapzania.Polygon.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Polygon.prototype.read = function (data) {
        if (data[0]) {
            for (var i = 0; i < data[0].length; i++) {
                var line = new ns.Line().read([[data[0][i]]]);
                if (i == 0)
                    this.shell = line;
                else
                    this.holes.push(line);
            }
        }

        return this;
    }

    /**
      * @doc Mapzania.Polygon.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
    Polygon.prototype.getExtent = function () {
        var algo = new ns.Algo();
        var ext = algo.getExtentFromLines([this.shell]);

        return ext;
    }

    /**
      * @doc Mapzania.Polygon.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    Polygon.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Polygon.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
    Polygon.prototype.intersects = function (geometry) {
        if (geometry instanceof Mapzania.Polygon) {
            return (new Mapzania.Intersections()).intersectsPolyPoly(this, geometry);
        }

        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Polygon.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Polygon.prototype.getPath = function () {
        var lines = [];

        lines.push(this.shell.getPath()[0][0]);

        for (var i = 0; i < this.holes.length; i++)
            lines.push(this.holes[i].getPath()[0][0]);

        return [lines];
    }

    /**
      * @doc Mapzania.Polygon.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	Polygon.prototype.getSize = function () {
		var res = 0;

		res += this.shell.getSize();

		for (var i = 0; i < this.holes.length; i++) {
			res += this.holes[i].getSize();
		}

		return res;
	}

    /**
      * @doc Mapzania.Polygon.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	Polygon.prototype.getCentroid = function () {
	    var dx = Number.MAX_VALUE;
	    var dy = Number.MAX_VALUE;
	    this.shell.points.forEach(function (cur) {
	        if (cur.x < dx) dx = cur.x;
	        if (cur.y < dy) dy = cur.y;
	    });

	    var paths = [];
	    this.shell.points.forEach(function (cur) {
	        var p = new ns.Point(cur.x, cur.y);
	        p.translate(-dx, -dy);
	        paths.push(p);
	    });

	    var X = 0;
	    var Y = 0;
	    var AA = 0;
	    for (var i = 0; i < paths.length - 1; i++) {
	        X += (paths[i].x + paths[i + 1].x) * (paths[i].x * paths[i + 1].y - paths[i + 1].x * paths[i].y);

	        Y += (paths[i].y + paths[i + 1].y) * (paths[i].x * paths[i + 1].y - paths[i + 1].x * paths[i].y);

	        AA += (paths[i].x * paths[i + 1].y - paths[i + 1].x * paths[i].y);
	    }
	    var A = AA * 0.5;
	    var x = 1 / (6 * A) * X;
	    var y = 1 / (6 * A) * Y;

	    return new ns.Point(x + dx, y + dy);
	}

    /**
      * @doc Mapzania.Polygon.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Polygon.prototype.getGeometryType = function () {
	    return 3;
	}

    /**
      * @doc Mapzania.Polygon.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Polygon.prototype.getType = function () {
	    return 3;
	}

    /**
      * @doc Mapzania.Polygon.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Polygon) An exact deep clone of this instance.
      */

	Polygon.prototype.clone = function () {
	    var result = new Mapzania.Polygon();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.Polygon.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	Polygon.prototype.translate = function (x, y) {
	    this.holes.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	    this.shell.translate(x, y);
	}

    /**
      * @doc Mapzania.Polygon.getArea
      *
      * Calculates the area of this polygon.
      *
      * @return (Number) The area of this polygon (excluding the area taken up by holes).
      */

	Polygon.prototype.getArea = function () {
	    var summedArea = this._areaPoints(this.shell.points);
	    var summedHoleArea = 0;
	    var me = this;
	    this.holes.forEach(function (cur) {
	        summedHoleArea += me._areaPoints(cur.points);
	    });

	    return summedArea - summedHoleArea;
	}

	Polygon.prototype._areaPoints = function (points) {
	    var summedArea = 0;

	    var funcArea = function (p1, p2) {
	        var width = p2.x - p1.x;
	        var height = (p2.y + p1.y) / 2;
	        return width * height;
	    };

	    for (var i = 0; i <= (points.length - 2) ; i++) {
	        summedArea += funcArea(points[i], points[i + 1]);
	    }
	    summedArea += funcArea(points[points.length - 1], points[0]);

	    return Math.abs(summedArea);
	}

	ns.Polygon = Polygon;

	if (!ns.FeatureTypes)
	    ns.FeatureTypes = {};

	ns.FeatureTypes["3"] = Polygon;

	return ns;

})(Mapzania || {})
