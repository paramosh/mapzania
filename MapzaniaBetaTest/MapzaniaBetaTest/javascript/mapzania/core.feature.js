﻿/**
 * @doc Mapzania.Feature.*ClassDescription*
 *
 * Encapsulates a single Feature displayed on the interactive map. 
 * A feature is defined by an Id, a geometry, and a set of attributes name-value pairs. 
 */
var Mapzania = (function (ns) {
    /**
      * @doc Mapzania.Feature.Feature
      *
      * Constructs an empty Feature instance. This Id and geometry is initialized to null, and the attributes property is set to an empty object. The label visibilty 
      * flag for the feature is set to true.
      *
      */
    function Feature() {

        /**
          * @doc Mapzania.Feature.id (Object)
          *
          * A unique identifier for this feature, relative to the layer.
          */
        this.id = null;

        /**
          * @doc Mapzania.Feature.id (Mapzania.Geometry)
          *
          * The geometry associated with this feature instance.
          */
        this.geometry = null;

        /**
          * @doc Mapzania.Feature.attributes (Object)
          *
          * Attributes associated with the feature. Object keys are attributes names.
          */
        this.attributes = {};

        /**
          * @doc Mapzania.Feature.dynamicProperties (Object)
          *
          * Internal attributes associated with this feature instance. This object typically has the form 
          * { labelIsVisible: true}, where labelIsVisible indicates that the label for this feature instance is
          * visible. 
          */
        this.dynamicProperties = {};
        this.dynamicProperties.labelIsVisible = true;
    }

    /**
      * @doc Mapzania.Feature.read
      *
      * Internal utility method. Not to be used by applications. 
      *
      */
    Feature.prototype.read = function (geometryType, layerFeed, index) {
        this.id = layerFeed.ids[index];

        if (!geometryType) {
            geometryType = ns.FeatureTypes[layerFeed.geometryTypes[index]];
        }

        if (layerFeed.geometries[index][0]) {
            this.geometry = new geometryType();
            this.geometry.read(layerFeed.geometries[index]);
        } else {
            this.geometry = null;
        }

        var attribs = layerFeed.attributes[index];

        var fields = layerFeed.attributeNames;
        for (var i = 0; i < fields.length; i++)
            this.attributes[fields[i]] = attribs[i];

        this.dynamicProperties.labelIsVisible = (layerFeed.dynamicProperties[index] == 1);

        return this;
    }

    /**
      * @doc Mapzania.Feature.readFromVectorServer
      *
      * Internal utility method. Not to be used by applications. 
      *
      */
    Feature.prototype.readFromVectorServer = function (attributeNames, featureData) {
        this.id = featureData[2];
        geometryType = ns.FeatureTypes[featureData[4]];
        

        if (featureData[1][0]) {
            this.geometry = new geometryType();
            this.geometry.read(featureData[1]);
        } else {
            this.geometry = null;
        }

        var attribs = featureData[0];

        var fields = attributeNames;
        for (var i = 0; i < fields.length; i++)
            this.attributes[fields[i]] = attribs[i];

        this.dynamicProperties.labelIsVisible = (featureData[3] == 1);

        return this;
    }

    /**
      * @doc Mapzania.Feature.clone
      *
      * Clones this feature instance, doing a deep clone.
      *
      * @return (Mapzania.Feature) A deep clone of this feature instance.
      */
    Feature.prototype.clone = function () {
        var result = new Mapzania.Feature();

        result.id = this.id;
        if (this.geometry)
            result.geometry = this.geometry.clone();
        $.extend(true, result.attributes, this.attributes);
        $.extend(true, result.dynamicProperties, this.dynamicProperties);
        return result;
    }

    ns.Feature = Feature;

    return ns;
})(Mapzania || {});


var Mapzania = (function (ns) {

    function ExpressionFormatter(expression) {
        this.formatters = [];
        var me = this;
        var list = expression.split(/({[.-}]*})/);

        var util = Mapzania.Util;

        list.forEach(function (cur) {
            if (util.startsWith(cur, "{") && util.endsWith(cur, "}")) {
                me.formatters.push(new Mapzania.ExprSegmentFormatter(cur));
            }
            else {
                me.formatters.push(new Mapzania.ConstSegmentFormatter(cur));
            }
        });
    }

    ExpressionFormatter.prototype.hasValue = function (feature) {
        var result = true;

        this.formatters.forEach(function (cur) {
            if (!cur.hasValue(feature)) {
                result = false;
            }
        });

        return result;
    }

    ExpressionFormatter.prototype.calculateValue = function (feature) {
        var result = "";

        this.formatters.forEach(function (cur) {
            result += cur.calculateValue(feature);
        });

        return result;
    }

    ns.ExpressionFormatter = ExpressionFormatter;

    return ns;

})(Mapzania || {});

var Mapzania = (function (ns) {

    function ConstSegmentFormatter(value) {
        this.value = value;
    }

    ConstSegmentFormatter.prototype.hasValue = function (feature) {
        return true;
    }

    ConstSegmentFormatter.prototype.calculateValue = function (feature) {
        return this.value;
    }

    ns.ConstSegmentFormatter = ConstSegmentFormatter;

    return ns;

})(Mapzania || {});



var Mapzania = (function (ns) {

    function ExprSegmentFormatter(expr) {
        this.expr = expr.slice(1, expr.length - 1);

        var list = this.expr.split(':');
        if (list.length > 0)
        {
            this.fieldName = list[0];
        }

        if (list.length > 1)
        {
            this.format = list[1];
        }
    }

    ExprSegmentFormatter.prototype.hasValue = function (feature) {
        if (feature.attributes.hasOwnProperty(this.fieldName))
        {
            var v = feature.attributes[this.fieldName];
            var value = "";
            if (v) {
                value = new String(v);
            }
            return value.length > 0;
        }

        return false;
    }

    ExprSegmentFormatter.prototype.calculateValue = function (feature) {
        var result = "";
        if (this.fieldName != null && feature.attributes.hasOwnProperty(this.fieldName)) {
            var value = feature.attributes[this.fieldName];
            result = value;

            if (this.format) {
                if (Mapzania.Util.endsWith(this.format, "ED")) {

                    var trunc = Mapzania.Util.truncate;

                    var d = parseFloat(value);
                    var i = trunc(d);
                    var p = Math.abs(d) - trunc(Math.abs(d));
                    var digits = parseInt(this.format.replace("ED", ""));
                    if (digits == 0) {
                        result = i;
                    }
                    else {
                        result = i + "." + trunc(p * Math.pow(10, digits));
                    }
                }
                else {
                    console.error("Invalid field expression format (originating from map schema file): " + this.expr);
                }
            }
        }

        return result;
    }

    ns.ExprSegmentFormatter = ExprSegmentFormatter;

    return ns;

})(Mapzania || {});


