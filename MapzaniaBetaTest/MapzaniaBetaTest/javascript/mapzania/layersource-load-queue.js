﻿/// <reference path="mapzania.js"/>

var Mapzania = (function (ns) {
    function LayerSourceLoadQueue(model) {

        this.progress = new ns.Event();

        var api = model.api;

        var _queue = [];
        var _controlledRequestCounter = 0; //Data intensive requests in progress
        var _timeoutRef = null;

        var _progressTotalRequests = 0;
        var _progressOutstandingRequests = 0;
        var _outstandingRequests = {};

        var me = this;

        this.getFeatures = function (layer, filters, options, callback) {
            _progressTotalRequests++;

            var queueObj = {
                type: options.tiled ? "VECTORTILED" : "VECTOR",
                layer: layer,
                sourceKey: layer.sourceKey,
                filters: filters,
                options: options,
                callback: callback,
            };

            // cancel outstanding requests for this layer
            if (_outstandingRequests && _outstandingRequests[layer.key]) {
                api.cancelRequests(_outstandingRequests[layer.key]);
                _outstandingRequests[layer.key] = null;
            }

            // Remove duplicate requests for this layer
            var q2 = [];
            _queue.forEach(function (cur) {
                if (cur.layer.key != layer.key) {
                    q2.push(cur);
                } else {
                    _progressTotalRequests--;
                }
            });
            _queue = q2;

            _queue.push(queueObj);

            _scheduleProcessQueue();
        }

        this.getTiles = function (sourceKey, extentToLoad, extentVisible, srid, filter, callback, requestId) {
            _progressTotalRequests++;

            // We don't queue tile requests
            api.getTiles(sourceKey, extentToLoad, extentVisible, srid, filter, callback, requestId);
        }

        var _scheduleProcessQueue = function () {
            if (!_timeoutRef) {
                _timeoutRef = setTimeout(_processQueue.bind(this), 0);
            }
        }

        var _processQueue = function () {
            _timeoutRef = null;

            var q2 = []; // Items to be processed in the next round

            if (!_outstandingRequests) {
                _outstandingRequests = {};
            }

            _queue.forEach(function (queueObj) {
                var isDataIntensive = _isDataIntensive(queueObj);
                var doRequest = false;
                if (isDataIntensive) {
                    if (_controlledRequestCounter < 2) {
                        _controlledRequestCounter++;
                        doRequest = true;
                    } else {
                        //console.log("IsDataIntensive: Adding request back to queue");
                        q2.push(queueObj);
                    }
                } else {
                    doRequest = true;
                }

                if (doRequest) {
                    var callback = null;
                    if (isDataIntensive) {
                        callback = function (data) {
                            _progressOutstandingRequests--;
                            _controlledRequestCounter--;
                            queueObj.callback(data);
                            _scheduleProcessQueue();
                        };
                    } else {
                        callback = function (data) {
                            _progressOutstandingRequests--;
                            queueObj.callback(data);
                            _scheduleProcessQueue();
                        };
                    }

                    _progressOutstandingRequests++;

                    if (queueObj.type == "VECTORTILED") {

                        _outstandingRequests[queueObj.layer.key] = api.getFeatureTiles(
                            queueObj.sourceKey,
                            queueObj.filters,
                            queueObj.options,
                            queueObj.callback);
                    }
                    else {

                        _outstandingRequests[queueObj.layer.key] = api.getFeatures(
                            queueObj.sourceKey,
                            queueObj.filters,
                            queueObj.options,
                            queueObj.callback);
                    }
                }
            });

            _queue = q2;

            // Report on the progress
            var outstanding = _progressOutstandingRequests + _queue.length;
            if (outstanding == 0) {
                _progressTotalRequests = 0;
            }

            me.progress.fire(me, { total: _progressTotalRequests, outstanding: outstanding });
        }

        var _isDataIntensive = function (queueObj) {
            var result =
                "true" == _getTagValue(queueObj.layer, "IsDataIntensive")
                ||
                "true" == model.getTagValue("IsDataIntensive");
            return result;
        }

        var _getTagValue = function (layer, tagName) {
            var value = null;
            if (layer.tags) {
                layer.tags.forEach(function (cur) {
                    if (cur.key == tagName) {
                        value = cur.value;
                    }
                });
            }
            return value;
        }
    }

    ns.LayerSourceLoadQueue = LayerSourceLoadQueue;

    return ns;

})(Mapzania || {});
