﻿/**
 * @doc Mapzania.Extent.*ClassDescription*
 *
 * Encapsulates a geographic extent. An extent defines a rectangular region and has Minimum X, Minimum Y, Maximum X and Maximum Y properties. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.Extent.Extent
      *
      * Constructor. 
      *
      *@param data (Object) This parameter indicated the initial size for this extent and has the form { minX: 10, minY: 10, maxX: 20, maxY: 20 }.
      */
    function Extent(data) {
        if (data) {
            this.minX = data.minX;
            this.minY = data.minY;
            this.maxX = data.maxX;
            this.maxY = data.maxY;
        }
    }

    /**
      * @doc Mapzania.Extent.getCenter
      *
      * Determines the center for this extent. 
      *
      * @returns (Mapzania.Point) The centroid for this extent instance.
      */
	Extent.prototype.getCenter = function () {
		var x = this.minX + (this.maxX - this.minX) / 2.0
		var y = this.minY + (this.maxY - this.minY) / 2.0
		return new ns.Point().read([[[x, y]]])
	}

    /**
      * @doc Mapzania.Extent.width
      *
      * Determines the width for this extent. 
      *
      * @returns (Number) The width for this extent.
      */
	Extent.prototype.width = function ()
	{
	    return Math.abs(this.maxX - this.minX); 
	}

    /**
      * @doc Mapzania.Extent.height
      *
      * Determines the height for this extent. 
      *
      * @returns (Number) The height for this extent.
      */
	Extent.prototype.height = function ()
	{
	    return Math.abs(this.maxY - this.minY); 
	}

    /**
      * @doc Mapzania.Extent.centerAt
      *
      * Centers this extent instance around a point.
      *
      * @param point (Mapzania.Point) A point instance around which this extent instance will be centered.
      */
	Extent.prototype.centerAt = function(point)
	{
	    var w = this.width();
	    var h = this.height();

	    this.minX = point.x - w / 2;
	    this.minY = point.y - h / 2;
	    this.maxX = point.x + w / 2;
	    this.maxY = point.y + h / 2;
	}

    /**
      * @doc Mapzania.Extent.grow
      *
      * Grow the extent and returns a resized extent instance. Note that this method does not change the width and height for this instance.
      *
      * @param proportionX (Number) The X-proportion that the extent needs to be grown.
      *                             A value of 2 indicates that the width of the new extent should be twice as much as the width for this instance.
      *                             A value 0f 0.5 indicates that the width of the new extent should be half the width for this instance.
      *
      * @param proportionY (Number) The Y-proportion that the extent needs to be grown.
      *                             A value of 2 indicates that the height of the new extent should be twice as much as the height for this instance.
      *                             A value 0f 0.5 indicates that the height of the new extent should be half the height for this instance.
      *
      * @returns (Mapzania.Extent) A new resized extent instance. 
      */
	Extent.prototype.grow = function (proportionX, proportionY) {
	    if (!proportionY)
	        proportionY = proportionX;

	    var box = Object.assign(new Extent(), this);

	    var width  = (box.width() * (1-proportionX)) / 2;
	    var height = (box.height() * (1-proportionY)) / 2;
	    box.minX += width;
	    box.minY += height;
	    box.maxX -= width;
	    box.maxY -= height;
	    return box;
	}

    /**
      * @doc Mapzania.Extent.normalise
      *
      * Resizes this extent instance so that the aspect ratio of this extent matches the aspect ratio of a provided size.
      *
      * @param size (Object) An object in the form {width: 100, height: 100}
      */
	Extent.prototype.normalise = function (size) {
	    var ratioTarget = parseFloat(size.width) / parseFloat(size.height);
	    var ratioThis = this.width() / this.height();

	    var diffY = 0;
	    var diffX = 0;

	    if (ratioTarget > ratioThis) {
	        diffX = (this.height() * ratioTarget - this.width()) / 2;
	    }

	    if (ratioTarget < ratioThis) {
	        diffY = (this.width() / ratioTarget - this.height()) / 2;
	    }
	    
	    this.minY -= diffY;
	    this.maxY += diffY;
	    this.minX -= diffX;
	    this.maxX += diffX;
	}

    /**
      * @doc Mapzania.Extent.containsPoint
      *
      * Determines if a point is located inside the bounds of this extent.
      *
      * @param point (Mapzania.Point) A valid point instance.
      *
      * @returns (boolean) true to indicate that the point is located within the bounds of this extent. Otherwise false.
      */
	Extent.prototype.containsPoint = function (p) {
	    return  (p.x >= this.minX && p.x <= this.maxX) && (p.y >= this.minY && p.y <= this.maxY);
	}

    /**
      * @doc Mapzania.Extent.equals
      *
      * Determines the bounds of this extent is the same as the bounds of another extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      *
      * @returns (boolean) true to indicate that the bounds of the provided extent is the same as the bounds of this instance. Otherwise false.
      */
	Extent.prototype.equals = function (extent) {
	    return (this.minX == extent.minX && this.minY == extent.minY && this.maxX == extent.maxX && this.maxY == extent.maxY);
	}

    /**
      * @doc Mapzania.Extent.join
      *
      * Joins this extent instance with a provided extent. This extent instance is resized to contain both this and the provided extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      */
	Extent.prototype.join = function (extent) {
	    if (extent) {
	        this.minX = Math.min(this.minX, extent.minX);
	        this.minY = Math.min(this.minY, extent.minY);
	        this.maxX = Math.max(this.maxX, extent.maxX);
	        this.maxY = Math.max(this.maxY, extent.maxY);
	    }
	}

    /**
      * @doc Mapzania.Extent.isPoint
      *
      * Determines if this extent is represented by a point (i.e. have a width and a height of zero).
      *
      * @returns (boolean) true to indicate that the width and height of this extent is zero. Otherwise false.
      */
	Extent.prototype.isPoint = function () {
	    return (this.minX == this.maxY && this.minY == this.maxY);
	}

    /**
      * @doc Mapzania.Extent.intersects
      *
      * Determines if this extent intersects with another extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      *
      * @returns (boolean) true to indicate that the two extents intersect. Otherwise false.
      */
	Extent.prototype.intersects = function (extent) {
	    var notIntersect =
            (
                (Math.max(extent.minY, extent.maxY) < this.minY)
                ||
                (Math.min(extent.minY, extent.maxY) > this.maxY)
                ||
                (Math.max(extent.minX, extent.maxX) < this.minX)
                ||
                (Math.min(extent.minX, extent.maxX) > this.maxX)
            );

	    return !notIntersect; 
	}

    /**
      * @doc Mapzania.Extent.contains
      *
      * Determines if this extent is contained within another extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      *
      * @returns (boolean) true to indicate that this extent instance is contained within the provided extent. Otherwise false.
      */
	Extent.prototype.contains = function (extent) {
	    if (!extent) {
	        return false;
	    }
        
	    var result =
            this.minX >= extent.minX
            &&
            this.minY >= extent.minY
            &&
            this.maxX <= extent.maxX
            &&
            this.maxY <= extent.maxY;

	    return result; 
	}

    /**
      * @doc Mapzania.Extent.translate
      *
      * Translate this Extent instance.
      *
      * @param x   (Number) The units to translate this Extent in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this Extent in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	Extent.prototype.translate = function (x, y) {
	    this.minX += x;
	    this.minY += y;
	    this.maxX += x;
	    this.maxY += y;
	}

	Extent.prototype.clone = function () {
	    return new Extent(this);
	}

	ns.Extent = Extent;

	return ns;

})(Mapzania || {})
