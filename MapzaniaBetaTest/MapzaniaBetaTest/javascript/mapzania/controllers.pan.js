﻿var Mapzania = (function (ns) {
    if (!ns.Controllers)
        ns.Controllers = {};

    function Pan(viewport, callback) {
        var me = this;

        viewport.mouseDown.add(function (evt) {
            if (!me._enabled)
                return;

            me.panning = true;
            me.start = evt.data;
        });

        viewport.mouseUp.add(function (evt) {
            if (!me._enabled)
                return;

            var hasPanned = ((evt.data.x - me.start.x) != 0) || ((evt.data.y - me.start.y) != 0);

            if (hasPanned && callback) {
                callback({
                    x: evt.data.x - me.start.x,
                    y: evt.data.y - me.start.y
                });
            }

            me.panning = false;
        });

        viewport.mouseMove.add(function (evt) {
            if (!me._enabled)
                return;
            if (me.panning) {
                var diffX = evt.data.x - me.start.x;
                var diffY = evt.data.y - me.start.y;
                //console.log("panning: x=" + diffX + "/y=" + diffY);
                viewport.pan(diffX, diffY);
            }
        });
    }

    Pan.prototype.enabled = function (val) {
        if (typeof val != 'undefined')
            this._enabled = val;

        return this._enabled;
    }

    Pan.prototype.canZoomMap = function () {
        return true;
    }

    Pan.prototype.isTempState = function () {
        return false;
    }

    ns.Controllers.Pan = Pan;

    return ns;
})(Mapzania || {})
