﻿/// <reference path="../jasmine/jasmine.js"/>
/// <reference path="../mapzania.js"/>
describe("GeoJson.convert", function () {

    it("should convert a Point to GeoJSON", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var result = Mapzania.GeoJson.toGeometry(point);
        expect(result.coordinates.length).toBe(2);
    })

    it("should convert a MultiPoint to GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var mpoint = new Mapzania.MultiPoint();
        mpoint.points = [point, point, point];

        var result = Mapzania.GeoJson.toGeometry(mpoint);
        expect(result.coordinates.length).toBe(3);
    })

    it("should convert a Line to GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point];

        var result = Mapzania.GeoJson.toGeometry(line);
        expect(result.coordinates.length).toBe(3);
    })

    it("should convert a MultiLine to GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point];

        var mline = new Mapzania.MultiLine();
        mline.lines = [line, line, line];

        var result = Mapzania.GeoJson.toGeometry(mline);
        expect(result.coordinates.length).toBe(3);
    })

    it("should convert a Polygon to GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point,point];

        var poly = new Mapzania.Polygon();
        poly.shell = line;
        poly.holes = [line, line, line];

        var result = Mapzania.GeoJson.toGeometry(poly);
        expect(result.coordinates.length).toBe(4);
    })

    it("should convert a MultiPolygon to GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point,point];

        var poly = new Mapzania.Polygon();
        poly.shell = line;
        poly.holes = [line, line, line];

        var mpoly = new Mapzania.MultiPolygon();
        mpoly.polygons = [poly,poly];

        var result = Mapzania.GeoJson.toGeometry(mpoly);
        expect(result.coordinates.length).toBe(2);
    })

    it("should convert a FeatureCollection from GeoJSON", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var gjPoint = Mapzania.GeoJson.toGeometry(point);

        var ftr = {
            type: "Feature",
            geometry: gjPoint,
            properties: []
        }

        var ftrs = {
            type: "FeatureCollection",
            features:[ftr,ftr]
        }

        var result = Mapzania.GeoJson.fromFeatureCollection(ftrs);

        expect(result.length).toBe(2);
        expect(result[0].geometry.x = 10);
    })

    it("should convert a FeatureCollection BoundingBox from GeoJSON", function () {

        var ftrs = [];

        var point = new Mapzania.Point();
        point.x = -10;
        point.y = -20;

        var ftr = new Mapzania.Feature();
        ftr.geometry = point;

        ftrs.push(ftr);

        point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        ftr = new Mapzania.Feature();
        ftr.geometry = point;

        ftrs.push(ftr);

        var result = Mapzania.GeoJson.toFeatureCollection(ftrs);

        expect(result.features.length).toBe(2);
        expect(result.bbox[0]).toBe(-10);
        expect(result.bbox[1]).toBe(-20);
        expect(result.bbox[2]).toBe(10);
        expect(result.bbox[3]).toBe(20); 
    })

    it("should convert a Feature from GeoJSON", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var gjPoint = Mapzania.GeoJson.toGeometry(point);

        var ftr = {
            type: "Feature",
            geometry: gjPoint,
            properties:[]
        }

        var result = Mapzania.GeoJson.fromFeature(ftr);

        expect(result).toEqual(jasmine.any(Mapzania.Feature));
        expect(result.geometry.x).toBe(10);
    })

    it("should convert a Point from GeoJSON", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var gjPoint = Mapzania.GeoJson.toGeometry(point);

        var result = Mapzania.GeoJson.fromGeometry(gjPoint);

        expect(result).toEqual(jasmine.any(Mapzania.Point));
        expect(result.x).toBe(10);
    })

    it("should convert a MultiPoint from GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var mpoint = new Mapzania.MultiPoint();
        mpoint.points = [point, point, point];

        var gj = Mapzania.GeoJson.toGeometry(mpoint);

        var result = Mapzania.GeoJson.fromGeometry(gj);

        expect(result).toEqual(jasmine.any(Mapzania.MultiPoint));
        expect(result.points.length).toBe(3);
    })

    it("should convert a Line from GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point];

        var gj = Mapzania.GeoJson.toGeometry(line);

        var result = Mapzania.GeoJson.fromGeometry(gj);

        expect(result).toEqual(jasmine.any(Mapzania.Line));
        expect(result.points.length).toBe(3);
    })

    it("should convert a MultiLine from GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point];

        var mline = new Mapzania.MultiLine();
        mline.lines = [line, line, line];

        var gj = Mapzania.GeoJson.toGeometry(mline);

        var result = Mapzania.GeoJson.fromGeometry(gj);

        expect(result).toEqual(jasmine.any(Mapzania.MultiLine));
        expect(result.lines.length).toBe(3);
    })

    it("should convert a Polygon from GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point, point];

        var poly = new Mapzania.Polygon();
        poly.shell = line;
        poly.holes = [line, line, line];

        var gj = Mapzania.GeoJson.toGeometry(poly);

        var result = Mapzania.GeoJson.fromGeometry(gj);

        expect(result).toEqual(jasmine.any(Mapzania.Polygon));
        expect(result.shell.points.length).toBe(4);
        expect(result.holes.length).toBe(3);
    })

    it("should convert a MultiPolygon from GeoJson", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var line = new Mapzania.Line();
        line.points = [point, point, point, point];

        var poly = new Mapzania.Polygon();
        poly.shell = line;
        poly.holes = [line, line, line];

        var mpoly = new Mapzania.MultiPolygon();
        mpoly.polygons = [poly, poly];

        var gj = Mapzania.GeoJson.toGeometry(mpoly);

        var result = Mapzania.GeoJson.fromGeometry(gj);

        expect(result).toEqual(jasmine.any(Mapzania.MultiPolygon));
        expect(result.polygons.length).toBe(2);
    })

    it("should convert a Point to GeoJSON", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var result = Mapzania.GeoJson.toGeometry(point);
        expect(result.coordinates.length).toBe(2);
    })

});