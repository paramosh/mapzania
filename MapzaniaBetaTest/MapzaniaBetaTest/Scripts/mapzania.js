if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.Api.*ClassDescription*
 *
 * Provides a method-based interface to web-endpoints exposed by the Geo-spatial API on the Mapzania Server.
 * Applications usually creates a single instance of this class. Methods provided in this class is mostly used by the Mapzania map model (Mapzania.Model). 
 * However, applications can also make use of the functionality provided in this class, and might be interested in the following functions:
 * <ul>
 *   <li>saveSchema
 *   <li>deleteSchema
 *   <li>getArea
 *   <li>getLength 
 * </ul>
 */
(function (ns) {
    /**
      * @doc Mapzania.Api.Api
      *
      * Constructor.
      *
      * @param url (String) The endpoint for the Mapzania Geospatial API. This URL may be relative to the application domain.
      */
    function Api(url) {
        this.url = url;
    }

    /**
      * @doc Mapzania.Api.getCoordinateSystems
      *
      * Get a list of supported coordinate systems from Mapzania Server. This function is asynchronous. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the list of supported coordinate systems. 
      */
    Api.prototype.getCoordinateSystems = function (callback) {
        this.fetch("api/srids", callback);
    }
    
    /**
      * @doc Mapzania.Api.getLayerSources
      *
      * Get a list of layer sources from Mapzania Server. This function is asynchronous. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the list of layer sources. 
      */
    Api.prototype.getLayerSources = function (callback) {
        this.fetch("api/sources", callback);
    }

    /**
      * @doc Mapzania.Api.getLayerSource
      *
      * Get the details for a single layer sources from Mapzania Server. This function is asynchronous. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the details of the layer sources. 
      */
    Api.prototype.getLayerSource = function (key, callback) {
        this.fetch("api/sources/" + key + "/", callback);
    }

    /**
      * @doc Mapzania.Api.getMaps
      *
      * Get a list of maps from Mapzania Server. This function is asynchronous. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the list of maps. 
      */
    Api.prototype.getMaps = function (callback) {
        this.fetch("api/maps", callback);
    }

    /**
      * @doc Mapzania.Api.getSchema
      *
      * Load a map schema from the Mapzania Server. This function is asynchronous. 
      *
      * @param mapKey (String) The map key to load.
      *
      * @param srid (String) An optional SRID. All coordinates and geographic extents in the map schema
      *                      will be presented in this coordinate system.
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the map schema. 
      */
    Api.prototype.getSchema = function (mapKey, srid, callback) {
        var params = [];

        if (typeof srid !== "undefined")
            params.push(["srid", srid]);

        var path = "api/maps/" + mapKey + "/schema?" + this.paramsToString(params);

        this.fetch(path, callback);
    }

    /**
      * @doc Mapzania.Api.saveSchema
      *
      * Save a map schema to the Mapzania Server, in volatile storage. This function is asynchronous. 
      *
      * @param options (Object) An object containing the following properties: mapKey, schema, minutesValid.
      *                         The mapKey property indicates the map key. All temporary maps are expected to start with the 
      *                         prefix "TEMP-". 
      *                         The schema property contains the map schema. The map schema is obtained by using the getSchema method on 
      *                         the Model class. 
      *                         The minutesValid proppery contains the number of minutes that the map will be valid for. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      */
    Api.prototype.saveSchema = function (options, callback) {
        var params = [];
        options.schema.mapKey = options.mapKey;
        params.push(["mapKey", options.mapKey]);
        if (options.minutesValid)
            params.push(["minutesValid", options.minutesValid]);
        var path = "api/maps/" + options.mapKey + "/save?" + this.paramsToString(params);

        // We make the saveSchema operation synchronous, since following a saveSchema call, a download in a new window / popup usually follows. 
        //  We need to do this, to preserve the event state on the call stack - i.e. popups are only allowed from user events / actions. 
        //  Pierre Gunter has more details on this. 
        this.fetch(path, callback, encodeURIComponent(JSON.stringify(options.schema)), null, true);
    }

    /**
      * @doc Mapzania.Api.deleteSchema
      *
      * Deletes a map schema from the Mapzania Server volatile storage. This function is asynchronous. 
      *
      * @param options (Object) An object containing the following property: mapKey.
      *                         The mapKey property indicates the map key. All temporary maps are expected to start with the 
      *                         prefix "TEMP-". 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      */
    Api.prototype.deleteSchema = function (options, callback) {
        var params = [];
        params.push(["mapKey", options.mapKey]);
        var path = "api/maps/" + mapKey + "/delete?" + this.paramsToString(params);
        this.fetch(path, callback);
    }

    /**
      * @doc Mapzania.Api.getMapDataExportFormats
      *
      * Get a list of supported export formats for a map. This function is asynchronous. 
      *
      * @param options (Object) An object containing the following property: mapKey.
      *                         The mapKey property indicates the map key. All temporary maps are expected to start with the 
      *                         prefix "TEMP-". 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      */
    Api.prototype.getMapDataExportFormats = function (options, callback) {
        var params = [];
        params.push(["mapKey", options.mapKey]);
        var path = "api/maps/" + mapKey + "/exportformats?" + this.paramsToString(params);
        this.fetch(path, callback);
    }

    /**
      * @doc Mapzania.Api.getFeatures
      *
      * Loads feature data for a given layer source. This function is asynchronous. 
      *
      * @param sourceKey (String) The layer source key.
      *
      * @param filter (String) A filter clause. 
      *
      * @param extent (Mapzania.Extent) The extent of the geospatial region to load. The coordinate system of this extent is indicated by the srid parameter.
      *
      * @param fields (String) A comma-separated list of field names (also known as feature attributes) to include in the result. Field names are case sensitive. 
      *
      * @param thinAlgo (String) This parameter is no longer used, and should be set to null.
      *
      * @param thinFactor (Number) The thinning factor to use when loading feature data. Refer to the Map Schema Guide for more information on this value. 
      *
      * @param accuracy (Number) The accuracy factor to use when loading feature data. Refer to the Map Schema Guide for more information on this value. 
      *
      * @param srid (String) The Spatial Reference Identifier (SRID) in which the request extent and feature data is presented. This will typcally be the same SRID as
      *                      the display SRID of the map.
      *
      * @param labelVisibilityClause (String) An optional label visibility clause. Refer to the Map Schema Guide for more information on this value. 
      *
      * @param sourceParams (Object) An optional list of layer source parameters. Refer to the Map Schema Guide for more information on this value. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation.  
      *
      * @param fieldsListIsStrict (Boolean) A true value indicates that an empty fields list should result in no attributes included in the result. 
      *                                     A false value indicates that an empty field list should result in all feature attributes included in the result.
      *
      */
    Api.prototype.getFeatures = function (sourceKey, filter, extent, fields, thinAlgo, thinFactor, accuracy, srid, labelVisibilityClause, sourceParams, callback, fieldsListIsStrict, fence) {
        if (!fieldsListIsStrict)
            fieldsListIsStrict = false;

        var params = [
			["filter", filter],
			["fields", fields],
			["thinFactor", thinFactor],
            ["thinAlgo", thinAlgo],
			["accuracy", accuracy],
			["srid", srid],
            ["labelVisibilityClause", labelVisibilityClause],
            ["fieldsListIsStrict", fieldsListIsStrict],
            ["geofence", JSON.stringify(fence)]
        ];

        if (extent) {
            params.push(["minx", extent.minX]);
            params.push(["miny", extent.minY]);
            params.push(["maxx", extent.maxX]);
            params.push(["maxy", extent.maxY]);
        }

        if (sourceParams) {
            sourceParams.forEach(function (cur) {
                params.push(["SOURCEPARAM:" + cur.name, cur.value]);
            }
            )
        }

        var unpack = function (data) {
            var res = [];

            for (var i = 0; i < data.geometries.length; i++) {

                var ftr = new ns.Feature();

                ftr.read(null, data, i);
                res.push(ftr);
            }

            callback(res);
        };

        var path = "api/sources/" + sourceKey + "/features";

        this.fetch(path, unpack, this.paramsToString(params), "Load features from server [" + sourceKey + "]");
    }

    Api.prototype.getVectorServerData = function (sourceKey, filter, extent, fields, thinAlgo, thinFactor, accuracy, srid, labelVisibilityClause, sourceParams, callback, fieldsListIsStrict, vectorServerName, mapKey, layerGroupName, z, x, y) {
        var params = [
			["filter", filter],
			["fields", fields],
			["thinFactor", thinFactor],
            ["thinAlgo", thinAlgo],
			["accuracy", accuracy],
			["srid", srid],
            ["labelVisibilityClause", labelVisibilityClause],
            ["layerSourceKey", sourceKey],
            ["serverName", vectorServerName],
            ["mapKey", mapKey],
            ["layerGroupName", layerGroupName],
            ["z", z],
            ["x", x],
            ["y", y]
        ];

        if (extent) {
            params.push(["minx", extent.minX]);
            params.push(["miny", extent.minY]);
            params.push(["maxx", extent.maxX]);
            params.push(["maxy", extent.maxY]);
        }
        var path = "api/sdk/content/VectorServer/VectorData/JSON";

        var httpVerb = "POST";
        if (z || x || y) {
            httpVerb = "GET"; // A work-around
        }

        this.fetch(path, callback, this.paramsToString(params), null, null, null, null, httpVerb);
    }

    Api.prototype.getVectorTile = function (callback, mapKey, srid, layerGroupName, z, x, y) {
        var path = "api/vector-server-tile/" + mapKey + "/" + srid + "/" + layerGroupName + "/" + z + "/" + x + "/" + y + "";

        this.fetch(path, callback, null, null, null, null, null, "GET");
    }

    /**
      * @doc Mapzania.Api.commitUpdates
      *
      * Commit feature updates to a layer source on the server. This function is asynchronous. 
      *
      * @param options (Object) An object containing the following properties: sourceKey, updateFeatureWrapperList.
      *                         The sourceKey property indicates the layer source key that needs to be updated. 
      *                         The updateFeatureWrapperList property contains the list of updates. The content of this property is unpublished and may change 
      *                         in future releases of Mapzania.
      *                         Make use of the addFeatures, updateFeatures and deleteFeatures methods on the Vector Layer Source class to 
      *                         modify features on a layer source. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      */
    Api.prototype.commitUpdates = function (options, callback) {
        var params = [];
        params.push(["sourceKey", options.sourceKey]);
        var path = "api/sources/" + options.sourceKey + "/CommitUpdates";
        this.fetch(path, callback, encodeURIComponent(JSON.stringify(options.updateFeatureWrapperList)), null, true, true);
    }

    /**
      * @doc Mapzania.Api.getLength
      *
      * Calculates the length of a list connecting a set of points. This function is asynchronous. 
      *
      * @param mapKey (String) The map key.
      *
      * @param srid (String) The spatial reference id (SRID) in which the line co-ordinates are presented. 
      *                      If this parameter is not set, it defaults to the display-SRID that is defined in the map schema.
      *
      * @param pts (String) A comma-delimited list of numbers which denote the x and y co-ordinates of a line (e.g "x1, y1, x2, y2, x3, y3"). 
      *                     These values are presented in the co-ordinate system that applies to the srid parameter.
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      */
    Api.prototype.getLength = function (mapKey, srid, pts, callback) {
        var params = [
			["srid", srid],
			["pts", pts]
        ];

        var path = "api/maps/" + mapKey + "/length?" + this.paramsToString(params);

        this.fetch(path, callback);
    }

    /**
      * @doc Mapzania.Api.getArea
      *
      * Calculates the area of a list connecting a set of points. This function is asynchronous. 
      *
      * @param mapKey (String) The map key.
      *
      * @param srid (String) The spatial reference id (SRID) in which the line co-ordinates are presented. 
      *                      If this parameter is not set, it defaults to the display-SRID that is defined in the map schema.
      *
      * @param pts (String) A comma-delimited list of numbers which denote the x and y co-ordinates of a line (e.g "x1, y1, x2, y2, x3, y3"). 
      *                     These values are presented in the co-ordinate system that applies to the srid parameter.
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      */
    Api.prototype.getArea = function (mapKey, srid, pts, callback) {
        var params = [
			["srid", srid],
			["pts", pts]
        ];

        var path = "api/maps/" + mapKey + "/area?" + this.paramsToString(params);

        this.fetch(path, callback);
    }

    /**
      * @doc Mapzania.Api.getTiles
      *
      * Retrieve a list of tiles definitions visible in a map extent. This function is asynchronous. 
      *
      * @param sourceKey (String) The raster tile layer source key.
      *
      * @param extentToLoad (Mapzania.Extent) The extent to use when determine the set of tiles to include. 
      *
      * @param extentVisible (Mapzania.Extent) An optional parameter indicating the visible map extent.  
      *
      * @param srid (String) A spatial reference identifier used to specify the extentToLoad and extentVisible parameters. 
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      *                            The second parameter to this function contains the requestId for this getTiles request.
      *
      * @param requestId (String) A unique identifier for this getTiles request. This identifier is provided to the callback function, allowing calling functions to identify the response.
      *
      * @param filter (String) A filter value, passed to the tile raster layer source.
      */
    Api.prototype.getTiles = function (sourceKey, extentToLoad, extentVisible, srid, filter, callback, requestId) {
        var params = [
			["srid", srid]
        ];

        if (extentToLoad) {
            params.push(["minx", extentToLoad.minX]);
            params.push(["miny", extentToLoad.minY]);
            params.push(["maxx", extentToLoad.maxX]);
            params.push(["maxy", extentToLoad.maxY]);
        }

        if (extentVisible) {
            params.push(["vminx", extentVisible.minX]);
            params.push(["vminy", extentVisible.minY]);
            params.push(["vmaxx", extentVisible.maxX]);
            params.push(["vmaxy", extentVisible.maxY]);
        }
        
        if (filter) {
            params.push(["filter", filter]);
        }

        var path = "api/sources/" + sourceKey + "/tiles?" + this.paramsToString(params);

        this.fetch(path, callback, null, null, null, null, requestId);
    }

    /**
      * @doc Mapzania.Api.getImageUrl
      *
      * This function returns an image or graphic representation of the map as defined by the schema key.
      *
      * @param mapKey (String) The map key.
      *
      * @param layerKeys (String) A comma-delimited list of layer keys. This list denotes the layers that should be drawn (treated as visible) on the image. If this parameter is not set, all layers that are visible by default in the schema will be drawn.
      *
      * @param filters (String) A comma-delimited list of filters. The filter is bash-delimited and denotes the layerKey to which it applies and the textual filter to apply. For example the filter "MAIN_ROADS|InUse=true" means the the textual filter "InUse=true" should be applied to the layer matching the key "MAIN_ROADS". If this parameter is not set, the default filters are applied.
      *
      * @param extent (Mapzania.Extent) The extent of the image to be drawn. This value is presented in the co-ordinate system that applies to the srid parameter. 
      *
      * @param size (Object) An object in the form {width: 100, height: 100}, and defines the size of the image in pixels. 
      *
      * @param srid (String) Sets the spatial reference id (SRID) in which the map image will be rendered. This effectively projects the image to the the reference system that corresponds with the SRID. If this parameter is not set, it defaults to the display-SRID that is defined in the map schema.
      *
      * @param showBackground (boolean) A value of "true" or "false" which indicates whether the background is drawn on the image. If this parameter is not set, its value is "false".
      *
      * @param format(String) Sets the format of the resulting image. Valid options are png, jpg or gif. If it is omitted the format defaults to png.
      *
      * @param options (Object) An optional object in the form {originX: 0, originX: 0, rotateZ: 0}, defining the map rotation parameters.
      *
      * @param callback (Function) A callback function to indicate the completion of this function. 
      *                            The first parameter to this function contains the result of this operation. 
      */
    Api.prototype.getImageUrl = function (mapKey, layerKeys, filters, extent, size, srid, showBackground, format, options) {
        if (!layerKeys)
            layerKeys = [];

        if (!filters)
            filters = [];

        if (!options)
            options = {};

        var params = [
			["layerKeys", layerKeys.join(",")],
			["filters", filters.join("|")],
			["srid", srid],
			["showBackground", showBackground],
			["format", format]
        ];

        if (extent) {
            params.push(["minx", extent.minX]);
            params.push(["miny", extent.minY]);
            params.push(["maxx", extent.maxX]);
            params.push(["maxy", extent.maxY]);
        }

        if (size) {
            params.push(["width", size.width]);
            params.push(["height", size.height]);
        }

        if (options.originX) params.push(["originX", options.originX]);
        if (options.originY) params.push(["originY", options.originY]);
        if (options.rotateZ) params.push(["rotateZ", options.rotateZ]);

        var path = this.url + "api/maps/" + mapKey + "/image?" + this.paramsToString(params);

        return path;
    }

    /**
      * @doc Mapzania.Api.getMapLegendIconUrl
      *
      * This function generates an URL to the endpoint that returns an icon image that is used for a specific theme and layer on a map legend.
      *
      * @param mapKey (String) The map key.
      *
      * @param layerKey (String) Sets the key of the layer within the map schema.
      *
      * @param themeKey (String) An optional parameter that sets the key of the theme within the vector layer. 
      *
      * @param featureValue (String) An optional parameter applicable to range themes. Sets the feature value for which to calculate the map legend icon.
      *
      * @param sizePixelsX (Number) Sets the icon width, in pixels.
      *
      * @param sizePixelsY (Number) Sets the icon height, in pixels.
      *
      * @param imageFormat (String) Sets the image format. The recommended format is <b>png</b>.
      *
      */
    Api.prototype.getMapLegendIconUrl = function (mapKey, layerKey, themeKey, featureValue, sizePixelsX, sizePixelsY, imageFormat) {
        var params = [
			["layerKey", layerKey],
			["themeKey", themeKey],
			["featureValue", featureValue],
			["sizePixelsX", sizePixelsX],
			["sizePixelsY", sizePixelsY],
			["imageFormat", imageFormat]
        ];

        var path = this.url + "api/maps/" + mapKey + "/legendicon?" + this.paramsToString(params);

        return path;
    }

    Api.prototype.fetch = function (path, callback, data, requestTitle, forceSync, includeFullResult, requestId, httpVerb) {
        if (!callback)
            throw "callback must be set";
        var p = this.url + path;

        var callbackSuccess = function (result, testStatus, jqXHR) {

            if (includeFullResult) {
                callback(result, requestId);
            } else {
                callback(result.data, requestId);
            }
        };

        forceSync = !(!forceSync); // We want a true / false value in this variable. 

        if (!httpVerb) httpVerb = "POST";

        $.ajax({
            type: httpVerb,
            url: p,
            data: data,
            success: callbackSuccess,
            async: !forceSync
        });
    }

    Api.prototype.paramsToString = function (params) {
        var terms = [];

        for (var i = 0; i < params.length; i++) {
            var param = params[i];

            var hasValue = param[1] != null && String(param[1]).length > 0;
            if (hasValue) {
                terms.push(param[0] + "=" + param[1]);
            }
        }

        if (terms.length == 0)
            return "";

        return terms.join("&");
    }

    ns.Api = Api;

})(Mapzania);
/// <reference path="mapzania.js"/>

var Mapzania = (function (ns) {
    var me;

    function Api2(url, isCompact) {
        this.isCompact = isCompact;
        this.url = url;
        me = this;

        //
        // Public Methods
        //
        this.getCoordinateSystems = function (callback) {
            get("srids", null, callback);
        }

        this.getLayerSources = function (callback) {
            get("sources", null, callback);
        }

        this.getMaps = function (callback) {
            get("maps", null, callback);
        }

        this.getSchema = function (mapKey, srid, callback) {
            var params = [];

            if (typeof srid !== "undefined")
                params.push(["srid", srid]);

            get("maps/" + mapKey, params, callback);
        }

        this.getFeatures = function (sourceKey, text, extent, fields, thinAlgo, thinFactor, accuracy, srid, labelVisibilityClause, sourceParams, callback, fieldsListIsStrict, fence, clientFilters) {

            var filters = [];

            var fact = Mapzania.Filters;

            if (text)
                filters.push(fact.filterByText(text));

            if (extent) {
                var ext = extent.clone();
                Mapzania.Projection.projectExtent(ext, me.wkt, "LATLONG");
                filters.push(fact.filterByBoundingBox([ext.minX, ext.minY, ext.maxX, ext.maxY]));
            }

            if (clientFilters)
                filters = filters.concat(clientFilters);

            //if (srid)
            //    filters.push(fact.changeCoordinateSystem(srid));

            if (!thinFactor) {
                var pixelWidth = sourceParams[0].value;
                var pixelHeight = sourceParams[1].value;
                var meterWidth = sourceParams[2].value;
                var meterHeight = sourceParams[3].value;

                thinfactor = (meterWidth * meterHeight) / (pixelWidth * pixelHeight) / 4000000000;
            }

            filters.push(fact.simplifyGeometry(thinfactor));

            me.getFeatures2(sourceKey, filters, callback);
        };

        this.getFeatures2 = function (sourceKey, filters, callback, options) {

            var defaults = {
                isProjected: true
            };

            settings = $.extend({}, defaults, options);

            var path = "sources/" + sourceKey + "/features";

            var body = {
                filters: filters,
                options: {
                    isCompact: this.isCompact
                }
            };

            post(path, body, function (data) {

                var res = [];

                if (isCompact) {
                    for (var i = 0; i < data.geometries.length; i++) {

                        var ftr = new ns.Feature();
                        ftr.read(null, data, i);
                        res.push(ftr);
                    }
                }
                else
                    res = Mapzania.GeoJson.fromFeatureCollection(data);

                if (settings.isProjected)
                    Mapzania.Projection.projectFeatureArray(res, "LATLONG", "GOOGLE");

                callback(res);
            });

        }

        this.getTiles = function (sourceKey, extentToLoad, extentVisible, srid, filter, callback, _requestId) {

            var params = [
                ["srid", srid]
            ];

            if (extentToLoad) {
                params.push(["minx", extentToLoad.minX]);
                params.push(["miny", extentToLoad.minY]);
                params.push(["maxx", extentToLoad.maxX]);
                params.push(["maxy", extentToLoad.maxY]);
            }

            if (extentVisible) {
                params.push(["vminx", extentVisible.minX]);
                params.push(["vminy", extentVisible.minY]);
                params.push(["vmaxx", extentVisible.maxX]);
                params.push(["vmaxy", extentVisible.maxY]);
            }

            if (filter)
                params.push(["filter", filter]);

            var path = "sources/" + sourceKey + "/tiles";

            get(path, params, callback, _requestId);
        };

        this.getImageUrl = function (mapKey, layerKeys, filters, extent, size, srid, showBackground, format, options) {
            if (!layerKeys)
                layerKeys = [];

            if (!filters)
                filters = [];

            if (!options)
                options = {};

            var params = [
                ["layerKeys", layerKeys.join(",")],
                ["filters", filters.join("|")],
                ["srid", srid],
                ["showBackground", showBackground],
                ["format", format]
            ];

            if (extent) {
                params.push(["minx", extent.minX]);
                params.push(["miny", extent.minY]);
                params.push(["maxx", extent.maxX]);
                params.push(["maxy", extent.maxY]);
            }

            if (size) {
                params.push(["width", size.width]);
                params.push(["height", size.height]);
            }

            if (options.originX) params.push(["originX", options.originX]);
            if (options.originY) params.push(["originY", options.originY]);
            if (options.rotateZ) params.push(["rotateZ", options.rotateZ]);

            var path = createUrl("maps/" + mapKey + "/image", params);

            return path;
        }

        //
        // Private Methods
        //
        var get = function (path, parameters, callback, requestId) {

            $.get(createUrl(path, parameters), function (data) {
                if (callback)
                    callback(data, requestId);
            });
        };

        var post = function (path, parameters, callback, requestId) {

            $.ajax({
                url: me.url + path,
                type: "POST",
                data: JSON.stringify(parameters),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (callback)
                        callback(data, requestId);
                }
            })
        }

        var createUrl = function (path, params) {

            var paramString = "";

            if (params) {
                var terms = [];

                for (var i = 0; i < params.length; i++) {
                    var param = params[i];

                    var hasValue = param[1] != null && String(param[1]).length > 0;
                    if (hasValue) {
                        terms.push(param[0] + "=" + param[1]);
                    }
                }

                if (terms.length == 0)
                    return "";

                paramString = "?" + terms.join("&");
            }

            var url = me.url + path + paramString;
            //console.log(url);

            return url;
        };
    }

    ns.Api2 = Api2;

    return ns;

})(Mapzania || {});


if (!Mapzania)
    var Mapzania = {};

(function (ns) {
    if (!ns.Controllers)
        ns.Controllers = {};

    var MultiSelector = function (viewport, mapper, callback, pointOnly, showFill) {

        //if (!MultiSelector.prototype.instances)
        //    MultiSelector.prototype.instances = [];

        //MultiSelector.prototype.instances.push(this);

        //this.id = MultiSelector.prototype.instances.length;

        this.viewport = viewport;
        this.mapper = mapper;

        this.points = [];

        var render = function (line, ping) {

            var max = 20;
            var radius = 3;
            var opacity = 100;


            if (line.points.length > 1) {

                mapper.clearLayer("ts_interactive");
                if (showFill)
                    mapper.fillPolygon("ts_interactive", [line], "#000000", null, 20);
                mapper.drawLine("ts_interactive", line, "#000000", 2, 100);
                mapper.drawLine("ts_interactive", line, "#FFFF00", 4, 60);
            }
            else {
                if (pointOnly) {
                    mapper.clearLayer("ts_interactive");
                    mapper.fillCircle("ts_interactive", line.points[0], 4, "#FFFF00", 60);
                    mapper.fillCircle("ts_interactive", line.points[0], 2, "#000000", 100);
                }
            }

            mapper.flush();

            var iter = 0;

            var id = setInterval(function () {
                mapper.clearLayer("ts_interactive_2");

                mapper.drawCircle("ts_interactive_2", ping, radius, "#FF0000", 2, opacity);
                mapper.drawCircle("ts_interactive_2", ping, radius, "#FFFFFF", 4, opacity);
                mapper.flush();

                radius *= 1.2;
                opacity *= 0.6;

                if (iter++ > 5) {
                    mapper.clearLayer("ts_interactive_2");
                    clearInterval(id);
                }
            }, 50);
        };

        var me = this;

        viewport.mouseUp.add(function (evt) {
            if (!me._enabled)
                return;

            clicked(evt);
        });

        var clicked = function (evt) {

            if (pointOnly)
                me.points = [];
            me.points.push(evt.data);

            line = { points: me.points };

            if (me.points.length > 2 && showFill) {
                var pnts = me.points.concat([me.points[0]]);
                line = { points: pnts };
            }

            render(line, evt.data);

            if (pointOnly && callback)
                callback(me.points);
        }

        viewport.doubleClick.add(function (evt) {
            if (!me._enabled)
                return;

            clicked(evt);

            if (callback)
                callback(me.points);
        });        
    }

    MultiSelector.prototype.enabled = function (val) {
        if (typeof val != 'undefined') {
            this._enabled = val;
            this.mapper.clearLayer("ts_interactive");
            this.points = [];
        }
        return this._enabled;
    }

    MultiSelector.prototype.canZoomMap = function () {
        return false;
    }

    MultiSelector.prototype.isTempState = function () {
        return false;
    }

    ns.Controllers.MultiSelector = MultiSelector;
})(Mapzania)

if (!Mapzania)
    var Mapzania = {};

(function (ns) {
    if (!ns.Controllers)
        ns.Controllers = {};

    function Pan(viewport, callback) {
        var me = this;

        viewport.mouseDown.add(function (evt) {
            if (!me._enabled)
                return;

            me.panning = true;
            me.start = evt.data;
        });

        viewport.mouseUp.add(function (evt) {
            if (!me._enabled)
                return;

            var hasPanned = ((evt.data.x - me.start.x) != 0) || ((evt.data.y - me.start.y) != 0);

            if (hasPanned && callback) {
                callback({
                    x: evt.data.x - me.start.x,
                    y: evt.data.y - me.start.y
                });
            }

            me.panning = false;
        });

        viewport.mouseMove.add(function (evt) {
            if (!me._enabled)
                return;
            if (me.panning) {
                var diffX = evt.data.x - me.start.x;
                var diffY = evt.data.y - me.start.y;
                //console.log("panning: x=" + diffX + "/y=" + diffY);
                viewport.pan(diffX, diffY);
            }
        });
    }

    Pan.prototype.enabled = function (val) {
        if (typeof val != 'undefined')
            this._enabled = val;

        return this._enabled;
    }

    Pan.prototype.canZoomMap = function () {
        return true;
    }

    Pan.prototype.isTempState = function () {
        return false;
    }

    ns.Controllers.Pan = Pan;
})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

(function (ns) {
	if (!ns.Controllers)
		ns.Controllers = {};

	function RectangleSelector(viewport, mapper, callback) {

		this._enabled = false;

		var me = this;

		viewport.mouseDown.add(function (evt) {
			if (!me._enabled)
				return;

			//console.log("zoom down");
			me.zooming = true;
			me.start = evt.data;
		});

		viewport.mouseUp.add(function (evt) {
			if (!me._enabled)
				return;
			me.zooming = false;

			var ext = new ns.Extent({
				minX: Math.min(me.start.x, evt.data.x),
				minY: Math.min(me.start.y, evt.data.y),
				maxX: Math.max(me.start.x, evt.data.x),
				maxY: Math.max(me.start.y, evt.data.y)
			});

			mapper.clearLayer("ts_interactive");

			if (callback)
			    callback(ext);
		});

		viewport.mouseMove.add(function (evt) {
			if (!me._enabled)
				return;

			if (me.zooming) {

				mapper.clearLayer("ts_interactive");
				
				var minX = Math.min(me.start.x, evt.data.x);
				var minY =	Math.min(me.start.y, evt.data.y);
				var maxX =	Math.max(me.start.x, evt.data.x);
				var maxY = Math.max(me.start.y, evt.data.y);

				var line = {
					points:
						[{ x: minX, y: minY },
						{ x: minX, y: maxY },
						{ x: maxX, y: maxY },
						{ x: maxX, y: minY },
						{ x: minX, y: minY }
						]
				};
				
				//console.log(line);
				mapper.fillPolygon("ts_interactive", [line], "#000000", null, 20, 1);
				mapper.drawLine("ts_interactive", line, "#000000", 1, 100);
				mapper.flush();
			}
		});
	}

	RectangleSelector.prototype.enabled = function (val) {
		if (typeof val != 'undefined')
			this._enabled = val;

		return this._enabled;
	}

	RectangleSelector.prototype.canZoomMap = function () {
	    return false;
	}

	RectangleSelector.prototype.isTempState = function () {
	    return false;
	}

	ns.Controllers.RectangleSelector = RectangleSelector;
})(Mapzania)

//
// File no longer in use
//
if (!Mapzania)
	var Mapzania = {};

(function (ns) {
    function Algo() { }

    Algo.prototype.getExtentFromPoints = function (pnts) {
        if (!pnts || pnts.length == 0)
            throw "pnts not valid";

        var minX = pnts[0].x;
        var minY = pnts[0].y;
        var maxX = pnts[0].x;
        var maxY = pnts[0].y;

        for (var i = 1; i < pnts.length; i++) {
            var p = pnts[i];

            if (p.x < minX)
                minX = p.x;

            if (p.y < minY)
                minY = p.y;

            if (p.x > maxX)
                maxX = p.x;

            if (p.y > maxY)
                maxY = p.y;
        }

        return new ns.Extent({ minX: minX, minY: minY, maxX: maxX, maxY: maxY });
    }

    Algo.prototype.getExtentFromLines = function (lines) {
        var ext = this.getExtentFromPoints(lines[0].points);

        for (var i = 1; i < lines.length; i++) {
            var lineExt = this.getExtentFromPoints(lines[i].points);

            if (lineExt.minX < ext.minX)
                ext.minX = lineExt.minX;
            if (lineExt.minY < ext.minY)
                ext.minY = lineExt.minY;
            if (lineExt.maxX > ext.maxX)
                ext.maxX = lineExt.maxX;
            if (lineExt.maxY > ext.maxY)
                ext.maxY = lineExt.maxY;
        }

        return ext;
    }

    Algo.prototype.ringContainsPoint = function (ring, p) {
        var i, i1;		// point index; i1 = i-1 mod n
        var xInt;		// x intersection of e with ray
        var crossings = 0;	// number of edge/ray crossings
        var x1, y1, x2, y2;
        var nPts = ring.length;

        // For each line edge l = (i-1, i), see if it crosses ray from test point in positive x direction.
        for (i = 1; i < nPts; i++) {
            i1 = i - 1;
            var p1 = ring[i];
            var p2 = ring[i1];
            x1 = p1.x - p.x;
            y1 = p1.y - p.y;
            x2 = p2.x - p.x;
            y2 = p2.y - p.y;

            if (((y1 > 0) && (y2 <= 0)) ||
                ((y2 > 0) && (y1 <= 0))) {
                // e straddles x axis, so compute intersection.
                xInt = (x1 * y2 - x2 * y1) / (y2 - y1);
                //xsave = xInt;
                // crosses ray if strictly positive intersection.
                if (0.0 < xInt) {
                    crossings++;
                }
            }
        }
        // p is inside if an odd number of crossings.
        if ((crossings % 2) == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    ns.Algo = Algo;

})(Mapzania);

//
// Intersections
//

(function (ns) {
    function Intersections() { }

    Intersections.prototype.intersectsPolyPoly = function (poly1, poly2) {
        if (!poly1 || !poly2) throw "Both polygons need to be valid instances.";

        var algo = new Mapzania.Algo();

        var extent1 = poly1.getExtent();
        var extent2 = poly2.getExtent();
        if (!extent1.intersects(extent2)) {
            return false;
        }

        var lineSegments1 = poly1.shell.getLineSegments(null);
        var lineSegments2 = poly2.shell.getLineSegments(null);

        for (var i = 0; i < lineSegments1.length; i++) {
            for (var j = 0; j < lineSegments2.length; j++) {
                if (lineSegments1[i].getIntersectionPoint(lineSegments2[j]) != null) {
                    return true;
                }
            }
        }

        if (algo.ringContainsPoint(poly1.shell.points, lineSegments2[0].p1)) {
            return true;
        }

        if (algo.ringContainsPoint(poly2.shell.points, lineSegments1[0].p1)) {
            return true;
        }

        return false;
    }

    ns.Intersections = Intersections;

})(Mapzania);


if (!Mapzania)
    var Mapzania = {};

(function (ns) {
    function Convert() {
    }

    Convert.prototype.fromDDToDMS = function (decimalDegrees) {
        var result = {};
        if (typeof (decimalDegrees) != "undefined" && decimalDegrees != null) {
            var frac = Math.abs(decimalDegrees - (decimalDegrees | 0));
            result.degrees = decimalDegrees | 0;
            result.minutes = (frac * 60) | 0;
            result.seconds = Math.round((frac * 3600 - result.minutes * 60) * 1000) / 1000;
        }
        return result;
    }

    Convert.prototype.fromDMSToDD = function (dms) {
        if (!dms)
            return null;

        var deg = parseFloat(dms.degrees);
        var min = parseFloat(dms.minutes);
        var sec = parseFloat(dms.seconds);
        var sign = (deg >= 0 ? 1 : -1);

        return sign * (Math.abs(deg) + min / 60.0 + sec / 3600.0);
    }

    ns.Convert = Convert;

})(Mapzania);


if (!Mapzania)
    var Mapzania = {};

/**
 * @doc Mapzania.Event.*ClassDescription*
 *
 * An utility class for dispatching events to a set of listeners.
 */
(function (ns) {
    /**
      * @doc Mapzania.Event.Event
      *
      * Constructs an empty event and set the list of listeners to an empty list.
      *
      */

	function Event() {
		this.listeners = []
	}

    /**
      * @doc Mapzania.Event.add
      *
      * Adds a listener to this event instance
      *
      * @param listener (Function) A handle to the listener function. This function should take as input a single object, containing a "source" and "data" property.
      *                             Duplicate listeners are not allowed.
      */
	Event.prototype.add = function (listener) {
		this.listeners.push(listener)
	}

    /**
      * @doc Mapzania.Event.remove
      *
      * Removes a listener from this event instance
      *
      * @param listener (Function) A handle to the listener function to remove. 
      */
	Event.prototype.remove = function (listener) {
	    for (var i = 0; i < this.listeners.length; i++) {
	        if (this.listeners[i] === listener) {
	            this.listeners.splice(i, 1);
	            return;
	        }
	    }
	}

    /**
      * @doc Mapzania.Event.fire
      *
      * Fires this event, by calling all the listeners with the source reference and data provided
      *
      * @param source (Object) A handle to the object that initiated the event fire. 
      *
      * @param data (Object) A handle to the event data. 
      */
	Event.prototype.fire = function (source, data) {
		for (var i = 0; i < this.listeners.length; i++) {
			this.listeners[i]({ source: source, data: data })
		}
	}

	ns.Event = Event;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.Extent.*ClassDescription*
 *
 * Encapsulates a geographic extent. An extent defines a rectangular region and has Minimum X, Minimum Y, Maximum X and Maximum Y properties. 
 */
(function (ns) {

    /**
      * @doc Mapzania.Extent.Extent
      *
      * Constructor. 
      *
      *@param data (Object) This parameter indicated the initial size for this extent and has the form { minX: 10, minY: 10, maxX: 20, maxY: 20 }.
      */
    function Extent(data) {
        if (data) {
            this.minX = data.minX;
            this.minY = data.minY;
            this.maxX = data.maxX;
            this.maxY = data.maxY;
        }
    }

    /**
      * @doc Mapzania.Extent.getCenter
      *
      * Determines the center for this extent. 
      *
      * @returns (Mapzania.Point) The centroid for this extent instance.
      */
	Extent.prototype.getCenter = function () {
		var x = this.minX + (this.maxX - this.minX) / 2.0
		var y = this.minY + (this.maxY - this.minY) / 2.0
		return new ns.Point().read([[[x, y]]])
	}

    /**
      * @doc Mapzania.Extent.width
      *
      * Determines the width for this extent. 
      *
      * @returns (Number) The width for this extent.
      */
	Extent.prototype.width = function ()
	{
	    return Math.abs(this.maxX - this.minX); 
	}

    /**
      * @doc Mapzania.Extent.height
      *
      * Determines the height for this extent. 
      *
      * @returns (Number) The height for this extent.
      */
	Extent.prototype.height = function ()
	{
	    return Math.abs(this.maxY - this.minY); 
	}

    /**
      * @doc Mapzania.Extent.centerAt
      *
      * Centers this extent instance around a point.
      *
      * @param point (Mapzania.Point) A point instance around which this extent instance will be centered.
      */
	Extent.prototype.centerAt = function(point)
	{
	    var w = this.width();
	    var h = this.height();

	    this.minX = point.x - w / 2;
	    this.minY = point.y - h / 2;
	    this.maxX = point.x + w / 2;
	    this.maxY = point.y + h / 2;
	}

    /**
      * @doc Mapzania.Extent.grow
      *
      * Grow the extent and returns a resized extent instance. Note that this method does not change the width and height for this instance.
      *
      * @param proportionX (Number) The X-proportion that the extent needs to be grown.
      *                             A value of 2 indicates that the width of the new extent should be twice as much as the width for this instance.
      *                             A value 0f 0.5 indicates that the width of the new extent should be half the width for this instance.
      *
      * @param proportionY (Number) The Y-proportion that the extent needs to be grown.
      *                             A value of 2 indicates that the height of the new extent should be twice as much as the height for this instance.
      *                             A value 0f 0.5 indicates that the height of the new extent should be half the height for this instance.
      *
      * @returns (Mapzania.Extent) A new resized extent instance. 
      */
	Extent.prototype.grow = function (proportionX, proportionY) {
	    if (!proportionY)
	        proportionY = proportionX;

	    var box = $.extend(new Extent(), this);

	    var width  = (box.width() * (1-proportionX)) / 2;
	    var height = (box.height() * (1-proportionY)) / 2;
	    box.minX += width;
	    box.minY += height;
	    box.maxX -= width;
	    box.maxY -= height;
	    return box;
	}

    /**
      * @doc Mapzania.Extent.normalise
      *
      * Resizes this extent instance so that the aspect ratio of this extent matches the aspect ratio of a provided size.
      *
      * @param size (Object) An object in the form {width: 100, height: 100}
      */
	Extent.prototype.normalise = function (size) {
	    var ratioTarget = parseFloat(size.width) / parseFloat(size.height);
	    var ratioThis = this.width() / this.height();

	    var diffY = 0;
	    var diffX = 0;

	    if (ratioTarget > ratioThis) {
	        diffX = (this.height() * ratioTarget - this.width()) / 2;
	    }

	    if (ratioTarget < ratioThis) {
	        diffY = (this.width() / ratioTarget - this.height()) / 2;
	    }
	    
	    this.minY -= diffY;
	    this.maxY += diffY;
	    this.minX -= diffX;
	    this.maxX += diffX;
	}

    /**
      * @doc Mapzania.Extent.containsPoint
      *
      * Determines if a point is located inside the bounds of this extent.
      *
      * @param point (Mapzania.Point) A valid point instance.
      *
      * @returns (boolean) true to indicate that the point is located within the bounds of this extent. Otherwise false.
      */
	Extent.prototype.containsPoint = function (p) {
	    return  (p.x >= this.minX && p.x <= this.maxX) && (p.y >= this.minY && p.y <= this.maxY);
	}

    /**
      * @doc Mapzania.Extent.equals
      *
      * Determines the bounds of this extent is the same as the bounds of another extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      *
      * @returns (boolean) true to indicate that the bounds of the provided extent is the same as the bounds of this instance. Otherwise false.
      */
	Extent.prototype.equals = function (extent) {
	    return (this.minX == extent.minX && this.minY == extent.minY && this.maxX == extent.maxX && this.maxY == extent.maxY);
	}

    /**
      * @doc Mapzania.Extent.join
      *
      * Joins this extent instance with a provided extent. This extent instance is resized to contain both this and the provided extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      */
	Extent.prototype.join = function (extent) {
	    if (extent) {
	        this.minX = Math.min(this.minX, extent.minX);
	        this.minY = Math.min(this.minY, extent.minY);
	        this.maxX = Math.max(this.maxX, extent.maxX);
	        this.maxY = Math.max(this.maxY, extent.maxY);
	    }
	}

    /**
      * @doc Mapzania.Extent.isPoint
      *
      * Determines if this extent is represented by a point (i.e. have a width and a height of zero).
      *
      * @returns (boolean) true to indicate that the width and height of this extent is zero. Otherwise false.
      */
	Extent.prototype.isPoint = function () {
	    return (this.minX == this.maxY && this.minY == this.maxY);
	}

    /**
      * @doc Mapzania.Extent.intersects
      *
      * Determines if this extent intersects with another extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      *
      * @returns (boolean) true to indicate that the two extents intersect. Otherwise false.
      */
	Extent.prototype.intersects = function (extent) {
	    var notIntersect =
            (
                (Math.max(extent.minY, extent.maxY) < this.minY)
                ||
                (Math.min(extent.minY, extent.maxY) > this.maxY)
                ||
                (Math.max(extent.minX, extent.maxX) < this.minX)
                ||
                (Math.min(extent.minX, extent.maxX) > this.maxX)
            );

	    return !notIntersect; 
	}

    /**
      * @doc Mapzania.Extent.contains
      *
      * Determines if this extent is contained within another extent.
      *
      * @param extent (Mapzania.Extent) A valid extent instance.
      *
      * @returns (boolean) true to indicate that this extent instance is contained within the provided extent. Otherwise false.
      */
	Extent.prototype.contains = function (extent) {
	    if (!extent) {
	        return false;
	    }
        
	    var result =
            this.minX >= extent.minX
            &&
            this.minY >= extent.minY
            &&
            this.maxX <= extent.maxX
            &&
            this.maxY <= extent.maxY;

	    return result; 
	}

    /**
      * @doc Mapzania.Extent.translate
      *
      * Translate this Extent instance.
      *
      * @param x   (Number) The units to translate this Extent in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this Extent in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	Extent.prototype.translate = function (x, y) {
	    this.minX += x;
	    this.minY += y;
	    this.maxX += x;
	    this.maxY += y;
	}

	Extent.prototype.clone = function () {
	    return new Extent(this);
	}

	ns.Extent = Extent;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.Feature.*ClassDescription*
 *
 * Encapsulates a single Feature displayed on the interactive map. 
 * A feature is defined by an Id, a geometry, and a set of attributes name-value pairs. 
 */
(function (ns) {
    /**
      * @doc Mapzania.Feature.Feature
      *
      * Constructs an empty Feature instance. This Id and geometry is initialized to null, and the attributes property is set to an empty object. The label visibilty 
      * flag for the feature is set to true.
      *
      */
    function Feature() {

        /**
          * @doc Mapzania.Feature.id (Object)
          *
          * A unique identifier for this feature, relative to the layer.
          */
        this.id = null;

        /**
          * @doc Mapzania.Feature.id (Mapzania.Geometry)
          *
          * The geometry associated with this feature instance.
          */
        this.geometry = null;

        /**
          * @doc Mapzania.Feature.attributes (Object)
          *
          * Attributes associated with the feature. Object keys are attributes names.
          */
        this.attributes = {};

        /**
          * @doc Mapzania.Feature.dynamicProperties (Object)
          *
          * Internal attributes associated with this feature instance. This object typically has the form 
          * { labelIsVisible: true}, where labelIsVisible indicates that the label for this feature instance is
          * visible. 
          */
        this.dynamicProperties = {};
        this.dynamicProperties.labelIsVisible = true;
    }

    ns.FeatureTypes = {}

    /**
      * @doc Mapzania.Feature.read
      *
      * Internal utility method. Not to be used by applications. 
      *
      */
    Feature.prototype.read = function (geometryType, layerFeed, index) {
        this.id = layerFeed.ids[index];

        if (!geometryType) {
            geometryType = ns.FeatureTypes[layerFeed.geometryTypes[index]];
        }

        if (layerFeed.geometries[index][0]) {
            this.geometry = new geometryType();
            this.geometry.read(layerFeed.geometries[index]);
        } else {
            this.geometry = null;
        }

        var attribs = layerFeed.attributes[index];

        var fields = layerFeed.attributeNames;
        for (var i = 0; i < fields.length; i++)
            this.attributes[fields[i]] = attribs[i];

        this.dynamicProperties.labelIsVisible = (layerFeed.dynamicProperties[index] == 1);

        return this;
    }

    /**
      * @doc Mapzania.Feature.readFromVectorServer
      *
      * Internal utility method. Not to be used by applications. 
      *
      */
    Feature.prototype.readFromVectorServer = function (attributeNames, featureData) {
        this.id = featureData[2];
        geometryType = ns.FeatureTypes[featureData[4]];
        

        if (featureData[1][0]) {
            this.geometry = new geometryType();
            this.geometry.read(featureData[1]);
        } else {
            this.geometry = null;
        }

        var attribs = featureData[0];

        var fields = attributeNames;
        for (var i = 0; i < fields.length; i++)
            this.attributes[fields[i]] = attribs[i];

        this.dynamicProperties.labelIsVisible = (featureData[3] == 1);

        return this;
    }

    /**
      * @doc Mapzania.Feature.clone
      *
      * Clones this feature instance, doing a deep clone.
      *
      * @return (Mapzania.Feature) A deep clone of this feature instance.
      */
    Feature.prototype.clone = function () {
        var result = new Mapzania.Feature();

        result.id = this.id;
        if (this.geometry)
            result.geometry = this.geometry.clone();
        $.extend(true, result.attributes, this.attributes);
        $.extend(true, result.dynamicProperties, this.dynamicProperties);
        return result;
    }

    ns.Feature = Feature;
})(Mapzania);


(function (ns) {

    function FieldExpressionEval(vectorLayer) {
        this.dictFormatters = {};
        var me = this;

        if (vectorLayer.fields) {
            vectorLayer.fields.forEach(function (cur) {
                if (cur.expression) {
                    var formatter = new Mapzania.ExpressionFormatter(cur.expression);
                    me.dictFormatters[cur.key] = formatter;
                }
            });
        }
    }

    FieldExpressionEval.prototype.populateCalculatedAttributes = function (features) {
        var me = this;
        features.forEach(function (feature) {
            for (var key in me.dictFormatters) {
                if (!feature.attributes.hasOwnProperty(key)) {
                    var formatter = me.dictFormatters[key];
                    if (formatter.hasValue(feature)) {
                        feature.attributes[key] = formatter.calculateValue(feature);
                    }
                    else {
                        feature.attributes[key] = null;
                    }
                }
            }
        });
    }

    ns.FieldExpressionEval = FieldExpressionEval;

})(Mapzania);

(function (ns) {

    function ExpressionFormatter(expression) {
        this.formatters = [];
        var me = this;
        var list = expression.split(/({[.-}]*})/);

        var util = Mapzania.Util;

        list.forEach(function (cur) {
            if (util.startsWith(cur, "{") && util.endsWith(cur, "}")) {
                me.formatters.push(new Mapzania.ExprSegmentFormatter(cur));
            }
            else {
                me.formatters.push(new Mapzania.ConstSegmentFormatter(cur));
            }
        });
    }

    ExpressionFormatter.prototype.hasValue = function (feature) {
        var result = true;

        this.formatters.forEach(function (cur) {
            if (!cur.hasValue(feature)) {
                result = false;
            }
        });

        return result;
    }

    ExpressionFormatter.prototype.calculateValue = function (feature) {
        var result = "";

        this.formatters.forEach(function (cur) {
            result += cur.calculateValue(feature);
        });

        return result;
    }

    ns.ExpressionFormatter = ExpressionFormatter;
})(Mapzania);

(function (ns) {

    function ConstSegmentFormatter(value) {
        this.value = value;
    }

    ConstSegmentFormatter.prototype.hasValue = function (feature) {
        return true;
    }

    ConstSegmentFormatter.prototype.calculateValue = function (feature) {
        return this.value;
    }

    ns.ConstSegmentFormatter = ConstSegmentFormatter;
})(Mapzania);



(function (ns) {

    function ExprSegmentFormatter(expr) {
        this.expr = expr.slice(1, expr.length - 1);

        var list = this.expr.split(':');
        if (list.length > 0)
        {
            this.fieldName = list[0];
        }

        if (list.length > 1)
        {
            this.format = list[1];
        }
    }

    ExprSegmentFormatter.prototype.hasValue = function (feature) {
        if (feature.attributes.hasOwnProperty(this.fieldName))
        {
            var v = feature.attributes[this.fieldName];
            var value = "";
            if (v) {
                value = new String(v);
            }
            return value.length > 0;
        }

        return false;
    }

    ExprSegmentFormatter.prototype.calculateValue = function (feature) {
        var result = "";
        if (this.fieldName != null && feature.attributes.hasOwnProperty(this.fieldName)) {
            var value = feature.attributes[this.fieldName];
            result = value;

            if (this.format) {
                if (Mapzania.Util.endsWith(this.format, "ED")) {

                    var trunc = Mapzania.Util.truncate;

                    var d = parseFloat(value);
                    var i = trunc(d);
                    var p = Math.abs(d) - trunc(Math.abs(d));
                    var digits = parseInt(this.format.replace("ED", ""));
                    if (digits == 0) {
                        result = i;
                    }
                    else {
                        result = i + "." + trunc(p * Math.pow(10, digits));
                    }
                }
                else {
                    console.error("Invalid field expression format (originating from map schema file): " + this.expr);
                }
            }
        }

        return result;
    }

    ns.ExprSegmentFormatter = ExprSegmentFormatter;
})(Mapzania);



if (!Mapzania)
    var Mapzania = {};

/**
 * @doc Mapzania.Geometry.*ClassDescription*
 *
 * Provides an interface specification for all Mapzania Geometry classes. Applications do not make use of this class directly, but will use the following implementations to represent the appropriate geometry:
 * <ul>
 *   <li>Mapzania.Point</li>
 *   <li>Mapzania.Line</li>
 *   <li>Mapzania.Polygon</li>
 *   <li>Mapzania.MultiPoint</li>
 *   <li>Mapzania.MultiLine</li>
 *   <li>Mapzania.MultiPolygon</li>
 * </ul>
 *
 */
(function (ns) {
    /**
      * @doc Mapzania.Geometry.Geometry
      *
      * Constructor.
      *
      * This class is provided here only as an interface specification for all Mapzania Geometry classes. The supported geometry classes are:
      * <ul>
      *   <li>Mapzania.Point</li>
      *   <li>Mapzania.Line</li>
      *   <li>Mapzania.Polygon</li>
      *   <li>Mapzania.MultiPoint</li>
      *   <li>Mapzania.MultiLine</li>
      *   <li>Mapzania.MultiPolygon</li>
      * </ul>
      */
    function Geometry() { 
    }

    /**
      * @doc Mapzania.Geometry.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Geometry.prototype.read = function (data) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
    Geometry.prototype.getExtent = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    Geometry.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
    Geometry.prototype.intersects = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Geometry.prototype.getPath = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
    Geometry.prototype.getSize = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
    Geometry.prototype.getCentroid = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Geometry.prototype.getGeometryType = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Geometry.prototype.getType = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Geometry) A deep clone of this instance.
      */

    Geometry.prototype.clone = function () {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Geometry.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */
    Geometry.prototype.translate = function (x, y) {
        throw "Not implemented";
    }

    ns.Geometry = Geometry;

    ns.FeatureTypes["-1"] = Geometry;
    ns.FeatureTypes["1000"] = Geometry;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.Line.*ClassDescription*
 *
 * Encapsulates a single Line geometry. A line geometry defines a set of connected points. 
 */
(function (ns) {

    /**
      * @doc Mapzania.Line.Line
      *
      * Constructs a Mapzania Geometry.
      *
      * @param points (Array) An array of Mapzania.Point instances.
      */
    function Line(points) {
        if (!points)
            this.points = [];
        else
            this.points = points;
    }

    /**
      * @doc Mapzania.Line.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Line.prototype.read = function (data) {
        for (var i = 1; i < data[0][0].length; i += 2)
            this.points.push(new ns.Point().read([[[data[0][0][i - 1], data[0][0][i]]]]));

        return this;
    }

    /**
      * @doc Mapzania.Line.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
    Line.prototype.getExtent = function () {
        var algo = new ns.Algo();
        return algo.getExtentFromPoints(this.points);
    }

    /**
      * @doc Mapzania.Line.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    Line.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Line.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
    Line.prototype.intersects = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Line.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Line.prototype.getPath = function () {
        var line = [];
        for (var i = 0; i < this.points.length; i++) {
            var pnt = this.points[i];
            line.push(pnt.x);
            line.push(pnt.y);
        }

        return [[line]];
    }

    /**
      * @doc Mapzania.Line.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
    Line.prototype.getSize = function () {
        return this.points.length;
    }

    /**
      * @doc Mapzania.Line.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
    Line.prototype.getCentroid = function () {
        //TODO:Better centroid algorithm
        return this.getExtent().getCenter();
    }

    /**
      * @doc Mapzania.Line.rotate
      *
      * Rotates a line around an origin, by a number of degrees (provided in radians). This method returns a new Line instance.
      *
      * @param origin (Mapzania.Point) The origin point.
      *
      * @param angleRandians (Number) The angle, in radians. A positive value indicates an anti-clockwise direction.
      *
      * @return (Mapzania.Line) A rotated line instance.
      */

    Line.prototype.rotate = function (origin, angleRandians) {
        var newPoints = [];
        this.points.forEach(function (cur) {
            newPoints.push(cur.rotate(origin, angleRandians));
        });
        return new Mapzania.Line(newPoints);
    }

    /**
      * @doc Mapzania.Line.getLineSegments
      *
      * Returns a list of line segments that makes up this line geometry.
      *
      * @param extent   (Mapzania.Extent) An optional extent. Only extents visible in this extent will be included in the result of this function.
      *                                     All segments are included when this parameter is set to null.
      *
      * @return (Mapzania.LineSegment []) A list of line segment instances.
      */

    Line.prototype.getLineSegments = function (extent) {
        var res = [];
        for (var i = 0; i < this.points.length - 1; i++) {
            var p1 = this.points[i];
            var p2 = this.points[i + 1];
            var ls = new Mapzania.LineSegment(p1, p2);
            if (extent) {
                if (ls.isVisibleInExtent(extent)) //TODO: Get the visible extent
                    res.push(ls);
            } else {
                res.push(ls);
            }
        }
        return res;
    }

    /**
      * @doc Mapzania.Line.fromLineSegments
      *
      * Populates this line geometry from an array of connected line segments.
      *
      * @param segments   (Mapzania.LineSegment []) An array of connected line segment instances. 
      *
      */

    Line.prototype.fromLineSegments = function (segments) {
        this.points = [];
        this.points.push(segments[0].p1);
        var me = this;
        segments.forEach(function (cur) {
            me.points.push(cur.p2);
        });
    }

    /**
      * @doc Mapzania.Line.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Line.prototype.getGeometryType = function () {
        return 2;
    }

    /**
      * @doc Mapzania.Line.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Line.prototype.getType = function () {
        return 2;
    }

    /**
      * @doc Mapzania.Line.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Line) An exact deep clone of this instance.
      */

    Line.prototype.clone = function () {
        var result = new Mapzania.Line();
        result.read(this.getPath());
        return result;
    }

    /**
      * @doc Mapzania.Line.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

    Line.prototype.translate = function (x, y) {
        for (var i = 0; i < this.points.length; i++) {
            this.points[i].translate(x, y);
        }
    }

    /**
      * @doc Mapzania.Line.getLength
      *
      * Calculates the length of the line.
      *
      * @return (Number) The line length.
      */

    Line.prototype.getLength = function () {
        var r = 0.0;
        for (var i = 0; i < this.points.length - 1; i++) {
            r += this.points[i].distanceTo(this.points[i + 1]);
        }
        return r;
    }

    /**
      * @doc Mapzania.Line.reverse
      *
      * Calculates a reversed instance of this line.
      *
      * @return (Mapzania.Line) The line instance.
      */

    Line.prototype.reverse = function () {
        var points = this.points.slice(0);
        points.reverse();
        return new Mapzania.Line(points);
    }

    /**
      * @doc Mapzania.Line.getPointAndSegmentAtLength
      *
      * Calculate the line segment and the point on the line that corresonds to the length of the line up to a provided length (starting at the first point on the line).
      *
      * @param length   (Number) A length. 
      *
      * @return (Object) An object in the form {segment: x, point: y}, where segment is an instance of Mapzania.LineSegment and point an instance of Mapzania.Point.
      *                  Applications may assume that the point is present on the corresponding line segment. 
      *                  The return value may be null if the length is greater than the length of this line.
      */

    Line.prototype.getPointAndSegmentAtLength = function (length) {
        var l = 0.0;
        var result = null;
        for (var i = 0; i < this.points.length - 1; i++) {
            var p1 = this.points[i];
            var p2 = this.points[i + 1];
            l += p1.distanceTo(p2);
            if (l >= length) {
                var delta = l - length;
                var d = p1.distanceTo(p2);
                var f = 1 - (delta / d);

                var pt = new Mapzania.Point(p1.x, p1.y);
                pt.translate((p2.x - p1.x) * f, (p2.y - p1.y) * f);

                result = { segment: new Mapzania.LineSegment(p1, p2), point: pt };
                return result;
            }
        }
        return result;
    }

    ns.Line = Line;

    ns.FeatureTypes["2"] = Line;

})(Mapzania);

if (!Mapzania)
    var Mapzania = {};

/**
 * @doc Mapzania.LineSegment.*ClassDescription*
 *
 * Encapsulates a single line segment. A line segment defines two connecting points. 
 */
(function (ns) {

    /**
      * @doc Mapzania.LineSegment.LineSegment
      *
      * Constructor. Constructs a Mapzania LineSegment.
      */
    function LineSegment(p1, p2) {
        this.p1 = p1;
        this.p2 = p2;
    }


    /**
      * @doc Mapzania.LineSegment.getExtent
      *
      * Determines the extent for this LineSegment.
      *
      * @return (Mapzania.Extent) The extent for this LineSegment.
      */
    LineSegment.prototype.getExtent = function () {
        if (!p1 | !p2) return null;

        var res = new Mapzania.Extent(
                {
                    minX: Math.min(p1.x, p2.x),
                    minY: Math.min(p1.y, p2.y),
                    maxX: Math.max(p1.x, p2.x),
                    maxY: Math.max(p1.y, p2.y)
                }
            );
        return res;
    }

    /**
      * @doc Mapzania.LineSegment.distanceTo
      *
      * Determines the closest distance from this LineSegment to another geometry. NB: This method is 
      * not implemented from all geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this and LineSegment provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    LineSegment.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.LineSegment.getSize
      *
      * Determines the total number of points in this LineSegment.
      *
      * @return (Number) The number of points in this LineSegment.
      */
    LineSegment.prototype.getSize = function () {
        return 2;
    }

    /**
      * @doc Mapzania.Geometry.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
    LineSegment.prototype.getCentroid = function () {
        return new Mapzania.Point((this.p1.x + this.p2.x) / 2, (this.p1.y + this.p2.y) / 2);
    }

    LineSegment.prototype.getIntersectionPoint = function (lineSegment) {
        // http://jsfiddle.net/justin_c_rounds/Gd2S2/light/
        var p1 = this.p1;
        var p2 = this.p2;
        var p3 = lineSegment.p1;
        var p4 = lineSegment.p2;

        var denominator, a, b, numerator1, numerator2, result = {
            x: null,
            y: null,
            onLine1: false,
            onLine2: false
        };
        denominator = ((p4.y - p3.y) * (p2.x - p1.x)) - ((p4.x - p3.x) * (p2.y - p1.y));
        if (denominator == 0) {
            return null;
        }
        a = p1.y - p3.y;
        b = p1.x - p3.x;
        numerator1 = ((p4.x - p3.x) * a) - ((p4.y - p3.y) * b);
        numerator2 = ((p2.x - p1.x) * a) - ((p2.y - p1.y) * b);
        a = numerator1 / denominator;
        b = numerator2 / denominator;

        // if we cast these lines infinitely in both directions, they intersect here:
        result.x = p1.x + (a * (p2.x - p1.x));
        result.y = p1.y + (a * (p2.y - p1.y));
        /*
            // it is worth noting that this should be the same as:
            x = p3.x + (b * (p4.x - p3.x));
            y = p3.x + (b * (p4.y - p3.y));
        */
        // if line1 is a segment and line2 is infinite, they intersect if:
        if (a > 0 && a < 1) {
            result.onLine1 = true;
        }
        // if line2 is a segment and line1 is infinite, they intersect if:
        if (b > 0 && b < 1) {
            result.onLine2 = true;
        }

        if (result.onLine1 && result.onLine2)
            return new Mapzania.Point(result.x, result.y);
        else
            return null;
    }

    LineSegment.prototype.isVisibleInExtent = function (extent) {
        if (extent.containsPoint(this.p1)) return true;
        if (extent.containsPoint(this.p2)) return true;

        var ls1 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.minY), new Mapzania.Point(extent.maxX, extent.minY));
        var ls2 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.minY), new Mapzania.Point(extent.maxX, extent.maxY));
        var ls3 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.maxY), new Mapzania.Point(extent.minX, extent.maxY));
        var ls4 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.maxY), new Mapzania.Point(extent.minX, extent.minY));

        return (this.getIntersectionPoint(ls1) != null || this.getIntersectionPoint(ls2) != null || this.getIntersectionPoint(ls3) != null || this.getIntersectionPoint(ls4) != null);
    }

    LineSegment.prototype.getVisibleIntersectionInExtent = function (extent) {
        // Both points inside the polygon
        if (extent.containsPoint(this.p1) && extent.containsPoint(this.p2)) {
            return this;
        }

        // Both points outside the polygon
        if (!extent.containsPoint(this.p1) && !extent.containsPoint(this.p2)) {
            return null;
        }
        
        // Now we know that one point is inside and the other outside the extent
        var ls1 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.minY), new Mapzania.Point(extent.maxX, extent.minY));
        var ls2 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.minY), new Mapzania.Point(extent.maxX, extent.maxY));
        var ls3 = new Mapzania.LineSegment(new Mapzania.Point(extent.maxX, extent.maxY), new Mapzania.Point(extent.minX, extent.maxY));
        var ls4 = new Mapzania.LineSegment(new Mapzania.Point(extent.minX, extent.maxY), new Mapzania.Point(extent.minX, extent.minY));

        // Find intersection point with the extent
        var p = this.getIntersectionPoint(ls1);
        if (!p) p = this.getIntersectionPoint(ls2);
        if (!p) p = this.getIntersectionPoint(ls3);
        if (!p) p = this.getIntersectionPoint(ls4);
        if (!p) console.error("Unexpected TS error: No point intersection with extent. Not expected.");

        if (extent.containsPoint(this.p1)) {
            return new Mapzania.LineSegment(this.p1, p);
        } else {
            return new Mapzania.LineSegment(p, this.p2);
        }
    }

    LineSegment.prototype.length = function () {
        return this.p1.distanceTo(this.p2);
    }

    /**
	 * @return 
	 * 		Computes and returns the angle of the line in radians, when measured 
	 * 		counterclockwise from the x axis. The return value is between positive pi and negative pi. 
	 */
    LineSegment.prototype.getAngleRadians = function () {
        var x = this.p2.x - this.p1.x;
        var y = this.p2.y - this.p1.y;
        return Math.atan2(y, x);
    }

    ns.LineSegment = LineSegment;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.MultiLine.*ClassDescription*
 *
 * Encapsulates a single Multi-Line geometry. A multi-line geometry defines a set of line geometries. 
 */
(function (ns) {

    /**
      * @doc Mapzania.MultiLine.MultiLine
      *
      * Constructs a Mapzania MultiLine Geometry.
      * 
      */
	function MultiLine() {
		this.lines = []
	}

    /**
      * @doc Mapzania.MultiLine.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiLine.prototype.read = function (data) {
	    for (var i = 0; i < data[0].length; i++) {
	        var oneLine = [[[]]];
	        oneLine[0][0] = data[0][i];
	        this.lines.push(new ns.Line().read(oneLine));
	    }

		return this;
	}

    /**
      * @doc Mapzania.MultiLine.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	MultiLine.prototype.getExtent = function () {
		var algo = new ns.Algo();
		return algo.getExtentFromLines(this.lines);
	}

    /**
      * @doc Mapzania.MultiLine.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	MultiLine.prototype.distanceTo = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiLine.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	MultiLine.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiLine.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiLine.prototype.getPath = function () {
	    var lines = [];

	    for (var i = 0; i < this.lines.length; i++) {
	        var line = this.lines[i];
	        var arr = [];
	        for (var j = 0; j < line.points.length; j++) {
	            var pnt = line.points[j];
	            arr.push(pnt.x);
	            arr.push(pnt.y);
	        }
            lines.push(arr);
	    }

	    return [lines];
	}

    /**
      * @doc Mapzania.MultiLine.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	MultiLine.prototype.getSize = function () {
		var res = 0;

		for (var i = 0; i < this.lines.length; i++) {
			res += this.lines[i].getSize();
		}

		return res;
	}

    /**
      * @doc Mapzania.MultiLine.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	MultiLine.prototype.getCentroid = function () {
	    //TODO:Better centroid algorithm
	    return this.getExtent().getCenter();
	}

    /**
      * @doc Mapzania.MultiLine.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiLine.prototype.getGeometryType = function () {
	    return 5;
	}

    /**
      * @doc Mapzania.MultiLine.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiLine.prototype.getType = function () {
	    return 5;
	}

    /**
      * @doc Mapzania.MultiLine.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.MultiLine) An exact deep clone of this instance.
      */

	MultiLine.prototype.clone = function () {
	    var result = new Mapzania.MultiLine();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.MultiLine.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	MultiLine.prototype.translate = function (x, y) {
	    this.lines.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	}

	ns.MultiLine = MultiLine;

	ns.FeatureTypes["5"] = MultiLine;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.MultiPoint.*ClassDescription*
 *
 * Encapsulates a single Multi-Point geometry. A multi-point geometry defines a set of point geometries. 
 */
(function (ns) {

    /**
      * @doc Mapzania.MultiPoint.MultiPoint
      *
      * Constructs a Mapzania MultiPoint Geometry.
      * 
      */
	function MultiPoint() {
		this.points = []
	}

    /**
      * @doc Mapzania.MultiPoint.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPoint.prototype.read = function (data) {
		for (var i = 1; i < data[0][0].length; i += 2)
			this.points.push(new ns.Point().read([[[data[0][0][i - 1], data[0][0][i]]]]))

		return this
	}

    /**
      * @doc Mapzania.MultiPoint.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	MultiPoint.prototype.getExtent = function () {
		var algo = new ns.Algo();
		return algo.getExtentFromPoints(this.points);
	}

    /**
      * @doc Mapzania.MultiPoint.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	MultiPoint.prototype.distanceTo = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPoint.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	MultiPoint.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPoint.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPoint.prototype.getPath = function () {
	    var points = [];
	    for (var i = 0; i < this.points.length; i++) {
	        var pnt = this.points[i];
	        points.push(pnt.x);
	        points.push(pnt.y);
	    }

	    return [[points]];
	}

    /**
      * @doc Mapzania.MultiPoint.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	MultiPoint.prototype.getSize = function () {
		return this.points.length;
	}

    /**
      * @doc Mapzania.MultiPoint.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	MultiPoint.prototype.getCentroid = function () {
	    //TODO:Better centroid algorithm
	    return this.getExtent().getCenter();
	}

    /**
      * @doc Mapzania.MultiPoint.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPoint.prototype.getGeometryType = function () {
	    return 4;
	}

    /**
      * @doc Mapzania.MultiPoint.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPoint.prototype.getType = function () {
	    return 4;
	}

    /**
      * @doc Mapzania.MultiPoint.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.MultiPoint) An exact deep clone of this instance.
      */

	MultiPoint.prototype.clone = function () {
	    var result = new Mapzania.MultiPoint();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.MultiPoint.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	MultiPoint.prototype.translate = function (x, y) {
	    this.points.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	}

	ns.MultiPoint = MultiPoint;

	ns.FeatureTypes["4"] = MultiPoint;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.MultiPolygon.*ClassDescription*
 *
 * Encapsulates a single Multi-Polygon geometry. A multi-polygon geometry defines a set of polygon geometries. 
 */
(function (ns) {

    /**
      * @doc Mapzania.MultiPolygon.MultiPolygon
      *
      * Constructs a Mapzania Geometry.
      * 
      */
	function MultiPolygon() {
		this.polygons = [];
	}

    /**
      * @doc Mapzania.MultiPolygon.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPolygon.prototype.read = function (data) {
	    for (var i = 0; i < data.length; i++) {
	        var onePoly = [[[]]];
	        onePoly[0] = data[i];
	        this.polygons.push(new ns.Polygon().read(onePoly));
	    }
		return this;
	}

    /**
      * @doc Mapzania.MultiPolygon.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	MultiPolygon.prototype.getExtent = function () {
		var algo = new ns.Algo();
		var ext = algo.getExtentFromLines([this.polygons[0].shell]);

		for (var i = 1; i < this.polygons.length; i++) {
			var polyExt = algo.getExtentFromLines([this.polygons[i].shell]);

			if (polyExt.minX < ext.minX)
				ext.minX = polyExt.minX;
			if (polyExt.minY < ext.minY)
				ext.minY = polyExt.minY;
			if (polyExt.maxX > ext.maxX)
				ext.maxX = polyExt.maxX;
			if (polyExt.maxY > ext.maxY)
				ext.maxY = polyExt.maxY;
		}

		return ext;
	}

    /**
      * @doc Mapzania.MultiPolygon.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	MultiPolygon.prototype.distanceTo = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPolygon.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	MultiPolygon.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.MultiPolygon.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	MultiPolygon.prototype.getPath = function () {
	    var res = [];
	    for (var i = 0; i < this.polygons.length; i++) 
	        res.push(this.polygons[i].getPath());

	    return res;
	}

    /**
      * @doc Mapzania.MultiPolygon.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	MultiPolygon.prototype.getSize = function () {
		var res = 0;

		for (var i = 0; i < this.polygons.length; i++) {
			res += this.polygons[i].getSize();
		}

		return res;
	}

    /**
      * @doc Mapzania.MultiPolygon.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	MultiPolygon.prototype.getCentroid = function () {
	    //TODO:Better centroid algorithm
	    return this.getExtent().getCenter();
	}

    /**
      * @doc Mapzania.MultiPolygon.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPolygon.prototype.getGeometryType = function () {
	    return 6;
	}

    /**
      * @doc Mapzania.MultiPolygon.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	MultiPolygon.prototype.getType = function () {
	    return 6;
	}

    /**
      * @doc Mapzania.MultiPolygon.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.MultiPolygon) An exact deep clone of this instance.
      */

	MultiPolygon.prototype.clone = function () {
	    var result = new Mapzania.MultiPolygon();
	    for (var i = 0; i < this.polygons.length; i++) {
	        result.polygons.push(this.polygons[i].clone());
	    }
	    //result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.MultiPolygon.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	MultiPolygon.prototype.translate = function (x, y) {
	    this.polygons.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	}

    /**
      * @doc Mapzania.MultiPolygon.getArea
      *
      * Calculates the area of this multi polygon.
      *
      * @return (Number) The area of this multi polygon (excluding the area taken up by holes).
      */

	MultiPolygon.prototype.getArea = function () {
	    var sum = 0;
	    this.polygons.forEach(function (cur) {
	        sum += cur.getArea();
	    });
	    return sum;
	}

	ns.MultiPolygon = MultiPolygon;

	ns.FeatureTypes["6"] = MultiPolygon;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.Point.*ClassDescription*
 *
 * Encapsulates a single Point geometry. A point geometry defines a x- and y- coordinate. 
 */
(function (ns) {

    /**
      * @doc Mapzania.Point.Geometry
      *
      * Constructs a Mapzania Point Geometry.
      *
      * @param x (Number) The x-coordinate for the point.
      * @param y (Number) The y-coordinate for the point.
      */
	function Point(x, y) {
		this.x = !x ? 0 : x;
		this.y = !y ? 0 : y;
	}

    /**
      * @doc Mapzania.Point.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
	Point.prototype.read = function (data) {
	    this.x = data[0][0][0];
		this.y = data[0][0][1];
		return this;
	}

    /**
      * @doc Mapzania.Point.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
	Point.prototype.getExtent = function () {
		return new ns.Extent({
			minX: this.x,
			minY: this.y,
			maxX: this.x,
			maxY: this.y
		});
	}

    /**
      * @doc Mapzania.Point.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
	Point.prototype.distanceTo = function (point) {
	    var dx = this.x - point.x;
	    var dy = this.y - point.y;

	    return Math.sqrt(dx * dx + dy * dy);
	}

    /**
      * @doc Mapzania.Point.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
	Point.prototype.intersects = function (geometry) {
	    throw "Not implemented";
	}

    /**
      * @doc Mapzania.Point.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
	Point.prototype.getPath = function () {
	    return [[[this.x, this.y]]];
	}

    /**
      * @doc Mapzania.Point.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	Point.prototype.getSize = function () {
		return 1;
	}

    /**
      * @doc Mapzania.Point.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	Point.prototype.getCentroid = function () {
	    return new Point(this.x, this.y);
	}

    /**
      * @doc Mapzania.Point.rotate
      *
      * Rotates a point around an origin, by a number of degrees (provided in radians). This method returns a new Point instance.
      *
      * @param origin (Mapzania.Point) The origin point.
      *
      * @param angleRandians (Number) The angle, in radians. A positive value indicates an anti-clockwise direction.
      *
      * @return (Mapzania.Point) A rotated point instance.
      */

	Point.prototype.rotate = function (origin, angleRandians) {
	    var _cos = Math.cos(angleRandians);
	    var _sin = Math.sin(angleRandians);

	    // Rotate the point (counter clockwise)
	    var x = (this.x - origin.x) * _cos - (this.y - origin.y) * _sin + origin.x;
	    var y = (this.x - origin.x) * _sin + (this.y - origin.y) * _cos + origin.y;

	    return new Mapzania.Point(x, y);
	}

    /**
      * @doc Mapzania.Point.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Point.prototype.getGeometryType = function () {
	    return 1;
	}

    /**
      * @doc Mapzania.Point.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Point.prototype.getType = function () {
	    return 1;
	}

    /**
      * @doc Mapzania.Point.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Point) An exact deep clone of this instance.
      */

	Point.prototype.clone = function () {
	    var result = new Mapzania.Point();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.Point.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	Point.prototype.translate = function (x, y) {
	    this.x += x;
	    this.y += y;
	}

	ns.Point = Point;

	ns.FeatureTypes["1"] = Point;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.Polygon.*ClassDescription*
 *
 * Encapsulates a single Polygon geometry. A polygon geometry defines a set of non-intersecting lines in a closed loop. 
 */
(function (ns) {

    /**
      * @doc Mapzania.Polygon.Polygon
      *
      * Constructs a Mapzania Polygon Geometry.
      * 
      */
    function Polygon() {
        this.holes = [];
        this.shell = new ns.Line();
    }

    /**
      * @doc Mapzania.Polygon.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Polygon.prototype.read = function (data) {
        if (data[0]) {
            for (var i = 0; i < data[0].length; i++) {
                var line = new ns.Line().read([[data[0][i]]]);
                if (i == 0)
                    this.shell = line;
                else
                    this.holes.push(line);
            }
        }

        return this;
    }

    /**
      * @doc Mapzania.Polygon.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
    Polygon.prototype.getExtent = function () {
        var algo = new ns.Algo();
        var ext = algo.getExtentFromLines([this.shell]);

        return ext;
    }

    /**
      * @doc Mapzania.Polygon.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    Polygon.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Polygon.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
    Polygon.prototype.intersects = function (geometry) {
        if (geometry instanceof Mapzania.Polygon) {
            return (new Mapzania.Intersections()).intersectsPolyPoly(this, geometry);
        }

        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Polygon.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Polygon.prototype.getPath = function () {
        var lines = [];

        lines.push(this.shell.getPath()[0][0]);

        for (var i = 0; i < this.holes.length; i++)
            lines.push(this.holes[i].getPath()[0][0]);

        return [lines];
    }

    /**
      * @doc Mapzania.Polygon.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
	Polygon.prototype.getSize = function () {
		var res = 0;

		res += this.shell.getSize();

		for (var i = 0; i < this.holes.length; i++) {
			res += this.holes[i].getSize();
		}

		return res;
	}

    /**
      * @doc Mapzania.Polygon.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
	Polygon.prototype.getCentroid = function () {
	    var dx = Number.MAX_VALUE;
	    var dy = Number.MAX_VALUE;
	    this.shell.points.forEach(function (cur) {
	        if (cur.x < dx) dx = cur.x;
	        if (cur.y < dy) dy = cur.y;
	    });

	    var paths = [];
	    this.shell.points.forEach(function (cur) {
	        var p = new ns.Point(cur.x, cur.y);
	        p.translate(-dx, -dy);
	        paths.push(p);
	    });

	    var X = 0;
	    var Y = 0;
	    var AA = 0;
	    for (var i = 0; i < paths.length - 1; i++) {
	        X += (paths[i].x + paths[i + 1].x) * (paths[i].x * paths[i + 1].y - paths[i + 1].x * paths[i].y);

	        Y += (paths[i].y + paths[i + 1].y) * (paths[i].x * paths[i + 1].y - paths[i + 1].x * paths[i].y);

	        AA += (paths[i].x * paths[i + 1].y - paths[i + 1].x * paths[i].y);
	    }
	    var A = AA * 0.5;
	    var x = 1 / (6 * A) * X;
	    var y = 1 / (6 * A) * Y;

	    return new ns.Point(x + dx, y + dy);
	}

    /**
      * @doc Mapzania.Polygon.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Polygon.prototype.getGeometryType = function () {
	    return 3;
	}

    /**
      * @doc Mapzania.Polygon.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

	Polygon.prototype.getType = function () {
	    return 3;
	}

    /**
      * @doc Mapzania.Polygon.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Polygon) An exact deep clone of this instance.
      */

	Polygon.prototype.clone = function () {
	    var result = new Mapzania.Polygon();
	    result.read(this.getPath());
	    return result;
	}

    /**
      * @doc Mapzania.Polygon.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

	Polygon.prototype.translate = function (x, y) {
	    this.holes.forEach(function (cur) {
	        cur.translate(x, y);
	    });
	    this.shell.translate(x, y);
	}

    /**
      * @doc Mapzania.Polygon.getArea
      *
      * Calculates the area of this polygon.
      *
      * @return (Number) The area of this polygon (excluding the area taken up by holes).
      */

	Polygon.prototype.getArea = function () {
	    var summedArea = this._areaPoints(this.shell.points);
	    var summedHoleArea = 0;
	    var me = this;
	    this.holes.forEach(function (cur) {
	        summedHoleArea += me._areaPoints(cur.points);
	    });

	    return summedArea - summedHoleArea;
	}

	Polygon.prototype._areaPoints = function (points) {
	    var summedArea = 0;

	    var funcArea = function (p1, p2) {
	        var width = p2.x - p1.x;
	        var height = (p2.y + p1.y) / 2;
	        return width * height;
	    };

	    for (var i = 0; i <= (points.length - 2) ; i++) {
	        summedArea += funcArea(points[i], points[i + 1]);
	    }
	    summedArea += funcArea(points[points.length - 1], points[0]);

	    return Math.abs(summedArea);
	}

	ns.Polygon = Polygon;

	ns.FeatureTypes["3"] = Polygon;

})(Mapzania)

var Mapzania = (function (ns) {
    var me;

    function FeatureStore() {

        this.changed = new ns.Event();

        var me = this;

        var store = {};

        this.set = function (layerKey, features, suppressChangedEvent) {
            store[layerKey] = features;
            if (!suppressChangedEvent)
                me.changed.fire(me, layerKey);
        }

        this.get = function (layerKey) {
            return store[layerKey];
        }
    }

    ns.FeatureStore = FeatureStore;

    return ns;

})(Mapzania || {});
/// <reference path="mapzania.js"/>
var Mapzania = (function (ns) {

    //--------------------TO-------------------------

    var toFeatureCollection = function (features) {
        var res = {
            type: "FeatureCollection", 
            features: []
        };

        var bbox = [Number.MAX_VALUE, Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE];

        for (var i = 0; i < features.length; i++) {

            var ftr = toFeature(features[i]);
            res.features.push(ftr);

            bbox[0] = Math.min(bbox[0], ftr.bbox[0]);
            bbox[1] = Math.min(bbox[1], ftr.bbox[1]);
            bbox[2] = Math.max(bbox[2], ftr.bbox[2]);
            bbox[3] = Math.max(bbox[3], ftr.bbox[3]);
            //console.log(bbox[3]);
        }

        res.bbox = bbox; 

        return res;
    }

    var toFeature = function (feature) {
        var res = {
            type: "Feature",
            geometry: toGeometry(feature.geometry),
            properties: feature.attributes,
            bbox: toBoundingBox(feature.geometry.getExtent()),
            style:feature.style
        };

        return res;
    }

    var toBoundingBox = function (extent) {
        return [extent.minX, extent.minY, extent.maxX, extent.maxY];
    }

    var toGeometry = function (geom) {

        var res = {
            //properties: ftr.attributes,
            type: "ERR",
            coordinates: []
        };

        var toPoint = function (point) {
            return [point.x, point.y];
        }

        var toPoints = function (points) {
            var res = [];
            for (var i = 0; i < points.length; i++)
                res.push(toPoint(points[i]));
            return res;
        }

        //Point
        if (geom.getType() == 1) {
            res.type = "Point";
            res.coordinates = toPoint(geom);
        }

        //MultiPoint
        if (geom.getType() == 4) {
            res.type = "MultiPoint";
            res.coordinates = toPoints(geom.points);
        }

        //Line
        if (geom.getType() == 2) {
            res.type = "LineString";
            res.coordinates = toPoints(geom.points);
        }

        //MultiLine
        if (geom.getType() == 5) {
            res.type = "MultiLineString";
            res.coordinates = [];
            for (var i = 0; i < geom.lines.length; i++)
                res.coordinates.push(toPoints(geom.lines[i].points));
        }

        var makeRing = function (line) {
            var pnts = line.points;

            //if (pnts.length == 0)
            //    throw "0 points";

            //while (pnts.length < 4) {
            //    var pnt = pnts[pnts.length - 1];
            //    var npnt = new Mapzania.Point(pnt.x + 0.000000000001, pnt.y + 0.000000000001);
            //    pnts.push(npnt);
            //}

            var first = pnts[0];
            var last = pnts[pnts.length - 1];

            if (first.x != last.x || first.y != last.y)
                pnts.push(first);

        }

        //Polygon
        if (geom.getType() == 3) {

            makeRing(geom.shell);
            for (var i = 0; i < geom.holes.length; i++) 
                makeRing(geom.holes[i]);


            if (geom.shell.points.length < 4)
                throw "broken polygon";

            res.type = "Polygon";
            res.coordinates = [];

            var lines = [geom.shell];
            lines = lines.concat(geom.holes);

            for (var i = 0; i < lines.length; i++) {
                if (lines[i].points.length < 4)
                    continue;
                res.coordinates.push(toPoints(lines[i].points));
            }
        }

        //MultiPolygon
        if (geom.getType() == 6) {
            res.type = "MultiPolygon";
            res.coordinates = [];

            for (var k = 0; k < geom.polygons.length; k++) {

                var poly = geom.polygons[k];

                makeRing(poly.shell);
                for (var i = 0; i < poly.holes.length; i++)
                    makeRing(poly.holes[i]);

                if (poly.shell.points.length < 4)
                    continue;

                var polyArr = [];
                res.coordinates.push(polyArr);

                var lines = [poly.shell];
                lines = lines.concat(poly.holes);

                for (var i = 0; i < lines.length; i++) {
                    if (lines[i].points.length < 4)
                        continue;
                    polyArr.push(toPoints(lines[i].points));
                }
            }
        }

        return res;
    }

    //--------------------FROM-------------------------

    var fromFeatureCollection = function (collection) {
        var res = [];

        var features = collection.features;

        for (var i = 0; i < features.length; i++) {
            res.push(fromFeature(features[i], i));
        }

        return res;
    }

    var fromFeature = function (feature, id) {
        var res = new Mapzania.Feature(); 

        if (!id)
            id = 1;

        res.id = id;
        res.attributes = feature.properties;
        res.geometry = fromGeometry(feature.geometry);
        res.style = feature.style;

        return res;
    }

    var fromBoundingBox = function (bbox) {
        return new Mapzania.Extent({
            minX: bbox[0],
            minY: bbox[1],
            maxX: bbox[2],
            maxY: bbox[3]
        });
    }

    var fromGeometry = function (geom) {

        var coords = geom.coordinates;

        var res = {
            type: "ERR"
        };

        var fromPoint = function (coordArray) {
            return new Mapzania.Point(coordArray[0], coordArray[1]);
        }

        var fromLine = function (coordArray) {
            var line = new Mapzania.Line();

            for (var i = 0; i < coordArray.length; i++)
                line.points.push(fromPoint(coordArray[i]));

            return line;
        }

        //Point
        if (geom.type == "Point") {
            res = fromPoint(coords);
        }

        //MultiPoint
        if (geom.type == "MultiPoint") {
            var res = new Mapzania.MultiPoint();

            for (var i = 0; i < coords.length; i++)
                res.points.push(fromPoint(coords[i]));
        }

        //Line
        if (geom.type == "LineString") {
            res = fromLine(coords); 
        }

        //MultiLine
        if (geom.type == "MultiLineString") {
            var res = new Mapzania.MultiLine();

            for (var i = 0; i < coords.length; i++)
                res.lines.push(fromLine(coords[i]))
        }

        //Polygon
        if (geom.type == "Polygon") {

            var res = new Mapzania.Polygon();

            for (var i = 0; i < coords.length; i++)
                if (i == 0)
                    res.shell = fromLine(coords[i]);
                else
                    res.holes.push(fromLine(coords[i]));
        } 

        //MultiPolygon
        if (geom.type == "MultiPolygon") {

            var res = new Mapzania.MultiPolygon();

            for (var i = 0; i < coords.length; i++) {
                var poly = new Mapzania.Polygon();
                res.polygons.push(poly);

                for (var j = 0; j < coords[i].length; j++)
                    if (j == 0)
                        poly.shell = fromLine(coords[i][j]);
                    else
                        poly.holes.push(fromLine(coords[i][j]));
            }
        }

        return res;
    }

    ns.GeoJson = {
        toFeatureCollection: toFeatureCollection,
        toFeature: toFeature,
        toBoundingBox:toBoundingBox,
        toGeometry: toGeometry,
        fromFeatureCollection: fromFeatureCollection,
        fromFeature: fromFeature,
        fromBoundingBox:fromBoundingBox,
        fromGeometry: fromGeometry
    };

    return ns;

})(Mapzania || {});
var Mapzania = (function (ns) {

    var me = this;

    var _setModel = function (model) {
        me.model = model;
    }

    var filterByText = function (statement, passthru) {
        if (!passthru)
            passthru = false;

        return {
            type: "FilterByText",
            statement: statement,
            passthru: passthru
        };
    };

    var filterByBoundingBox = function (bbox) {
        return {
            type: "FilterByBoundingBox",
            minX: bbox[0],
            minY: bbox[1],
            maxX: bbox[2],
            maxY: bbox[3]
        };
    };

    var filterByNearest = function (point, take, includeDistance) {
        var n = 1;

        if (take)
            n = take;

        return {
            type: "FilterByNearest",
            x: point.coordinates[0],
            y: point.coordinates[1],
            take: n,
            includeDistance: includeDistance
        };
    };

    var filterByFeatureCollection = function (features) {
        return {
            type: "FilterByFeatureCollection",
            features: features
        };
    };

    var filterByLayer = function (layerKey, layerFilters) {
        var src = me.model.getLayer(layerKey).src;
        var filters = [];

        if (layerFilters)
            filters = layerFilters;
        return {
            type: "FilterByLayer",
            sourceKey: src.key,
            filters: filters
        };
    }

    var clipByFeatureCollection = function (features) {
        return {
            type: "ClipByFeatureCollection",
            features: features
        };
    };

    var changeProperties = function (properties) {
        //http://stackoverflow.com/questions/26195975/regex-to-match-only-commas-but-not-inside-multiple-parentheses

        if (typeof (properties) == "string")
            properties = properties.split(/,(?![^()]*(?:\([^()]*\))?\))/);

        return {
            type: "ChangeProperties",
            properties: properties
        };
    };

    var changeCoordinateSystem = function (srid) {
        return {
            type: "ChangeCoordinateSystem",
            srid: srid
        };
    };

    var changeGeometryAccuracy = function (figures) {
        return {
            type: "ChangeGeometryAccuracy",
            figures: figures
        };
    };

    var simplifyGeometry = function (factor) {
        return {
            type: "SimplifyGeometry",
            factor: factor
        };
    };

    var addAreaProperty = function () {
        return {
            type: "AddAreaProperty"
        };
    };

    var addLengthProperty = function () {
        return {
            type: "AddLengthProperty"
        };
    };

    var convertToCentroids = function () {
        return {
            type: "ConvertToCentroids"
        };
    }

    var bufferGeometry = function () {
        return {
            type: "BufferGeometry"
        };
    }

    var aggregateLinesByOriginAndDestination = function (gridSize, includeCount) {
        if (!includeCount)
            includeCount = true;

        return {
            type: "AggregateLinesByOriginAndDestination",
            gridSize: gridSize,
            includeCount: includeCount
        };
    }

    var clusterPoints = function (factor, properties) {

        if (typeof (properties) == "string")
            properties = properties.split(/,(?![^()]*(?:\([^()]*\))?\))/);

        return {
            type: "ClusterPoints",
            factor: factor,
            properties: properties
        };
    }

    ns.Filters = {
        _setModel: _setModel,
        filterByText: filterByText,
        filterByBoundingBox: filterByBoundingBox,
        filterByNearest: filterByNearest,
        filterByFeatureCollection: filterByFeatureCollection,
        filterByLayer: filterByLayer,
        clipByFeatureCollection: clipByFeatureCollection,
        changeProperties: changeProperties,
        changeCoordinateSystem: changeCoordinateSystem,
        changeGeometryAccuracy: changeGeometryAccuracy,
        simplifyGeometry: simplifyGeometry,
        addAreaProperty: addAreaProperty,
        addLengthProperty: addLengthProperty,
        convertToCentroids: convertToCentroids,
        bufferGeometry: bufferGeometry,
        aggregateLinesByOriginAndDestination: aggregateLinesByOriginAndDestination,
        clusterPoints: clusterPoints
    };

    return ns;

})(Mapzania || {});
if (!Mapzania)
    var Mapzania = {};

(function (ns) {
    function HighlightManager(model, renderer, highlightLayerPrefix, highlightColor) {
        this.model = model;
        this.renderer = renderer;
        this.highlightLayerPrefix = (highlightLayerPrefix != null) ? highlightLayerPrefix : "HM_LAYER";
        this.highlightColor = (highlightColor != null) ? highlightColor : "#FF0000";

        this._layerIdAttributeNames = {};
        this._layerFeatureIdList = {};
        this._layersOnMap = {};

        // This event is fired as soon as highlights are changed (added or removed)
        this.onHighlightsChanged = new ns.Event();
        this.onHighlightsChangedPending = false;

        // A signature over all the filters to determine if the highlight set has changed
        //  This field is used to determine when to fire onHighlightsChanged
        this._filterSignature = null;

        // The extent of all the highlighted features
        this.onHighlightExtentChanged = new ns.Event();
        this._highlightExtent = null;

        this._listenForMapLoadEvent();
    }

    // Clear all highlights added to this highlight manager
    HighlightManager.prototype.clearAllHighlights = function () {
        var me = this;
        this._layerFeatureIdList = {};
        
        for(var cur in this._layersOnMap) {
            me.model.removeLayer(cur);
        };

        this._layersOnMap = {};

        this._highlightExtent = null;
        this.onHighlightExtentChanged.fire(me);
        
        //
        // Fire the onHighlightsChanged event, if the set of highlighted elements actually changed
        //
        if (this._filterSignature != null) {
            this._filterSignature = null;
            this.onHighlightsChanged.fire(me);
        }
        this.onHighlightsChangedPending = false;
    }

    // Set the name of the ID attribute for features on a layer. Features are identified using this
    //  attribute value.
    HighlightManager.prototype.setLayerIdAttributeName = function (layerKey, IdAttributeName) {
        this._layerIdAttributeNames[layerKey] = IdAttributeName;
    }

    // Adds a feature ID that requires highlighting. 
    HighlightManager.prototype.addFeatureId = function (layerKey, featureIdValue) {
        if (!this._layerFeatureIdList[layerKey]) {
            this._layerFeatureIdList[layerKey] = [];
        }
        this._layerFeatureIdList[layerKey].push(featureIdValue);

        this._updateUI();
    }

    HighlightManager.prototype.getHighlightedExtent = function () {
        return this._highlightExtent;
    }

    //
    // Internal utility method - create and update layers
    //
    HighlightManager.prototype._updateUI = function () {
        if (this.timerRef != null) {
            clearTimeout(this.timerRef);
            this.timerRef = null;
        }

        var me = this;
        this.timerRef = setTimeout(function () {
            //
            //
            //
            me._highlightExtent = null;
            var newFilterSignature = "--";
            for (var layerKey in me._layerFeatureIdList) {
                newFilterSignature += (layerKey + "--");
                var idAttrName = me._layerIdAttributeNames [layerKey];
                if (idAttrName) {
                    newFilterSignature += (idAttrName + "--");
                    var filter = null;
                    me._layerFeatureIdList[layerKey].forEach(function (curValue) {
                        if (filter) filter = filter + " OR ";
                        if (!filter) filter = "";
                        filter = filter + idAttrName + " = '"+curValue+"'";
                    });
                    if (!filter) filter = "(1=2)";
                    newFilterSignature += (filter + "--");
                    me._ensureHMLayerExistWithFilter(layerKey, filter);
                }
            }

            //
            // Fire the onHighlightsChanged event, if the set of highlighted elements actually changed
            //
            if (me._filterSignature != newFilterSignature) {
                me._filterSignature = newFilterSignature;
                me.onHighlightsChangedPending = true;
            }

        }, 100);
    }

    //
    // Ensures that a HM layer exsits for the specified layer. And sets the filter on that layer.
    //
    HighlightManager.prototype._ensureHMLayerExistWithFilter = function (layerKey, filter) {
        var hmLayerKey = this.highlightLayerPrefix + "_" + layerKey;
        if (!this._layersOnMap[hmLayerKey]) {
            //
            // Clone the layer
            //
            var layerCopy = {};
            $.extend(true, layerCopy, this.model.getLayer(layerKey));
            layerCopy.ordinal = this.model._getSmallestLayerOrdinal() - 1; 
            layerCopy.key = hmLayerKey;
            layerCopy.filter = filter;
            this._convertThemesForHMLayer(layerCopy);

            //
            // Add the new layer at the top of the map
            //
            this._layersOnMap[hmLayerKey] = true;
            this.model.addLayer(layerCopy);
        } else {
            //
            // The layer already exists on the map. We simply set the filter now
            //
            this.model.filterLayer(hmLayerKey, filter);
        }
    }

    //
    // Converts the themes of the layer to themes that is suitable for highlighting
    //
    HighlightManager.prototype._convertThemesForHMLayer = function (layer) {
        var me = this;
        layer.showInLegend = false;
        layer.themes.forEach(function (curTheme) {
            curTheme.labeller = null;
            if (curTheme.symbol) {
                me._convertSymbol(curTheme.symbol);
            }
        });
    };

    //
    // Listen for map load events, so that we can determine the extent of the highlighted
    //  features
    //
    HighlightManager.prototype._listenForMapLoadEvent = function (layer) {
        var me = this;
        this.model.loaded.add(function (e) {
            var oldHighlightExtent = me._highlightExtent;
            for (var j = 0; j < e.data.length; j++) {
                var itm = e.data[j];
                if (itm.layer.key.indexOf(me.highlightLayerPrefix) == 0) {
                    itm.data.forEach(function (curFeature) {
                        if (!me._highlightExtent) {
                            me._highlightExtent = curFeature.geometry.getExtent();
                        } else {
                            me._highlightExtent.join(curFeature.geometry.getExtent());
                        }
                    });
                }
            }

            if (me.onHighlightsChangedPending) {
                me.onHighlightsChangedPending = false;
                me.onHighlightsChanged.fire(me);
            }

            var hasChanged = (oldHighlightExtent && me._highlightExtent && (!oldHighlightExtent.equals(me._highlightExtent)));
            if  (hasChanged  || (me._highlightExtent && !oldHighlightExtent)) {
                me.onHighlightExtentChanged.fire(me);
            }
        });
    };

    //
    // Converts a generic symbol to a symbol that is suitable for highlighting
    //
    HighlightManager.prototype._convertSymbol = function (symbol) {
        if (!symbol)
            return;
        var me = this;
        switch(symbol.instanceTypeName) {
            case "PointSymbol":
                if (symbol.color) symbol.color = me.highlightColor;
                this._convertSymbol(symbol.border);
                break;
            case "FillSymbol":
                if (symbol.color) symbol.color = me.highlightColor;
                if (symbol.color2) symbol.color2 = me.highlightColor;
                if (symbol.opacity) symbol.opacity = 40;
                this._convertSymbol(symbol.outline);
                break;
            case "LineSymbol":
                if (symbol.color) symbol.color = me.highlightColor;
                break;
            case "CompoundLineSymbol":
                if (symbol.lines)
                    symbol.lines.forEach(function (cur) {
                        me._convertSymbol(cur);
                    });
                break;
            case "CompoundPointSymbol":
                if (symbol.points)
                    symbol.points.forEach(function (cur) {
                        me._convertSymbol(cur);
                    });
                break;
            default:
                break;
        }
    };

    ns.HighlightManager = HighlightManager;

})(Mapzania)

/// <reference path="../lib/swfobject-1.5.js" />

if (!Canvas)
	var Canvas = {};

/**
 * @doc Canvas.Mapper.*ClassDescription*
 *
 * An interactive map drawer implementation, using the HTML Canvas component. 
 * Applications typically instantiate this class once, and does not make direct use of any functions defined in this class.
 */
(function (ns) {

	var ts = Mapzania;

    /**
      * @doc Canvas.Mapper.Mapper
      *
      * Constructor. The properties and functions defined in this class are not public, and reserved for the framework and SDK.
      *
      */
	function Mapper() {
		this.contexts = {};
		this.canvasses = {};

		this.viewChanged = new ts.Event();
	}

	Mapper.prototype.init = function ($el, callback) {
	    this.$el = $el;

	    var html = "<canvas id='invisible_canvas_element_text_measure" + "'></canvas>";
	    this.$el.append(html);
	    this._canvasTextMeasure = $("#invisible_canvas_element_text_measure")[0];

	    if (callback)
	        callback();
	}

	Mapper.prototype.zoom = function (factor) {
		//this.swf.zoom(factor);
	}

	Mapper.prototype.onResized = function () {
	    for (cur in this.canvasses) {
	        this.canvasses[cur][0].width = this.$el.width();
	        this.canvasses[cur][0].height = this.$el.height();
	    }
	}

	Mapper.prototype.setBackColor = function (color) {
		this.$el.css("background-color", color);
	}

	Mapper.prototype.setBackImageUrl = function (url) {
		this.$el.css("background-image", "url('" + url + "')");
		this.$el.css("background-repeat", "repeat");
	}

	Mapper.prototype.addLayer = function (key, index, greyScale) {
	    var $canvas;
	    var html = "<canvas id='canvas_mapper_layer_id_"+key+"'></canvas>";   

	    if (typeof index != 'undefined') {
	        if (index > this.$el.children().length) {
	            //console.log("NB!: index > this.$el.children().length");
	            index = this.$el.children().length - 1;
	        }

	        if (this.$el.children().length == 0) {
	            this.$el.append(html);
	        } else {
	            this.$el.children().eq(index).before(html);
	        }
	        
	        $canvas = this.$el.children().eq(index);
	    }
	    else {
	        this.$el.append(html);
	        $canvas = this.$el.children().last();
	    }

		$canvas.css("position", 'absolute');
		$canvas.css("top", '0');
		$canvas.css("left", '0');
		$canvas.css("bottom", '0');
		$canvas.css("right", '0');
		$canvas.css("overflow", 'none');

		if (greyScale) {
		    $canvas.css("filter", "url('data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale')");
		    $canvas.css("-webkit-filter", "grayscale(100%)");
		    //$canvas.addClass("ts-grey-scale-layer");
		}

		$canvas[0].width = this.$el.width();
		$canvas[0].height = this.$el.height();

		this.canvasses[key] = $canvas;
		this.contexts[key] = $canvas[0].getContext('2d');
		this.contexts[key].imageSmoothingEnabled = true;
		this.contexts[key].greyScale = greyScale;
		this.contexts[key].layerSessionId = Mapzania.Util.createPseudoGuid();

        // A hack to get maps to show correctly on Google Chrome. Not sure if this is 100% reliable, but it does seem to work for the demos.
		this.drawCircle(key, { x: 0, y: 0 }, 1, "#000000", 1, 0);
	}

	Mapper.prototype.removeLayer = function (key) {
	    var id = "canvas_mapper_layer_id_" + key;
	    delete this.contexts[key];
	    delete this.canvasses[key];
	    $("#" + id).remove();
	}

	Mapper.prototype.removeAllLayers = function () {
	    this.contexts = {};
	    this.canvasses = {};
	    this.$el.html("");
	}

	Mapper.prototype.hideLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeOut();
	}

	Mapper.prototype.showLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeIn();
	}

	Mapper.prototype.clearLayer = function (key) {
		var ctx = this._getContext(key);
		var size = this._getSize();
		ctx.layerSessionId = Mapzania.Util.createPseudoGuid();
		ctx.clearRect(0, 0, size.width, size.height);
	}

	Mapper.prototype.drawCircle = function (layerKey, center, radius, color, size, opacity) {
	    var ctx = this._getContext(layerKey);

	    ctx.strokeStyle = color;
	    ctx.lineWidth = size;
	    ctx.globalAlpha = opacity / 100;

	    ctx.beginPath();
	    ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI, false);
	    ctx.closePath();
	    ctx.stroke();
	}

	Mapper.prototype.fillCircle = function (layerKey, center, radius, color, opacity) {
		var ctx = this._getContext(layerKey);

		ctx.fillStyle = color;
		ctx.globalAlpha = opacity / 100;

		ctx.beginPath();
		ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI, false);
		ctx.closePath();
		ctx.fill();
	}

	Mapper.prototype.drawLine = function (layerKey, line, color, size, opacity, endCap, joinType, dashArray) {
		opacity = opacity / 100.0;

		if (!dashArray)
			dashArray = [];

		if (!endCap || dashArray.length > 0)
			endCap = 0;

		var caps = ["round", "butt", "square"];
		var cap = caps[endCap];

		if (!joinType)
		    joinType = 0;

		var joins = ["round", "bevel", "miter"];
		var join = joins[joinType];

		var ctx = this._getContext(layerKey);

		ctx.beginPath();
		ctx.strokeStyle = color;
		ctx.lineWidth = size < 0.7 ? 0.7 : size;
		ctx.globalAlpha = opacity;
		ctx.lineCap = cap;
		ctx.lineJoin = join;
		ctx.setLineDash(dashArray);

		var pnts = line.points;
		ctx.moveTo(pnts[0].x, pnts[0].y);

		for (var i = 1; i < pnts.length; i++)
			ctx.lineTo(pnts[i].x, pnts[i].y);

		ctx.stroke();

	}

	Mapper.prototype.fillPolygon = function (layerKey, lines, color, color2, opacity, type) {
	    if (type == 1) {
	        var ctx = this._getContext(layerKey);

	        ctx.fillStyle = color;
	        ctx.globalAlpha = opacity / 100;

	        ctx.beginPath();

	        for (var i = 0; i < lines.length; i++) {
	            var pnts = lines[i].points;

	            ctx.moveTo(pnts[0].x, pnts[0].y);

	            for (var j = 1; j < pnts.length; j++)
	                ctx.lineTo(pnts[j].x, pnts[j].y);

	            ctx.closePath();
	        }

	        ctx.fill();
	    }
	}

	Mapper.prototype.drawText = function (layerKey, location, text, font, size, color, backColor,
				opacity, align, haloSize, haloColor, bold, italic, underline, addMark, angleRadiansCounterClockwise, offset) {

		var al;
		switch (align) {
			case 0: al = "left"; break;
			case 1: al = "center"; break;
			case 2: al = "right"; break;
			case 3: al = "justify"; break;
			default: al = "center";
		}

		var fontTxt = (size * 1.386) + "px " + font // We use 1.386 to get capitals of the font to match the size. (Used Verdana to get this size)

        if (italic)
            fontTxt = "italic " + fontTxt;

		if (bold)
		    fontTxt = "bold " + fontTxt;

		var ctx = this._getContext(layerKey);
		
		ctx.fillStyle = color;
		ctx.globalAlpha = opacity / 100.0;
		ctx.lineStyle = color;
		ctx.font = fontTxt;
		ctx.textAlign = al;
		ctx.textBaseline = "middle";

		if (angleRadiansCounterClockwise && angleRadiansCounterClockwise != 0) {
		    var p = new Mapzania.Point(location.x + offset.x, location.y - offset.y);
		    var p2 = p.rotate(new Mapzania.Point(location.x, location.y), -angleRadiansCounterClockwise);
		    ctx.save();
		    ctx.translate(p2.x, p2.y);
		    ctx.rotate(-angleRadiansCounterClockwise);
		    this._drawTextRaw(ctx, text, 0, 0, haloSize, haloColor);
		    ctx.restore();
		}
		else {
		    this._drawTextRaw(ctx, text, location.x + offset.x, location.y - offset.y, haloSize, haloColor);
		}
	}

	Mapper.prototype._drawTextRaw = function (ctx, text, x, y, haloSize, haloColor) {
	    if (haloColor) {
	        ctx.shadowBlur = haloSize;
	        ctx.shadowColor = haloColor;

	        ctx.shadowOffsetX = haloSize;
	        ctx.shadowOffsetY = haloSize;
	        ctx.fillText(text, x, y);

	        ctx.shadowOffsetX = haloSize;
	        ctx.shadowOffsetY = -haloSize;
	        ctx.fillText(text, x, y);

	        ctx.shadowOffsetX = -haloSize;
	        ctx.shadowOffsetY = haloSize;
	        ctx.fillText(text, x, y);

	        ctx.shadowOffsetX = -haloSize;
	        ctx.shadowOffsetY = -haloSize;
	        ctx.fillText(text, x, y);
	    } else {
	        ctx.fillText(text, x, y);
	    }
	}

	Mapper.prototype.fillBitmapRotated = function (layerKey, pTopLeft, width, height, src, opacity, angleRadiansCounterClockwise) {
	    opacity = opacity / 100.0;

	    var ctx = this._getContext(layerKey);

	    ctx.globalAlpha = opacity;

	    var img = new Image();
	    img.layerSessionId = ctx.layerSessionId;

	    img.onload = function () {
	        if (img.layerSessionId == ctx.layerSessionId) {
	            ctx.save();
	            ctx.translate(pTopLeft.x, pTopLeft.y);
	            ctx.rotate(-angleRadiansCounterClockwise);
	            ctx.drawImage(img, 0, 0, width, height);
	            ctx.restore();
	        }
	    }

	    img.src = this._mapTileSource(layerKey, src);
	}

	Mapper.prototype.fillBitmap = function (layerKey, extent, src, opacity) {

	    opacity = opacity / 100.0;
	    var me = this;
	    var ctx = this._getContext(layerKey);

	    ctx.globalAlpha = opacity;

	    var img = new Image();
	    img.layerSessionId = ctx.layerSessionId;
	    img.onload = function () {
	        if (img.layerSessionId == ctx.layerSessionId) {
	            ctx.drawImage(img, extent.minX, extent.minY, extent.width(), extent.height());
	        }
	    }

	    img.src = this._mapTileSource(layerKey, src);
	}

	Mapper.prototype.drawImage = function (layerKey, extent, src, opacity) {
	    opacity = opacity / 100.0;
	    var ctx = this._getContext(layerKey);
	    ctx.globalAlpha = opacity;

	    var img = new Image();
	    img.onload = function () {
	        ctx.drawImage(img, extent.minX, extent.minY, extent.width(), extent.height());
	    }

	    img.src = src;
	}

	Mapper.prototype.flush = function () { }

	Mapper.prototype._getContext = function (key) {

		var ctx = this.contexts[key];

		if (!ctx)
			throw "No canvas matches key=" + key;

		return ctx;
	}

	Mapper.prototype._mapTileSource = function (layerKey, src) {
	    // A function to detect IE 10 and 11
	    var isIE = function () {
	        return ("" + navigator.userAgent).indexOf("MSIE 10.0") >= 0 || ("" + navigator.userAgent).indexOf("Trident/7.0") >= 0;
	    };

	    // IE 10 and 11 has no (CSS) filters to map images to gray scale. So, for these browsers, we simply let the server do the conversion for us.
	    //  For other browsers, we let the filters do the work for us
	    if (this._getContext(layerKey).greyScale && isIE()) {
	        return "/api/temp/osmgrayscale/?url=" + encodeURIComponent(src);
	    }

	    return src;
	}

	Mapper.prototype._getSize = function () {
	    return {
	        height: this.$el.height(),
	        width: this.$el.width()
	    };
	}

	Mapper.prototype.measureText = function (string, font, fontSize) {
	    var ctx = this._canvasTextMeasure.getContext("2d");
	    ctx.font = "" + (fontSize*1.386) + "px" + " " + font;
	    return {
	        height: (fontSize * 1.386),
	        width: ctx.measureText(string).width
	    };
	}

	ns.Mapper = Mapper;

})(Canvas)
/// <reference path="../lib/swfobject-1.5.js" />

if (!Mapzania)
	var Mapzania = {};

(function (ns) {

	function PixelTransformer(params) {

		this.params = {
			originX: 0, originY: 0, scaleX: 1, scaleY: 1, translateX: 0, translateY: 0, rotateZ: 0, skewX: 0, skewY: 0
		};

		$.extend(this.params, params);

		this.params._cos = Math.cos(this.params.rotateZ);
		this.params._sin = Math.sin(this.params.rotateZ);
	}

	PixelTransformer.prototype.to = function (geomType, geom) {
		var me = this;

		var conv = function (point) {
		    var pointOrigin = new Mapzania.Point(me.params.originX, me.params.originY);

            // Translate the point to the rotated world coordinate system
			var res = point.rotate(pointOrigin, me.params.rotateZ);
			
            // Then translate it for the drawer
			res.x = (res.x - me.params.translateX) * me.params.scaleX;
			res.y = (res.y - me.params.translateY) * me.params.scaleY;

			return res;
		}

		switch (geomType) {
			case -1: return this.transformExtent(geom, conv);
			case 1: return conv(geom);//point
			case 2: return this.transformLine(geom, conv);
			case 3: return this.transformPolygon(geom, conv);
			case 4: return this.transformMultiPoint(geom, conv);
			case 5: return this.transformMultiLine(geom, conv);
			case 6: return this.transformMultiPolygon(geom, conv);
			default:
				throw "unknown geometry type";
		}
	}

	PixelTransformer.prototype.labelRotationTo = function (rotation) {
	    return rotation + this.params.rotateZ;
	}

	PixelTransformer.prototype.from = function (geomType, geom) {
		var me = this;

		var conv = function (point) {
		    var pointOrigin = new Mapzania.Point(me.params.originX, me.params.originY);

		    var res = new ns.Point(point.x, point.y);

		    // Translater from the drawer to world coordinates
		    res.x = res.x / me.params.scaleX + me.params.translateX;
		    res.y = res.y / me.params.scaleY + me.params.translateY;

		    // Rotate the point back to the Model's coordinate system(clockwise)
		    res = res.rotate(pointOrigin, -me.params.rotateZ);

		    return res;
		}

		switch (geomType) {
			case -1: return this.transformExtent(geom, conv);
			case 1: return conv(geom);//point
			case 2: return this.transformLine(geom, conv);
			case 3: return this.transformPolygon(geom, conv);
			case 4: return this.transformMultiPoint(geom, conv);
			case 5: return this.transformMultiLine(geom, conv);
			case 6: return this.transformMultiPolygon(geom, conv);
			default:
				throw "unknown geometry type";
		}
	}

	PixelTransformer.prototype.toSize = function (size) {
	    var avgScale = (Math.abs((this.params.scaleX) + Math.abs(this.params.scaleY)) / 2.0);
	    //console.log(avgScale);
	    return avgScale *size;
	    var origin = new Mapzania.Point(this.params.translateX, this.params.translateY);

	    var pnt1 = this.to(1, origin);
	    var pnt2 = this.to(1, new Mapzania.Point(origin.x + size, origin.y));

		var width = Math.abs(pnt1.x - pnt2.x);

		return width;
	}

	PixelTransformer.prototype.fromSize = function (size) {
	    var avgScale = (Math.abs((this.params.scaleX) + Math.abs(this.params.scaleY)) / 2.0);
	    return size / avgScale;
	    var origin = new Mapzania.Point(0, 0);

	    var pnt1 = this.from(1, origin);
	    var pnt2 = this.from(1, new Mapzania.Point(origin.x + size, origin.y));

	    var width = Math.abs(pnt1.x - pnt2.x);

	    return width;
	}

	PixelTransformer.prototype.transformExtent = function (extent, pointFunc) {

		var min = pointFunc(new ns.Point(extent.minX, extent.minY));
		var max = pointFunc(new ns.Point(extent.maxX, extent.maxY));

		var minX = Math.min(min.x, max.x);
		var minY = Math.min(min.y, max.y);
		var maxX = Math.max(min.x, max.x);
		var maxY = Math.max(min.y, max.y);

		var res = new ns.Extent({ minX: minX, minY: minY, maxX: maxX, maxY: maxY });

		return res;
	}

	PixelTransformer.prototype.transformMultiPoint = function (mpoint, pointFunc) {
		var res = new ns.MultiPoint();
		for (var i = 0; i < mpoint.points.length; i++)
			res.points.push(pointFunc(mpoint.points[i]));

		return res;
	}

	PixelTransformer.prototype.transformLine = function (line, pointFunc) {
		var res = new ns.Line();
		for (var i = 0; i < line.points.length; i++)
			res.points.push(pointFunc(line.points[i]));

		return res;
	}

	PixelTransformer.prototype.transformMultiLine = function (mline, pointFunc) {
		var res = new ns.MultiLine();

		for (var i = 0; i < mline.lines.length; i++)
			res.lines.push(this.transformLine(mline.lines[i], pointFunc));

		return res;
	}

	PixelTransformer.prototype.transformPolygon = function (polygon, pointFunc) {
		var res = new ns.Polygon();
		res.shell = this.transformLine(polygon.shell, pointFunc);

		for (var i = 0; i < polygon.holes.length; i++)
			res.holes.push(this.transformLine(polygon.holes[i], pointFunc));

		return res;
	}

	PixelTransformer.prototype.transformMultiPolygon = function (mpoly, pointFunc) {
		var res = new ns.MultiPolygon();
		for (var i = 0; i < mpoly.polygons.length; i++)
			res.polygons.push(this.transformPolygon(mpoly.polygons[i], pointFunc));

		return res;
	}

	ns.PixelTransformer = PixelTransformer;

})(Mapzania)

if (!TRaphael)
    var TRaphael = {};

/**
 * @doc TRaphael.Mapper.*ClassDescription*
 *
 * An interactive map drawer implementation, using the Raphael toolkit. See <a href="http://raphaeljs.com/" target="_blank">http://raphaeljs.com/</a> for more information.
 * Applications typically instantiate this class once, and does not make direct use of any functions defined in this class.
 */
(function (ns) {

	var ts = Mapzania;

    /**
      * @doc TRaphael.Mapper.Mapper
      *
      * Constructor. The properties and functions defined in this class are not public, and reserved for the framework and SDK.
      */
	function Mapper() {
		this.papers = {};
		this.canvasses = {};
		this.viewChanged = new ts.Event();
        
		this.layerParams = [];
	}

	Mapper.prototype.init = function ($el, callback) {
	    this.$el = $el;

	    this._measureTextPaper = Raphael(0, 0, 0, 0);
	    this._measureTextPaper.canvas.style.visibility = 'hidden';

	    if (callback)
	        callback();
	}

	Mapper.prototype.zoom = function (factor) {
		//this.swf.zoom(factor);
	}

	Mapper.prototype.onResized = function () {
        // We implement a bit of a work-around to get map resizes to work on the Raphael renderer. We remove all the layers and re-add them all.
	    var oldLayerParams = this.layerParams;
	    this.layerParams = [];
	    this.papers = [];
	    this.canvasses = [];
	    this.$el.html("");
	    var me = this;
	    oldLayerParams.forEach(function (cur) {
	        me.addLayer(cur.key, cur.index, cur.greyScale);
	    });
	}

	Mapper.prototype.setBackColor = function (color) {
		this.$el.css("background-color", color);
	}

	Mapper.prototype.setBackImageUrl = function (url) {
		this.$el.css("background-image", "url('" + url + "')");
		this.$el.css("background-repeat", "repeat");
	}

	Mapper.prototype.zoomTo = function (extent) {
	}

	Mapper.prototype.addLayer = function (key, index, greyScale) {
	    this.layerParams.push({ key: key, index: index, greyScale: greyScale });

	    var $canvas;

	    var html = "<div id='raphael_mapper_layer_id_" + key + "'></div>";

	    if (typeof index != 'undefined') {
	        if (index > this.$el.children().length) {
	            console.log("NB!: index > this.$el.children().length");
	            index = this.$el.children().length - 1;
	        }

	        if (this.$el.children().length == 0) {
	            this.$el.append(html);
	        } else {
	            this.$el.children().eq(index).before(html);
	        }

	        $canvas = this.$el.children().eq(index);
	    }
        else {
	        this.$el.append(html);
	        $canvas = this.$el.children().last();
	    }

		$canvas.addClass("layername-" + key);

		$canvas.css("position", 'absolute');
		$canvas.css("top", '0');
		$canvas.css("left", '0');
		$canvas.css("bottom", '0');
		$canvas.css("right", '0');
		$canvas.css("overflow", 'none');

		if (greyScale) {
		    $canvas.css("filter", "url('data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale')");
		    $canvas.css("-webkit-filter", "grayscale(100%)");
		    //$canvas.addClass("ts-grey-scale-layer");
		}

		$canvas[0].width = this.$el.width();
		$canvas[0].height = this.$el.height();

		this.canvasses[key] = $canvas;

		var size = this._getSize();

		this.papers[key] = Raphael($canvas[0], size.width, size.height);

		this.papers[key].greyScale = greyScale;
	}

	Mapper.prototype.removeLayer = function (key) {
	    var id = "raphael_mapper_layer_id_" + key;
	    delete this.papers[key];
	    delete this.canvasses[key];
	    $("#" + id).remove();

	    for (var index = 0; index < this.layerParams.length; index++) {
	        if (this.layerParams[index].key == key) {
	            this.layerParams.splice(index, 1);
	            break;
	        }
	    }
	}

	Mapper.prototype.removeAllLayers = function () {
	    this.papers = {};
	    this.canvasses = {};
	    this.layerParams = [];
	    this.$el.html("");
	}

	Mapper.prototype.hideLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeOut();
	}

	Mapper.prototype.showLayer = function (key) {
		var $canvas = this.canvasses[key];
		$canvas.fadeIn();
	}

	Mapper.prototype.clearLayer = function (key) {
	    var paper = this._getPaper(key);
	    paper.clear();
	}

	Mapper.prototype._clearAllLayers = function (key) {
	    for (var cur in this.papers) {
	        this.clearLayer(cur);
	    }
	}

	Mapper.prototype.drawCircle = function (layerKey, center, radius, color, size, opacity) {

	    var paper = this._getPaper(layerKey);

		var circle = paper.circle(center.x, center.y, radius);

		circle.attr({
		    'fill-opacity': 0,
		    'stroke': color,
		    'stroke-width': size,
		    'stroke-opacity': opacity / 100.0
		});
	}

	Mapper.prototype.fillCircle = function (layerKey, center, radius, color, opacity) {

		var paper = this._getPaper(layerKey);

		var circle = paper.circle(center.x, center.y, radius);
		circle.attr({ fill: color, 'stroke-width': 0, opacity: opacity / 100.0 });
	}

	Mapper.prototype.drawLine = function (layerKey, line, color, size, opacity, endCap, joinType, dashArray) {


	    if (!dashArray)
	        dashArray = [];

	    var da = [""];

	    if (dashArray.length > 1) {
	        if (dashArray[0] == dashArray[1])
	            da = ["."];

	        if (dashArray[0] > dashArray[1])
	            da = ["--"];
	    }

	    if (!endCap || dashArray.length > 0)
	        endCap = 0;

	    var caps = ["round", "butt", "square"];
	    var cap = caps[endCap];

	    if (!joinType)
	        joinType = 0;

	    var joins = ["round", "bevel", "miter"];
	    var join = joins[joinType];

	    var paper = this._getPaper(layerKey);

	    var str = "";

	    var pnts = line.points;

	    str += this._moveTo(pnts[0]);
	    for (var i = 1; i < pnts.length; i++) {
	        str += this._lineTo(pnts[i]);
	    }


	    var path = paper.path(str);

	    path.attr({
	        'stroke': color,
	        'stroke-width': size,
	        'stroke-opacity': opacity / 100,
	        'stroke-linecap': cap,
	        'stroke-linejoin': join,
	        'stroke-dasharray': da
	    });
	}

	Mapper.prototype.fillPolygon = function (layerKey, lines, color, color2, opacity, type) {
	    if (type == 1) {
	        var paper = this._getPaper(layerKey);

	        var str = "";

	        for (var lineNo = 0; lineNo < lines.length; lineNo++) {
	            var pnts = lines[lineNo].points;

	            str += this._moveTo(pnts[0]);
	            for (var i = 1; i < pnts.length; i++) {
	                str += this._lineTo(pnts[i]);
	            }

	            str += " Z ";
	        }

	        var path = paper.path(str);

	        path.attr({ fill: color, 'stroke-width': 0, opacity: opacity / 100.0 });
	    }
	}

	Mapper.prototype.drawText = function (layerKey, location, text, font, size, color, backColor,
				opacity, align, haloSize, haloColor, bold, italic, underline, addMark, angleRadiansCounterClockwise, offset) {
		var al;
		switch (align) {
			case 0: al = "start"; break;
			case 1: al = "middle"; break;
			case 2: al = "end"; break;
			case 3: al = "middle"; break;
			default: al = "middle";
		}

		if (!angleRadiansCounterClockwise) angleRadiansCounterClockwise = 0.0;
		var p = new Mapzania.Point(location.x + offset.x, location.y - offset.y);
		var p2 = p.rotate(new Mapzania.Point(location.x, location.y), -angleRadiansCounterClockwise);

		var paper = this._getPaper(layerKey);

		var text = paper.text(p2.x, p2.y, text);

		text.attr({
			"fill": color,
			"font-family": font,
			"font-size": ( size * 1.386 ) + "px", // We use 1.386 to get capitals of the font to match the size. (Used Verdana to get this size) 
			"text-anchor": al
		});

		if (italic)
		    text.attr("font-style", italic ? "italic" : "");

		if(bold)
		    text.attr("font-weight", bold ? "bold" : "");

		var bbox = text.getBBox(); // Get the text bounding box before it is rotated

		if (angleRadiansCounterClockwise && angleRadiansCounterClockwise != 0) {
		    var degrees = -angleRadiansCounterClockwise * (180 / Math.PI);
		    text.transform("r" + degrees+","+p2.x+","+p2.y+"");
		}
		
		if (haloColor) {
		    var halo = this._backHalo(paper, bbox, haloSize, haloColor, angleRadiansCounterClockwise);
		    text.insertBefore(halo);
		}
	}

	Mapper.prototype._backHalo = function (paper, bbox, haloSize, haloColor, angleRadiansCounterClockwise) {
	    var rect = paper.rect(bbox.x, bbox.y + bbox.height * 0.42, bbox.width, bbox.height / 6).attr('stroke', 'none');
	    if (angleRadiansCounterClockwise && angleRadiansCounterClockwise != 0) {
	        var degrees = -angleRadiansCounterClockwise * (180 / Math.PI);
	        rect.transform("r" + degrees);
	    }
	    rect.glow({
	        color: haloColor,
	        width: haloSize * 12,
	        opacity: 1,
	    });

	    return rect;
	}

	Mapper.prototype.fillBitmap = function (layerKey, extent, src, opacity) {
	    var paper = this._getPaper(layerKey);

	    var img = paper.image(this._mapTileSource(layerKey, src), extent.minX, extent.minY, extent.width(), extent.height());

	    img.attr({ opacity: opacity / 100.0 });
	}

	Mapper.prototype.fillBitmapRotated = function (layerKey, pTopLeft, width, height, src, opacity, angleRadiansCounterClockwise) {
	    var paper = this._getPaper(layerKey);
	    var degrees = -angleRadiansCounterClockwise * (180 / Math.PI);

	    var img = paper.image(this._mapTileSource(layerKey, src), 0, 0, width + 1, height + 1);

	    img.translate(pTopLeft.x, pTopLeft.y);
	    img.rotate(degrees, 0, 0);

	    img.attr({ opacity: opacity / 100.0 });
	}

	Mapper.prototype.drawImage = function (layerKey, extent, src, opacity) {
	    var paper = this._getPaper(layerKey);

	    var img = paper.image(src, extent.minX, extent.minY, extent.width(), extent.height());

	    img.attr({ opacity: opacity / 100.0 });
	}

	Mapper.prototype.flush = function () { }

	Mapper.prototype._getPaper = function (key) {

		var ctx = this.papers[key];

		if (!ctx)
			throw "No paper matches key=" + key;

		return ctx;
	}

	Mapper.prototype._mapTileSource = function (layerKey, src) {
        // A function to detect IE 10 and 11
	    var isIE = function () {
	        return ("" + navigator.userAgent).indexOf("MSIE 10.0") >= 0 || ("" + navigator.userAgent).indexOf("Trident/7.0") >= 0;
	    };

	    // IE 10 and 11 has no (CSS) filters to map images to gray scale. So, for these browsers, we simply let the server do the conversion for us.
        //  For other browsers, we let the filters do the work for us
	    if (this._getPaper(layerKey).greyScale && isIE()) {
	        return "/api/temp/osmgrayscale/?url=" + encodeURIComponent(src);
	    }

	    return src;
	}

	Mapper.prototype._getSize = function () {
	    return {
	        height: this.$el.height(),
	        width: this.$el.width()
	    };
	}

	Mapper.prototype._moveTo = function (point) {
		return " M " + point.x + " " + point.y;
	}

	Mapper.prototype._lineTo = function (point) {
		return " L " + point.x + " " + point.y;
	}

	Mapper.prototype.measureText = function (string, font, fontSize) {
	    var el = this._measureTextPaper.text(0, 0, string);
	    el.attr('font-family', font);
	    el.attr('font-size', (fontSize  * 1.386) + "px");
	    var bBox = el.getBBox();
	    el.remove();
	    return {
	        width: bBox.width,
	        height: bBox.height
	    };
	}

	ns.Mapper = Mapper;

})(TRaphael)
if (!Mapzania)
	var Mapzania = {};

(function (ns) {
	function TextMapper(elementId, map, onReady) {

		var $el = $(elementId)

		var me = this

		if (onReady) onReady();

		map.loaded.add(function () {
			me.writeLine($el, "Map loaded. [StartExtent=" + JSON.stringify(map.startExtent) + "]")

			var layers = map.getLayers()

			for (var i = 0; i < layers.length; i++) {
				var layer = layers[i];
				layer.updated.add(function (data) {
					me.writeLine($el, "Layer updated. [Info=" + JSON.stringify(data) + "]")
				})
			}
		})
	}

	TextMapper.prototype.writeLine = function (el, line) {
		el.append("<div>" + line + "</div>")
	}

	ns.TextMapper = TextMapper
})(Mapzania)

if (!Mapzania)
    var Mapzania = {};

//
// The class LayerSourceLoadQueue is not public, and should not be used by application / project implementations.
//

(function (ns) {
    function LayerSourceLoadQueue(model) {
        this._model = model;
        this._queue = [];
        this._controlledRequestCounter = 0; //Data intensive requests in progress
        this._timeoutRef = null;

        this._progressTotalRequests = 0;
        this._progressOutstandingRequests = 0;
        this._progress = new ns.Event();
    }

    LayerSourceLoadQueue.prototype.getFeatures = function (layer, sourceKey, filter, extent, fields, thinAlgo, thinFactor, accuracy, srid, labelVisibilityClause, sourceParams, callback, fieldsListIsStrict, vectorServerName, mapKey, layerGroupName, z, x, y, fence) {
        var me = this;
        this._progressTotalRequests++;

        var queueObj = {
            type: "VECTOR",
            layer: layer,
            sourceKey: sourceKey,
            filter: filter,
            extent: extent,
            fields: fields,
            thinAlgo: thinAlgo,
            thinFactor: thinFactor,
            accuracy: accuracy,
            srid: srid,
            labelVisibilityClause: labelVisibilityClause,
            sourceParams: sourceParams,
            callback: callback,
            fieldsListIsStrict: fieldsListIsStrict,
            vectorServerName: vectorServerName,
            mapKey: mapKey,
            layerGroupName: layerGroupName,
            z: z,
            x: x,
            y: y,
            fence: fence,
            filters:layer.src.filters
        };

        //console.log(queueObj);

        // Remove duplicate requests for this layer
        var q2 = [];
        this._queue.forEach(function (cur) {
            if (cur.layer.key != layer.key) {
                q2.push(cur);
            } else {
                me._progressTotalRequests--;
            }
        });
        this._queue = q2;

        this._queue.push(queueObj);

        this._scheduleProcessQueue();
    }

    LayerSourceLoadQueue.prototype.getTiles = function (sourceKey, extentToLoad, extentVisible, srid, filter, callback, requestId) {
        this._progressTotalRequests++;

        // We don't queue tile requests
        this._model.api.getTiles(sourceKey, extentToLoad, extentVisible, srid, filter, callback, requestId);
    }

    LayerSourceLoadQueue.prototype._scheduleProcessQueue = function () {
        if (!this._timeoutRef) {
            this._timeoutRef = setTimeout(this._processQueue.bind(this), 0);
        }
    }

    LayerSourceLoadQueue.prototype._processQueue = function () {
        this._timeoutRef = null;

        var q2 = []; // Items to be processed in the next round
        var me = this;
        this._queue.forEach(function (queueObj) {
            var isDataIntensive = me._isDataIntensive(queueObj);
            var doRequest = false;
            if (isDataIntensive) {
                if (me._controlledRequestCounter < 2) {
                    me._controlledRequestCounter++;
                    doRequest = true;
                } else {
                    //console.log("IsDataIntensive: Adding request back to queue");
                    q2.push(queueObj);
                }
            } else {
                doRequest = true;
            }

            if (doRequest) {
                var callback = null;
                if (isDataIntensive) {
                    callback = function (data) {
                        me._progressOutstandingRequests--;
                        me._controlledRequestCounter--;
                        queueObj.callback(data);
                        me._scheduleProcessQueue();
                    };
                } else {
                    callback = function (data) {
                        me._progressOutstandingRequests--;
                        queueObj.callback(data);
                        me._scheduleProcessQueue();
                    };
                }

                me._progressOutstandingRequests++;
                if (queueObj.vectorServerName) {
                    me._model.api.getVectorServerData(
                        queueObj.sourceKey,
                        queueObj.filter,
                        queueObj.extent,
                        queueObj.fields,
                        queueObj.thinAlgo,
                        queueObj.thinFactor,
                        queueObj.accuracy,
                        queueObj.srid,
                        queueObj.labelVisibilityClause,
                        queueObj.sourceParams,
                        callback,
                        queueObj.fieldsListIsStrict,
                        queueObj.vectorServerName,
                        queueObj.mapKey,
                        queueObj.layerGroupName,
                        queueObj.z,
                        queueObj.x,
                        queueObj.y);
                } else {
                    me._model.api.getFeatures(
                        queueObj.sourceKey,
                        queueObj.filter,
                        queueObj.extent,
                        queueObj.fields,
                        queueObj.thinAlgo,
                        queueObj.thinFactor,
                        queueObj.accuracy,
                        queueObj.srid,
                        queueObj.labelVisibilityClause,
                        queueObj.sourceParams,
                        callback,
                        queueObj.fieldsListIsStrict,
                        queueObj.fence,
                        queueObj.filters);
                }
            }
        });

        this._queue = q2;

        // Report on the progress
        var outstanding = this._progressOutstandingRequests + this._queue.length;
        if (outstanding == 0) {
            this._progressTotalRequests = 0;
        }

        this._progress.fire(me, { total: this._progressTotalRequests, outstanding: outstanding });
    }

    LayerSourceLoadQueue.prototype._isDataIntensive = function (queueObj) {
        var result =
            "true" == this._getTagValue(queueObj.layer, "IsDataIntensive")
            ||
            "true" == this._model.getTagValue("IsDataIntensive");
        return result;
    }

    LayerSourceLoadQueue.prototype._getTagValue = function (layer, tagName) {
        var value = null;
        if (layer.tags) {
            layer.tags.forEach(function (cur) {
                if (cur.key == tagName) {
                    value = cur.value;
                }
            });
        }
        return value;
    }

    ns.LayerSourceLoadQueue = LayerSourceLoadQueue;

})(Mapzania);

if (!Mapzania)
    Mapzania = {};

(function (ns) {

    function Legend($el, mdl, title) {

        this.$el = $el;
        this.mdl = mdl;
        this.title = title;

        var me = this;

        mdl.schemaLoaded.add(function () {
            me.draw();
        });
    }

    Legend.prototype.draw = function () {
        this.$el.html("<div class='legend-title'>" + this.title + "</div>");

        var layers = this.mdl.getLayers();

        for (var i = 0; i < layers.length; i++) {
            if (layers[i].showInLegend == true)
                new ns.LegendItem(this.$el, this.mdl, i);
        }
    }

    ns.Legend = Legend;

    function LegendItem($parent, mdl, layerIdx) {
        var lyr = mdl.getLayers()[layerIdx];

        $parent.append("<div class='legend-item'><input type='checkbox'>" + lyr.name + "</div>");
        var cb = $parent.find("input:last");

        if (lyr.visible)
            cb.attr("checked", "checked");

        cb.click(function () {
            if (cb.is(":checked"))
                mdl.showLayer(lyr.key);
            else
                mdl.hideLayer(lyr.key);
        });

    }

    ns.LegendItem = LegendItem;

})(Mapzania);

/// <reference path="mapzania.js"/>

var Mapzania = (function (ns) {

    ns.MouseModes = {
        dragToZoomIn: 1,
        clickToZoomOut: 2,
        dragToPanAndWheelToZoom: 3,
        clickToDraw: 4
    };

    ns.DrawingModes = {
        drawPoint: 1,
        drawLine: 2,
        drawPolygon: 3,
        // TODO: Add functionality for drawing modes below
        //drawRectangle: 4,
        //drawCircle: 5,
        //drawMarker: 6
    }

    function Map(elementId, mapKey, callback, options) {

        var settings, api, model, mapper, viewport, renderer,
            toolsManager, highlightManager;

        var drawingMode = ns.DrawingModes.drawPoint;
        
        var displayWkt;
        var mouseMode;

        var me = this;

        this.init = function (elementId, mapKey, callback, options) {
            //TODO: Flesh out defaults
            var defaults = {
                displaySrid: "GOOGLE",
                apiRoot: "mz",
                isCompact:false
            };

            settings = $.extend({}, defaults, options);

            var l = window.location;
            var path = l.protocol + "//" + l.host + "/" + settings.apiRoot + "/";

            api = new Mapzania.Api2(path,settings.isCompact);

            model = new Mapzania.Model(api);

            Mapzania.Filters._setModel(model);

            model.store.changed.add(function (evt) {
                //return;
                var layerKey = evt.data;

                var ftrs = model.store.get(layerKey);

                var res = $.map(ftrs, function (ftr) {
                    return ftr.clone();
                });

                Mapzania.Projection.projectFeatureArray(res, displayWkt, "LATLONG");
                res = Mapzania.GeoJson.toFeatureCollection(res);

                me.fire(layerKey.toLowerCase() + "_layer_changed", res);
            });

            //model.stateChanged.add(function (evt) {
            //    me.mouseModeChanged.fire(me, evt);
            //})

            model.queried.add(function (evt) {
                var geom = evt.data.geom;
                Mapzania.Projection.projectGeometry(geom, displayWkt, "LATLONG");
                var res = Mapzania.GeoJson.toGeometry(geom);

                if (res.type == "Point")
                    me.fire("point_drawn", res);

                if (res.type == "LineString")
                    me.fire("line_drawn", res);

                if (res.type == "Polygon")
                    me.fire("polygon_drawn", res);

                me.fire("drawing_done", { type: "FeatureCollection", features: [{ type: "Feature", geometry: res }] });
            });

            mapper = new Canvas.Mapper();
            viewport = new Mapzania.Viewport("#" + elementId, mapper);

            viewport.mouseUp.add(function (evt) {
                me.fire("mouseup", evt);
            })

            viewport.init(function () {

                renderer = new Mapzania.Renderer(model, viewport, mapper);
                toolsManager = new Mapzania.ToolsManager(model, viewport, mapper);
                //highlightManager = new Mapzania.HighlightManager(model, viewport, renderer);

                model.schemaLoaded.add(function () {

                    var wkt = model.srids.find(function (itm) {
                        return itm.key == model.displaySRID;
                    }).val;

                    displayWkt = wkt;
                    api.wkt = wkt;

                    model.load();

                    me.setMouseMode(ns.MouseModes.dragToPanAndWheelToZoom);
                    me.setDrawingMode(ns.DrawingModes.drawPoint);

                    if (callback)
                        callback();
                });

                model.loadSchema(mapKey, settings.displaySrid);
            });
        }

        //-------------------- MAP MANIPULATION ---------------------------------------

        this.setMouseMode = function (mode) {

            if (mode > 4)
                throw "Mapzania.Map.setMouseMode: Invalid MouseMode value.";

            if (mode == ns.MouseModes.clickToDraw) {
                model.changeState(drawingMode + 5);
            }
            else {
                model.changeState(mode);
            }

            mouseMode = mode;
        }

        this.setDrawingMode = function (mode) {
            drawingMode = mode;
            me.setMouseMode(mouseMode);
        }

        this.refresh = function () {
            //HACK: must fix
            model.extent.minX++;
            model.zoomTo(model.extent);
        };

        this.reset = function () {
            model.reset();
            me.setMouseMode(ns.MouseModes.dragToPanAndWheelToZoom);
            me.setDrawingMode(ns.DrawingModes.drawPoint);
        };

        this.fitToBoundingBox = function (bbox, buffer) {

            var extent = Mapzania.GeoJson.fromBoundingBox(bbox);
            if (buffer)
                extent = extent.grow(buffer, buffer);
            Mapzania.Projection.projectExtent(extent, "LATLONG", displayWkt);
            model.zoomTo(extent);
        };

        // --------------------------- LAYERS --------------------------------------
        this.getLayers = function () {
            return model.getLayers().map(function (lyr) {
                return {
                    key: lyr.key,
                    name: lyr.name,
                    ordinal: lyr.ordinal,
                    visible: lyr.visible
                };
            });
        };

        this.getLayer = function (layerKey) {
            return me.getLayers().find(function (m) {
                m.key == layerKey;
            });
        };

        this.addLayer = function (layerKey, options) {

            if (this.getLayer(layerKey))
                throw "addLayer: a layer with that layerKey already exists.";

            var srcId = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });

            var defaults = {
                ordinal: 0,
                visible: true,
                name: "(not set)",
                sourceKey: (layerKey + "_SRC_" + srcId).toUpperCase(),
                scaleType: 0,
                allowDuplicateLabels: false,
                themes: []
            };
            settings = $.extend({}, defaults, options);

            var sources = model._sourceFactory.sources;

            sources[settings.sourceKey] = {
                key: settings.sourceKey,
                sourceType: "LOCALVECTOR"
            };

            var layer = {
                key: layerKey,
                ordinal: settings.ordinal,
                name: settings.name,
                visible: settings.visible,
                sourceKey: settings.sourceKey,
                backgroundColor: settings.backgroundColor,
                themes: settings.themes,
                allowDuplicateLabels: settings.allowDuplicateLabels
            };

            var layers = this.getLayers();

            for (var i = 0; i < layers.length; i++) {
                var lyr = layers[i];
                if (settings.ordinal < lyr.ordinal) {
                    model.addLayer(layer, i);
                    return;
                }
            }

            model.addLayer(layer, layers.length);

            return;

            var defaults = {
                ordinal: 0,
                visible: true,
                backgroundColor: null,
                name: "(not set)",
                sourceKey: layerKey + "_SRC",
                scaleType: 0,
                allowDuplicateLabels: false,
                themes: []
            };

            settings = $.extend({}, defaults, options);

            var sources = model._sourceFactory.sources;

            sources[settings.sourceKey] = {
                key: settings.sourceKey,
                sourceType: "LOCALVECTOR"
            };

            var layer = {
                key: layerKey,
                ordinal: settings.ordinal,
                name: settings.name,
                visible: settings.visible,
                sourceKey: settings.sourceKey,
                themes: settings.themes,
                backgroundColor: settings.backgroundColor,
                allowDuplicateLabels: settings.allowDuplicateLabels,
                src: new Mapzania.LocalVectorLayerSource(settings.sourceKey, model.store),
                ignoreSchema: true
            };

            var layers = this.getLayers();

            for (var i = 0; i < layers.length; i++) {
                var lyr = layers[i];
                if (settings.ordinal < lyr.ordinal) {
                    model.addLayer(layer, i);
                    return;
                }
            }
        }

        this.updateLayer = function (layerKey) {

            var layer = model.getLayer(layerKey);

            if (layer.src.source)
                layer.src.source.cacheKey = null;

            model.refreshLayer(layerKey);
        }

        this._clearLayer = function (layerKey) {
            mapper.clearLayer(layerKey);
        }

        //---------------------LAYER MANIPULATION--------------------------------

        this.showLayer = function (layerKey) {
            model.showLayer(layerKey);
        };

        this.hideLayer = function (layerKey) {
            model.hideLayer(layerKey);
        };

        this.addFeatures = function (layerKey, features) {

            var ftrs = [];

            if (features)
                ftrs = Mapzania.GeoJson.fromFeatureCollection(features);
            Mapzania.Projection.projectFeatureArray(ftrs, "LATLONG", displayWkt);

            var layer = model.getLayer(layerKey);
            layer.src.setFeatures(ftrs);
        }

        this.clearFeatures = function (layerKey) {
            //model.store.set(layerKey, []);
            //renderer.updateLayer(layerKey);
            var layer = model.getLayer(layerKey);
            layer.src.setFeatures([]);
        }

        //-------------------- FILTERS ------------------------------------------

        this._setFilter = function (layerKey, filter) {
            var layer = model.getLayer(layerKey);
            layer.filter = filter;
        }

        this.getFilters = function (layerKey) {
            var src = model.getLayer(layerKey).src;

            var res = src.filters || [];

            return res;
        }

        this.addFilter = function (layerKey, filter, index) {
            var src = model.getLayer(layerKey).src;

            if (!src.filters)
                src.filters = [];

            if (index)
                src.filters.splice(index, 0, filter)
            else
                src.filters.push(filter);
        }

        this.removeFilter = function (index) {
            var src = model.getLayer(layerKey).src;
            src.filters.splice(index, 1);
        }

        this.clearFilters = function (layerKey) {
            var src = model.getLayer(layerKey).src;

            if (src.filters)
                src.filters.length = 0;
        }

        //-------------------------TOOLBAR----------------------------------------

        function buildToolbar($el, options) {

            for (var i = 0; i < options.layout.length; i++) {
                var $btn = $("<button></button");
                var btnKey = options.layout[i];
                var btn = ns.Toolbar.buttons[btnKey];

                if (!btn)
                    continue;

                $btn.addClass(btn.key);
                $btn.addClass("btn");

                if (options.showIcons) {
                    //if (typeof $.fn.button == "function") { }
                    //else {
                    $btn.append("<img src='" + options.iconPath + "/" + btn.key + ".png' />");
                    //}
                }

                if (options.showText)
                    $btn.append("<span>" + btn.text + "</span>");
                $el.append($btn);
                //$el.append("<button class='" + btn.key + "'>" + btn.text + "</button>");
            }
        }

        this.createToolbar = function (elementId, notificationElementId, options) {

            var defaults = {
                layout: ns.Toolbar.Layouts.simple,
                showText: true,
                showIcons: false
            };

            settings = $.extend({}, defaults, options);

            var $el = $("#" + elementId);

            if ($el.children().length == 0)
                buildToolbar($el, settings);

            var tb = new Mapzania.Toolbar($el, model);

            model.stateChanged.add(function (e) {
                var msg = $("#" + notificationElementId);

                switch (e.data) {
                    case Mapzania.MapState.zoomIn:
                        msg.html("Click & drag to zoom in");
                        break;
                    case Mapzania.MapState.zoomOut:
                        msg.html("Click to zoom out");
                        break;
                    case Mapzania.MapState.pan:
                        msg.html("Click & drag to pan");
                        break;
                    case Mapzania.MapState.length:
                        msg.html("Click to measure distance");
                        break;
                    case Mapzania.MapState.area:
                        msg.html("Click to measure area");
                        break;
                    case Mapzania.MapState.queryPoint:
                        msg.html("Click a point on the map to query");
                        break;
                    case Mapzania.MapState.queryLine:
                        msg.html("Click line points on the map to query");
                        break;
                    case Mapzania.MapState.queryPolygon:
                        msg.html("Click polygon points on the map to query");
                        break;
                    case Mapzania.MapState.printMap:
                        msg.html("Print map");
                        DemoTools.terrascapePrintMap(me.mapModel);
                        break;
                    case Mapzania.MapState.exportMap:
                        msg.html("Export map");
                        DemoTools.terrascapeExportMap(me.mapModel);
                        break;
                    default:
                        console.log("Unknown Map State: " + e.data);
                        break;
                }
            });
        }

        // -------------------------- QUERY MAP   -----------------------------------

        this.query = function (layerKey, filters, options) {
            var defaults = {
                simplified: false,
                //ignoreLayerFilter: false,
                ignoreAppliedFilters: false
            };

            settings = $.extend({}, defaults, options);

            filters = [].concat(filters);

            var fact = Mapzania.Filters;

            var layer = model.getLayer(layerKey);

            var allFilters = [];

            //if (!settings.ignoreMapFilters) {
            //TODO: Bring in the theme filter
            if (layer.filter && layer.filter != "")
                allFilters.push(fact.filterByText(layer.filter)); 
            //}

            if (!settings.ignoreAppliedFilters) {
                allFilters = allFilters.concat(me.getFilters(layerKey));
            }

            if (settings.simplified) {
                //TODO: Implement options.simplified
                throw new "Mapzania.map.query: options.simplified is not yet implemented."
            }

            allFilters = allFilters.concat(filters);

            api.getFeatures2(layer.src.key, allFilters, function (data) {
                var res = Mapzania.GeoJson.toFeatureCollection(data);
                me.fire(layerKey.toLowerCase() + "_layer_queried", res);
            }, { isProjected: false });
        }

        this.getFeatures = function (layerKey) {
            var ftrs = model.store.get(layerKey) || [];

            var clonedFeatures = ftrs.map(function (m) {
                return m.clone();
            });

            Mapzania.Projection.projectFeatureArray(clonedFeatures, displayWkt, "LATLONG");
            var res = Mapzania.GeoJson.toFeatureCollection(clonedFeatures);
            return res;
        }

        this.getBoundingBox = function () {
            var ext = new Mapzania.Extent(model.extent);
            Mapzania.Projection.projectExtent(ext, displayWkt, "LATLONG");
            return [ext.minX, ext.minY, ext.maxX, ext.maxY];
        };

        this.getInitialBoundingBox = function () {
            var ext = new Mapzania.Extent(model.startExtent);
            Mapzania.Projection.projectExtent(ext, displayWkt, "LATLONG");
            return [ext.minX, ext.minY, ext.maxX, ext.maxY];
        };

        // ------------------------ STYLING --------------------------------------

        this.addStyle = function (layerKey, style) {
            var layer = model.getLayer(layerKey);
            layer.ignoreSchema = true;
            layer.style = style;
        }

        this.clearStyles = function (layerKey) {
            var layer = model.getLayer(layerKey);
            layer.ignoreSchema = false;
            layer.style = null;
        }

        // -------------------------- EVENTS   -----------------------------------

        var listeners = [];

        this.on = function (type, fn) {
            listeners.push({
                type: type,
                once: false,
                fn: fn
            });
        }

        this.once = function (type, fn) {
            listeners.push({
                type: type,
                once: true,
                fn: fn
            });
        }

        this.off = function (type) {

            for (var i = listeners.length - 1; i >= 0 ; i--) {

                var l = listeners[i];

                if (l.type == type)
                    listeners.splice(i, 1);
            }
        }

        this.fire = function (type, data) {
            console.log(type + " fired. (" + listeners.length + " listeners)");

            for (var i = listeners.length - 1; i >= 0 ; i--) {

                var l = listeners[i];

                if (l.type == type) {
                    l.fn(data);
                    if (l.once)
                        listeners.splice(i, 1);
                }
            }
        }

        // -----------------------------------------------------------------------

        this.init(elementId, mapKey, callback, options);
    };

    ns.Map = Map;

    return ns;

})(Mapzania || {});
if (!Mapzania)
	var Mapzania = {};

/**
 * @doc Mapzania.Model.*ClassDescription*
 *
 * Applications uses the Mapzania.Model class as the primary entry point for interacting with a Mapzania map. Events are exposed, allowing applications to listen
 * for various events generated by the interactive map. Examples of these are when a map is panned or zoomed, or when a layer is set visible or hidden. 
 * Additionally, functions are provided to perform actions on the map. Examples of these are the ability to add or removed layers and the ability to apply filters to existing layers.
 */
(function (ns) {
    ns.MapState = {
        zoomIn: 1,
        zoomOut: 2,
        pan: 3,
        length: 4,
        area: 5,
        queryPoint: 6,
        queryLine: 7,
        queryPolygon: 8,
        printMap: 9,
        exportMap: 10,
        queryPolygonEnclosed: 11
    }

    /**
      * @doc Mapzania.Model#Model
      *
      * Constructor.
      *
      * @param api (Mapzania.Api) A handle to a valid Mapzania.Api instance.
      */
    function Model(api) {
        var me = this;

        this.store = new ns.FeatureStore();

        Mapzania.Debug = Mapzania.Debug || {};
        Mapzania.Debug.store = this.store;
        Mapzania.Debug.model = this;

        /**
          * @doc Mapzania.Model.api (Mapzania.Api)
          *
          * A handle to the API instance for the model. All interactions with the Mapzania server / geospatial API is conducted using this API instance. 
          */
		this.api = api;

        /**
          * @doc Mapzania.Model.schemaLoaded (Mapzania.Event)
          *
          * An event fired when the map schema has been set, or after the map schema was loaded. Applications will typically call the <i>load</i> method on this
          * model instance after the schema was loaded, to indicate that the layer data needs to start loading. 
          */
		this.schemaLoaded = new ns.Event();

        /**
          * @doc Mapzania.Model.loading (Mapzania.Event)
          *
          * An event fired when map data has started to load. The data associated with the event is the extent loading.
          * Note that this event may be fired once when an entire map has started to load, or it may fire when individual layers
          * are loading.
          */
		this.loading = new ns.Event();

        /**
          * @doc Mapzania.Model.loadingProgress (Mapzania.Event)
          *
          * An event fired to indicate that the loading progress of the map has changed. The event data contains the 
          * number of requests queued and in progress. Applications typically do not make use of this event, and is used 
          * internally by the application framework to render a progress bar on interactive maps.
          */
		this.loadingProgress = new ns.Event();

        /**
          * @doc Mapzania.Model.loaded (Mapzania.Event)
          *
          * An event fired when map data has loaded and is ready to be rendered. The event contains the data to be rendered, which may
          * be for multiple layers. Applications typically do not make use of this event, and is used 
          * internally by the application framework to render map layers.
          */
		this.loaded = new ns.Event();

        /**
          * @doc Mapzania.Model.zoomed (Mapzania.Event)
          *
          * An event fired when the map display area has changed. The event data is the extent zoomed to.
          */
		this.zoomed = new ns.Event();

        /**
          * @doc Mapzania.Model.stateChanged (Mapzania.Event)
          *
          * An event fired when the map toolbar selection state has changed. Refer to the object Mapzania.MapState
          * for a complete set of map toolbar states. For example, this event fires when the user selects the "Query point" toolbar
          * item. The numeric identifier associated with this state is 6. 
          */
		this.stateChanged = new ns.Event();

        /**
          * @doc Mapzania.Model.layerShown (Mapzania.Event)
          *
          * An event fired when the showLayer method was invoked on the model. This indicates that a map layer needs to become visible.  
          */
		this.layerShown = new ns.Event();

        /**
          * @doc Mapzania.Model.layerHidden (Mapzania.Event)
          *
          * An event fired when the hideLayer method was invoked on the model. This indicates that a map layer needs to become hidden.  
          */
		this.layerHidden = new ns.Event();

        /**
          * @doc Mapzania.Model.layerCleared (Mapzania.Event)
          *
          * An event fired when the clearLayer method was invoked on the model. This indicates that a map layer needs to be cleared from any feature or raster content.  
          */
		this.layerCleared = new ns.Event();

        /**
          * @doc Mapzania.Model.layerAdded (Mapzania.Event)
          *
          * An event fired when the addLayer method was invoked on the model. This indicates that a dynamic map layer was added to the map schema.  
          */
		this.layerAdded = new ns.Event();

        /**
          * @doc Mapzania.Model.layerRemoved (Mapzania.Event)
          *
          * An event fired when the removeLayer method was invoked on the model. This indicates that a layer needs to be dynamically removed the map schema.  
          */
		this.layerRemoved = new ns.Event();

        /**
          * @doc Mapzania.Model.gotLength (Mapzania.Event)
          *
          * An event fired when the getLength method was invoked on the model. The event data indicates the length of the line joining the points that were passed
          * as parameters to the getLength function.
          */
		this.gotLength = new ns.Event();

        /**
          * @doc Mapzania.Model.gotArea (Mapzania.Event)
          *
          * An event fired when the getArea method was invoked on the model. The event data indicates the area of the polygon joining the points that were passed
          * as parameters to the getArea function.
          */
		this.gotArea = new ns.Event();

        /**
          * @doc Mapzania.Model.queried (Mapzania.Event)
          *
          * An event fired when the queryAt method was invoked on the model. The event data is the same as the data passed to the method queryAt.
          * An example of the event data is an object with the format { type: 3, geom: res, enclosed: true }, where type represents the geometry type to be queried
          * and geom the corresponding geometry instance. The enclosed field is applicable to Polygon shape queries and have a value of true or false. 
          */
		this.queried = new ns.Event();

		this._viewportSize = null; // A hint for the viewport size. A value might not always be available.

		this._sourceFactory = null; // A handle to the source factory. 

		this._layerSourceLoadQueue = new ns.LayerSourceLoadQueue(this);
		this._layerSourceLoadQueue._progress.add(function (e) {
		    me.loadingProgress.fire(e.source, e.data);
		});

		this._vtlgManager = new ns.VTLGManager(this);

        // Private propperties only used by Leaflet 
		this._disableLoadOnZoomTo = false;
		this._ignorePanInZoomTo = false;
	}

    /**
      * @doc Mapzania.Model.loadSchema
      *
      * Uses the API instance to load the map schema definition. Once the map schema is loaded, the setSchema 
      * method is called by this method to set the map schema on the model. Applications will typicaly call the
      * load() method once the schema has been set. This can be achieved by listening 
      * and reacting to the schemaLoaded event, as the schemaLoaded event is only fired once the map
      * schema is set.
      *
      * @param mapKey   (String)    The map key to load, as defined in the map schema file.
      *
      * @param srid     (String)    The display coordinate system to use when displaying the map. This parameter may 
      *                             be omitted, in which case the displaySRID property from the map schema is used as a default.
      */

	Model.prototype.loadSchema = function (mapKey, srid) {
		var me = this;

		this.api.getSchema(mapKey, srid, function (schema) {
			if (srid)
				schema.displaySRID = srid;

			me.api.getCoordinateSystems(function (data) {
			    me.srids = data;
			    me.setSchema(schema);
			});
		});
	}

    /**
      * @doc Mapzania.Model.setSchema
      *
      * Typically, applications will not be calling this method directly. This method is used to set 
      * the map schema and is called by the method loadSchema(mapKey, srid).
      * Applications may use this method to set the map schema when the schema was loaded seperately using the Mapzania API,
      * or when an in-memory map schama was created on the client side. 
      *
      * @param schema   (Object)    The map schema object.
      */

	Model.prototype.setSchema = function (schema) {
		$.extend(true, this, schema);

		this.startExtent = new ns.Extent(this.startExtent);
		this._setCurrentExtentTo(this.startExtent);
		
		var sourceFactory = new Mapzania.SourceFactory(this.api, this.displaySRID, this._layerSourceLoadQueue, this._vtlgManager);
		var me = this;
		sourceFactory.init(function () {
			for (var i = 0; i < me.layers.length; i++) {
				var layer = me.layers[i];
				me._initLayer(sourceFactory, layer);
			}
			me.schemaLoaded.fire(me, schema);
		});

		this._sourceFactory = sourceFactory;
	}

    /**
      * @doc Mapzania.Model.getSchema
      *
      * Obtain the map schema object used by the model. This
      * schema may be saved to the Mapzania server as a temporary map, using the 
      * method saveSchema on the Mapzania API.
      *
      * @return (Object)    A map schema object, 
      *                     representing the current map. This
      *                     schema may be saved to the Mapzania server as a temporary map, using the 
      *                     method saveSchema on the Mapzania API.
      */
	Model.prototype.getSchema = function () {
	    var schema =
            {
                backColor: this.backColor,
                backImageUrl: this.backImageUrl,
                backOpacity: this.backOpacity,
                displaySRID: this.displaySRID,
                key: this.key,
                layers: this.layers,
                name: this.name,
                namedZooms: this.namedZooms,
                rotation: this.rotation,
                sRID: this.displaySRID,
                scaleType: this.scaleType,
                startExtent: new ns.Extent(this.extent),
                tags: this.tags,
                unifiedZoomSizes: this.unifiedZoomSizes,
                snapUIToUnifiedZoomSizes: this.snapUIToUnifiedZoomSizes,
                removeConflictingLabels: this.removeConflictingLabels
            };
	    var schemaDeep = {};
	    $.extend(true, schemaDeep, schema);
	    schemaDeep.layers.forEach(function (cur) { cur.src = null;});
	    return schemaDeep;
	}

    /**
      * @doc Mapzania.Model.addLayer
      *
      * Adds a new layer to the map schema, display the layer if visible and fires the layerAdded
      * event. The showLayer event is fired if the layer is visible and displayed. The interactive map
      * User Interface is updated as a result of this call. 
      *
      * @param layer    (Object)    The schema for the new layer to be added.
      *
      * @param indexSchema    (Number)    An optional index for the layer . Smaller indexes are 
      *                                   placed closer to the user, i.e. top of the map.
      */
	Model.prototype.addLayer = function (layer, indexSchema) {
	    if (!indexSchema) {
	        indexSchema = 0;
	    }

	    layer.src = undefined;
	    this.layers.splice(indexSchema, 0, layer);
	    this._initLayer(this._sourceFactory, layer);
	    this.layerAdded.fire(this, { layer: layer, indexSchema: indexSchema, layers: this.layers });

	    if (layer.visible) {
	        this.loading.fire(this, this.extent);
	        this.showLayer(layer.key);
	    }
	}
    
    /**
      * @doc Mapzania.Model.removeLayer
      *
      * Removes a layer from the model and fire the layerRemoved event. The interactive map
      * User Interface is updated as a result of this call. 
      * 
      * @param key      (String)    The layer key.
      */
	Model.prototype.removeLayer = function (key) {
	    var layer = this.getLayer(key);
	    var index = -1;
	    $.each(this.layers, function (i, cur) {
	        if (cur.key == key) index = i;
	    });

	    this.layers.splice(index, 1);
	    this.layerRemoved.fire(this, { layer: layer});
	}

    /** 
      * Initializes a layer by setting the layer source and attached appropriate load
      * events (for loading features and tiles on the layer).
      * 
      * @param sourceFactory    (Mapzania.SourceFactory)  A handle to the Mapzania Source Factory.
      *
      * @param layer            (Object)  A handle to the layer object.
      */
	Model.prototype._initLayer = function (sourceFactory, layer) {

	    var src = sourceFactory.getSource(layer.sourceKey, layer, this);
	    layer.src = src;
	    src.layer = layer;

	    if (layer.src.gotFeatures) {
	        layer.src.gotFeatures.add(function (evt) {
	            //---ANDRE----
	            this.store.set(evt.source.layer.key, evt.data.features);
                //------------
	            if (this.noOfLoaded > 0) {
	                this.loadedData.push({ layer: evt.source.layer, data: evt.data });

	                if (this.loadedData.length == this.noOfLoaded) {
	                    //console.log("loadedData=" + this.loadedData.length);
	                    //console.log(this.loadedData);
	                    this.noOfLoaded = -1;
	                    this.loaded.fire(this, this.loadedData);
	                }
	            }
	            else {
	                var data = [{ layer: evt.source.layer, data: evt.data }];
	                this.loaded.fire(this, data);
	            }
	        }.bind(this))
	    }

	    if (layer.src.gotTiles) {
	        layer.src.gotTiles.add(function (evt) {
	            var data = [{ layer: evt.source.layer, data: evt.data }];
	            this.loaded.fire(this, data);
	        }.bind (this));
	    }
	}

    /**
      * @doc Mapzania.Model.load
      *
      * Start loading the map data (features / raster image tiles) for each visible layer. This method
      * should be called after the map schema was loaded or set on this model.
      * 
      * @param toDelayVectorData (Boolean) A flag indicating that the loading of vector data needs to be delayed slightly, to allow for raster tiles to load first
      */
	Model.prototype.load = function (toDelayVectorData) {
		this.noOfLoaded = 0;
		this.loadedData = [];

		toDelayVectorData = !(!toDelayVectorData);
		this.loading.fire(this, this.extent);
		var me = this;
		for (var i = 0; i < this.layers.length; i++) {
		    var layer = this.layers[i];

		    if (layer.src.getFeatures) {
		        setTimeout(function (layer) {
		            me._getFeatures(layer);
		        }, toDelayVectorData ? 1000 : 0, layer);
		    }

		    if (layer.src.getTiles) {
		        this._getTiles(layer);
		    }
		}
	}

    /**
      * @doc Mapzania.Model.getTheme
      *
      * Determines and return the active theme for a layer, using the current zoom level for the map. 
      * 
      * @param layer            (Object)  A handle to the layer object.
      *
      * @return (Object)        A handle to the theme that is applicable to the current zoom range.
      *                         A value of null is returned to indicate that no zoom range is applicable.
      */
	Model.prototype.getTheme = function (layer) {
	    var zoom = Math.max(this.extent.width(), this.extent.height());
	    if (this.currentZoomSize) {
	        zoom = this.currentZoomSize.zoom;
	    }

	    for (var i = 0; i < layer.themes.length; i++) {
	        var theme = layer.themes[i];
	        if (zoom >= theme.minZoom && zoom <= theme.maxZoom)
	            return theme;
	    }
	    return null;
	}

    /**
      * @doc Mapzania.Model.updateLayer
      *
      * This method currently does nothing and is kept for backwards compatibility reasons. 

      */
	Model.prototype.updateLayer = function (key) {
	}

    /**
      * @doc Mapzania.Model.getFilter
      *
      * Calculates a filter string applicable to a given layer and the active theme on that layer. Typically, application will not
      * make use of this method, and is used by the Mapzania.Model class internally. An example value returned by this method
      * is: (Type='1') AND (Age<10), where the former sub-expression is the filter defined on the layer and the latter sub-expression
      * is the filter defined on the theme.
      * 
      * @param layer            (Object)  A handle to the layer object.
      *
      * @param theme            (Object)  A handle to the active theme object (optional).
      *
      * @return (String)        A combined filter for the layer and theme.
      */
	Model.prototype.getFilter = function (layer, theme) {
		var filter;

		if (layer.filter && theme && theme.filter)
			filter = "(" + theme.filter + ") AND (" + layer.filter + ")";
		else
			if (theme && theme.filter)
				filter = theme.filter;
			else
				filter = layer.filter;

		return filter;
	}

    /**
      * @doc Mapzania.Model.getLayers
      *
      * Returns the array of layers from the map model.
      * 
      * @return (Array)        An array of layer objects.
      */
	Model.prototype.getLayers = function () {
		return this.layers;
	}

    /**
      * @doc Mapzania.Model.getLayer
      *
      * Retrieves a layer, provided a layer key. The implementation generates an exception
      * if the layer key does not exist in the map schema.
      * 
      * @param key              (String)  The layer key.
      *
      * @return (Object)        A layer object from the map schema.
      *
      */
	Model.prototype.getLayer = function (key) {
		for (var i = 0; i < this.layers.length; i++) {
			var lyr = this.layers[i];
			if (lyr.key == key)
				return lyr;
		}

		throw "Model.getLayer: Invalid layerKey:" + key;
	}

    /**
      * @doc Mapzania.Model.getLayerSource
      *
      * Retrieves the corresponding layer source instance for a layer key. The implementation generates an exception
      * if the layer key does not exist in the map schema. Layer sources can be used to manipulate the content
      * of layers on the map (i.e. add / update / delete Features on a layer).
      * 
      * @param key              (String)  The layer key.
      *
      * @return (Object)        A layer source instance.
      */
	Model.prototype.getLayerSource = function (key) {
	    for (var i = 0; i < this.layers.length; i++) {
	        var lyr = this.layers[i];
	        if (lyr.key == key)
	            return lyr.src;
	    }

	    throw "Model.getLayerSource: Invalid layerKey:" + key;
	}

    /**
      * @doc Mapzania.Model.hasLayer
      *
      * Determines if a layer with a given layer key exist.
      * 
      * @param key              (String)  The layer key.
      *
      * @return (boolean)        True if the layer key exists, otherwise false.
      */
	Model.prototype.hasLayer = function (key) {
	    for (var i = 0; i < this.layers.length; i++) {
	        var lyr = this.layers[i];
	        if (lyr.key == key)
	            return true;
	    }
	    return false;
	}

    /**
      * @doc Mapzania.Model.filterLayer
      *
      * Applies a filter to a Vector layer. The feature data for that layer is loaded and the
      * interactive map UI is updated accordingly.
      * 
      * @param key              (String)  The layer key.
      *
      * @param filter           (String)  A valid filter clause.
      */
	Model.prototype.filterLayer = function (key, filter, fence) {
	    var lyr = this.getLayer(key);
	    lyr.filter = filter;
	    lyr.fence = fence;

		if (this.updating)
			this.noOfLoaded++;

		this.loading.fire(this, this.extent);
		this.clearLayer(key);
		this._getFeatures(lyr);
	}

    /**
      * @doc Mapzania.Model.refreshLayer
      *
      * Refresh the display content of a layer.
      * 
      * @param key              (String)  The layer key.
      */
	Model.prototype.refreshLayer = function (key) {
	    var layer = this.getLayer(key);
	    this.clearLayer(key);

	    if (layer.src.getFeatures) {
            this._getFeatures(layer);
	    }

	    if (layer.src.getTiles) {
	        this._getTiles(layer);
	    }
	}

    /**
      * @doc Mapzania.Model.setFeaturesOnSchema
      * 
      * This method allows for features and geometries that are created on the web front-end, to be draw on the interactive map. 
      * For this functionality to work correctly, applications needs to ensure that the layer source for this layer is a valid pre-defined
      * "SCHEMASOURCE" layer source. 
      * 
      * @param layerKey         (String)  The layer key. This layer's source key needs to be a SCHEMASOURCE-*
      *                                   layers. This method generates an exception if the layer is not a SCHEMASOURCE layer. 
      *
      * @param filter           (String)  An array, of Mapzania.Feature objects. The SRID for these features are assumed to be the same
      *                                   as the Display SRID of the map.
      */

	Model.prototype.setFeaturesOnSchema = function (layerKey, features) {
	    var lyr = this.getLayer(layerKey);
	    if (!lyr.src.setFeatures)
	        throw "The layer [" + layerKey + "] does not seem to be a 'schema source' (SCHEMASOURCE-*) layer.";
	    lyr.src.setFeatures(features);
	    this.clearLayer(layerKey);
	    this._getFeatures(lyr);
	}

    /**
      * @doc Mapzania.Model.changeState
      *
      * Fires a stateChanged event. See the description on the event stateChanged for more information.
      * 
      * @param state            (Number)  The new map state. Refer to the object Mapzania.MapState
      *                                   for a complete set of map toolbar states.
      */
	Model.prototype.changeState = function (state) {
		this.stateChanged.fire(this, state);
	}

    /**
      * @doc Mapzania.Model.zoomTo
      *
      * Sets the display extent of the model, load the new layer data according to the new 
      * extent and fires a zoomed event.
      * 
      * @param extent (Mapzania.Extent)   A handle to the new display extend 
      *                                     in the display coordinates of the map.
      *
      * @param isZoomOut (boolean)          A flag to indicate if this is a zoom-out operation, using the Zoom Out map toolbar button. 
      *
      * @param isZoomIn (boolean)          A flag to indicate if this is a zoom-in operation, using the Zoom In map toolbar button. 
      *
      * @param toDelayVectorData (Boolean) A flag indicating that the loading of vector data needs to be delayed slightly, to allow for raster tiles to load first
      *
      * @param isPan (boolean)             A flag to indicate if this is a map pan operation, using the Pan map toolbar button. 
      */
	Model.prototype.zoomTo = function (extent, isZoomOut, isZoomIn, toDelayVectorData, isPan) {
	    if (this._ignorePanInZoomTo && isPan) {
	        return;
	    }

	    this._setCurrentExtentTo(extent, isZoomOut, isZoomIn);

	    for (var i = 0; i < this.layers.length; i++) {
	        var key = this.layers[i].key;
	        this.clearLayer(key);
	    }

	    this.zoomed.fire(this, this.extent);

	    if (!this._disableLoadOnZoomTo) {
	        this.load(toDelayVectorData);
	    }
	}

	Model.prototype._zoomToLeaflet = function (extent, zoom) {
	    this.extent = extent;
	    this.currentZoomSize = null;
	    for (var i = 0; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];
	        if (zoom == cur.zoom) {
	            this.currentZoomSize = cur;
	            break;
	        }
	    }
	    
	    for (var i = 0; i < this.layers.length; i++) {
	        var key = this.layers[i].key;
	        this.clearLayer(key);
	    }

	    this.load(true);
	}

    /**
      * @doc Mapzania.Model.zoomIn
      *
      * Performs a map zoom-in (around an optionally specified point) and update the map user interface. 
      *
      * @param point (Mapzania.Point) An optional value, indicatig the center point of the zoom-in operation. The center of the map will be used if this parameter is not specified.
      */

	Model.prototype.zoomIn = function (point) {
	    this._zoomWithFactorAroundPoint(0.5, point, false, true);
	}

    /**
      * @doc Mapzania.Model.zoomOut
      *
      * Performs a map zoom-out and update the map user interface. 
      *
      * @param point (Mapzania.Point) An optional value, indicatig the center point of the zoom-out operation. The center of the map will be used if this parameter is not specified.
      */
	Model.prototype.zoomOut = function (point) {
	    this._zoomWithFactorAroundPoint(1.8, point, true, false);
	}

    // Internal utility method used when zooming in or out
	Model.prototype._zoomWithFactorAroundPoint = function (factor, point, isZoomOut, isZoomIn) {
	    var curExtent = new Mapzania.Extent(this.extent);
	    if (!point) {
	        point = curExtent.getCenter();
	    }
	    var curCenter = curExtent.getCenter();
	    var dx = point.x - curCenter.x;
	    var dy = point.y - curCenter.y;
	    var w = curExtent.width();
	    var h = curExtent.height();
	    var pdx = dx / (w / 2);
	    var pdy = dy / (h / 2);

	    var newExtent = curExtent.grow(factor);
	    var result = this._calculateBestFitExtentAndZoomSize(newExtent, isZoomOut, isZoomIn);
	    newExtent = result.extent;
	    var w2 = newExtent.width();
	    var h2 = newExtent.height();

	    var pointNew = new Mapzania.Point(curCenter.x + (w2 / 2) * pdx, curCenter.y + (h2 / 2) * pdy);
	    newExtent.translate(-(pointNew.x - point.x), -(pointNew.y - point.y));

	    this.zoomTo(newExtent, null, null, true);
	}

    /**
      * @doc Mapzania.Model.refresh
      *
      * Refresh and update the map UI, by reloading all the visible layers using the current map extent.
      * 
      */
	Model.prototype.refresh = function () {
	    this.zoomTo(this.extent);
	}

    /**
      * Internal utility method. Set the current extent, and possibly update that extent to suite
      * sticky zooms and the aspect ratio of the map display.
      *
      */
	Model.prototype._setCurrentExtentTo = function (extent, isZoomOut, isZoomIn) {
	    var x = this._calculateBestFitExtentAndZoomSize(extent, isZoomOut, isZoomIn);
	    this.extent = x.extent;
	    this.currentZoomSize = x.currentZoomSize;
	}

    // Internal utility method to determine the best fitting extent and zoom Size
	Model.prototype._calculateBestFitExtentAndZoomSize = function (extent, isZoomOut, isZoomIn) {
	    isZoomOut = !(!isZoomOut);
	    isZoomIn = !(!isZoomIn);
	    var oldExtent = this.extent;
	    var extent = this.resizeExtentToRatioDisplay(extent);
	    var currentZoomSize = null;

	    if (this.useUnifiedZoomSizes && this.unifiedZoomSizes) {
	        var oldZoomSize = this.currentZoomSize;

	        var zoomRange = Math.max(extent.width(), extent.height());
	        var usedWidth = (zoomRange == extent.width());
	        currentZoomSize = this.findSuitableZoomSize(zoomRange);

	        if (this.snapUIToUnifiedZoomSizes) {
	            if (oldZoomSize && oldExtent && oldZoomSize.zoom >= currentZoomSize.zoom && isZoomIn) {
	                currentZoomSize = this.findZoomSizeAfter(oldZoomSize.zoom);
	            }

	            var max = (usedWidth ? extent.width() : extent.height());
	            var factor = currentZoomSize.size / max;
	            extent = extent.grow(factor);
	        }
	    }

	    return { extent: extent, currentZoomSize: currentZoomSize };
	}

    /**
     * Resize an extent to fit the aspect ration of the current map display.
     */
	Model.prototype.resizeExtentToRatioDisplay = function (extent) {
	    var e = new Mapzania.Extent(extent);
	    if (this._viewportSize)
	        e.normalise(this._viewportSize);
	    return e;
	}

    /**
     * Find the most suitable zoom size, given a zoom range.
     */
	Model.prototype.findSuitableZoomSize = function (size) {
	    var res = null;
	    for (var i = 0; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];
	        if ((size < cur.size) || (Math.abs(size - cur.size) / 100 < 0.05)) {
	            res = cur;
	        }
	    }
	    if (!res)
	        res = this.unifiedZoomSizes[0];
	    return res;
	}

    /**
     * Find the zoom level before a given zoom level.
     */
	Model.prototype.findZoomSizeBefore = function (zoom) {
	    var res = null;
	    for (var i = 1; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];
	        
	        if (zoom > cur.zoom) {
	            res = cur;
	        }
	    }
	    if (!res)
	        res = this.unifiedZoomSizes[0];
	    return res;
	}

    /**
     * Find the zoom level before a given zoom level.
     */
	Model.prototype.findZoomSizeAfter = function (zoom) {
	    var res = null;
	    for (var i = 0; i < this.unifiedZoomSizes.length; i++) {
	        var cur = this.unifiedZoomSizes[i];

	        if (zoom < cur.zoom) {
	            return cur;
	        }
	    }

	    return this.unifiedZoomSizes[this.unifiedZoomSizes.length - 1];
	}

    /**
      * @doc Mapzania.Model.reset
      *
      * Resets and update the map to the start extent. 
      */
	Model.prototype.reset = function () {
		this.zoomTo(this.startExtent);
	}

    /**
      * @doc Mapzania.Model.showLayer
      *
      * Sets the visibility of a layer to true and start loading the content for the layer.
      * 
      * @param key              (String)  The layer key.
      */
	Model.prototype.showLayer = function (key) {
		var layer = this.getLayer(key);
		layer.visible = true;

		if (this.updating)
		    this.noOfLoaded++;

		this.layerShown.fire(this, key);

		if (layer.src.gotFeatures) {
		    this.clearLayer(key);
		    this._getFeatures(layer);
		}

		if (layer.src.gotTiles) {
		    this._getTiles(layer);
		}
	}

    /**
      * @doc Mapzania.Model.hideLayer
      *
      * Sets the visibilty of a layer to false and hide the layer on the interactive map.
      * 
      * @param key              (String)  The layer key.
      */
	Model.prototype.hideLayer = function (key) {
		var layer = this.getLayer(key);
		layer.visible = false;

		this.layerHidden.fire(this, key);
	}

    /**
      * @doc Mapzania.Model.clearLayer
      *
      * Clear the content of a layer on the interactive map.
      * 
      * @param key              (String)  The layer key.
      */
	Model.prototype.clearLayer = function (key) {
		this.layerCleared.fire(this, key);
	}

    /**
      * @doc Mapzania.Model.getLength
      *
      * The great-circle or orthodromic distance is the shortest distance 
      * between two points on the surface of a sphere, measured along the surface of the sphere 
      * (as opposed to a straight line through the sphere's interior). 
      * The distance between two points in Euclidean space is the length of a 
      * straight line between them, but on the sphere there are no straight lines.
      * This function returns this latter distance. 
      *
      * Note that this function does not return the length. The gotLength event is fired, with the
      * lenght as the event data.
      * 
      * @param points              (Array)  An array of Mapzania.Points object instance.
      */
	Model.prototype.getLength = function (points) {
		var me = this;

		var coords = [];

		for (var i = 0; i < points.length; i++) {
			var pnt = points[i];
			coords.push(pnt.x);
			coords.push(pnt.y);
		}
		this.api.getLength(this.key, this.displaySRID, coords, function (val) {
			me.gotLength.fire(me, val);
		});
	}

    /**
     * 
     * @doc Mapzania.Model.getArea 
     *
     * This method calculates the area of the polygon on the surface of the earth. 
     *
     * Note that this function does not return the area. The gotArea event is fired, with the
     * area as the event data.
     * 
     * @param points              (Array)  An array of Mapzania.Points object instance.
     */
	Model.prototype.getArea = function (points) {
		var me = this;

		var coords = [];

		for (var i = 0; i < points.length; i++) {
			var pnt = points[i];
			coords.push(pnt.x);
			coords.push(pnt.y);
		}
		this.api.getArea(this.key, this.displaySRID, coords, function (val) {
			me.gotArea.fire(me, val);
		});
	}

    /**
     * @doc Mapzania.Model.queryAt
     *
     * Fires the queried event. 
     *
     * @param data (Object)  Event data. See the queried event description for more details on this object.
     */
	Model.prototype.queryAt = function (data) {
		this.queried.fire(this, data);
	}

    /*
     * Internal utility method. Called from the viewport. 
     */
	Model.prototype.setViewportSize = function (size) {
	    this._viewportSize = size;
	}

    /**
      * @doc Mapzania.Model.getTagValue
      *
      * Get the value of a a tag associated with the map.
      * 
      * @param tagName         (String)  The tag name.
      *
      * @return (String)        The corresponding tag value.
      */
	Model.prototype.getTagValue = function (tagName) {
	    var value = null;
	    if (this.tags) {
	        this.tags.forEach(function (cur) {
	            if (cur.key == tagName) {
	                value = cur.value;
	            }
	        });
	    }
	    return value;
	}

    // Starts loading the layer data, or clear the layer when it is not visible
	Model.prototype._getFeatures = function (layer) {
	    var theme = this.getTheme(layer);
	    var sourceParams = [];
	    if (theme && theme.sourceParams)
	        theme.sourceParams.forEach(function (cur) { sourceParams.push(cur); });

	    if (this._viewportSize) {
	        sourceParams.push({ name: "DisplaySizePixelWidth", value: this._viewportSize.width });
	        sourceParams.push({ name: "DisplaySizePixelHeight", value: this._viewportSize.height });
	    }
	    sourceParams.push({ name: "DisplaySizeMeterWidth", value: this.extent.width() });
	    sourceParams.push({ name: "DisplaySizeMeterHeight", value: this.extent.height() });
	    if (this.currentZoomSize) {
	        sourceParams.push({ name: "ZoomSize", value: this.currentZoomSize.zoom });
	    }

	    var filter = this.getFilter(layer, theme);

	    var labelVisibilityClause = null;
	    if (theme && theme.labeller && theme.labeller.visibilityClause)
	        labelVisibilityClause = theme.labeller.visibilityClause;

	    if (layer.visible) {
	        var fields = [];
	        if (layer.fields)
	            layer.fields.forEach(function (cur) { if (!cur.expression) { fields.push(cur.key); } });
	        var fieldsStr = null;

	        if (fields.length > 0)
	            fieldsStr = fields.join(",");

	        if (theme)
	            layer.src.getFeatures(filter, this._getExtentToLoad(this.extent), fieldsStr, theme.thinAlgo, theme.thinFactor, theme.accuracy, labelVisibilityClause, sourceParams, layer.fence);
	        else
	            layer.src.getFeatures(filter, this._getExtentToLoad(this.extent), fieldsStr, null, null, null, labelVisibilityClause, sourceParams, layer.fence);
	    }
	    else {
	        this.clearLayer(layer.key);
	        // Sometimes, the loading event is fired, but no data is actually loaded, leaving the renderer in a loading state. This workaround fixes that issue. 
	        var me = this;
	        setTimeout(function (cur) {
	            me.loaded.fire(me, [{ layer: layer, data: { seqNo: 0, features: [], isFinal: true } }]);
	        }, 100);
	    }
	}

	Model.prototype._getTiles = function (layer) {
	    if (layer.visible) {
	        layer.src.getTiles(this._getExtentToLoad(this.extent), this.extent, layer.filter);
	    }
	}

	Model.prototype._getExtentToLoad = function (extent) {
	    var rotateZ = (this.rotation && this.rotation.rotateZ ? this.rotation.rotateZ : 0.0);
	    return this.extent.grow( (1 + Math.abs( Math.sin(rotateZ) ) ) * 1.5);
	}

	Model.prototype._getSmallestLayerOrdinal = function () {
	    var MAX = 99999999999;
	    var ordinal = MAX;
	    this.getLayers().forEach(function (cur) {
	        if (cur.ordinal < ordinal) ordinal = cur.ordinal;
	    });
	    if (ordinal == MAX) ordinal = 0;
	    return ordinal;
	}

	ns.Model = Model;

})(Mapzania)

var Mapzania = (function (ns) {
    QueryType = { point: 1, polygon: 2, polygonEnclosed: 3 };
    QueryGeometryType = { point: "Point", polygon: "Polygon" };

    var me;

    function Query(api, model) {

        this.model = model;
        this.api = api;
        me = this;

        this.queryAtPoint = function (layerKey, point, callback, options) {

            var defaults = {
                includeArea: false,
                includeLength: false,
                visibleOnly: true,
                bufferPerc: 2
            };

            var settings = $.extend({}, defaults, options);

            //Get map's current extent
            var mapExtent = me.model.extent;

            //Set point buffer
            var buffer = Math.max((mapExtent.width() * settings.bufferPerc * 0.01),
                (mapExtent.height() * settings.bufferPerc * 0.01));

            var sourceKey = me.model.getLayer(layerKey).src.key;
            var dSrid = model.getSchema().displaySRID;
            var srid = model.getSchema().sRID;

            //Filters
            var extent = {
                type: "EXTENT",
                minX: point.x - buffer,
                minY: point.y - buffer,
                maxX: point.x + buffer,
                maxY: point.y + buffer,
                srid: dSrid
            };

            var nearest = {
                type: "NEAR",
                x: point.x,
                y: point.y,
                take: 1,
                srid: dSrid
            };

            var filters = [extent, nearest];

            //Filters
            var filters = [];

            if (settings.includeArea) {

                var addArea = {
                    type: "ADD_AREA",
                    srid: srid
                };

                filters.push(addArea);
            }

            if (settings.includeLength) {

                var addArea = {
                    type: "ADD_LEN",
                    srid: srid
                };

                filters.push(addArea);
            }

            var proj = {
                type: "PROJ",
                srid: dSrid
            };

            filters.push(proj);

            me.api.getFeatures2(sourceKey, filters, filters, callback, { isProjected: false });
        };
    }

    ns.Query = Query;

    return ns;
})(Mapzania)

if (!Mapzania)
    var Mapzania = {};

(function (ns) {
    QueryType = { point: 1, polygon: 2, polygonEnclosed: 3 };
    QueryGeometryType = { point: "Point", polygon: "Polygon" };

    //#region Constructor

    function QueryManager(model, viewport, bufferPerc, geoFenceLayerSourceKey, selectableLayerNames) {
        var me = this;

        this.model = model;
        this.viewport = viewport;
        this.bufferPerc = bufferPerc;
        this.geoFenceLayerSourceKey = geoFenceLayerSourceKey;
        this.selectableLayerNames = selectableLayerNames;

        //This object gets returned in the featuresFetched event
        this.featuresFetchedResultObject = {
            layerKey: "",
            sourceKey: "",
            queryType: null,
            features: []
        };

        this.geoFenceFilterObject = {
            OriginalLayerSourceKey: "",
            OriginalLayerSourceFilter: "",
            GeoFenceSRID: "",
            GeoFenceFeatures: [],
            GeoFenceEnclose: false,
            DisplaySRID: null
        };

        this.selectableLayersObject = {
            layerKey: null,
            sourceKey: null,
            layerName: null,
            layerOrdinal: null,
            layerVisible: false
        };

        this.selectableLayers = [];

        this.featuresFetched = new Mapzania.Event();

        me.model.schemaLoaded.add(function (evt) {
            me._buildQueryableLayers(evt.data, me.selectableLayerNames);
        });

        me.model.layerShown.add(function (evt) {
            for (var i = 0; i < me.selectableLayers.length; i++) {
                if (evt.data.lastIndexOf(me.selectableLayers[i].layerKey, 0) === 0) {
                    me.selectableLayers[i].layerVisible = true;
                    return false;
                }
            }
        });

        me.model.layerHidden.add(function (evt) {
            for (var i = 0; i < me.selectableLayers.length; i++) {
                if (evt.data.lastIndexOf(me.selectableLayers[i].layerKey, 0) === 0) {
                    me.selectableLayers[i].layerVisible = false;
                    return false;
                }
            }
        });
    }

    //#endregion

    //#region Public Methods

    QueryManager.prototype.updateQueryableLayers = function (newSelectableLayerNames) {
        var me = this;

        me.selectableLayerNames = newSelectableLayerNames;
        me._buildQueryableLayers(me.model.getSchema(), me.selectableLayerNames);
    }

    QueryManager.prototype.queryPoint = function (point, displaySRID) {
        var me = this;

        //Shows waiting div
        //me.viewport.setIsLoading(true);

        //Get map's current extent
        var currentExtent = me.model.extent;

        if (me.bufferPerc == null)
            me.bufferPerc = 2;

        //Set point buffer
        var buffer = Math.max((currentExtent.width() * me.bufferPerc * 0.01), (currentExtent.height() * me.bufferPerc * 0.01));

        //Fetch the closest feature
        me._fetchFeaturesForPoint(point, buffer, me.model.getSchema(), displaySRID);
    }

    QueryManager.prototype.queryPolygon = function (polygon, displaySRID, enclosing) {
        var me = this;

        //Shows waiting div
        me.viewport.setIsLoading(true);

        //This method checks whether the polyon is closed, if not it closes the polygon
        polygon = me._closePolygon(polygon);

        //Fetch the closest feature
        me._fetchFeaturesForPolygon(polygon, me.model.getSchema(), displaySRID, enclosing);
    }

    //#endregion

    //#region Private Methods

    QueryManager.prototype._closePolygon = function (polygon) {
        var me = this;

        if (polygon != null) {
            if (polygon.shell != null) {
                if (polygon.shell.points != null && polygon.shell.points.length > 0) {
                    var l = polygon.shell.points.length;
                    if (l == 1 || (polygon.shell.points[0].x != polygon.shell.points[l - 1].x || polygon.shell.points[0].y != polygon.shell.points[l - 1].y)) {
                        polygon.shell.points.push(new Mapzania.Point(polygon.shell.points[0].x, polygon.shell.points[0].y));
                    }
                }
            }
        }

        return polygon;
    }

    QueryManager.prototype._buildQueryableLayers = function (schema, selectableLayerNames) {
        var me = this;

        me.selectableLayers = [];

        if (selectableLayerNames == null)
            return;

        for (var i = 0; i < schema.layers.length; i++) {
            if (selectableLayerNames.length == 0 || selectableLayerNames.indexOf(schema.layers[i].key) >= 0) {
                if (!(typeof schema.layers[i].fields === "undefined") &&
                    (schema.layers[i].fields.length > 0)) {

                    me.selectableLayersObject = {
                        layerKey: schema.layers[i].key,
                        sourceKey: schema.layers[i].sourceKey,
                        layerName: schema.layers[i].name,
                        layerOrdinal: schema.layers[i].ordinal,
                        layerVisible: schema.layers[i].visible
                    };

                    me.selectableLayers.push(me.selectableLayersObject);
                }
            }
        }

        me.selectableLayers = me.selectableLayers.sort(function (a, b) {
            return a.layerOrdinal > b.layerOrdinal;
        });
    }

    QueryManager.prototype._fetchFeaturesForPoint = function (point, buffer, schema, displaySRID) {
        var me = this;

        var featuresFetchedResults = [];

        var featuresFetchLoop = function (i) {
            if (i >= me.selectableLayers.length) {
                //me.viewport.setIsLoading(false);
                me.featuresFetched.fire(me, featuresFetchedResults);
                return;
            }

            if (me.selectableLayers[i].layerVisible == true) {
                var orgFilter = "";

                var layer = $.grep(schema.layers, function (e) { return e.key === me.selectableLayers[i].layerKey; })[0];

                if (layer != null) {
                    orgFilter = layer.filter;
                }

                var polygon = new Mapzania.Polygon();
                polygon.shell = new Mapzania.Line([
                    new Mapzania.Point(point.x - buffer, point.y - buffer),
                    new Mapzania.Point(point.x - buffer, point.y + buffer),
                    new Mapzania.Point(point.x + buffer, point.y + buffer),
                    new Mapzania.Point(point.x + buffer, point.y - buffer),
                    new Mapzania.Point(point.x - buffer, point.y - buffer),
                ]);

                var geoFenceFeature = new Mapzania.Feature();
                geoFenceFeature.geometry = polygon;
                geoFenceFeature.attributes = null;

                me.geoFenceFilterObject = {
                    OriginalLayerSourceKey: me.selectableLayers[i].sourceKey,
                    OriginalLayerSourceFilter: orgFilter,
                    GeoFenceSRID: displaySRID,
                    GeoFenceFeatures: [geoFenceFeature],
                    GeoFenceEnclose: false,
                    DisplaySRID: displaySRID
                };

                me.model.api.getFeatures(
                    me.geoFenceLayerSourceKey,
                    JSON.stringify(me.geoFenceFilterObject),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    function (ftrs) {
                        me.featuresFetchedResultObject = {
                            layerKey: me.selectableLayers[i].layerKey,
                            sourceKey: me.selectableLayers[i].sourceKey,
                            queryType: QueryType.point,
                            features: ftrs,
                            queryGeometryType: QueryGeometryType.point,
                            queryGeometry: point,
                            querySRID: displaySRID
                        };

                        featuresFetchedResults.push(me.featuresFetchedResultObject);
                        featuresFetchLoop(i + 1);
                    });
            } else {
                featuresFetchLoop(i + 1);
            }
        };
        featuresFetchLoop(0);
    }

    QueryManager.prototype._fetchFeaturesForPolygon = function (polygon, schema, displaySRID, enclosing) {
        var me = this;

        var featuresFetchedResults = [];

        var featuresFetchLoop = function (i) {
            if (i >= me.selectableLayers.length) {
                me.viewport.setIsLoading(false);
                me.featuresFetched.fire(me, featuresFetchedResults);
                return;
            }

            if (me.selectableLayers[i].layerVisible == true) {
                var orgFilter = "";

                var layer = $.grep(schema.layers, function (e) { return e.key === me.selectableLayers[i].layerKey; })[0];

                if (layer != null) {
                    orgFilter = layer.filter;
                }

                var geoFenceFeature = new Mapzania.Feature();
                geoFenceFeature.geometry = polygon;
                geoFenceFeature.attributes = null;

                me.geoFenceFilterObject = {
                    OriginalLayerSourceKey: me.selectableLayers[i].sourceKey,
                    OriginalLayerSourceFilter: orgFilter,
                    GeoFenceSRID: displaySRID,
                    GeoFenceFeatures: [geoFenceFeature],
                    GeoFenceEnclose: enclosing,
                    DisplaySRID: displaySRID
                };

                me.model.api.getFeatures(
                    me.geoFenceLayerSourceKey,
                    JSON.stringify(me.geoFenceFilterObject),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    function (ftrs) {
                        me.featuresFetchedResultObject = {
                            layerKey: me.selectableLayers[i].layerKey,
                            sourceKey: me.selectableLayers[i].sourceKey,
                            queryType: (enclosing) ? QueryType.polygonEnclosed : QueryType.polygon,
                            features: ftrs,
                            queryGeometryType: QueryGeometryType.polygon,
                            queryGeometry: polygon,
                            querySRID: displaySRID
                        };

                        featuresFetchedResults.push(me.featuresFetchedResultObject);
                        featuresFetchLoop(i + 1);
                    });
            } else {
                featuresFetchLoop(i + 1);
            }
        };
        featuresFetchLoop(0);
    }

    //#endregion

    ns.QueryManager = QueryManager;
})(Mapzania)

/// <reference path="../lib/swfobject-1.5.js" />

if (!Mapzania)
    var Mapzania = {};

//
// We add sheets to a mapper/drawer in a lazy fashion - i.e. a sheet is only added to the map UI if it contains visual data (i.e. features / labels). The LayerSheetManager class is responsible
//  for mapping between map layers and these "sheets" on the drawers. 
//
//  This is an internal utility class, and should not be used by applications.
//

(function (ns) {
    function LayerSheetManager() {
        // Array of layer data objects, where each object has a key, hasFeatureSheet, hasLabelSheet and greyScale property. 
        //  Index 0 is closest to the user - i.e. on top of the map. This indexing-scheme matches layers on the schema layer list.
        this.layerData = [];

        // A dictionary of sheets names - used for fast lookups
        this.sheetNames = {};
    }

    LayerSheetManager.prototype.removeAllLayers = function () {
        this.layerData = [];
        this.sheetNames = {};
    }

    LayerSheetManager.prototype.addLayer = function (layerKey, indexSchema, greyScale, layerSource) {
        this.layerData.splice(
            indexSchema,
            0,
            {
                key: layerKey,
                greyScale: greyScale,
                hasFeatureSheet: false,
                hasLabelSheet: false,
                layerSource: layerSource
            });

        this.sheetNames[layerKey] = false;
        this.sheetNames[layerKey + "_label"] = false;
    }

    LayerSheetManager.prototype.removeLayer = function (layerKey) {
        var index = this.layerKeyIndex(layerKey);
        if (index >= 0) {
            this.layerData.splice(index, 1);
            this.sheetNames[layerKey] = false;
            this.sheetNames[layerKey + "_label"] = false;
        }
    }

    LayerSheetManager.prototype.getSheetNames = function (layerKey) {
        var sheetNames = [];
        var index = this.layerKeyIndex(layerKey);
        if (index >= 0) {
            var layerObj = this.layerData[index];
            if (layerObj.hasFeatureSheet) {
                sheetNames.push(layerKey);
            }
            if (layerObj.hasLabelSheet) {
                sheetNames.push(layerKey + "_label");
            }
        }
        return sheetNames;
    }

    LayerSheetManager.prototype.layerKeyIndex = function (layerKey) {
        for (var i = 0; i < this.layerData.length; i++) {
            if (this.layerData[i].key == layerKey) {
                return i;
            }
        }
        return -1;
    }

    LayerSheetManager.prototype.hasLayerKey = function (key) {
        return this.layerKeyIndex(key) >= 0;
    }

    LayerSheetManager.prototype.getLayerCount = function () {
        return this.layerData.length;
    }

    LayerSheetManager.prototype.hasSheet = function (layerKey, sheetType) {
        var sheetName = layerKey;
        if (sheetType == 1) {
            sheetName = sheetName + "_label";
        }
        return this.sheetNames[sheetName];
    }

    LayerSheetManager.prototype.addSheet = function (layerKey, sheetType) {
        var uiIndex = 0;

        // Evaluate feature sheets
        for (var i = this.layerData.length - 1; i >= 0; i--) {
            var obj = this.layerData[i];
            if (obj.key == layerKey && sheetType == 0) {
                obj.hasFeatureSheet = true;
                this.sheetNames[layerKey] = true;
                return uiIndex;
            }

            if (obj.hasFeatureSheet) {
                uiIndex++;
            }
        }

        // Evaluate label sheets
        for (var i = this.layerData.length - 1; i >= 0; i--) {
            var obj = this.layerData[i];
            if (obj.key == layerKey && sheetType == 1) {
                obj.hasLabelSheet = true;
                this.sheetNames[layerKey + "_label"] = true;
                return uiIndex;
            }

            if (obj.hasLabelSheet) {
                uiIndex++;
            }
        }

        throw "Layer is not known: " + layerKey;
    }

    LayerSheetManager.prototype.isGreyScale = function (layerKey) {
        return this.layerData[this.layerKeyIndex(layerKey)].greyScale;
    }

    LayerSheetManager.prototype.layerSource = function (layerKey) {
        return this.layerData[this.layerKeyIndex(layerKey)].layerSource;
    }

    ns.LayerSheetManager = LayerSheetManager;

})(Mapzania);

/**
 * @doc Mapzania.Renderer.*ClassDescription*
 *
 * A class responsible for coordinating the rendering of the interactive map, using the Model, Viewport and Mapper (also known as the drawer, such as Canvas or Raphael).
 * Applications typically instantiate this class once, and is not making direct use of any functionality provided in this class.
 */

(function (ns) {

    /**
      * @doc Mapzania.Renderer.Renderer
      *
      * Constructor.
      *
      * @param model (Mapzania.Model) A handle to the Mapzania Model instance. 
      *
      * @param viewport (Mapzania.Viewport) A handle to the viewport for this map
      *
      * @param drawer (Mapzania.Mapper) The map drawer instance to use. Valid class instances are Canvas.Mapper, Raphael.Mapper or Flash.Mapper.
      */
    function Renderer(model, viewport, drawer) {
        this.model = model;
        this.viewport = viewport;
        this.drawer = drawer;

        if (this.viewport.setRendererModel) {
            this.viewport.setRendererModel(this, this.model);
        }

        this.model.setViewportSize(viewport.getSize());

        this.rendered = new ns.Event();

        this.layerSheetManager = new Mapzania.LayerSheetManager();

        // Keys are layer names, values are arrays of drawing instructions for the labels
        this.labelsAll = {};  // All labels
        this.labelsVisible = {}; // Ones chosen to be visible

        var me = this;
        model.schemaLoaded.add(function (e) {
            me.removeAllLayers();

            me.drawer.setBackColor(model.backColor);

            if (model.backImageUrl)
                me.drawer.setBackImageUrl(model.backImageUrl);

            for (var i = 0; i < model.layers.length; i++) {
                var layer = model.layers[i];
                me.addLayer(layer.key, i, layer.greyScale, layer.src);
            }

            me.drawer.addLayer("ts_interactive");
            me.drawer.addLayer("ts_interactive_2");
            me.drawer.addLayer("ts_selected_feature");

            if (me.model.rotation) {
                me.viewport.rotation = me.model.rotation;
            }
        });

        model.loading.add(function (e) {
            me.viewport.setIsLoading(true);
        });

        model.loadingProgress.add(function (e) {
            me.viewport.setLoadingProgress(e.data.total, e.data.outstanding);
        });

        model.loaded.add(function (e) {
            var standardLabelFormattingStrategy = new Mapzania.StandardLabelFormattingStrategy(me.model.extent.grow(1.0));

            for (var j = 0; j < e.data.length; j++) {
                var itm = e.data[j];
                var scaleType = itm.layer.scaleType != null ? itm.layer.scaleType : model.scaleType;
                me.drawLayer(scaleType, itm.layer, itm.layer.src, itm.data, standardLabelFormattingStrategy);
            }
            me.viewport.reset();
            me.viewport.setIsLoading(false);
            me.rendered.fire(me);
        });

        model.layerShown.add(function (e) {
            me.showLayer(e.data);
        });

        model.layerHidden.add(function (e) {
            me.hideLayer(e.data);
        });

        model.layerCleared.add(function (e) {
            me.clearLayer(e.data);
        });

        model.layerAdded.add(function (e) {
            me.addLayer(e.data.layer.key, e.data.indexSchema, e.data.layer.greyScale, e.data.layer.src);
        });

        model.layerRemoved.add(function (e) {
            me.removeLayer(e.data.layer.key);
        });

        model.zoomed.add(function (e) {
            var extent = e.data;
            var ext;

            if (extent == null)
                ext = $.extend({}, model.startExtent);
            else
                var ext = $.extend({}, extent);

            ext.normalise(me.getSize());

            me.viewport.zoomTo(me._trf().to(-1, ext), ext);
        });

        drawer.viewChanged.add(function () {
            //console.log("view changed");
        });

        viewport.resized.add(function () {
            model.setViewportSize(viewport.getSize());
            model.refresh();
        });
    }

    Renderer.prototype.removeAllLayers = function () {
        this.layerSheetManager.removeAllLayers();
        this.drawer.removeAllLayers();
    }

    // -------AS 2016-08-27----------
    Renderer.prototype.updateLayer = function (layerKey) {
        var labelStrategy = new Mapzania.StandardLabelFormattingStrategy(model.extent.grow(1.0));

        var layer = model.getLayer(layerKey);
        var data = model.store.get(layerKey);
        var scaleType = layer.scaleType != null ? layer.scaleType : model.scaleType;

        // AS: HACK to ensure src is complete
        if (!layer.src.gotFeatures)
            layer.src.gotFeatures = new Mapzania.Event();

        this.drawLayer(scaleType, layer, layer.src, { features: data }, labelStrategy);

        this.flush();
    };
    //-------------------------------

    // The entry point for when a layer is drawn
    Renderer.prototype.drawLayer = function (scaleTypeLayer, layer, src, data, standardLabelFormattingStrategy) {
        if (!this.model.hasLayer(layer.key))
            return;

        if (src.gotFeatures) {
            var count = 0;

            if (data.features.length == 0)
                return;

            var model = this.model;

            var theme = model.getTheme(layer);

            themer = new ns.ThemerFactory().getThemer(theme, layer.ignoreSchema, layer.style);

            if (themer.evalFeature) {
                for (var i = 0; i < data.features.length; i++) {
                    var ftr = data.features[i];
                    themer.evalFeature(ftr);
                }
            }

            if (themer.doneWithEvalFeatures) {
                themer.doneWithEvalFeatures();
            }

            var symbolisedGeometry = [];

            var scaleType = scaleTypeLayer;
            if (theme && theme.scaleType) {
                scaleType = theme.scaleType;
            }

            for (var i = 0; i < data.features.length; i++) {
                var ftr = data.features[i];

                var sym = themer.getSymbol(ftr);

                if (sym == null)
                    continue;

                var sg = {};
                sg.geometry = ftr.geometry;
                sg.symbol = sym;

                var labelInfo = standardLabelFormattingStrategy.getLabelInstance(layer, themer, ftr);
                if (labelInfo) {
                    sg.label = labelInfo;
                }

                count++;
                symbolisedGeometry.push(sg);
            }

            // Filter out labels
            var labelFilter = new Mapzania.StandardLabelFilterStrategy();
            labelFilter.filterLabels(layer, symbolisedGeometry);

            this._drawSymbolisedGeometry(layer, scaleType, symbolisedGeometry);

            this.drawer.flush();

            if (this.model.removeConflictingLabels) {
                this._startLabelTimer();
            }
        }

        if (src.gotTiles) {
            this.ensureSheetExist(layer.key, 0);
            this._sortTilesFromCenter(data);
            for (var i = 0; i < data.length; i++) {
                this.drawTile(layer.key, data[i], layer.opacity);
            }
            this.drawer.flush();
        }
    }

    Renderer.prototype._sortTilesFromCenter = function (data) {
        var pCenter = this.model.extent.getCenter();
        data.forEach(function (cur) {
            var e = new Mapzania.Extent(cur);
            cur.distance = pCenter.distanceTo(e.getCenter());
        });

        data.sort(function (a, b) {
            if (a.distance > b.distance) return 1;
            if (a.distance < b.distance) return -1;
            return 0;
        });
    }

    Renderer.prototype._drawSymbolisedGeometry = function (layer, scaleType, sgs) {

        var instructions = this._symbolisedGeometryToDrawingInstructions(
            parseInt(layer.src.geometryType),
            scaleType,
            sgs);

        var dr = this.drawer;

        var checkedFeatureSheet = false;
        var checkedLabelSheet = false;

        this.labelsAll[layer.key] = [];
        
        for (var i = 0; i < instructions.length; i++) {
            if (!checkedFeatureSheet) {
                this.ensureSheetExist(layer.key, 0);
                checkedFeatureSheet = true;
            }

            var ist = instructions[i];

            switch (ist.type) {
                case "draw-circle":
                    dr.drawCircle(layer.key, ist.center, ist.radius, ist.color, ist.size, ist.opacity);
                    break;
                case "fill-circle":
                    dr.fillCircle(layer.key, ist.center, ist.radius, ist.color, ist.opacity);
                    break;
                case "draw-line":
                    dr.drawLine(layer.key, ist.line, ist.color, ist.size, ist.opacity, ist.endCap, ist.join, ist.dashArray);
                    break;
                case "fill-polygon":
                    dr.fillPolygon(layer.key, ist.lines, ist.color, ist.color2, ist.opacity, ist.fillType);
                    break;
                case "draw-text":
                    var labelLines = this._measureLabelLines(ist.text, ist.font, ist.size);
                    ist.labelLines = labelLines;

                    if (this.model.removeConflictingLabels) {
                        // We don't draw the labels yet. We siply calculate the label bounds - they are filtered and drawn later
                        var P = ist.location.clone();
                        var Pcenter = P.clone();

                        P.translate(ist.offset.x, -ist.offset.y);
                        if (ist.align == 0) P.translate(labelLines.width / 2, 0);
                        if (ist.align == 1) P.translate(0, 0);
                        if (ist.align == 2) P.translate(-labelLines.width / 2, 0);

                        var W = labelLines.width / 2;
                        var H = labelLines.height / 2;
                        var line = new Mapzania.Line();
                        line.points = [
                            new Mapzania.Point(P.x - W, P.y - H),
                            new Mapzania.Point(P.x - W, P.y + H),
                            new Mapzania.Point(P.x + W, P.y + H),
                            new Mapzania.Point(P.x + W, P.y - H),
                            new Mapzania.Point(P.x - W, P.y - H)
                        ];
                        line = line.rotate(Pcenter, -ist.rotation);
                        var labelPolygon = new Mapzania.Polygon();
                        labelPolygon.shell = line;
                        ist.labelPolygon = labelPolygon;

                        this.labelsAll[layer.key].push(ist);
                    } else {
                        if (!checkedLabelSheet) {
                            this.ensureSheetExist(layer.key, 1);
                            checkedLabelSheet = true;
                        }
                        
                        this._drawOneLabelInstruction(layer.key+"_label", ist);
                    }
                    break;
                case "fill-bitmap":
                    dr.fillBitmap(layer.key, ist.extent, ist.src, ist.opacity);
                    break;
                case "image":
                    dr.drawImage(layer.key, ist.extent, ist.src, ist.opacity);
                    break;
                default:
                    throw "Invalid instruction type=" + ist.type;
            };
        }

        //
        // Sort labels according to weight. This is done once only, after the layer is loaded and drawn.
        //
        if (this.labelsAll[layer.key]) {
            this.labelsAll[layer.key].sort(function (x, y) {
                return (-1 * (x.labelWeight - y.labelWeight)); // We want the bigger polygons to have a higher priority
            });
        }
    }

    Renderer.prototype._startLabelTimer = function () {
        if (this._labelTimerRef) {
            clearTimeout(this._labelTimerRef);
            this._labelTimerRef = null;
        }

        var me = this;
        this._labelTimerRef = setTimeout(function () {
            me._filterAndDrawLabelInstructionsAllLayers();
            me._labelTimerRef = null;
        }, 100);
    }

    Renderer.prototype._filterAndDrawLabelInstructionsAllLayers = function () {
        var startTime = (new Date()).getTime();
        this.labelsVisible = {};
        for (var i = 0; i < this.model.layers.length; i++) {
            var layer = this.model.layers[i];
            if (layer.visible) {
                this._filterLabelInstructions(layer);
            }
        }
        var endTime = (new Date()).getTime();
        console.log("Eliminating duplicate labels for all layers (time in ms): ", (endTime - startTime));

        this._drawLabelInstructions();
    }

    Renderer.prototype._filterLabelInstructions = function (layer) {
        var me = this;

        //
        // Eliminate the duplicates
        //
        this.labelsVisible[layer.key] = [];
        if (this.labelsAll[layer.key]) {
            this.labelsAll[layer.key].forEach(function (cur) {
                var hasConflict = false;

                for (var cur2 in me.labelsVisible) {
                    var visibleList = me.labelsVisible[cur2];
                    for (var i = 0; i < visibleList.length && !hasConflict; i++) {
                        if (visibleList[i].labelPolygon.intersects(cur.labelPolygon)) {
                            hasConflict = true;
                        }
                    }
                    if (hasConflict) break;
                }

                if (!hasConflict) {
                    visibleList.push(cur);
                }
            });
        }
    }

    Renderer.prototype._drawLabelInstructions = function () {
        //
        // Draw the labels
        //
        var me = this;
        for (var curLayerKey in this.labelsVisible) {
            if (this.layerSheetManager.hasSheet(curLayerKey, 1)) {
                this.drawer.clearLayer(curLayerKey + "_label");
            }
            if (this.labelsVisible[curLayerKey].length > 0) {
                me.ensureSheetExist(curLayerKey, 1);
            }
            this.labelsVisible[curLayerKey].forEach(function (ist) {
                var line = ist.labelPolygon.shell;
                me._drawOneLabelInstruction(curLayerKey + "_label", ist);
                //me.drawer.fillPolygon(curLayerKey + "_label", [line], "#000000", null, 30, 1);
            });
            this.drawer.flush();
        }
    }

    Renderer.prototype._symbolisedGeometryToDrawingInstructions = function (geometryType, scaleType, sgs) {
        var me = this;
        var trf = this._trf();

        var calcSize = function (size) {
            if (scaleType == 0)
                return size;

            var res = trf.toSize(size);
            if (res != 0 && res < 1)
                res = 1;
            return res;
        }

        var passes = [[]];

        var addInstruction = function (pass, inst) {
            if (pass > passes.length - 1)
                passes.push([]);

            passes[pass].push(inst);
        }

        var generatePointGeometry = function (geometry) {
            if (sym.points) { // Compount Point Symbol
                for (var j = 0; j < sym.points.length; j++) {
                    var psym = sym.points[j];
                    var instrs = me._generatePointSymbolInstructions(geometry, psym, trf, calcSize, getDashArray);

                    if (sym.drawByLayer) {
                        instrs.forEach(function (cur) {
                            addInstruction(j, cur);
                        });
                    }
                    else {
                        instrs.forEach(function (cur) {
                            addInstruction(0, cur);
                        });
                    }
                }
            }
            else { // Point Symbol
                var instrs = me._generatePointSymbolInstructions(geometry, sym, trf, calcSize, getDashArray);
                instrs.forEach(function (cur) {
                    addInstruction(0, cur);
                });
            }
        }

        var generateMultiPointGeometry = function (geometry) {
            geometry.points.forEach(function (cur) {
                generatePointGeometry(cur);
            });
        }

        var generateLineArrowInstruction = function (geometry, psym, interval) {
            if (!interval) interval = 0;

            var result = [];
            var length = 0;
            while (true) {
                var r = geometry.getPointAndSegmentAtLength(length);
                if (r) {
                    var p0 = r.segment.p1;
                    var p1 = r.segment.p2;
                    var angleRadians = Math.PI / 2 - Math.atan2(p1.y - p0.y, p1.x - p0.x) + Math.PI;
                    var instrs = me._generatePointSymbolInstructions(r.point, psym, trf, calcSize, getDashArray, angleRadians);
                    result = result.concat(instrs);
                    if (!interval) {
                        break;
                    } else {
                        length += interval;
                    }
                } else {
                    break; // No segments found
                }
            }
            
            return result;
        };

        var generateLineGeometry = function (geometry) {
            if (sym.lines) {
                for (var j = 0; j < sym.lines.length; j++) {
                    var lsym = sym.lines[j];
                    var instructions = [];
                    var instr = { type: "draw-line", line: trf.to(2, geometry), size: calcSize(lsym.size), color: lsym.color, opacity: lsym.opacity, endCap: lsym.endCap, join: lsym.join, dashArray: getDashArray(lsym) };
                    instructions.push(instr);

                    // Start Arrow
                    if (lsym.startArrow) {
                        instructions = instructions.concat(generateLineArrowInstruction(geometry, lsym.startArrow, lsym.startArrowInterval));
                    }

                    if (lsym.endArrow) {
                        instructions = instructions.concat(generateLineArrowInstruction(geometry.reverse(), lsym.endArrow, lsym.endArrowInterval));
                    }

                    if (sym.drawByLayer) {
                        instructions.forEach(function (cur) {
                            addInstruction(j, cur);
                        });
                    }
                    else {
                        instructions.forEach(function (cur) {
                            addInstruction(0, cur);
                        });
                    }
                }
            }
            else {
                var instr = { type: "draw-line", line: trf.to(2, geometry), size: calcSize(sym.size), color: sym.color, opacity: sym.opacity, endCap: sym.endCap, join: sym.join, dashArray: getDashArray(sym) };
                addInstruction(0, instr);

                if (sym.startArrow) {
                    instructions = generateLineArrowInstruction(geometry, sym.startArrow, sym.startArrowInterval);
                    instructions.forEach(function (cur) {
                        addInstruction(0, cur);
                    });
                }

                if (sym.endArrow) {
                    instructions = generateLineArrowInstruction(geometry.reverse(), sym.endArrow, sym.endArrowInterval);
                    instructions.forEach(function (cur) {
                        addInstruction(0, cur);
                    });
                }
            }
        }

        var generateMultiLineGeometry = function (geometry) {
            geometry.lines.forEach(function (cur) {
                generateLineGeometry(cur);
            });
        }

        var generatePolygonGeometry = function (geometry) {
            var g = trf.to(3, geometry);
            var lines = [g.shell].concat(g.holes);

            if (sym.color) {
                var instr = { type: "fill-polygon", lines: lines, color: sym.color, opacity: sym.opacity, fillType: sym.type };
                addInstruction(0, instr);
            }

            if (sym.outline) {
                var osym = sym.outline;

                for (var k = 0; k < lines.length; k++) {

                    var line = lines[k];

                    if (osym.lines) {
                        for (var j = 0; j < osym.lines.length; j++) {
                            var lsym = osym.lines[j];
                            var instr = { type: "draw-line", line: line, size: calcSize(lsym.size), color: lsym.color, opacity: lsym.opacity, endCap: lsym.endCap, join: lsym.join, dashArray: getDashArray(lsym) };

                            if (osym.drawByLayer)
                                addInstruction(j, instr);
                            else
                                addInstruction(0, instr);
                        }
                    }
                    else {
                        var instr = { type: "draw-line", line: line, size: calcSize(osym.size), color: osym.color, opacity: osym.opacity, endCap: osym.endCap, join: osym.join, dashArray: getDashArray(osym) };
                        addInstruction(0, instr);
                    }
                }
            }
        }

        var generateMultiPolygonGeometry = function (geometry) {
            geometry.polygons.forEach(function (cur) {
                generatePolygonGeometry(cur);
            });
        }

        var getDashArray = function (sym) {

            var size = calcSize(sym.size);

            if (size == 0)
                return [];

            var dashes = [];

            if (sym.type == 1)
                dashes = [4 * size, 2 * size];

            if (sym.type == 2)
                dashes = [2 * size, 2 * size];

            return dashes;
        }

        // A function to determine the sign of a number
        var funcSign = function (n) {
            if (!n) return +1;
            if (n >= 0) {
                return +1;
            } else {
                return -1;
            }
        };

        for (var i = 0; i < sgs.length; i++) {
            var sg = sgs[i];

            var sym = sg.symbol;

            if (sg.label) {
                var lsym = sg.label.symbol;

                var rotation = sg.label.rotation;
                if (sg.label.rotationFollowGeometry)
                    rotation = trf.labelRotationTo(sg.label.rotation);

                var instr = {
                    type: "draw-text",
                    location: trf.to(1, sg.label.center),
                    text: sg.label.text,
                    font: lsym.fontFamily,
                    size: calcSize(lsym.fontSize),
                    color: lsym.color,
                    backColor: lsym.backColor,
                    opacity: lsym.opacity,
                    align: lsym.horizontalAlignment,
                    haloSize: calcSize(lsym.haloThickness),
                    haloColor: lsym.haloColor,
                    bold: lsym.bold,
                    italic: lsym.italic,
                    underline: lsym.underline,
                    addMark: false,
                    rotation: rotation,

                    // The calcSize function assumes positive size values. We need to preserve negative sizes here.
                    offset: {
                        x: funcSign(lsym.offset.x) * calcSize(Math.abs(lsym.offset.x)),
                        y: funcSign(lsym.offset.y) * calcSize(Math.abs(lsym.offset.y))
                    },
                    labelWeight: sg.label.weight
                };

                addInstruction(0, instr);
            }

            if (sg.geometry) {
                switch (sg.geometry.getGeometryType()) {
                    case 1: //point
                        generatePointGeometry(sg.geometry);
                        break;
                    case 2: //line
                        generateLineGeometry(sg.geometry);
                        break;
                    case 3: //polygon
                        generatePolygonGeometry(sg.geometry);
                        break;
                    case 4: //multi point
                        generateMultiPointGeometry(sg.geometry);
                        break;
                    case 5: //multi line
                        generateMultiLineGeometry(sg.geometry);
                        break;
                    case 6: //multi polygon
                        generateMultiPolygonGeometry(sg.geometry);
                        break;
                    default:
                        throw "Invalid geometry type " + geometryType;
                }
            }
        }

        var res = [];

        for (var i = 0; i < passes.length; i++) {
            for (var j = 0; j < passes[i].length; j++) {
                res.push(passes[i][j]);
            }
        }

        return res;
    }

    Renderer.prototype.drawTile = function (layerKey, tile, opacity) {
        var trf = this._trf();

        if (this.viewport.rotation.rotateZ == 0) {
            var ext = trf.to(-1, tile.extent);
            this.drawer.fillBitmap(layerKey, ext, tile.url, opacity);
        }
        else {
            if (this.drawer.fillBitmapRotated) {
                var pTopLeft = new Mapzania.Point(tile.extent.minX, tile.extent.maxY);
                var pTopRight = new Mapzania.Point(tile.extent.maxX, tile.extent.maxY);
                var pBottomLeft = new Mapzania.Point(tile.extent.minX, tile.extent.minY);

                var pTopLeftRotated = trf.to(1, pTopLeft);
                var pTopRightRotated = trf.to(1, pTopRight);
                var pBottomLeftRotated = trf.to(1, pBottomLeft);

                var width = pTopLeftRotated.distanceTo(pTopRightRotated);
                var height = pTopLeftRotated.distanceTo(pBottomLeftRotated);

                this.drawer.fillBitmapRotated(layerKey, pTopLeftRotated, width, height, tile.url, opacity, this.viewport.rotation.rotateZ);
            } else {
                console.log("Error: Drawer/Mapper does not support rotated tiles.");
            }
        }
    }

    // indexSchema = 0 for closest to the user
    Renderer.prototype.addLayer = function (layerKey, indexSchema, greyScale, source) {
        if (this.layerSheetManager.hasLayerKey(layerKey)) {
            throw "Layer key already exist: " + layerKey;
        }

        if (typeof indexSchema == 'undefined') {
            indexSchema = this.layerSheetManager.getLayerCount();
        }

        this.layerSheetManager.addLayer(layerKey, indexSchema, greyScale, source);
    }

    //sheetType: 0 = Data, 1 = Label
    Renderer.prototype.ensureSheetExist = function (layerKey, sheetType) {
        if (!this.layerSheetManager.hasSheet(layerKey, sheetType)) {
            var indexUI = this.layerSheetManager.addSheet(layerKey, sheetType);

            var sheetName = layerKey;
            if (sheetType == 1) {
                sheetName = sheetName + "_label";
            }

            this.drawer.addLayer(sheetName, indexUI, this.layerSheetManager.isGreyScale(layerKey), this.layerSheetManager.layerSource(layerKey));
        }
    }

    Renderer.prototype.removeLayer = function (layerKey) {
        var me = this;
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.removeLayer(cur);
        });
        this.layerSheetManager.removeLayer(layerKey);
    }

    Renderer.prototype.showLayer = function (layerKey) {
        var me = this;
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.showLayer(cur);
        });
    }

    Renderer.prototype.hideLayer = function (layerKey) {
        var me = this;
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.hideLayer(cur);
        });

        if (this.model.removeConflictingLabels) {
            this._startLabelTimer();
        }
    }

    Renderer.prototype.clearLayer = function (layerKey) {
        var me = this;
        this.labelsAll[layerKey] = [];
        this.labelsVisible[layerKey] = [];
        this.layerSheetManager.getSheetNames(layerKey).forEach(function (cur) {
            me.drawer.clearLayer(cur);
        });
    }

    Renderer.prototype.getSize = function (extent) {
        return this.viewport.getSize();
    }

    Renderer.prototype.flush = function () {
        this.drawer.flush();
    }

    Renderer.prototype._trf = function () {
        return this.viewport.getTransformer(this.model.extent);
    }

    Renderer.prototype._generatePointSymbolInstructions = function (point, sym, trf, calcSize, getDashArray, angleRadians) {
        if (!angleRadians) angleRadians = 0.0;
        var res = [];
        var center = trf.to(1, point);
        var radius = calcSize(sym.size);
        if (!sym.type)
            sym.type = 0;

        switch (sym.type) {
            case 0:
                // Point symbol
                var instr = { type: "fill-circle", center: center, radius: radius, color: sym.color, opacity: sym.opacity };
                res.push(instr);

                if (sym.border && sym.border.size > 0) {
                    var instr = { type: "draw-circle", center: trf.to(1, point), radius: radius, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity };
                    res.push(instr);
                }
                break;
            case 1:
                // Square
                var points = [];
                points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));
                points.push(new Mapzania.Point(center.x + (radius * 1), center.y + (radius * 1)));
                points.push(new Mapzania.Point(center.x + (radius * 1), center.y - (radius * 1)));
                points.push(new Mapzania.Point(center.x - (radius * 1), center.y - (radius * 1)));
                points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));

                var lines = new Mapzania.Line(points);
                lines = lines.rotate(center, angleRadians);

                var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                res.push(instr);
                if (sym.border && sym.border.size > 0) {
                    var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    res.push(instr);
                }
                break;
            case 2:
                // Triangle
                var points = [];
                points.push(new Mapzania.Point(center.x, center.y - radius));
                points.push(new Mapzania.Point(center.x + (radius * 1), center.y + (radius * 1)));
                points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));
                points.push(new Mapzania.Point(center.x, center.y - radius));

                var lines = new Mapzania.Line(points);
                lines = lines.rotate(center, angleRadians);

                var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                res.push(instr);
                if (sym.border && sym.border.size > 0) {
                    var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    res.push(instr);
                }
                break;
            case 3:
                // Cross
                var points = [];
                points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));
                points.push(new Mapzania.Point(center.x + (radius * 1), center.y - (radius * 1)));
                var line1 = new Mapzania.Line(points);
                line1 = line1.rotate(center, angleRadians);

                points = [];
                points.push(new Mapzania.Point(center.x + (radius * 1), center.y + (radius * 1)));
                points.push(new Mapzania.Point(center.x - (radius * 1), center.y - (radius * 1)));
                var line2 = new Mapzania.Line(points);
                line2 = line2.rotate(center, angleRadians);

                var instr1 = { type: "draw-line", line: line1, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                var instr2 = { type: "draw-line", line: line2, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                res.push(instr1);
                res.push(instr2);
                break;
            case 4:
                // Diamand
                var points = [];
                points.push(new Mapzania.Point(center.x, center.y + radius));
                points.push(new Mapzania.Point(center.x + (radius * 0.707), center.y));
                points.push(new Mapzania.Point(center.x, center.y - radius));
                points.push(new Mapzania.Point(center.x - (radius * 0.707), center.y));
                points.push(new Mapzania.Point(center.x, center.y + radius));

                var lines = new Mapzania.Line(points);
                lines = lines.rotate(center, angleRadians);

                var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                res.push(instr);
                if (sym.border && sym.border.size > 0) {
                    var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    res.push(instr);
                }
                break;
            case 5:
                // Crosshair
                var points = [];
                points.push(new Mapzania.Point(center.x - radius, center.y));
                points.push(new Mapzania.Point(center.x + radius, center.y));
                var line1 = new Mapzania.Line(points);
                line1 = line1.rotate(center, angleRadians);

                points = [];
                points.push(new Mapzania.Point(center.x, center.y + radius));
                points.push(new Mapzania.Point(center.x, center.y - radius));
                var line2 = new Mapzania.Line(points);
                line2 = line2.rotate(center, angleRadians);

                var instr1 = { type: "draw-line", line: line1, size: calcSize(sym.border.size) / 2.0, color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                var instr2 = { type: "draw-line", line: line2, size: calcSize(sym.border.size) / 2.0, color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                res.push(instr1);
                res.push(instr2);

                var instr3 = { type: "draw-circle", center: center, radius: radius, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity };
                res.push(instr3);
                break;
            case 6:
                // Triangle for a Line Arrow
                var points = [];
                points.push(new Mapzania.Point(center.x, center.y - radius));
                points.push(new Mapzania.Point(center.x + (radius * 0.866), center.y + (radius * 0.5)));
                points.push(new Mapzania.Point(center.x - (radius * 0.866), center.y + (radius * 0.5)));
                points.push(new Mapzania.Point(center.x, center.y - radius));
                
                var lines = new Mapzania.Line(points);
                lines = lines.rotate(center, angleRadians);

                var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                res.push(instr);
                if (sym.border && sym.border.size > 0) {
                    var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    res.push(instr);
                }
                break;
            case 500:
                // Image / Icon symbol
                var p = new Mapzania.Point(center.x, center.y);
                p.translate(sym.imageOffset.x, -sym.imageOffset.y);

                var extent = new Mapzania.Extent({
                    minX: p.x - sym.imageSize.width / 2,
                    minY: p.y - sym.imageSize.height / 2,
                    maxX: p.x + sym.imageSize.width / 2,
                    maxY: p.y + sym.imageSize.height / 2
                });
                var instr = { type: "image", extent: extent, opacity: sym.opacity, src: sym.imageUrl };
                res.push(instr);
                break;
            default:
                throw ("Unknown point symbol type: " + sym.type);
        }

        return res;
    }

    Renderer.prototype._measureLabelLines = function (text, font, size) {
        var me = this;
        var result = { lines: [], width: 0, height:0 };
        if (text) {
            text = new String(text);
            var lines = text.split("\n");
            lines.forEach(function (curLine) {
                curLine = curLine.replace("\r", "");
                curLine = curLine.trim();
                if (curLine.length > 0) {
                    var dimensions = me.drawer.measureText(curLine, font, size);
                    result.lines.push({
                        text: curLine,
                        width: dimensions.width,
                        height: dimensions.height
                    });
                    result.height += dimensions.height;
                    result.width = Math.max(result.width, dimensions.width);
                }
            });
        }
        return result;
    }

    Renderer.prototype._drawOneLabelInstruction = function (sheetKey, ist) {
        var curOffset = {};
        $.extend(curOffset, ist.offset);

        var Y = 0;
        var lines = ist.labelLines.lines;
        // TODO : We need to find a formula for this
        if (lines.length == 2) Y = 0.5 * (lines[0].height);
        if (lines.length == 3) Y = 0.5 * (lines[0].height) + 0.5 * (lines[1].height);
        if (lines.length == 4) Y = 0.5 * (lines[0].height) + (lines[1].height);
        if (lines.length == 5) Y = 0.5 * (lines[0].height) + (lines[1].height) + 0.5 * (lines[2].height);
        curOffset.y += Y;

        for (var i = 0; i < ist.labelLines.lines.length; i++) {
            var cur = ist.labelLines.lines[i];

            this.drawer.drawText(sheetKey, ist.location, cur.text, ist.font, ist.size, ist.color,
                ist.backColor, ist.opacity, ist.align, ist.haloSize, ist.haloColor, ist.bold,
                ist.italic, ist.underline, ist.addMark, ist.rotation, curOffset);

            curOffset.y -= (cur.height);
        }
    }

    ns.Renderer = Renderer;

})(Mapzania);

//
//
//

(function (ns) {
    function StandardLabelFormattingStrategy(visibleExtent) {
        this.visibleExtent = visibleExtent;
    }

    StandardLabelFormattingStrategy.prototype.getLabelInstance = function (layer, themer, feature) {
        if (this.shouldDiscardLabel(layer, themer, feature)) {
            return null;
        }

        var centerPnt = null;
        var rotation = 0;
        var symbol = themer.getLabelSymbol(feature);
        if (symbol.rotation != 0) rotation = symbol.rotation;
        var text = themer.getLabelText(feature);
        var rotationFollowGeometry = false;
        var weight = 0.0;

        if (this.isLineGeometry(feature.geometry)) {
            var x = this.calculateLineLabelDetails(symbol, feature.geometry);
            if (!x) {
                return null;
            } else {
                rotation = x.rotation;
                centerPnt = x.centerPnt;
                rotationFollowGeometry = x.rotationFollowGeometry;
                weight = x.weight;
            }
        } else {
            if (this.isPolygonGeometry(feature.geometry)) {
                weight = feature.geometry.getArea();
            }

            centerPnt = feature.geometry.getCentroid();
        }

        return {
            center: centerPnt,
            text: text,
            symbol: symbol,
            rotation: rotation,
            rotationFollowGeometry: rotationFollowGeometry,
            weight: weight
        };
    }

    StandardLabelFormattingStrategy.prototype.shouldDiscardLabel = function (layer, themer, feature) {
        if (feature.dynamicProperties && !feature.dynamicProperties.labelIsVisible) return true;
        if (themer == null) return true;
        if (themer.getLabelSymbol == null) return true;

        var text = themer.getLabelText(feature);
        if (!text) return true;

        return false;
    }

    StandardLabelFormattingStrategy.prototype.calculateLineLabelDetails = function (symbol, geometry) {
        var lines = this.flattenLine(geometry);
        var linesVisible = this.calculateVisibleLinesInExtent(lines, this.visibleExtent);

        var maxLengthLine = null;
        var maxLength = 0;
        linesVisible.forEach(function (cur) {
            var l = cur.getLength();
            if (l > maxLength) {
                maxLength = l;
                maxLengthLine = cur;
            }
        });

        if (maxLengthLine) {
            var r = maxLengthLine.getPointAndSegmentAtLength(maxLength / 2.0);
            if (r) {
                var centerPnt = r.point;
                var rotation = 0;
                var rotationFollowGeometry = false;
                if (symbol.rotation != 0) rotation = symbol.rotation;
                if (symbol.autoRotate) {
                    rotation = r.segment.getAngleRadians();
                    // Ensure the labels does not show upside down
                    if (rotation > Math.PI / 2) rotation -= (Math.PI);
                    if (rotation < -Math.PI / 2) rotation += (Math.PI);
                    rotationFollowGeometry = true;
                }
                return { centerPnt: centerPnt, rotation: rotation, rotationFollowGeometry: rotationFollowGeometry, weight: maxLength };
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    StandardLabelFormattingStrategy.prototype.calculateVisibleLinesInExtent = function (lines, extent) {
        var linesResult = [];
        lines.forEach(function (curLine) {
            var segments = curLine.getLineSegments(null);
            var r = [];
            segments.forEach(function (curSegment) {
                var t = curSegment.getVisibleIntersectionInExtent(extent);
                if (t) {
                    if (t == curSegment) {
                        r.push(t);
                    } else {
                        if (r.length > 0) {
                            r.push(t);
                            var lineResult = new Mapzania.Line();
                            lineResult.fromLineSegments(r);
                            linesResult.push(lineResult);
                            r = [];
                        } else {
                            r.push(t);
                        }
                    }
                }
            });
            if (r.length > 0) {
                var lineResult = new Mapzania.Line();
                lineResult.fromLineSegments(r);
                linesResult.push(lineResult);
            }
        });

        return linesResult;
    }

    StandardLabelFormattingStrategy.prototype.flattenLine = function (geom) {
        var result = [];
        if (geom instanceof Mapzania.Line) {
            result.push(geom);
        }
        if (geom instanceof Mapzania.MultiLine) {
            geom.lines.forEach(function (cur) {
                result.push(cur);
            });
        }
        return result;
    }

    StandardLabelFormattingStrategy.prototype.isLineGeometry = function (geom) {
        return (geom instanceof Mapzania.Line || geom instanceof Mapzania.MultiLine);
    }

    StandardLabelFormattingStrategy.prototype.isPolygonGeometry = function (geom) {
        return (geom instanceof Mapzania.Polygon || geom instanceof Mapzania.MultiPolygon);
    }

    ns.StandardLabelFormattingStrategy = StandardLabelFormattingStrategy;

})(Mapzania);

//
// StandardLabelFilterStrategy
//

(function (ns) {
    function StandardLabelFilterStrategy() {
    }

    StandardLabelFilterStrategy.prototype.filterLabels = function (layer, symbolisedGeometry) {
        if (!layer.allowDuplicateLabels) {
            this.filterDuplicateLabels(layer, symbolisedGeometry);
        }
    }

    StandardLabelFilterStrategy.prototype.filterDuplicateLabels = function (layer, symbolisedGeometry) {
        var withLabels = [];
        var withoutLabels = [];
        symbolisedGeometry.forEach(function (cur) {
            if (cur.label) {
                withLabels.push(cur);
            } else {
                withoutLabels.push(cur);
            }
        });

        // Sort in descending order
        withLabels.sort(function (x, y) {
            if (x.weight < y.weight) return +1;
            if (x.weight > y.weight) return -1;
            return 0;
        });

        var labelsToRemove = {};
        withLabels.forEach(function (cur) {
            if (labelsToRemove.hasOwnProperty(cur.label.text)) {
                delete cur.label;
            } else {
                labelsToRemove[cur.label.text] = true;
            }
        });

        symbolisedGeometry.splice(0, symbolisedGeometry.length);
        withLabels.forEach(function (cur) {
            symbolisedGeometry.push(cur);
        });
        withoutLabels.forEach(function (cur) {
            symbolisedGeometry.push(cur);
        });
    }

    ns.StandardLabelFilterStrategy = StandardLabelFilterStrategy;

})(Mapzania);

if (!Mapzania)
    var Mapzania = {};

/**
 * @doc Mapzania.SourceFactory.*ClassDescription*
 *
 * A factory class for layer sources. 
 * Applications typically do not make direct use of any functionality provided in this class.
 */
(function (ns) {

    /**
      * @doc Mapzania.SourceFactory.SourceFactory
      *
      * Constructor.
      *
      * @param api (Mapzania.Api) A handle to a valid Mapzania.Api instance.
      *
      * @param srid (String) The current map display SRID.
      */

    function SourceFactory(api, srid, layerSourceLoadQueue, vtlgManager) {
        this.api = api;
        this.srid = srid;
        this.layerSourceLoadQueue = layerSourceLoadQueue;
        this.vtlgManager = vtlgManager;
        this.sources = {};
    }

    /**
      * @doc Mapzania.SourceFactory.init
      *
      * Initializes this source factory.
      * 
      * @param callback (Function)  An optional callback function that needs to to be invoked to indicate the completion of this method.
      *                             This function accepts no parameters.
      *
      */

    SourceFactory.prototype.init = function (callback) {
        var srcs = this.sources;
        var me = this;
        this.api.getLayerSources(function (data) {

            for (var i = 0; i < data.length; i++) {
                var def = data[i];
                srcs[def.key] = def;
            }

            if (callback)
                callback();
        });
    }

    /**
      * @doc Mapzania.SourceFactory.getSource
      *
      * Obtain a new vector layer source for a given layer.
      * 
      * @param sourceKey (String)  The layer source key for the layer.
      * 
      * @param layer     (Object)  The layer instance in use by the Map Model for the layer.
      *
      * @param model     (Mapzania.Model)  A handle to the current map model.
      *
      * @return (Mapzania.VectorLayerSource) A valid vector layer source for the given layer. 
      *
      */

    SourceFactory.prototype.getSource = function (sourceKey, layer, model) {
        var mapKey = model.key;
        var key = sourceKey.toUpperCase();
        if (!this.sources[key])
            return {};
            //throw "Invalid sourceKey : " + sourceKey;

        var def = this.sources[key];

        //<ANDRE>
        if (def.sourceType == "LOCALVECTOR") {
            return new ns.LocalVectorLayerSource(layer.key, model.store);
        }
        //</ANDRE>

        if (def.sourceType == "VECTOR") {
            var source = null;
            if (key.indexOf("SCHEMASOURCE-") == 0) {
                // Client layer source
                //console.log(sourceKey + "=VectorLayerSourceSchema");
                source = new ns.VectorLayerSourceSchema(def, this.api, this.srid, layer);
            } else {
                // Server layer source
                if (layer.vectorServerName) {
                    if (layer.vectorServerName == "LayerGroupVectorTiles") {
                        //console.log(sourceKey + "=VTLGLayerSource");
                        source = new ns.VTLGLayerSource(def, this.api, this.srid, layer, this.layerSourceLoadQueue, this.vtlgManager, mapKey);
                    } else {
                        console.log(sourceKey + "=VectorLayerSourceServerVectorServer");
                        source = new ns.VectorLayerSourceServerVectorServer(def, this.api, this.srid, layer, this.layerSourceLoadQueue);
                    }
                } else {
                    //console.log(sourceKey + "=VectorLayerSourceServer");
                    source = new ns.VectorLayerSourceServer(def, this.api, this.srid, layer, this.layerSourceLoadQueue);
                }
            }

            if (model.getTagValue("CacheVectorData") == "true" || layer.cacheLayer) {
                //console.log(sourceKey + "=VectorLayerSourceClientFeatures");
                return new ns.VectorLayerSourceClientFeatures(def, new ns.VectorLayerSourceCache(new ns.VectorLayerSourceCompleteCache(source)));
            } else {
                //console.log(sourceKey + "=VectorLayerSourceClientFeatures");
                return new ns.VectorLayerSourceClientFeatures(def, new ns.VectorLayerSourceCache(source));
            }
        }
        if (def.sourceType == "TILED_RASTER") {
            //console.log(sourceKey + "=TiledRasterLayerSource");
            return new ns.TiledRasterLayerSource(def, this.api, this.srid, this.layerSourceLoadQueue);
        }
    }

    ns.SourceFactory = SourceFactory
})(Mapzania)

var Mapzania = (function (ns) {

    var toPointSymbol = function (simplestyle) {

        if (!simplestyle.size)
            simplestyle.size = 10;

        return {
            size: simplestyle.size,
            color: simplestyle.fillColor,
            opacity: (simplestyle.fillOpacity || 1) * 100,
            border: {
                color: simplestyle.strokeColor,
                size: simplestyle.strokeWeight,
                opacity: (simplestyle.strokeOpacity || 1) * 100
            }
        };
    }

    var toLineSymbol = function (simplestyle) {
        return {
            color: simplestyle.strokeColor,
            size: simplestyle.strokeWeight,
            opacity: (simplestyle.strokeOpacity || 1) * 100
        };
    }

    var toFillSymbol = function (simplestyle) {

        var s = simplestyle;

        if (!s.fillColor)
            s.fillOpacity = 0;

        var outline = {
            color: s.strokeColor,
            opacity: (s.strokeOpacity || 0) * 100,
            size: s.strokeWeight || 0
        };

        if (s.strokeColor && s.strokeOpacity == null)
            outline.opacity = 100;

        if (s.strokeColor && !s.strokeWeight == null)
            outline.size = 1;

        if (s.strokeWeight && s.strokeColor == null)
            outline.color = "#000000";

        if (s.strokeWeight && s.strokeOpacity == null)
            outline.opacity = 100;

        var res = {
            type: 1,
            color: s.fillColor,
            opacity: (s.fillOpacity || 1) * 100,
            outline: outline
        };

        return res;
    }

    var toLabelSymbol = function (label) {

        var isBold = label.fontWeight && label.fontWeight.toLowerCase() == "bold";
        var res = {
            color: label.fill,
            bold: isBold,
            fontSize: 10,
            rotation: 0,
            fontFamily:"arial",
            offset: { x: 0, y: 0 }
        };

        return res;
    }

    ns.Symbols = {
        toPointSymbol: toPointSymbol,
        toLineSymbol: toLineSymbol,
        toFillSymbol: toFillSymbol,
        toLabelSymbol: toLabelSymbol
    };

    return ns;

})(Mapzania || {});
if (!Mapzania)
    var Mapzania = {};

(function (ns) {

    var _style;

    function DynamicThemer(style) {
        _style = style;
    }

    DynamicThemer.prototype.getSymbol = function (feature) {
        var type = feature.geometry.getType();

        var style = feature.style;

        if (!style)
            style = _style;

        if (typeof style == "function")
            style = style(feature.attributes);

        if (type == 1 || type == 4)
            return Mapzania.Symbols.toPointSymbol(style);

        if (type == 2 || type == 5)
            return Mapzania.Symbols.toLineSymbol(style);

        if (type == 3 || type == 6)
            return Mapzania.Symbols.toFillSymbol(style);

        throw "DynamicThemer: unsupported geometry type.";
    }

    DynamicThemer.prototype.getLabelText = function (feature) {
        var style = feature.style;

        if (!style)
            style = _style;

        if (typeof style == "function")
            style = style(feature.attributes);

        if (!style.label)
            return null;

        var label;

        if (typeof style.label == "function")
            label = style.label(feature.attributes);
        else
            label = style.label;

        var text;
        if (typeof label.text == "function")
            text = label.text(feature.attributes);
        else
            text = label.text;

        return text;
    }

    DynamicThemer.prototype.getLabelSymbol = function (feature) {
        var style = feature.style;

        if (!style)
            style = _style;

        if (typeof style == "function")
            style = style(feature.attributes);

        if (!style.label)
            return null;

        var label;

        if (typeof style.label == "function")
            label = style.label(feature.attributes);
        else
            label = style.label;

        var symbol = Mapzania.Symbols.toLabelSymbol(label);

        return symbol;
    }

    ns.DynamicThemer = DynamicThemer;

})(Mapzania)

if (!Mapzania)
    var Mapzania = {};

(function (ns) {

    function HeatMapThemer(theme) {
        this.theme = theme;
        this.minValue = null;
        this.maxValue = null;
        this.lookup = [];
    }

    HeatMapThemer.prototype.evalFeature = function (feature) {
        var featVal = feature.attributes[this.theme.field];
        if (this.minValue == null) {
            this.minValue = featVal;
            this.maxValue = featVal;
        } else {
            if (featVal < this.minValue) this.minValue = featVal;
            if (featVal > this.maxValue) this.maxValue = featVal;
        }
    }

    HeatMapThemer.prototype.doneWithEvalFeatures = function () {
        var step = (this.maxValue - this.minValue) / (this.theme.minToMaxSymbols.length - 1);
        for (var i = 0; i < this.theme.minToMaxSymbols.length - 1; i++) {
            var fromS = this.theme.minToMaxSymbols[i];
            var toS = this.theme.minToMaxSymbols[i + 1];
            var fromV = (this.minValue + (i * step));
            var toV = (this.minValue + (i * step)) + step;
            this.lookup.push({fromV: fromV, toV: toV, fromS: fromS, toS: toS});
        }
    }

    HeatMapThemer.prototype.getSymbol = function (feature) {
        var sym = null;
        var featVal = feature.attributes[this.theme.field];

        for (var i = 0; i < this.lookup.length; i++) {
            var cur = this.lookup[i];
            if (featVal >= cur.fromV && featVal <= cur.toV) {
                sym = this.buildSymbol(cur, featVal);
            }
        }

        return sym;
    }

    HeatMapThemer.prototype.getLabeller = function (feature) {
        return this.theme.labeller;
    }

    HeatMapThemer.prototype.buildSymbol = function (lookupEntry, featVal) {
        var sym = {};
        $.extend(true, sym, lookupEntry.fromS);

        var perc = (featVal - lookupEntry.fromV) / (lookupEntry.toV - lookupEntry.fromV);

        var fromRGB = this.hexToRgb(lookupEntry.fromS.color);
        var toRGB = this.hexToRgb(lookupEntry.toS.color);
        var colorR = Math.floor((toRGB.r - fromRGB.r) * perc + fromRGB.r);
        var colorG = Math.floor((toRGB.g - fromRGB.g) * perc + fromRGB.g);
        var colorB = Math.floor((toRGB.b - fromRGB.b) * perc + fromRGB.b);

        sym.color = this.rgbToHex(colorR, colorG, colorB);
        return sym;
    }

    HeatMapThemer.prototype.hexToRgb = function (hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function (m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    HeatMapThemer.prototype.rgbToHex = function (r, g, b) {
        var componentToHex = function (c) {
            var hex = c.toString(16);
            return hex.length == 1 ? "0" + hex : hex;
        }
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    ns.HeatMapThemer = HeatMapThemer;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

(function (ns) {

	function RangeThemer(theme) {
	    this.theme = theme
	    this.minValue = null;
	    this.maxValue = null;
	}

	RangeThemer.prototype.evalFeature = function (feature) {
	    if (this.theme.normalizeValues) {
	        var featVal = feature.attributes[this.theme.field];
	        if (this.minValue == null) {
	            this.minValue = featVal;
	            this.maxValue = featVal;
	        } else {
	            if (featVal < this.minValue) this.minValue = featVal;
	            if (featVal > this.maxValue) this.maxValue = featVal;
	        }
	    }
	}

	RangeThemer.prototype.doneWithEvalFeatures = function () {
	    
	}

	RangeThemer.prototype.getSymbol = function (feature) {
	    var sym;
	    var themeValue = this._getThemeValue(feature);
	    if (themeValue && themeValue.symbol) {
	        sym = themeValue.symbol;
	    } else {
	        sym = this.theme.defaultSymbol;
	    }
		return sym;
	}

	//RangeThemer.prototype.getLabeller = function (feature) {
	//    var labeller = null;
	//    var themeValue = this._getThemeValue(feature);
	//    if (themeValue && themeValue.labeller) {
	//        labeller = themeValue.labeller;
	//    } else {
	//        labeller = this.theme.labeller;
	//    }

	//    return labeller;
    //}

	RangeThemer.prototype.getLabelText = function (feature) {
	    var labeller = null;
	    var themeValue = this._getThemeValue(feature);
	    if (themeValue && themeValue.labeller) {
	        labeller = themeValue.labeller;
	    } else {
	        labeller = this.theme.labeller;
	    }

	    if (!labeller)
	        return null;

	    return feature.attributes[labeller.field];
	}

	RangeThemer.prototype.getLabelSymbol = function (feature) {
	    var labeller = null;
	    var themeValue = this._getThemeValue(feature);
	    if (themeValue && themeValue.labeller) {
	        labeller = themeValue.labeller;
	    } else {
	        labeller = this.theme.labeller;
	    }

	    return labeller.symbol;
	}

	RangeThemer.prototype._getThemeValue = function (feature) {
	    var sym;
	    var featVal = feature.attributes[this.theme.field];

	    if (this.theme.normalizeValues) {
	        var diff = this.maxValue - this.minValue;
	        var perc = 0;
	        if (diff != 0) {
	            perc = (featVal - this.minValue) / diff * 100.0;
	        }
	        if (perc > 100) perc = 100;
	        if (perc < 0) perc = 0;
	        featVal = perc;
	    }

	    var funcStrCmpIgnoreCase = function (s1, s2) {
	        s1 = s1 ? String(s1) : "";
	        s2 = s2 ? String(s2) : "";
	        return s1.toUpperCase().trim() == s2.toUpperCase().trim();
	    };

	    for (var i = 0; i < this.theme.values.length; i++) {
	        var themeVal = this.theme.values[i];

	        if (themeVal.value) { // unique range
	            if (funcStrCmpIgnoreCase(featVal, themeVal.value)) {
	                return themeVal;
	            }
	        } else { // range
	            if (featVal >= themeVal.min && featVal <= themeVal.max) {
	                return themeVal;
	            }
	        }
	    }

	    return null;
	}

	ns.RangeThemer = RangeThemer;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

(function (ns) {

	function StandardThemer(theme) {
		this.theme = theme
	}

	StandardThemer.prototype.getSymbol = function (feature) {
	    return this.theme.symbol;
	}

	//StandardThemer.prototype.getLabeller = function (feature) {
	//    return this.theme.labeller;
	//}

	StandardThemer.prototype.getLabelText = function (feature) {
	    if (!this.theme.labeller)
	        return null;

	    return feature.attributes[this.theme.labeller.field];
	}

	StandardThemer.prototype.getLabelSymbol = function (feature) {
	    return this.theme.labeller.symbol;
	}

	ns.StandardThemer = StandardThemer;

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

(function (ns) {
	function ThemerFactory() { }

	ThemerFactory.prototype.getThemer = function (theme, ignoreSchema, style) {
	    var themer

	    if (ignoreSchema || theme == null)
	        return new ns.DynamicThemer(style);

		if (theme.symbol)
			themer = new ns.StandardThemer(theme)

		if (theme.values)
			themer = new ns.RangeThemer(theme)

		if (theme.instanceTypeName == "HeatMapTheme")
		    themer = new ns.HeatMapThemer(theme);

		if (themer == null)
			throw "Unknown themer"

		return themer;
	}

	ns.ThemerFactory = ThemerFactory

})(Mapzania)

if (!Mapzania)
	var Mapzania = {};

(function(ns) {

    function TiledRasterLayerSource(src, api, srid, layerSourceLoadQueue) {
		$.extend(this, src);
		this.api = api;
		this.srid = srid;
		this.layerSourceLoadQueue = layerSourceLoadQueue;

		this.gotTiles = new ns.Event();
	}

	TiledRasterLayerSource.prototype.getTiles = function (extentToLoad, extentVisible, filter) {
	    var me = this;
	    if (!extentVisible)
	        extentVisible = extentToLoad;

	    this.lastRequestId = Mapzania.Util.createPseudoGuid();

	    var callback = function (data, responseId) {
	        if (me.lastRequestId == responseId) {
	            for (var i = 0; i < data.length; i++) {
	                var d = data[i];
	                d.extent = new ns.Extent({
	                    minX: d.minX,
	                    minY: d.minY,
	                    maxX: d.maxX,
	                    maxY: d.maxY
	                });
	            }
	            me.gotTiles.fire(me, data);
	        }
	    };

	    this.layerSourceLoadQueue.getTiles(this.key, extentToLoad, extentVisible, this.srid, filter, callback, this.lastRequestId);
	}

	ns.TiledRasterLayerSource = TiledRasterLayerSource;

})(Mapzania)

var Mapzania = (function (ns) {

    var Toolbar = function ($el, model) {
        this.$el = $el;

        $(".reset", $el).click(function () {
            model.reset();
        });

        $(".zoomIn", $el).click(function () {
            model.changeState(Mapzania.MapState.zoomIn);
        });

        $(".zoomOut", $el).click(function () {
            model.zoomOut();
        });

        $(".pan", $el).click(function () {
            model.changeState(Mapzania.MapState.pan);
        });

        $(".queryPoint", $el).click(function () {
            model.changeState(Mapzania.MapState.queryPoint);
        });

        $(".queryLine", $el).click(function () {
            model.changeState(Mapzania.MapState.queryLine);
        });

        $(".queryPolygon", $el).click(function () {
            model.changeState(Mapzania.MapState.queryPolygon);
        });

        //$(".length", $el).click(function () {
        //    model.changeState(Mapzania.MapState.length);
        //});

        //$(".area", $el).click(function () {
        //    model.changeState(Mapzania.MapState.area);
        //});

        //$(".queryPolygonEnclosed", $el).click(function (e) {
        //    model.changeState(Mapzania.MapState.queryPolygonEnclosed);
        //});

        //$(".printMap", $el).click(function () {
        //    model.changeState(Mapzania.MapState.printMap);
        //});

        //$(".exportMap", $el).click(function () {
        //    model.changeState(Mapzania.MapState.exportMap);
        //});
    }

    Toolbar.buttons = {
        reset: { key: "reset", text: "Reset" },
        zoomIn: { key: "zoomIn", text: "Zoom In" },
        zoomOut: { key: "zoomOut", text: "Zoom Out" },
        pan: { key: "pan", text: "Pan" },
        queryPoint: { key: "queryPoint", text: "Query Point" },
        queryLine: { key: "queryLine", text: "Query Line" },
        queryPolygon: { key: "queryPolygon", text: "Query Polygon" },
    };

    var btns = Toolbar.buttons;

    Toolbar.Layouts = {
        simple: ["reset", "zoomIn", "zoomOut", "pan"],
        standard: ["reset", "zoomIn", "zoomOut", "pan", "queryPoint", "queryLine", "queryPolygon"]
    };

    ns.Toolbar = Toolbar;

    return ns;
})(Mapzania || {});

/// <reference path="../lib/swfobject-1.5.js" />

if (!Mapzania)
	var Mapzania = {};

(function (ns) {

    //====================Canvas Checker====================

    function CanvasCheck() { }

    CanvasCheck.browserSupportsCanvas = function () {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
    }

    //========================Toggler=======================

    function Toggler() {
        this.ctrlrs = [];
    }

    Toggler.prototype.add = function (key, ctrlr) {
        this.ctrlrs.push({ key: key, ctrlr: ctrlr });
    }

    Toggler.prototype.toggle = function (key) {
        // The Tools Manager only manages certain map states. The result may, in some cases, be null.
        var result = null;
        for (var i = 0; i < this.ctrlrs.length; i++) {
            var item = this.ctrlrs[i];
            item.ctrlr.enabled(item.key == key);
            if (item.key == key) {
                result = {
                    state: key,
                    control: item.ctrlr
                }
            }
        }
        return result;
    }

    //======================ToolsManager====================

    function ToolsManager(model, viewport, drawer) {

        this.model = model;
        this.viewport = viewport;
        this.drawer = drawer;

        // The leaflet viewport needs a handle to the tools manager. We needs this, as certain tools should not allow
        //  leaflet to capture events - i.e. certain tools should disable map pan and zoom interactions.
        this.viewport._toolsManager = this; 

        this.toggler = new Toggler();

        this.defaultMapState = Mapzania.MapState.pan;
        this.currentControlBag = null;

        var ctns = Mapzania.Controllers;

        ////-----------------Area Selector----------------------
        //var area = new ctns.MultiSelector(viewport, drawer, function (data) {
        //    var line = new ns.Line();
        //    line.points = data;

        //    var res = me._trf().from(2, line);

        //    model.getArea(res.points);
        //}, false, true);

        //this.toggler.add(ns.MapState.area, area);

        ////---------------Length Selector----------------------
        //var length = new ctns.MultiSelector(viewport, drawer, function (data) {
        //    var line = new ns.Line();
        //    line.points = data;

        //    var res = me._trf().from(2, line);

        //    model.getLength(res.points);
        //}, false, false);

        //this.toggler.add(ns.MapState.length, length);

        //--------------------Point Selector------------------
        var queryPoint = new ctns.MultiSelector(viewport, drawer, function (data) {
            if (data.length == 0)
                return;

            var pnt = me._trf().from(1, data[0]);
            model.queryAt({ type: 1, geom: pnt, pixel:data[0] });

        }, true, false);

        this.toggler.add(ns.MapState.queryPoint, queryPoint);

        //----------------------Line Selector------------------------        
        var queryLine = new ctns.MultiSelector(viewport, drawer, function (data) {

            if (data.length >= 2) {
                var line = new ns.Line();
                line.points = data;
                var res = me._trf().from(2, line);
                model.queryAt({ type: 2, geom: res });
            }
        }, false, false);

        this.toggler.add(ns.MapState.queryLine, queryLine);

        //---------------------Polygon Selector-----------------
        var queryPolygon = new ctns.MultiSelector(viewport, drawer, function (data) {

            if (data.length >= 3) {
                var line = new ns.Line();
                line.points = data;
                var poly = new ns.Polygon();
                poly.shell = line;

                var res = me._trf().from(3, poly);
                model.queryAt({ type: 3, geom: res, enclosed: true });
            }
        }, false, true);

        this.toggler.add(ns.MapState.queryPolygon, queryPolygon);

        //--------------Enclosed Polygon Selector----------
        var queryPolygonEnclosed = new ctns.MultiSelector(viewport, drawer, function (data) {
            var line = new ns.Line();
            line.points = data;
            //line.points.push(data[0]);

            var poly = new ns.Polygon();
            poly.shell = line;

            var res = me._trf().from(3, poly);
            model.queryAt({ type: 3, geom: res, enclosed: true });

        }, false, true);

        this.toggler.add(ns.MapState.queryPolygonEnclosed, queryPolygonEnclosed);

        //----------------------Pan------------------------
        this.toggler.add(ns.MapState.pan, new ctns.Pan(viewport, function (diffs) {
            // During a Pan, we Pan with a delta x and y value in pixels (input parameters).
            //  This gives us an angle. The delta x and y in world coordinates gives as a
            //  length in world coordinates (from pixel point (0, 0)). 
            //  All we do then, is to Pan the model's extent with the given angle in world coordinates. 

            var pDiffPixels = new Mapzania.Point(diffs.x, diffs.y);
            var pWorld = me._trf().from(1, pDiffPixels);
            var pPixelZero = new Mapzania.Point(0, 0);
            var pWorldZero = me._trf().from(1, pPixelZero);

            var diffXWorld = pWorld.x - pWorldZero.x;
            var diffYWorld = pWorld.y - pWorldZero.y;

            var lengthWorld = Math.sqrt(diffXWorld * diffXWorld + diffYWorld * diffYWorld);
            var angle = Math.atan2(-diffs.y, diffs.x);

            var diffX = lengthWorld * Math.cos(angle);
            var diffY = lengthWorld * Math.sin(angle);

            var extNew = {};
            $.extend(extNew, me.model.extent);

            extNew.minX -= diffXWorld;
            extNew.maxX -= diffXWorld;
            extNew.minY -= diffYWorld;
            extNew.maxY -= diffYWorld;

            // Instruct the model to zoomTo, indicating that this is comming from a PAN operation.
            model.zoomTo(new Mapzania.Extent(extNew), false, false, false, true);
        }));

        //----------------------Zoom In------------------------
        this.toggler.add(ns.MapState.zoomIn, new ctns.RectangleSelector(viewport, drawer, function (ext) {
            if (ext.width() < 10 || ext.height() < 10)
                return;
            var extNew = me._trf().from(-1, new ns.Extent(ext));
            model.zoomTo(extNew, false, true);
            model.changeState(me.defaultMapState);
        }));

        var me = this;

        model.stateChanged.add(function (e) {
            var result = me.toggler.toggle(e.data);
            if (result) {
                // We know it is a control that we manage
                if (result.control.isTempState()) {
                    if (me.currentControlBag && !me.currentControlBag.control.isTempState()) {
                        setTimeout(function () { me.model.changeState(me.defaultMapState); }, 0);
                    }
                } else {
                    me.currentControlBag = result;
                }
            } else {
                // A control (or state) that we do not manage, i.e. Print, Export or custom toolbar items. We assume these are temporary states
                if (me.currentControlBag && !me.currentControlBag.control.isTempState()) {
                    setTimeout(function () { me.model.changeState(me.defaultMapState); }, 0);
                }
            }
        });

        this.viewport.mouseWheelUp.add(function (e) {
            if (me.currentControlBag && me.currentControlBag.control.canZoomMap()) {
                var pnt = me._trf().from(1, e.data);
                me.model.zoomIn(pnt);
            }
        });

        this.viewport.doubleClick.add(function (e) {
            if (me.currentControlBag && me.currentControlBag.control.canZoomMap()) {
                var pnt = me._trf().from(1, e.data);
                me.model.zoomIn(pnt);
            }
        });

        this.viewport.mouseWheelDown.add(function (e) {
            if (me.currentControlBag && me.currentControlBag.control.canZoomMap()) {
                var pnt = me._trf().from(1, e.data);
                me.model.zoomOut(pnt);
            }
        });

        //
        // During map startup, we make the Pan control the default
        //
        this.model.schemaLoaded.add(function () {
            me.model.changeState(me.defaultMapState);
        });
    }

    ToolsManager.prototype._trf = function () {
        return this.viewport.getTransformer(this.model.extent);
    }

    ns.CanvasCheck = CanvasCheck;
    ns.ToolsManager = ToolsManager;

})(Mapzania);

if (!Mapzania)
    var Mapzania = {};

(function (ns) {

    var createPseudoGuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    var truncate = function (x) {
        return x < 0 ? Math.ceil(x) : Math.floor(x);
    };

    var startsWith = function (str, subStr) {
        return str.slice(0, subStr.length) == subStr;
    };

    var endsWith = function (str, subStr) {
        return str.slice(-subStr.length) == subStr;
    };

    ns.Util = {
        createPseudoGuid: createPseudoGuid,
        truncate: truncate,
        startsWith: startsWith,
        endsWith:endsWith
    };

})(Mapzania);
if (!Mapzania)
    var Mapzania = {};

//
// VectorLayerSource base class
//

(function (ns) {
    function VectorLayerSource() {
        this.gotFeatures = new ns.Event(); // An event to indicate that new entire layer data was received and map needs to be updated
    }

    VectorLayerSource.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams) {
        // me.gotFeatures.fire(me, {seqNo:x, features: [list of feature objects], isFinal: true}); Where seqNo is zero based
    }

    VectorLayerSource.prototype.getCurrentFeatures = function () {
        // Returns an array of current / active features. Typically the features displayed on the map.
        //  These features might be a trimmed / thinned version of features. And may be filtered. But is good to get the features displayed on the map.
    }

    VectorLayerSource.prototype.addFeatures = function (features) {
    }

    VectorLayerSource.prototype.updateFeatures = function (features) {
    }

    VectorLayerSource.prototype.deleteFeatures = function (features) {
    }

    VectorLayerSource.prototype.setFeatures = function (features) {
        // Set the feature content of this layer. Deprecated. Make use of the add / update / delete Features methods instead.
    }

    VectorLayerSource.prototype.commit = function (callback) {
        // callback (result);, where result.success exists
    }

    VectorLayerSource.prototype.rollback = function () {
    }

    ns.VectorLayerSource = VectorLayerSource;

})(Mapzania);

//
// VectorLayerSourceClientFeatures
//

(function (ns) {
    function VectorLayerSourceClientFeatures(layerSourceDef, source) {
        $.extend(this, layerSourceDef);
        this.source = source;
        this.gotFeatures = new ns.Event(); // An event to indicate that new entire layer data was received and map needs to be updated accordingly

        //
        // A dictionary, with feature ids as the keys and the following objects as the values: {action:"A/U/D", feature: feature}
        //
        this.clientFeatures = {};

        //
        // The last combined feature list we passed on to the map
        //
        this.lastCombinedFeatureList = [];

        //
        // Perform the pass-through of features retrieved
        //
        var me = this;
        this.source.gotFeatures.add(function (e) {
            if (e.data.seqNo == 0) {
                // For this vector layer source, we assume that we will only get getFeatures server sources (i.e. we will receive features in seqNo 0)
                me.lastCombinedFeatureList = me._getCombinedFeatureList(e.data.features);
                me.gotFeatures.fire(me, { seqNo: 0, features: me.lastCombinedFeatureList, isFinal: e.data.isFinal });
            } else {
                me.gotFeatures.fire(me, e.data);
            }
        });
    }

    VectorLayerSourceClientFeatures.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams, fence) {
        this.source.getFeatures(filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams, fence);
    }

    VectorLayerSourceClientFeatures.prototype.getCurrentFeatures = function () {
        return this.lastCombinedFeatureList;
    }

    VectorLayerSourceClientFeatures.prototype.addFeatures = function (features) {
        if (features) {
            var me = this;
            features.forEach (function (cur) {
                if (cur.id) {
                    if (me.clientFeatures[cur.id]) {
                        console.log("Feature id already exist. The current feature is replaced (in the add operation).", "VectorLayerSourceClientFeatures", cur);
                    }
                    me.clientFeatures[cur.id] = { action: "A", feature: cur };
                } else {
                    console.log("Feature has no id. Feature was not added.", "VectorLayerSourceClientFeatures", cur);
                }
            });
        }
    }

    VectorLayerSourceClientFeatures.prototype.updateFeatures = function (features) {
        if (features) {
            var me = this;
            features.forEach(function (cur) {
                if (cur.id) {
                    if (me.clientFeatures[cur.id]) {
                        var curAction = me.clientFeatures[cur.id].action;
                        if (curAction == "A") {
                            // Replace the previously added feature
                            me.clientFeatures[cur.id] = { action: "A", feature: cur };
                        } else {
                            // Previously, this feature was updated or deleted. We now update the feature
                            me.clientFeatures[cur.id] = { action: "U", feature: cur };
                        }
                    } else {
                        me.clientFeatures[cur.id] = { action: "U", feature: cur };
                    }
                } else {
                    console.error("Feature has no id. Feature was not added.", "VectorLayerSourceClientFeatures", cur);
                }
            });
        }
    }

    VectorLayerSourceClientFeatures.prototype.deleteFeatures = function (features) {
        if (features) {
            var me = this;
            features.forEach(function (cur) {
                if (cur.id) {
                    me.clientFeatures[cur.id] = { action: "D", feature: cur };
                } else {
                    console.log("Feature has no id. Feature was not added.", "VectorLayerSourceClientFeatures", cur);
                }
            });
        }
    }

    VectorLayerSourceClientFeatures.prototype.setFeatures = function (features) {
        this.source.setFeatures(features);
    }

    VectorLayerSourceClientFeatures.prototype.commit = function (callback) {
        var listAdded = [];
        var listUpdated = [];
        var listDeleted = [];

        this.source.rollback();

        for (var cur in this.clientFeatures) {
            var obj = this.clientFeatures[cur];
            switch (obj.action) {
                case "A":
                    listAdded.push(obj.feature);
                    break;
                case "U":
                    listUpdated.push(obj.feature);
                    break;
                case "D":
                    listDeleted.push(obj.feature);
                    break;
            }
        }
        this.source.addFeatures(listAdded);
        this.source.updateFeatures(listUpdated);
        this.source.deleteFeatures(listDeleted);

        var me = this;
        var funcCallback = function (result) {
            if (result.success) {
                me.clientFeatutes = {};
            }

            if (callback) {
                callback(result);
            }
        };

        this.source.commit(funcCallback);
    }

    VectorLayerSourceClientFeatures.prototype.rollback = function () {
        this.clientFeatures = {};
        this.lastCombinedFeatureList = this.source.getCurrentFeatures();
        this.source.rollback();
    }

    VectorLayerSourceClientFeatures.prototype._getCombinedFeatureList = function (externalFeatures) {
        var me = this;
        var result = [];
        if (externalFeatures) {
            externalFeatures.forEach(function (cur) {
                if (cur.id) {
                    var x = me.clientFeatures[cur.id];
                    if (!x) {
                        // The feature is not affected by client side features so we add it to to the result list
                        result.push(cur);
                    }
                } else {
                    result.push(cur);
                }
            });
        }

        for (var cur in this.clientFeatures) {
            var obj = this.clientFeatures[cur];
            if (obj.action == "A" || obj.action == "U") {
                result.push(obj.feature);
            }
        }

        return result;
    }

    ns.VectorLayerSourceClientFeatures = VectorLayerSourceClientFeatures;

})(Mapzania);

//
// VectorLayerSourceCache: A very simple cache that only caters for the case where features are retrieved consecutively, using the same parameters. Usefull when
//  a layer is hidden and then displayed after that while the map was not panned or zoomed. 
//

(function (ns) {
    function VectorLayerSourceCache(source) {
        this.source = source;

        this.gotFeatures = new ns.Event(); // An event to indicate that new entire layer data was received and map needs to be updated

        this.cacheData = null; // A list of Features
        this.cacheKey = null;

        //
        // Perform the pass-through of features retrieved
        //
        var me = this;
        this.source.gotFeatures.add(function (e) {
            me.cacheKey = me.cacheKeyForExpectedData;
            if (e.data.seqNo == 0) {
                me.cacheData = e.data.features;
            } else {
                me.cacheData = me.cacheData.concat(e.data.features);
            }
            me.gotFeatures.fire(me, e.data);
        });
    }

    VectorLayerSourceCache.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams, fence) {
        var obj = {
            filter: filter, extent: extent, fields:fields, thinAlgo:thinAlgo, thinFactor:thinFactor, accuracy:accuracy, labelVisibilityClause:labelVisibilityClause, sourceParams:sourceParams, fence:fence
        };
        //ANDRE: This effectively reloads the layer (if the parameter signature is different)
        var key = JSON.stringify(obj);
        if (key == this.cacheKey) {
            this.gotFeatures.fire(this, { seqNo: 0, features: this.cacheData, isFinal: true });
        } else {
            this.cacheKeyForExpectedData = key;
            this.source.getFeatures(filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams, fence);
        }
    }

    VectorLayerSourceCache.prototype.getCurrentFeatures = function () {
        return this.cacheData ? this.cacheData : [];
    }

    VectorLayerSourceCache.prototype.addFeatures = function (features) {
        this.source.addFeatures(features);
    }

    VectorLayerSourceCache.prototype.updateFeatures = function (features) {
        this.source.updateFeatures(features);
    }

    VectorLayerSourceCache.prototype.deleteFeatures = function (features) {
        this.source.deleteFeatures(features);
    }

    VectorLayerSourceCache.prototype.setFeatures = function (features) {
        this.source.setFeatures(features);
        this.cacheData = null;
        this.cacheKey = null;
    }

    VectorLayerSourceCache.prototype.commit = function (callback) {
        var me = this;
        var funcCallback = function (result) {
            if (result.success) {
                me.cacheKey = null;
                me.cacheData = null;
            }

            if (callback) {
                callback(result);
            }
        };

        this.source.commit(funcCallback);
    }

    VectorLayerSourceCache.prototype.rollback = function () {
        this.source.rollback();
    }

    ns.VectorLayerSourceCache = VectorLayerSourceCache;

})(Mapzania);

//
// VectorLayerSourceCompleteCache: Completely caches data on the client side during the initial load. 
//

(function (ns) {
    function VectorLayerSourceCompleteCache(source) {
        this.source = source;

        this.gotFeatures = new ns.Event(); // An event to indicate that new entire layer data was received and map needs to be updated

        this.cacheData = null; // List of Feature objects

        this.lastExtent = null;

        //
        // Only incude the features that are contained within the extent required for display
        //
        var me = this;
        this.source.gotFeatures.add(function (e) {
            if (e.data.seqNo == 0) {
                me.cacheData = e.data.features;
            } else {
                me.cacheData = me.cacheData.concat(e.data.features);
            }
            
            var result = me._filterFeatures(me.cacheData, me.lastExtent);
            me.gotFeatures.fire(me, { seqNo: 0, features: result , isFinal: true});
        });
    }

    VectorLayerSourceCompleteCache.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams) {
        if (this.cacheData) {
            this.gotFeatures.fire(this, { seqNo: 0, features: this._filterFeatures(this.cacheData, extent), isFinal: true });
        } else {
            this.lastExtent = extent;
            this.source.getFeatures(filter, null /*extent*/, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams);
        }
    }

    VectorLayerSourceCompleteCache.prototype._filterFeatures = function (data, extent) {
        var result = null;
        if (extent) {
            result = [];
            data.forEach(function (cur) {
                if (cur.geometry && cur.geometry.getExtent().intersects(extent)) {
                    result.push(cur);
                }
            });
        } else {
            result = data;
        }

        return result;
    }

    VectorLayerSourceCompleteCache.prototype.getCurrentFeatures = function () {
        return this.cacheData ? this.cacheData : [];
    }

    VectorLayerSourceCompleteCache.prototype.addFeatures = function (features) {
        throw "Not implemented";
    }

    VectorLayerSourceCompleteCache.prototype.updateFeatures = function (features) {
        throw "Not implemented";
    }

    VectorLayerSourceCompleteCache.prototype.deleteFeatures = function (features) {
        throw "Not implemented";
    }

    VectorLayerSourceCompleteCache.prototype.setFeatures = function (features) {
        throw "Not implemented";
    }

    VectorLayerSourceCompleteCache.prototype.commit = function (callback) {
        throw "Not implemented";
    }

    VectorLayerSourceCompleteCache.prototype.rollback = function () {
        throw "Not implemented";
    }

    ns.VectorLayerSourceCompleteCache = VectorLayerSourceCompleteCache;

})(Mapzania);

//
// VectorLayerSourceServer
//

(function (ns) {
    function VectorLayerSourceServer(src, api, srid, layer, layerSourceLoadQueue) {
        $.extend(this, src);
        this.api = api;
        this.srid = srid;
        this.layer = layer;
        this.layerSourceLoadQueue = layerSourceLoadQueue;
        
        this.gotFeatures = new ns.Event();

        this._clearInternalList();
    }

    VectorLayerSourceServer.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams, fence) {
        var me = this;
        var callback = function (data) {
            var res = data;
            //var res = [];
            //for (var i = 0; i < data.ids.length; i++) {
            //    var ftr = new ns.Feature();
            //    ftr.read(null, data, i);
            //    res.push(ftr);
            //}

            var exprEval = new Mapzania.FieldExpressionEval(me.layer);
            exprEval.populateCalculatedAttributes(res);

            me.gotFeatures.fire(me, { seqNo: 0, features: res, isFinal: true });
        };

        this.layerSourceLoadQueue.getFeatures(this.layer, this.key, filter, extent, fields, thinAlgo, thinFactor, accuracy, this.srid, labelVisibilityClause, sourceParams, callback, true, null, null, null,  null, null, null, fence);
    }

    VectorLayerSourceServer.prototype.getCurrentFeatures = function () {
        throw "Not implemented";
    }

    VectorLayerSourceServer.prototype.addFeatures = function (features) {
        if (features) {
            this.listAdded = this.listAdded.concat(features);
        }
    }

    VectorLayerSourceServer.prototype.updateFeatures = function (features) {
        if (features) {
            this.listUpdated = this.listUpdated.concat(features);
        }
    }

    VectorLayerSourceServer.prototype.deleteFeatures = function (features) {
        if (features) {
            this.listDeleted = this.listDeleted.concat(features);
        }
    }

    VectorLayerSourceServer.prototype.setFeatures = function (features) {
        throw "The layer source type [VectorLayerSourceServer] does not support the [setFeatures] operation. Make use of the add / update / delete Features methods instead.";
    }

    VectorLayerSourceServer.prototype.commit = function (callback) {
        var me = this;
        var compact = {
            srid: this.srid,   // The map display SRID
            attributes: [],
            geometries: [],
            geometryTypes: [],
            ids: [],
            updateActions: []
        };
        
        var funcAddToList = function (features, operation) {
            features.forEach(function (cur) {
                compact.ids.push(cur.id);
                compact.updateActions.push(operation);
                if (cur.geometry) {
                    compact.geometryTypes.push(cur.geometry.getGeometryType());
                    compact.geometries.push(cur.geometry.getPath());
                } else {
                    compact.geometryTypes.push(null);
                    compact.geometries.push(null);
                }
                var attributeList = [];
                for (var curName in cur.attributes) {
                    attributeList.push({ name: curName, value: cur.attributes[curName] });
                }
                compact.attributes.push(attributeList);
            });
        };

        funcAddToList(this.listAdded, "A");
        funcAddToList(this.listUpdated, "U");
        funcAddToList(this.listDeleted, "D");

        var funcCallback = function (result) {
            if (result.success) {
                me._clearInternalList();
            }
            
            if (callback)
                callback(result);
        };

        this.api.commitUpdates(
            {
                sourceKey : this.key, 
                updateFeatureWrapperList: compact
            },
            funcCallback);
    }

    VectorLayerSourceServer.prototype.rollback = function () {
        this._clearInternalList();
    }

    VectorLayerSourceServer.prototype._clearInternalList = function () {
        this.listAdded = [];
        this.listUpdated = [];
        this.listDeleted = [];
    }

    ns.VectorLayerSourceServer = VectorLayerSourceServer;

})(Mapzania);

//
// VectorLayerSourceServerVectorServer
//

(function (ns) {
    function VectorLayerSourceServerVectorServer(src, api, srid, layer, layerSourceLoadQueue) {
        $.extend(this, src);
        this.api = api;
        this.srid = srid;
        this.layer = layer;
        this.layerSourceLoadQueue = layerSourceLoadQueue;

        this.gotFeatures = new ns.Event();
    }

    VectorLayerSourceServerVectorServer.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams) {
        var me = this;
        var callback = function (data) {
            res = data;
            //var res = [];
            //var attributeNames = data[0];
            //var featureData = data[1];
            //for (var i = 0; i < featureData.length; i++) {
            //    var ftr = new ns.Feature();
            //    ftr.readFromVectorServer(attributeNames, featureData[i]);
            //    res.push(ftr);
            //}

            var exprEval = new Mapzania.FieldExpressionEval(me.layer);
            exprEval.populateCalculatedAttributes(res);

            me.gotFeatures.fire(me, { seqNo: 0, features: res, isFinal: true });
        };

        this.layerSourceLoadQueue.getFeatures(this.layer, this.key, filter, extent, fields, thinAlgo, thinFactor, accuracy, this.srid, labelVisibilityClause, sourceParams, callback, true, this.layer.vectorServerName);
    }

    VectorLayerSourceServerVectorServer.prototype.getCurrentFeatures = function () {
        throw "Not implemented";
    }

    VectorLayerSourceServerVectorServer.prototype.addFeatures = function (features) {
        throw "The layer source type [VectorLayerSourceServerVectorServer] does not support the [addFeatures] operation.";
    }

    VectorLayerSourceServerVectorServer.prototype.updateFeatures = function (features) {
        throw "The layer source type [VectorLayerSourceServerVectorServer] does not support the [updateFeatures] operation.";
    }

    VectorLayerSourceServerVectorServer.prototype.deleteFeatures = function (features) {
        throw "The layer source type [VectorLayerSourceServerVectorServer] does not support the [deleteFeatures] operation.";
    }

    VectorLayerSourceServerVectorServer.prototype.setFeatures = function (features) {
        throw "The layer source type [VectorLayerSourceServerVectorServer] does not support the [setFeatures] operation.";
    }

    VectorLayerSourceServerVectorServer.prototype.commit = function (callback) {
        throw "The layer source type [VectorLayerSourceServerVectorServer] does not support the [commit] operation.";
    }

    VectorLayerSourceServerVectorServer.prototype.rollback = function () {
        throw "The layer source type [VectorLayerSourceServerVectorServer] does not support the [rollback] operation.";
    }

    ns.VectorLayerSourceServerVectorServer = VectorLayerSourceServerVectorServer;

})(Mapzania);


//<ANDRE>
// A new type of layersource for completely local datasets. Has no relationship to server or server-configured sources
//</ANDRE>
(function (ns) {
    function LocalVectorLayerSource(layerKey, store) {
        this.gotFeatures = new ns.Event(); // An event to indicate that new entire layer data was received and map needs to be updated
        this.store = store;
        this.layerKey = layerKey;
        store.set(layerKey, [], true);
    }

    LocalVectorLayerSource.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams) {
        //TODO: Implement filters
        var res = this.store.get(this.layerKey);
        this.gotFeatures.fire(this, { seqNo: 0, features: res, isFinal: true });
    }

    LocalVectorLayerSource.prototype.getCurrentFeatures = function () {
        //TODO Implement filters
        var res = this.store.get(this.layerKey);
        this.gotFeatures.fire(this, { seqNo: 0, features: res, isFinal: true });
    }

    LocalVectorLayerSource.prototype.addFeatures = function (features) {
        throw "Not implemented";
    }

    LocalVectorLayerSource.prototype.updateFeatures = function (features) {
        throw "Not implemented";
    }

    LocalVectorLayerSource.prototype.deleteFeatures = function (features) {
        throw "Not implemented";
    }

    LocalVectorLayerSource.prototype.setFeatures = function (features) {
        this.store.set(this.layerKey, features, true);
        //this.gotFeatures.fire(this, { seqNo: 0, features: features, isFinal: true });
    }

    LocalVectorLayerSource.prototype.commit = function (callback) {
        throw "Not implemented";
    }

    LocalVectorLayerSource.prototype.rollback = function () {
        throw "Not implemented";
    }

    ns.LocalVectorLayerSource = LocalVectorLayerSource;

})(Mapzania);

if (!Mapzania)
	var Mapzania = {};

(function(ns) {

	function VectorLayerSourceSchema(src, api, srid, layer) {
		$.extend(this, src);
		this.api = api;
		this.srid = srid;
		this.layer = layer;

        // Set the list of committed features for this layer
		this.features = [];

        // List of uncommitted features
		this.listAdded = [];
		this.listUpdated = [];
		this.listDeleted = [];

		this.gotFeatures = new ns.Event();
	}

	VectorLayerSourceSchema.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams) {
	    // TODO : It could be that features on this layer is not in the display projection of the map. If this is the case, we should make a round-trip to the 
	    //  server to make this projection and the only then return the features. But this will typically only happen when a map schema was saved and reloaded
        //  as a different projection
	    this.gotFeatures.fire(this, { seqNo: 0, features: this.features, isFinal: true });
	}

	VectorLayerSourceSchema.prototype.getCurrentFeatures = function () {
	    throw "Not implemented";
	}

	VectorLayerSourceSchema.prototype.addFeatures = function (features) {
	    if (features) {
	        this.listAdded = this.listAdded.concat(features);
	    }
	}

	VectorLayerSourceSchema.prototype.updateFeatures = function (features) {
	    if (features) {
	        this.listUpdated = this.listUpdated.concat(features);
	    }
	}

	VectorLayerSourceSchema.prototype.deleteFeatures = function (features) {
	    if (features) {
	        this.listDeleted = this.listDeleted.concat(features);
	    }
	}

	VectorLayerSourceSchema.prototype.setFeatures = function (features) {
	    this.features = (features ? features : []);
	    this.layer.filter = JSON.stringify(this._toCompactFeatureList());
	}

	VectorLayerSourceSchema.prototype.commit = function (callback) {
	    // callback (result);, where result.success exists
	    var me = this;

        // Added features
	    this.listAdded.forEach(function (cur) {
	        var i = me._getCommittedFeatureById(cur.id);
	        if (i >= 0) {
	            me.features[i] = cur;
	        } else {
	            me.features.push(cur);
	        }
	    });

	    // Updated features
	    this.listUpdated.forEach(function (cur) {
	        var i = me._getCommittedFeatureById(cur.id);
	        if (i >= 0) {
	            me.features[i] = cur;
	        } 
	    });

	    // Deleted features
	    this.listDeleted.forEach(function (cur) {
	        var i = me._getCommittedFeatureById(cur.id);
	        if (i >= 0) {
	            me.features.splice(i, 1);
	        }
	    });

	    this.listAdded = [];
	    this.listUpdated = [];
	    this.listDeleted = [];
	    this.setFeatures(this.features);

	    if (callback)
	        callback({success: true});
	}

	VectorLayerSourceSchema.prototype.rollback = function () {
	    this.listAdded = [];
	    this.listUpdated = [];
	    this.listDeleted = [];
	}

	VectorLayerSourceSchema.prototype._toCompactFeatureList = function () {
	    var compact = {
            srid : this.srid,   // The map display SRID
	        attributes: [],
	        geometries: [],
	        ids: [],
	        dynamicProperties: [],
	        attributeNames: []
	    };

	    if (this.features.length > 0) {
	        for (var curAttribute in this.features[0].attributes) {
	            compact.attributeNames.push(curAttribute);
	        }
	    }

	    this.features.forEach(function (curFeature) {
	        var attributes = [];
	        for (var curAttribute in curFeature.attributes) {
	            attributes.push(curFeature.attributes[curAttribute]);
	        }
	        compact.attributes.push(attributes);
	        compact.ids.push(curFeature.id);
	        compact.dynamicProperties.push(curFeature.dynamicProperties.labelIsVisible ? 1 : 0);
	        compact.geometries.push(curFeature.geometry.getPath());
	    });

	    return compact;
	}

	VectorLayerSourceSchema.prototype._getCommittedFeatureById = function (id) {
	    for (var i = 0; i < this.features.length; i++) {
	        if (this.features[i].id == id) {
	            return i;
	        }
	    }
	    return -1;
	}

	ns.VectorLayerSourceSchema = VectorLayerSourceSchema;

})(Mapzania);

//
// VTLG = Vector Tile Layer Group
//

(function (ns) {
    function VTLGManager(model) {
        this._model = model;
        this._loaders = {};

        var me = this;
        this._model.schemaLoaded.add(function () {
            // We will discard the existing loaders once the map schema was set or updated to another schema.
            me._loaders = {};
        });
    }

    VTLGManager.prototype.getLoader = function (layerGroupName) {
        if (!this._loaders[layerGroupName]) {
            var lgl = new Mapzania.VTLGLoader(this, layerGroupName);
            this._loaders[layerGroupName] = lgl;
        }

        return this._loaders[layerGroupName];
    }

    VTLGManager.prototype.getModel = function () {
        return this._model;
    }

    ns.VTLGManager = VTLGManager;

})(Mapzania);

//
// VTLGLoader: A loader for a layer group
//

(function (ns) {
    function VTLGLoader(manager, layerGroupName) {
        this._manager = manager;
        this._layerGroupName = layerGroupName;
        this._currentRequests = {};
    }

    VTLGLoader.prototype.getTiles = function (callback, vectorServerName, mapKey, srid, z, tileXYExtent) {
        var me = this;
        var tileKey = function (z, x, y) {
            return "" + z + "-" + x + "-" + y;
        };

        var callbackP = function (tile) {
            var modelLayerGroup = new ns.VTLGModelLayerGroup(tile);
            var key = tileKey(tile.z, tile.x, tile.y);
            if (me._currentRequests[key]) {
                me._currentRequests[key].forEach(function (curCallback) {
                    curCallback(modelLayerGroup);
                });

                delete me._currentRequests[key];
            }
        };

        for (var x = tileXYExtent.minX; x <= tileXYExtent.maxX; x++) {
            for (var y = tileXYExtent.minY; y <= tileXYExtent.maxY; y++) {

                var key = tileKey(z, x, y);
                var doRequest = true;
                if (this._currentRequests[key]) {
                    this._currentRequests[key].push(callback);
                    doRequest = false;
                } else {
                    this._currentRequests[key] = [callback];
                }

                if (doRequest) {
                    this._manager.getModel().api.getVectorTile(
                        callbackP,
                        mapKey,
                        srid,
                        this._layerGroupName,
                        z,
                        x,
                        y);
                }
            }
        }
    }

    ns.VTLGLoader = VTLGLoader;

})(Mapzania);

//
//
//

(function (ns) {
    function VTLGZoomLevel(z, model) {
        this.z = z;
        this.size = -1;
        for (var i = 0; i < model.unifiedZoomSizes.length; i++) {
            var cur = model.unifiedZoomSizes[i];
            if (z == cur.zoom)  {
                this.size = cur.size / 4;
                break;
            }
        }
        if (this.size == -1) throw "Unable to find zoom level in map schema: " + z;

        this.bottomLeftReferencePoint = new Mapzania.Point(-100000000, -100000000);
    }

    VTLGZoomLevel.prototype.getTileAtCoordinate = function (coord) {
        var relativeCoord = coord.clone();
        relativeCoord.translate(-this.bottomLeftReferencePoint.x, -this.bottomLeftReferencePoint.y);
        var x = parseInt(relativeCoord.x / this.size);
        var y = parseInt(relativeCoord.y / this.size);
        return {z:this.z, x:x, y:y};
    }

    ns.VTLGZoomLevel = VTLGZoomLevel;

})(Mapzania);

//
// VTLGLayerSource : Layer source
//

(function (ns) {
    function VTLGLayerSource(src, api, srid, layer, layerSourceLoadQueue, vtlgManager, mapKey) {
        $.extend(this, src);
        this.api = api;
        this.srid = srid;
        this.layer = layer;
        this.layerSourceLoadQueue = layerSourceLoadQueue;
        this.vtlgManager = vtlgManager;
        this.mapKey = mapKey;

        this.gotFeatures = new ns.Event(); // An event to indicate that an entire layer data was received and map needs to be updated
    }

    VTLGLayerSource.prototype.getFeatures = function (filter, extent, fields, thinAlgo, thinFactor, accuracy, labelVisibilityClause, sourceParams) {
        var me = this;

        // TODO
        //var callback = function (data) {
            //var exprEval = new Mapzania.FieldExpressionEval(me.layer);
            //exprEval.populateCalculatedAttributes(res);
        //    me.gotFeatures.fire(me, res); // TODO : sequencing
        //};

        var zoom = null;
        sourceParams.forEach(function (cur) {
            if (cur.name == "ZoomSize") {
                zoom = cur.value;
            }
        });

        var zl = new ns.VTLGZoomLevel(zoom, this.vtlgManager.getModel());
        var tileMin = zl.getTileAtCoordinate(new Mapzania.Point(extent.minX, extent.minY));
        var tileMax = zl.getTileAtCoordinate(new Mapzania.Point(extent.maxX, extent.maxY));
        var tileXYExtent = new ns.Extent({
            minX: tileMin.x, 
            minY: tileMin.y,
            maxX: tileMax.x,
            maxY: tileMax.y
        });

        this.features = {};
        this.totalTileCount = (tileXYExtent.width() + 1) * (tileXYExtent.height() + 1);
        this.tilesReceivedCount = 0
        this.gotFeaturesSeqNo = 0;

        var tileKey = function (z, x, y) {
            return "" + z + "-" + x + "-" + y;
        };

        var layerGroupName = this.layer.vectorLayerGroupName;
        var loader = this.vtlgManager.getLoader(layerGroupName);

        var tileCallback = function (modelLayerGroup) {
            me.tilesReceivedCount++;
            var isLastTile = (me.tilesReceivedCount == me.totalTileCount);
            setTimeout(function (isLastTile) {
                me.tileReceived(modelLayerGroup, isLastTile);
            }, 0, isLastTile);
        };

        loader.getTiles(
            tileCallback,
            this.layer.vectorServerName,
            this.mapKey,
            this.srid,
            zoom,
            tileXYExtent);
    }

    VTLGLayerSource.prototype.tileReceived = function (modelLayerGroup, isLastTile) {
        var modelSingleLayer = modelLayerGroup.getModelSingleLayer(this.layer.key);

        var appendParts = function (wrapper, parts) {
            parts.forEach(function (cur) {
                if (!wrapper.parts[cur.d[0]]) {
                    wrapper.parts[cur.d[0]] = [];
                }
                wrapper.parts[cur.d[0]].push(cur);
            });
            wrapper.partsReceived += parts.length;
        };

        for (var curId in modelSingleLayer.data.fs) {
            var wrapper = this.features[curId];
            if (wrapper) {
                appendParts(wrapper, modelSingleLayer.data.fs[curId].p);
            } else {
                var f = this.buildInitialFeature(modelLayerGroup, modelSingleLayer, curId, modelSingleLayer.data.fs[curId]);
                wrapper = {
                    feature: f,
                    parts: {}, // Key = MultiPart Seq No; Value = [] (list if parts)
                    totalParts: modelSingleLayer.data.fs[curId].tp,
                    partsReceived: 0
                };
                appendParts(wrapper, modelSingleLayer.data.fs[curId].p);
                this.features[curId] = wrapper;
            }
        }
        
        if (isLastTile) {
            var list = this.buildFinalFeatureList();
            this.gotFeatures.fire(this, { seqNo: this.gotFeaturesSeqNo, features: list, isFinal: isLastTile });
            this.gotFeaturesSeqNo++;

        }
    }

    VTLGLayerSource.prototype.buildFinalFeatureList = function () {
        var result = [];
        var stitcher = new ns.VTLGStitcher();
        for (var id in this.features) {
            var wrapper = this.features[id];
                stitcher.stitch(wrapper.feature, wrapper.parts);
                result.push(wrapper.feature);
        }

        this.features = {}; // We wont be needing this data anymore. Clear this so that the memory could be released

        return result;
    }

    VTLGLayerSource.prototype.buildInitialFeature = function (modelLayerGroup, modelSingleLayer, id, modelFeature) {
        var feature = new ns.Feature();
        feature.id = id;
        for (var i = 0; i < modelSingleLayer.data.an.length; i++) {
            feature.attributes[modelSingleLayer.data.an[i]] = modelLayerGroup.tile.avl[ modelFeature.v[i] ];
        }
        feature.dynamicProperties.labelIsVisible = modelFeature.l;
        switch (modelFeature.t) {
            case 1: feature.geometry = new ns.MultiPoint();
                break;
            case 2: feature.geometry = new ns.MultiLine();
                break;
            case 3: feature.geometry = new ns.MultiPolygon();
                break;
            case 4: feature.geometry = new ns.MultiPoint();
                break;
            case 5: feature.geometry = new ns.MultiLine();
                break;
            case 6: feature.geometry = new ns.MultiPolygon();
                break;
        }
        return feature;
    }

    VTLGLayerSource.prototype.getCurrentFeatures = function () {
        throw "Not implemented";
    }

    VTLGLayerSource.prototype.addFeatures = function (features) {
        throw "Not implemented";
    }

    VTLGLayerSource.prototype.updateFeatures = function (features) {
        throw "Not implemented";
    }

    VTLGLayerSource.prototype.deleteFeatures = function (features) {
        throw "Not implemented";
    }

    VTLGLayerSource.prototype.setFeatures = function (features) {
        throw "Not implemented";
    }

    VTLGLayerSource.prototype.commit = function (callback) {
        throw "Not implemented";
    }

    VTLGLayerSource.prototype.rollback = function () {
        throw "Not implemented";
    }

    ns.VTLGLayerSource = VTLGLayerSource;

})(Mapzania);

//
//
//

(function (ns) {
    function VTLGModelLayerGroup(tile) {
        this.tile = tile; // A shared tile. 
        this.singleLayers = {};
    }

    VTLGModelLayerGroup.prototype.getModelSingleLayer = function (layerKey) {
        var x = this.singleLayers[layerKey];
        if (!x) {
            x = new ns.VTLGModelSingleLayer(this, layerKey);
            this.singleLayers[layerKey] = x;
        }
        return x;
    }

    ns.VTLGModelLayerGroup = VTLGModelLayerGroup;

})(Mapzania);

//
//
//

(function (ns) {
    function VTLGModelSingleLayer(modelLayerGroup, layerKey) {
        this.modelLayerGroup = modelLayerGroup;
        this.data = {};
        $.extend(true, this.data, modelLayerGroup.tile.l[layerKey]);
        this.scaleAndTranslate();
    }

    VTLGModelSingleLayer.prototype.scaleAndTranslate = function () {
        var me = this;
        for (var curId in this.data.fs) {
            var obj = this.data.fs[curId];
            obj.p.forEach(function (cur) {
                var points = cur.d[3];
                for (var i = 0; i < points.length - 1;) {
                    points[i] = (points[i] + me.modelLayerGroup.tile.tx) / me.modelLayerGroup.tile.s;
                    points[i+1] = (points[i+1] + me.modelLayerGroup.tile.ty) / me.modelLayerGroup.tile.s;
                    i += 2;
                }
            });
        }
    }

    ns.VTLGModelSingleLayer = VTLGModelSingleLayer;

})(Mapzania);


//
//
//

(function (ns) {
    function VTLGStitcher() {

    }

    VTLGStitcher.prototype.stitch = function (initialFeature, parts) {
        //
        // Build the geometry
        //
        if (initialFeature.geometry instanceof Mapzania.MultiPoint) {
            this.stitchPoint(initialFeature, parts);
        }
        else
        if (initialFeature.geometry instanceof Mapzania.MultiLine) {
            //
            // Sort the parts: We need to do this for Line features only
            //
            for (var curKey in parts) {
                parts[curKey].sort(function (x, y) {
                    if (x.d[1] < y.d[1]) return -1; // Part Seq No
                    if (x.d[1] > y.d[1]) return +1;
                    console.log("NB: Unexpected condition (duplicate geometry part number).");
                    return 0;
                });
            }

            this.stitchLine(initialFeature, parts);
        }
        else
        if (initialFeature.geometry instanceof Mapzania.MultiPolygon) {
            this.stitchPolygon(initialFeature, parts);
        }
        else {
            throw "Not implemented, yet.";
        }
    }

    VTLGStitcher.prototype.stitchPoint = function (initialFeature, parts) {
        for (var curKey in parts) {
            var partsList = parts[curKey];

            partsList.forEach(function (cur) {
                initialFeature.geometry.points.push(new Mapzania.Point(cur.d[3][0], cur.d[3][1]));
            });
        }
    }
    
    VTLGStitcher.prototype.stitchLine = function (initialFeature, parts) {
        for (var curKey in parts) { // For each multi part geometry
            var partsList = parts[curKey];
            var curPath = [];
            var lastPartSeqNo = -9;
            
            partsList.forEach(function (cur) {
                var curPartSeqNo = cur.d[1];
                var curPartSeqNoCount = cur.d[2];
                var curPoints = cur.d[3];

                if (lastPartSeqNo + 1 != curPartSeqNo) {
                    //
                    // A new non-sequential part, or the first part of a line 
                    //
                    if (curPath.length > 0) {
                        // We had a previous part, since we have points. Bank the current path and start a new path
                        var line = new Mapzania.Line();
                        line.read([[curPath]]);
                        initialFeature.geometry.lines.push(line);
                    }

                    curPath = curPoints;
                } else {
                    curPath = curPath.concat(curPoints);
                }

                lastPartSeqNo = curPartSeqNo;
            });

            if (curPath.length > 0) {
                var line = new Mapzania.Line();
                line.read([[curPath]]);
                initialFeature.geometry.lines.push(line);
            }
        }
    },

    VTLGStitcher.prototype.stitchPolygon = function (initialFeature, parts) {
        for (var curKey in parts) { // For each multi part geometry
            var partsList = parts[curKey];
            var points = {};

            partsList.forEach(function (cur) {
                var curPartSeqNo = cur.d[1];
                var curPartSeqNoCount = cur.d[2];
                var curPoints = cur.d[3];
                points[curPartSeqNo] = curPoints;
            });

            var polygon = new Mapzania.Polygon();
            polygon.shell.read([[points[0]]]);
            // TODO: Holes
            initialFeature.geometry.polygons.push(polygon);
        }
    }

    ns.VTLGStitcher = VTLGStitcher;

})(Mapzania);

if (!Mapzania)
    var Mapzania = {};

/**
 * @doc Mapzania.Viewport.*ClassDescription*
 *
 * A class responsible for coordinating the web user interface and the Mapper (also known as the drawer, such as Canvas or Raphael).
 * Applications typically instantiate this class once, and does not make direct use of any functionality provided in this class.
 */
(function (ns) {

    var ts = Mapzania;

    /**
      * @doc Mapzania.Viewport.Viewport
      *
      * Constructor.
      *
      * @param elId (String) A CSS selector for the element (typically a DIV) in which the map User Interface will be inject. 
      *                      An example value for this parameter is "#map", where the map containing DIV has an id of "map".
      *
      * @param mapper (Mapzania.Mapper) The map renderer instance to use. Valid class instances are Canvas.Mapper, Raphael.Mapper or Flash.Mapper.
      */
    function Viewport(elId, mapper) {

        this.mapper = mapper;
        this.elId = elId;
        this.$el = $(elId);

        this.mouseDown = new ts.Event();
        this.mouseUp = new ts.Event();
        this.mouseMove = new ts.Event();
        this.doubleClick = new ts.Event();
        this.mouseWheelUp = new ts.Event();
        this.mouseWheelDown = new ts.Event();
        this.resized = new ns.Event();

        this.buffer = 2;
        this.scale = 2;

        this.rotation = {
            originX: 0,
            originY: 0,
            rotateZ: 0
        };
    }

    /**
      * @doc Mapzania.Viewport.init
      *
      * Initializes the viewport by injecting the map HTML UI component(s) in the web application.
      *
      * @param callback (Function) A callback function to call when the operation completed.
      */
    Viewport.prototype.init = function (callback) {
        $el = this.$el;
        $el.css("overflow", "hidden");

        $el.html("<div></div><div class = 'ts-loading-progress'></div><div></div>");

        this.$waitEl = $(this.elId + " div:last-child");
        this.$waitEl.addClass("ts-viewport-loading");

        this.$progressLoading = $(this.elId + " div:nth-child(2)");

        this.$mapEl = $(this.elId + " div:first-child");
        this.$mapEl.height(this.scale * $el.height());
        this.$mapEl.width(this.scale * $el.width());
        this.$mapEl.css("position", "absolute");

        var w = this.$mapEl.width();
        var h = this.$mapEl.height();
        var left = -(w - w / this.buffer) / 2;
        var top = -(h - h / this.buffer) / 2;
        this.startPos = { left: left, top: top };
        this.setPos(this.startPos);

        var me = this;
        function calcOffset(event) {
            var w = me.$mapEl.width();
            var h = me.$mapEl.height();
            var left = -(w - w / me.buffer) / 2;
            var top = -(h - h / me.buffer) / 2;
            var res = {
                x: event.pageX - me.$el.offset().left - left,
                y: event.pageY - me.$el.offset().top - top
            };
            return res;
        }

        $el.mousedown(function (e) {
            var res = calcOffset(e);
            res.raw = { x: e.pageX, y: e.pageY };
            me.mouseDown.fire(me, res);
            $(this).css('cursor', 'default');
            return false;
        });

        $el.mouseup(function (e) {
            var res = calcOffset(e);
            res.raw = { x: e.pageX, y: e.pageY };
            me.mouseUp.fire(me, res);
            $(this).css('cursor', 'default');
            return false;
        });

        $el.mousemove(function (e) {
            me.mouseMove.fire(me, calcOffset(e));
            return false;
        });

        $el.on('DOMMouseScroll mousewheel', function (e) {
            if (e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) { //alternative options for wheelData: wheelDeltaX & wheelDeltaY
                me.mouseWheelDown.fire(me, calcOffset(e));
            } else {
                me.mouseWheelUp.fire(me, calcOffset(e));
            }
            return false; //prevent page fom scrolling
        });

        $el.dblclick(function (e) {
            var res = calcOffset(e);
            res.raw = { x: e.pageX, y: e.pageY };
            me.doubleClick.fire(me, res);
            $(this).css('cursor', 'default');
            return false;
        });

        this.mapper.init(this.$mapEl, callback);

        this._startMonitoringSize();
    }

    /**
      * @doc Mapzania.Viewport.getSize
      *
      * Returns the map viewport size, in pixels.
      *
      * @return (Object) An object in the form {width: 100, height: 100}, where both width and height are values in pixels.
      */
    Viewport.prototype.getSize = function () {
        return {
            height: this.$el.height(),
            width: this.$el.width()
        };
    }

    Viewport.prototype.onResized = function () {
        console.log("Detected that map was resized: "+ JSON.stringify(this.getSize()), "Mapzania.ViewPort");

        this.$mapEl = $(this.elId + " div:first-child");
        this.$mapEl.height(this.scale * $el.height());
        this.$mapEl.width(this.scale * $el.width());
        this.$mapEl.css("position", "absolute");

        var w = this.$mapEl.width();
        var h = this.$mapEl.height();
        var left = -(w - w / this.buffer) / 2;
        var top = -(h - h / this.buffer) / 2;
        this.startPos = { left: left, top: top };
        this.setPos(this.startPos);

        this.mapper.onResized();

        this.resized.fire(this);
    }

    Viewport.prototype._startMonitoringSize = function () {
        var currentSize = this.getSize();
        var me = this;
        var func = function () {
            var s = me.getSize();
            if (s.height != currentSize.height || s.width != currentSize.width) {
                if (s.width != 0 && s.height != 0) {
                    currentSize = s;
                    me.onResized();
                }
            }
            setTimeout(func, 500);
        };
        setTimeout(func, 500);
    }

    Viewport.prototype.getTransformer = function (extent) {
        var size = this.getSize();

        var wr = size.width / Math.abs(extent.maxX - extent.minX);
        var hr = size.height / Math.abs(extent.maxY - extent.minY);

        var ratio = Math.min(wr, hr);

        var scale = ratio * this.scale / this.buffer;

        var x = size.width / ratio;
        var y = size.height / ratio;

        var xoff = ratio != wr ? (size.width / wr - x) / 2 : 0;
        var yoff = ratio != hr ? (size.height / hr - y) / 2 : 0;

        var pointCenter = extent.getCenter();
        var pointCenterRotated = pointCenter.rotate(new Mapzania.Point(this.rotation.originX, this.rotation.originY), this.rotation.rotateZ);

        var params = {
            translateX: (pointCenterRotated.x - extent.width() / 2) + xoff - x * (this.buffer - 1) / 2,
            translateY: (pointCenterRotated.y + extent.height() / 2) - yoff + y * (this.buffer - 1) / 2,
            scaleX: scale,
            scaleY: -scale
        };

        $.extend(params, this.rotation);
        
        return new Mapzania.PixelTransformer(params);
    }

    Viewport.prototype.pan = function (deltaX, deltaY) {
        var diff = { x: deltaX, y: deltaY };
        //console.log("canvas pan");
        var start = this.startPos;

        var pos = this.$mapEl.position();
        this.setPos({
            left: start.left + deltaX,
            top: start.top + deltaY
        });
    }

    Viewport.prototype.reset = function () {
        this.setPos(this.startPos);
    }

    Viewport.prototype.panDone = function () {
        this.setPos(this.startPos);
    }

    Viewport.prototype.setIsLoading = function (isLoading) {
        if (isLoading)
            this.$waitEl.css("display", "block");
        else
            this.$waitEl.css("display", "none");
    }

    Viewport.prototype.setLoadingProgress = function (total, outstanding) {
        if (outstanding == 0 || total == 1) {
            this.$progressLoading.css("display", "none");
            this.$progressLoading.html("");
        } else {
            var perc = (total - outstanding) / total * 100;
            if (perc < 1) perc = 1;
            var html = "<table><tr><td style = 'background-color: black;' width='" + perc + "%'></td><td></td></tr></table>"
            this.$progressLoading.html(html);
            this.$progressLoading.css("display", "block");
        }
    }

    Viewport.prototype.zoomTo = function (extent) {
    }

    Viewport.prototype.setPos = function(pos){
        this.$mapEl.css("left", pos.left + "px");
        this.$mapEl.css("top", pos.top + "px");
    }

    ns.Viewport = Viewport;

})(Mapzania)

!function (e) {
    if ("object" == typeof exports) module.exports = e(); else if ("function" == typeof define && define.amd) define(e); else { var f; "undefined" != typeof window ? f = window : "undefined" != typeof global ? f = global : "undefined" != typeof self && (f = self), f.proj4 = e() }
}(function () {
    var define, module, exports; return (function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); throw new Error("Cannot find module '" + o + "'") } var f = n[o] = { exports: {} }; t[o][0].call(f.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, f, f.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++) s(r[o]); return s })({
        1: [function (_dereq_, module, exports) {
var mgrs = _dereq_('mgrs');

function Point(x, y, z) {
  if (!(this instanceof Point)) {
    return new Point(x, y, z);
  }
  if (Array.isArray(x)) {
    this.x = x[0];
    this.y = x[1];
    this.z = x[2] || 0.0;
  }else if(typeof x === 'object'){
    this.x = x.x;
    this.y = x.y;
    this.z = x.z || 0.0;
  } else if (typeof x === 'string' && typeof y === 'undefined') {
    var coords = x.split(',');
    this.x = parseFloat(coords[0], 10);
    this.y = parseFloat(coords[1], 10);
    this.z = parseFloat(coords[2], 10) || 0.0;
  }
  else {
    this.x = x;
    this.y = y;
    this.z = z || 0.0;
  }
  console.warn('proj4.Point will be removed in version 3, use proj4.toPoint');
}

Point.fromMGRS = function(mgrsStr) {
  return new Point(mgrs.toPoint(mgrsStr));
};
Point.prototype.toMGRS = function(accuracy) {
  return mgrs.forward([this.x, this.y], accuracy);
};
module.exports = Point;
},{"mgrs":67}],2:[function(_dereq_,module,exports){
var parseCode = _dereq_("./parseCode");
var extend = _dereq_('./extend');
var projections = _dereq_('./projections');
var deriveConstants = _dereq_('./deriveConstants');

function Projection(srsCode,callback) {
  if (!(this instanceof Projection)) {
    return new Projection(srsCode);
  }
  callback = callback || function(error){
    if(error){
      throw error;
    }
  };
  var json = parseCode(srsCode);
  if(typeof json !== 'object'){
    callback(srsCode);
    return;
  }
  var modifiedJSON = deriveConstants(json);
  var ourProj = Projection.projections.get(modifiedJSON.projName);
  if(ourProj){
    extend(this, modifiedJSON);
    extend(this, ourProj);
    this.init();
    callback(null, this);
  }else{
    callback(srsCode);
  }
}
Projection.projections = projections;
Projection.projections.start();
module.exports = Projection;

},{"./deriveConstants":33,"./extend":34,"./parseCode":37,"./projections":39}],3:[function(_dereq_,module,exports){
module.exports = function(crs, denorm, point) {
  var xin = point.x,
    yin = point.y,
    zin = point.z || 0.0;
  var v, t, i;
  for (i = 0; i < 3; i++) {
    if (denorm && i === 2 && point.z === undefined) {
      continue;
    }
    if (i === 0) {
      v = xin;
      t = 'x';
    }
    else if (i === 1) {
      v = yin;
      t = 'y';
    }
    else {
      v = zin;
      t = 'z';
    }
    switch (crs.axis[i]) {
    case 'e':
      point[t] = v;
      break;
    case 'w':
      point[t] = -v;
      break;
    case 'n':
      point[t] = v;
      break;
    case 's':
      point[t] = -v;
      break;
    case 'u':
      if (point[t] !== undefined) {
        point.z = v;
      }
      break;
    case 'd':
      if (point[t] !== undefined) {
        point.z = -v;
      }
      break;
    default:
      //console.log("ERROR: unknow axis ("+crs.axis[i]+") - check definition of "+crs.projName);
      return null;
    }
  }
  return point;
};

},{}],4:[function(_dereq_,module,exports){
var HALF_PI = Math.PI/2;
var sign = _dereq_('./sign');

module.exports = function(x) {
  return (Math.abs(x) < HALF_PI) ? x : (x - (sign(x) * Math.PI));
};
},{"./sign":21}],5:[function(_dereq_,module,exports){
var TWO_PI = Math.PI * 2;
// SPI is slightly greater than Math.PI, so values that exceed the -180..180
// degree range by a tiny amount don't get wrapped. This prevents points that
// have drifted from their original location along the 180th meridian (due to
// floating point error) from changing their sign.
var SPI = 3.14159265359;
var sign = _dereq_('./sign');

module.exports = function(x) {
  return (Math.abs(x) <= SPI) ? x : (x - (sign(x) * TWO_PI));
};
},{"./sign":21}],6:[function(_dereq_,module,exports){
module.exports = function(x) {
  if (Math.abs(x) > 1) {
    x = (x > 1) ? 1 : -1;
  }
  return Math.asin(x);
};
},{}],7:[function(_dereq_,module,exports){
module.exports = function(x) {
  return (1 - 0.25 * x * (1 + x / 16 * (3 + 1.25 * x)));
};
},{}],8:[function(_dereq_,module,exports){
module.exports = function(x) {
  return (0.375 * x * (1 + 0.25 * x * (1 + 0.46875 * x)));
};
},{}],9:[function(_dereq_,module,exports){
module.exports = function(x) {
  return (0.05859375 * x * x * (1 + 0.75 * x));
};
},{}],10:[function(_dereq_,module,exports){
module.exports = function(x) {
  return (x * x * x * (35 / 3072));
};
},{}],11:[function(_dereq_,module,exports){
module.exports = function(a, e, sinphi) {
  var temp = e * sinphi;
  return a / Math.sqrt(1 - temp * temp);
};
},{}],12:[function(_dereq_,module,exports){
module.exports = function(ml, e0, e1, e2, e3) {
  var phi;
  var dphi;

  phi = ml / e0;
  for (var i = 0; i < 15; i++) {
    dphi = (ml - (e0 * phi - e1 * Math.sin(2 * phi) + e2 * Math.sin(4 * phi) - e3 * Math.sin(6 * phi))) / (e0 - 2 * e1 * Math.cos(2 * phi) + 4 * e2 * Math.cos(4 * phi) - 6 * e3 * Math.cos(6 * phi));
    phi += dphi;
    if (Math.abs(dphi) <= 0.0000000001) {
      return phi;
    }
  }

  //..reportError("IMLFN-CONV:Latitude failed to converge after 15 iterations");
  return NaN;
};
},{}],13:[function(_dereq_,module,exports){
var HALF_PI = Math.PI/2;

module.exports = function(eccent, q) {
  var temp = 1 - (1 - eccent * eccent) / (2 * eccent) * Math.log((1 - eccent) / (1 + eccent));
  if (Math.abs(Math.abs(q) - temp) < 1.0E-6) {
    if (q < 0) {
      return (-1 * HALF_PI);
    }
    else {
      return HALF_PI;
    }
  }
  //var phi = 0.5* q/(1-eccent*eccent);
  var phi = Math.asin(0.5 * q);
  var dphi;
  var sin_phi;
  var cos_phi;
  var con;
  for (var i = 0; i < 30; i++) {
    sin_phi = Math.sin(phi);
    cos_phi = Math.cos(phi);
    con = eccent * sin_phi;
    dphi = Math.pow(1 - con * con, 2) / (2 * cos_phi) * (q / (1 - eccent * eccent) - sin_phi / (1 - con * con) + 0.5 / eccent * Math.log((1 - con) / (1 + con)));
    phi += dphi;
    if (Math.abs(dphi) <= 0.0000000001) {
      return phi;
    }
  }

  //console.log("IQSFN-CONV:Latitude failed to converge after 30 iterations");
  return NaN;
};
},{}],14:[function(_dereq_,module,exports){
module.exports = function(e0, e1, e2, e3, phi) {
  return (e0 * phi - e1 * Math.sin(2 * phi) + e2 * Math.sin(4 * phi) - e3 * Math.sin(6 * phi));
};
},{}],15:[function(_dereq_,module,exports){
module.exports = function(eccent, sinphi, cosphi) {
  var con = eccent * sinphi;
  return cosphi / (Math.sqrt(1 - con * con));
};
},{}],16:[function(_dereq_,module,exports){
var HALF_PI = Math.PI/2;
module.exports = function(eccent, ts) {
  var eccnth = 0.5 * eccent;
  var con, dphi;
  var phi = HALF_PI - 2 * Math.atan(ts);
  for (var i = 0; i <= 15; i++) {
    con = eccent * Math.sin(phi);
    dphi = HALF_PI - 2 * Math.atan(ts * (Math.pow(((1 - con) / (1 + con)), eccnth))) - phi;
    phi += dphi;
    if (Math.abs(dphi) <= 0.0000000001) {
      return phi;
    }
  }
  //console.log("phi2z has NoConvergence");
  return -9999;
};
},{}],17:[function(_dereq_,module,exports){
var C00 = 1;
var C02 = 0.25;
var C04 = 0.046875;
var C06 = 0.01953125;
var C08 = 0.01068115234375;
var C22 = 0.75;
var C44 = 0.46875;
var C46 = 0.01302083333333333333;
var C48 = 0.00712076822916666666;
var C66 = 0.36458333333333333333;
var C68 = 0.00569661458333333333;
var C88 = 0.3076171875;

module.exports = function(es) {
  var en = [];
  en[0] = C00 - es * (C02 + es * (C04 + es * (C06 + es * C08)));
  en[1] = es * (C22 - es * (C04 + es * (C06 + es * C08)));
  var t = es * es;
  en[2] = t * (C44 - es * (C46 + es * C48));
  t *= es;
  en[3] = t * (C66 - es * C68);
  en[4] = t * es * C88;
  return en;
};
},{}],18:[function(_dereq_,module,exports){
var pj_mlfn = _dereq_("./pj_mlfn");
var EPSLN = 1.0e-10;
var MAX_ITER = 20;
module.exports = function(arg, es, en) {
  var k = 1 / (1 - es);
  var phi = arg;
  for (var i = MAX_ITER; i; --i) { /* rarely goes over 2 iterations */
    var s = Math.sin(phi);
    var t = 1 - es * s * s;
    //t = this.pj_mlfn(phi, s, Math.cos(phi), en) - arg;
    //phi -= t * (t * Math.sqrt(t)) * k;
    t = (pj_mlfn(phi, s, Math.cos(phi), en) - arg) * (t * Math.sqrt(t)) * k;
    phi -= t;
    if (Math.abs(t) < EPSLN) {
      return phi;
    }
  }
  //..reportError("cass:pj_inv_mlfn: Convergence error");
  return phi;
};
},{"./pj_mlfn":19}],19:[function(_dereq_,module,exports){
module.exports = function(phi, sphi, cphi, en) {
  cphi *= sphi;
  sphi *= sphi;
  return (en[0] * phi - cphi * (en[1] + sphi * (en[2] + sphi * (en[3] + sphi * en[4]))));
};
},{}],20:[function(_dereq_,module,exports){
module.exports = function(eccent, sinphi) {
  var con;
  if (eccent > 1.0e-7) {
    con = eccent * sinphi;
    return ((1 - eccent * eccent) * (sinphi / (1 - con * con) - (0.5 / eccent) * Math.log((1 - con) / (1 + con))));
  }
  else {
    return (2 * sinphi);
  }
};
},{}],21:[function(_dereq_,module,exports){
module.exports = function(x) {
  return x<0 ? -1 : 1;
};
},{}],22:[function(_dereq_,module,exports){
module.exports = function(esinp, exp) {
  return (Math.pow((1 - esinp) / (1 + esinp), exp));
};
},{}],23:[function(_dereq_,module,exports){
module.exports = function (array){
  var out = {
    x: array[0],
    y: array[1]
  };
  if (array.length>2) {
    out.z = array[2];
  }
  if (array.length>3) {
    out.m = array[3];
  }
  return out;
};
},{}],24:[function(_dereq_,module,exports){
var HALF_PI = Math.PI/2;

module.exports = function(eccent, phi, sinphi) {
  var con = eccent * sinphi;
  var com = 0.5 * eccent;
  con = Math.pow(((1 - con) / (1 + con)), com);
  return (Math.tan(0.5 * (HALF_PI - phi)) / con);
};
},{}],25:[function(_dereq_,module,exports){
exports.wgs84 = {
  towgs84: "0,0,0",
  ellipse: "WGS84",
  datumName: "WGS84"
};
exports.ch1903 = {
  towgs84: "674.374,15.056,405.346",
  ellipse: "bessel",
  datumName: "swiss"
};
exports.ggrs87 = {
  towgs84: "-199.87,74.79,246.62",
  ellipse: "GRS80",
  datumName: "Greek_Geodetic_Reference_System_1987"
};
exports.nad83 = {
  towgs84: "0,0,0",
  ellipse: "GRS80",
  datumName: "North_American_Datum_1983"
};
exports.nad27 = {
  nadgrids: "@conus,@alaska,@ntv2_0.gsb,@ntv1_can.dat",
  ellipse: "clrk66",
  datumName: "North_American_Datum_1927"
};
exports.potsdam = {
  towgs84: "606.0,23.0,413.0",
  ellipse: "bessel",
  datumName: "Potsdam Rauenberg 1950 DHDN"
};
exports.carthage = {
  towgs84: "-263.0,6.0,431.0",
  ellipse: "clark80",
  datumName: "Carthage 1934 Tunisia"
};
exports.hermannskogel = {
  towgs84: "653.0,-212.0,449.0",
  ellipse: "bessel",
  datumName: "Hermannskogel"
};
exports.ire65 = {
  towgs84: "482.530,-130.596,564.557,-1.042,-0.214,-0.631,8.15",
  ellipse: "mod_airy",
  datumName: "Ireland 1965"
};
exports.rassadiran = {
  towgs84: "-133.63,-157.5,-158.62",
  ellipse: "intl",
  datumName: "Rassadiran"
};
exports.nzgd49 = {
  towgs84: "59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993",
  ellipse: "intl",
  datumName: "New Zealand Geodetic Datum 1949"
};
exports.osgb36 = {
  towgs84: "446.448,-125.157,542.060,0.1502,0.2470,0.8421,-20.4894",
  ellipse: "airy",
  datumName: "Airy 1830"
};
exports.s_jtsk = {
  towgs84: "589,76,480",
  ellipse: 'bessel',
  datumName: 'S-JTSK (Ferro)'
};
exports.beduaram = {
  towgs84: '-106,-87,188',
  ellipse: 'clrk80',
  datumName: 'Beduaram'
};
exports.gunung_segara = {
  towgs84: '-403,684,41',
  ellipse: 'bessel',
  datumName: 'Gunung Segara Jakarta'
};
exports.rnb72 = {
  towgs84: "106.869,-52.2978,103.724,-0.33657,0.456955,-1.84218,1",
  ellipse: "intl",
  datumName: "Reseau National Belge 1972"
};
},{}],26:[function(_dereq_,module,exports){
exports.MERIT = {
  a: 6378137.0,
  rf: 298.257,
  ellipseName: "MERIT 1983"
};
exports.SGS85 = {
  a: 6378136.0,
  rf: 298.257,
  ellipseName: "Soviet Geodetic System 85"
};
exports.GRS80 = {
  a: 6378137.0,
  rf: 298.257222101,
  ellipseName: "GRS 1980(IUGG, 1980)"
};
exports.IAU76 = {
  a: 6378140.0,
  rf: 298.257,
  ellipseName: "IAU 1976"
};
exports.airy = {
  a: 6377563.396,
  b: 6356256.910,
  ellipseName: "Airy 1830"
};
exports.APL4 = {
  a: 6378137,
  rf: 298.25,
  ellipseName: "Appl. Physics. 1965"
};
exports.NWL9D = {
  a: 6378145.0,
  rf: 298.25,
  ellipseName: "Naval Weapons Lab., 1965"
};
exports.mod_airy = {
  a: 6377340.189,
  b: 6356034.446,
  ellipseName: "Modified Airy"
};
exports.andrae = {
  a: 6377104.43,
  rf: 300.0,
  ellipseName: "Andrae 1876 (Den., Iclnd.)"
};
exports.aust_SA = {
  a: 6378160.0,
  rf: 298.25,
  ellipseName: "Australian Natl & S. Amer. 1969"
};
exports.GRS67 = {
  a: 6378160.0,
  rf: 298.2471674270,
  ellipseName: "GRS 67(IUGG 1967)"
};
exports.bessel = {
  a: 6377397.155,
  rf: 299.1528128,
  ellipseName: "Bessel 1841"
};
exports.bess_nam = {
  a: 6377483.865,
  rf: 299.1528128,
  ellipseName: "Bessel 1841 (Namibia)"
};
exports.clrk66 = {
  a: 6378206.4,
  b: 6356583.8,
  ellipseName: "Clarke 1866"
};
exports.clrk80 = {
  a: 6378249.145,
  rf: 293.4663,
  ellipseName: "Clarke 1880 mod."
};
exports.clrk58 = {
  a: 6378293.645208759,
  rf: 294.2606763692654,
  ellipseName: "Clarke 1858"
};
exports.CPM = {
  a: 6375738.7,
  rf: 334.29,
  ellipseName: "Comm. des Poids et Mesures 1799"
};
exports.delmbr = {
  a: 6376428.0,
  rf: 311.5,
  ellipseName: "Delambre 1810 (Belgium)"
};
exports.engelis = {
  a: 6378136.05,
  rf: 298.2566,
  ellipseName: "Engelis 1985"
};
exports.evrst30 = {
  a: 6377276.345,
  rf: 300.8017,
  ellipseName: "Everest 1830"
};
exports.evrst48 = {
  a: 6377304.063,
  rf: 300.8017,
  ellipseName: "Everest 1948"
};
exports.evrst56 = {
  a: 6377301.243,
  rf: 300.8017,
  ellipseName: "Everest 1956"
};
exports.evrst69 = {
  a: 6377295.664,
  rf: 300.8017,
  ellipseName: "Everest 1969"
};
exports.evrstSS = {
  a: 6377298.556,
  rf: 300.8017,
  ellipseName: "Everest (Sabah & Sarawak)"
};
exports.fschr60 = {
  a: 6378166.0,
  rf: 298.3,
  ellipseName: "Fischer (Mercury Datum) 1960"
};
exports.fschr60m = {
  a: 6378155.0,
  rf: 298.3,
  ellipseName: "Fischer 1960"
};
exports.fschr68 = {
  a: 6378150.0,
  rf: 298.3,
  ellipseName: "Fischer 1968"
};
exports.helmert = {
  a: 6378200.0,
  rf: 298.3,
  ellipseName: "Helmert 1906"
};
exports.hough = {
  a: 6378270.0,
  rf: 297.0,
  ellipseName: "Hough"
};
exports.intl = {
  a: 6378388.0,
  rf: 297.0,
  ellipseName: "International 1909 (Hayford)"
};
exports.kaula = {
  a: 6378163.0,
  rf: 298.24,
  ellipseName: "Kaula 1961"
};
exports.lerch = {
  a: 6378139.0,
  rf: 298.257,
  ellipseName: "Lerch 1979"
};
exports.mprts = {
  a: 6397300.0,
  rf: 191.0,
  ellipseName: "Maupertius 1738"
};
exports.new_intl = {
  a: 6378157.5,
  b: 6356772.2,
  ellipseName: "New International 1967"
};
exports.plessis = {
  a: 6376523.0,
  rf: 6355863.0,
  ellipseName: "Plessis 1817 (France)"
};
exports.krass = {
  a: 6378245.0,
  rf: 298.3,
  ellipseName: "Krassovsky, 1942"
};
exports.SEasia = {
  a: 6378155.0,
  b: 6356773.3205,
  ellipseName: "Southeast Asia"
};
exports.walbeck = {
  a: 6376896.0,
  b: 6355834.8467,
  ellipseName: "Walbeck"
};
exports.WGS60 = {
  a: 6378165.0,
  rf: 298.3,
  ellipseName: "WGS 60"
};
exports.WGS66 = {
  a: 6378145.0,
  rf: 298.25,
  ellipseName: "WGS 66"
};
exports.WGS7 = {
  a: 6378135.0,
  rf: 298.26,
  ellipseName: "WGS 72"
};
exports.WGS84 = {
  a: 6378137.0,
  rf: 298.257223563,
  ellipseName: "WGS 84"
};
exports.sphere = {
  a: 6370997.0,
  b: 6370997.0,
  ellipseName: "Normal Sphere (r=6370997)"
};
},{}],27:[function(_dereq_,module,exports){
exports.greenwich = 0.0; //"0dE",
exports.lisbon = -9.131906111111; //"9d07'54.862\"W",
exports.paris = 2.337229166667; //"2d20'14.025\"E",
exports.bogota = -74.080916666667; //"74d04'51.3\"W",
exports.madrid = -3.687938888889; //"3d41'16.58\"W",
exports.rome = 12.452333333333; //"12d27'8.4\"E",
exports.bern = 7.439583333333; //"7d26'22.5\"E",
exports.jakarta = 106.807719444444; //"106d48'27.79\"E",
exports.ferro = -17.666666666667; //"17d40'W",
exports.brussels = 4.367975; //"4d22'4.71\"E",
exports.stockholm = 18.058277777778; //"18d3'29.8\"E",
exports.athens = 23.7163375; //"23d42'58.815\"E",
exports.oslo = 10.722916666667; //"10d43'22.5\"E"
},{}],28:[function(_dereq_,module,exports){
exports.ft = {to_meter: 0.3048};
exports['us-ft'] = {to_meter: 1200 / 3937};

},{}],29:[function(_dereq_,module,exports){
var proj = _dereq_('./Proj');
var transform = _dereq_('./transform');
var wgs84 = proj('WGS84');

function transformer(from, to, coords) {
  var transformedArray;
  if (Array.isArray(coords)) {
    transformedArray = transform(from, to, coords);
    if (coords.length === 3) {
      return [transformedArray.x, transformedArray.y, transformedArray.z];
    }
    else {
      return [transformedArray.x, transformedArray.y];
    }
  }
  else {
    return transform(from, to, coords);
  }
}

function checkProj(item) {
  if (item instanceof proj) {
    return item;
  }
  if (item.oProj) {
    return item.oProj;
  }
  return proj(item);
}
function proj4(fromProj, toProj, coord) {
  fromProj = checkProj(fromProj);
  var single = false;
  var obj;
  if (typeof toProj === 'undefined') {
    toProj = fromProj;
    fromProj = wgs84;
    single = true;
  }
  else if (typeof toProj.x !== 'undefined' || Array.isArray(toProj)) {
    coord = toProj;
    toProj = fromProj;
    fromProj = wgs84;
    single = true;
  }
  toProj = checkProj(toProj);
  if (coord) {
    return transformer(fromProj, toProj, coord);
  }
  else {
    obj = {
      forward: function(coords) {
        return transformer(fromProj, toProj, coords);
      },
      inverse: function(coords) {
        return transformer(toProj, fromProj, coords);
      }
    };
    if (single) {
      obj.oProj = toProj;
    }
    return obj;
  }
}
module.exports = proj4;
},{"./Proj":2,"./transform":65}],30:[function(_dereq_,module,exports){
var HALF_PI = Math.PI/2;
var PJD_3PARAM = 1;
var PJD_7PARAM = 2;
var PJD_GRIDSHIFT = 3;
var PJD_WGS84 = 4; // WGS84 or equivalent
var PJD_NODATUM = 5; // WGS84 or equivalent
var SEC_TO_RAD = 4.84813681109535993589914102357e-6;
var AD_C = 1.0026000;
var COS_67P5 = 0.38268343236508977;
var datum = function(proj) {
  if (!(this instanceof datum)) {
    return new datum(proj);
  }
  this.datum_type = PJD_WGS84; //default setting
  if (!proj) {
    return;
  }
  if (proj.datumCode && proj.datumCode === 'none') {
    this.datum_type = PJD_NODATUM;
  }

  if (proj.datum_params) {
    this.datum_params = proj.datum_params.map(parseFloat);
    if (this.datum_params[0] !== 0 || this.datum_params[1] !== 0 || this.datum_params[2] !== 0) {
      this.datum_type = PJD_3PARAM;
    }
    if (this.datum_params.length > 3) {
      if (this.datum_params[3] !== 0 || this.datum_params[4] !== 0 || this.datum_params[5] !== 0 || this.datum_params[6] !== 0) {
        this.datum_type = PJD_7PARAM;
        this.datum_params[3] *= SEC_TO_RAD;
        this.datum_params[4] *= SEC_TO_RAD;
        this.datum_params[5] *= SEC_TO_RAD;
        this.datum_params[6] = (this.datum_params[6] / 1000000.0) + 1.0;
      }
    }
  }

  // DGR 2011-03-21 : nadgrids support
  this.datum_type = proj.grids ? PJD_GRIDSHIFT : this.datum_type;

  this.a = proj.a; //datum object also uses these values
  this.b = proj.b;
  this.es = proj.es;
  this.ep2 = proj.ep2;
  if (this.datum_type === PJD_GRIDSHIFT) {
    this.grids = proj.grids;
  }
};
datum.prototype = {


  /****************************************************************/
  // cs_compare_datums()
  //   Returns TRUE if the two datums match, otherwise FALSE.
  compare_datums: function(dest) {
    if (this.datum_type !== dest.datum_type) {
      return false; // false, datums are not equal
    }
    else if (this.a !== dest.a || Math.abs(this.es - dest.es) > 0.000000000050) {
      // the tolerence for es is to ensure that GRS80 and WGS84
      // are considered identical
      return false;
    }
    else if (this.datum_type === PJD_3PARAM) {
      return (this.datum_params[0] === dest.datum_params[0] && this.datum_params[1] === dest.datum_params[1] && this.datum_params[2] === dest.datum_params[2]);
    }
    else if (this.datum_type === PJD_7PARAM) {
      return (this.datum_params[0] === dest.datum_params[0] && this.datum_params[1] === dest.datum_params[1] && this.datum_params[2] === dest.datum_params[2] && this.datum_params[3] === dest.datum_params[3] && this.datum_params[4] === dest.datum_params[4] && this.datum_params[5] === dest.datum_params[5] && this.datum_params[6] === dest.datum_params[6]);
    }
    else if (this.datum_type === PJD_GRIDSHIFT || dest.datum_type === PJD_GRIDSHIFT) {
      //alert("ERROR: Grid shift transformations are not implemented.");
      //return false
      //DGR 2012-07-29 lazy ...
      return this.nadgrids === dest.nadgrids;
    }
    else {
      return true; // datums are equal
    }
  }, // cs_compare_datums()

  /*
   * The function Convert_Geodetic_To_Geocentric converts geodetic coordinates
   * (latitude, longitude, and height) to geocentric coordinates (X, Y, Z),
   * according to the current ellipsoid parameters.
   *
   *    Latitude  : Geodetic latitude in radians                     (input)
   *    Longitude : Geodetic longitude in radians                    (input)
   *    Height    : Geodetic height, in meters                       (input)
   *    X         : Calculated Geocentric X coordinate, in meters    (output)
   *    Y         : Calculated Geocentric Y coordinate, in meters    (output)
   *    Z         : Calculated Geocentric Z coordinate, in meters    (output)
   *
   */
  geodetic_to_geocentric: function(p) {
    var Longitude = p.x;
    var Latitude = p.y;
    var Height = p.z ? p.z : 0; //Z value not always supplied
    var X; // output
    var Y;
    var Z;

    var Error_Code = 0; //  GEOCENT_NO_ERROR;
    var Rn; /*  Earth radius at location  */
    var Sin_Lat; /*  Math.sin(Latitude)  */
    var Sin2_Lat; /*  Square of Math.sin(Latitude)  */
    var Cos_Lat; /*  Math.cos(Latitude)  */

    /*
     ** Don't blow up if Latitude is just a little out of the value
     ** range as it may just be a rounding issue.  Also removed longitude
     ** test, it should be wrapped by Math.cos() and Math.sin().  NFW for PROJ.4, Sep/2001.
     */
    if (Latitude < -HALF_PI && Latitude > -1.001 * HALF_PI) {
      Latitude = -HALF_PI;
    }
    else if (Latitude > HALF_PI && Latitude < 1.001 * HALF_PI) {
      Latitude = HALF_PI;
    }
    else if ((Latitude < -HALF_PI) || (Latitude > HALF_PI)) {
      /* Latitude out of range */
      //..reportError('geocent:lat out of range:' + Latitude);
      return null;
    }

    if (Longitude > Math.PI) {
      Longitude -= (2 * Math.PI);
    }
    Sin_Lat = Math.sin(Latitude);
    Cos_Lat = Math.cos(Latitude);
    Sin2_Lat = Sin_Lat * Sin_Lat;
    Rn = this.a / (Math.sqrt(1.0e0 - this.es * Sin2_Lat));
    X = (Rn + Height) * Cos_Lat * Math.cos(Longitude);
    Y = (Rn + Height) * Cos_Lat * Math.sin(Longitude);
    Z = ((Rn * (1 - this.es)) + Height) * Sin_Lat;

    p.x = X;
    p.y = Y;
    p.z = Z;
    return Error_Code;
  }, // cs_geodetic_to_geocentric()


  geocentric_to_geodetic: function(p) {
    /* local defintions and variables */
    /* end-criterium of loop, accuracy of sin(Latitude) */
    var genau = 1e-12;
    var genau2 = (genau * genau);
    var maxiter = 30;

    var P; /* distance between semi-minor axis and location */
    var RR; /* distance between center and location */
    var CT; /* sin of geocentric latitude */
    var ST; /* cos of geocentric latitude */
    var RX;
    var RK;
    var RN; /* Earth radius at location */
    var CPHI0; /* cos of start or old geodetic latitude in iterations */
    var SPHI0; /* sin of start or old geodetic latitude in iterations */
    var CPHI; /* cos of searched geodetic latitude */
    var SPHI; /* sin of searched geodetic latitude */
    var SDPHI; /* end-criterium: addition-theorem of sin(Latitude(iter)-Latitude(iter-1)) */
    var At_Pole; /* indicates location is in polar region */
    var iter; /* # of continous iteration, max. 30 is always enough (s.a.) */

    var X = p.x;
    var Y = p.y;
    var Z = p.z ? p.z : 0.0; //Z value not always supplied
    var Longitude;
    var Latitude;
    var Height;

    At_Pole = false;
    P = Math.sqrt(X * X + Y * Y);
    RR = Math.sqrt(X * X + Y * Y + Z * Z);

    /*      special cases for latitude and longitude */
    if (P / this.a < genau) {

      /*  special case, if P=0. (X=0., Y=0.) */
      At_Pole = true;
      Longitude = 0.0;

      /*  if (X,Y,Z)=(0.,0.,0.) then Height becomes semi-minor axis
       *  of ellipsoid (=center of mass), Latitude becomes PI/2 */
      if (RR / this.a < genau) {
        Latitude = HALF_PI;
        Height = -this.b;
        return;
      }
    }
    else {
      /*  ellipsoidal (geodetic) longitude
       *  interval: -PI < Longitude <= +PI */
      Longitude = Math.atan2(Y, X);
    }

    /* --------------------------------------------------------------
     * Following iterative algorithm was developped by
     * "Institut for Erdmessung", University of Hannover, July 1988.
     * Internet: www.ife.uni-hannover.de
     * Iterative computation of CPHI,SPHI and Height.
     * Iteration of CPHI and SPHI to 10**-12 radian resp.
     * 2*10**-7 arcsec.
     * --------------------------------------------------------------
     */
    CT = Z / RR;
    ST = P / RR;
    RX = 1.0 / Math.sqrt(1.0 - this.es * (2.0 - this.es) * ST * ST);
    CPHI0 = ST * (1.0 - this.es) * RX;
    SPHI0 = CT * RX;
    iter = 0;

    /* loop to find sin(Latitude) resp. Latitude
     * until |sin(Latitude(iter)-Latitude(iter-1))| < genau */
    do {
      iter++;
      RN = this.a / Math.sqrt(1.0 - this.es * SPHI0 * SPHI0);

      /*  ellipsoidal (geodetic) height */
      Height = P * CPHI0 + Z * SPHI0 - RN * (1.0 - this.es * SPHI0 * SPHI0);

      RK = this.es * RN / (RN + Height);
      RX = 1.0 / Math.sqrt(1.0 - RK * (2.0 - RK) * ST * ST);
      CPHI = ST * (1.0 - RK) * RX;
      SPHI = CT * RX;
      SDPHI = SPHI * CPHI0 - CPHI * SPHI0;
      CPHI0 = CPHI;
      SPHI0 = SPHI;
    }
    while (SDPHI * SDPHI > genau2 && iter < maxiter);

    /*      ellipsoidal (geodetic) latitude */
    Latitude = Math.atan(SPHI / Math.abs(CPHI));

    p.x = Longitude;
    p.y = Latitude;
    p.z = Height;
    return p;
  }, // cs_geocentric_to_geodetic()

  /** Convert_Geocentric_To_Geodetic
   * The method used here is derived from 'An Improved Algorithm for
   * Geocentric to Geodetic Coordinate Conversion', by Ralph Toms, Feb 1996
   */
  geocentric_to_geodetic_noniter: function(p) {
    var X = p.x;
    var Y = p.y;
    var Z = p.z ? p.z : 0; //Z value not always supplied
    var Longitude;
    var Latitude;
    var Height;

    var W; /* distance from Z axis */
    var W2; /* square of distance from Z axis */
    var T0; /* initial estimate of vertical component */
    var T1; /* corrected estimate of vertical component */
    var S0; /* initial estimate of horizontal component */
    var S1; /* corrected estimate of horizontal component */
    var Sin_B0; /* Math.sin(B0), B0 is estimate of Bowring aux variable */
    var Sin3_B0; /* cube of Math.sin(B0) */
    var Cos_B0; /* Math.cos(B0) */
    var Sin_p1; /* Math.sin(phi1), phi1 is estimated latitude */
    var Cos_p1; /* Math.cos(phi1) */
    var Rn; /* Earth radius at location */
    var Sum; /* numerator of Math.cos(phi1) */
    var At_Pole; /* indicates location is in polar region */

    X = parseFloat(X); // cast from string to float
    Y = parseFloat(Y);
    Z = parseFloat(Z);

    At_Pole = false;
    if (X !== 0.0) {
      Longitude = Math.atan2(Y, X);
    }
    else {
      if (Y > 0) {
        Longitude = HALF_PI;
      }
      else if (Y < 0) {
        Longitude = -HALF_PI;
      }
      else {
        At_Pole = true;
        Longitude = 0.0;
        if (Z > 0.0) { /* north pole */
          Latitude = HALF_PI;
        }
        else if (Z < 0.0) { /* south pole */
          Latitude = -HALF_PI;
        }
        else { /* center of earth */
          Latitude = HALF_PI;
          Height = -this.b;
          return;
        }
      }
    }
    W2 = X * X + Y * Y;
    W = Math.sqrt(W2);
    T0 = Z * AD_C;
    S0 = Math.sqrt(T0 * T0 + W2);
    Sin_B0 = T0 / S0;
    Cos_B0 = W / S0;
    Sin3_B0 = Sin_B0 * Sin_B0 * Sin_B0;
    T1 = Z + this.b * this.ep2 * Sin3_B0;
    Sum = W - this.a * this.es * Cos_B0 * Cos_B0 * Cos_B0;
    S1 = Math.sqrt(T1 * T1 + Sum * Sum);
    Sin_p1 = T1 / S1;
    Cos_p1 = Sum / S1;
    Rn = this.a / Math.sqrt(1.0 - this.es * Sin_p1 * Sin_p1);
    if (Cos_p1 >= COS_67P5) {
      Height = W / Cos_p1 - Rn;
    }
    else if (Cos_p1 <= -COS_67P5) {
      Height = W / -Cos_p1 - Rn;
    }
    else {
      Height = Z / Sin_p1 + Rn * (this.es - 1.0);
    }
    if (At_Pole === false) {
      Latitude = Math.atan(Sin_p1 / Cos_p1);
    }

    p.x = Longitude;
    p.y = Latitude;
    p.z = Height;
    return p;
  }, // geocentric_to_geodetic_noniter()

  /****************************************************************/
  // pj_geocentic_to_wgs84( p )
  //  p = point to transform in geocentric coordinates (x,y,z)
  geocentric_to_wgs84: function(p) {

    if (this.datum_type === PJD_3PARAM) {
      // if( x[io] === HUGE_VAL )
      //    continue;
      p.x += this.datum_params[0];
      p.y += this.datum_params[1];
      p.z += this.datum_params[2];

    }
    else if (this.datum_type === PJD_7PARAM) {
      var Dx_BF = this.datum_params[0];
      var Dy_BF = this.datum_params[1];
      var Dz_BF = this.datum_params[2];
      var Rx_BF = this.datum_params[3];
      var Ry_BF = this.datum_params[4];
      var Rz_BF = this.datum_params[5];
      var M_BF = this.datum_params[6];
      // if( x[io] === HUGE_VAL )
      //    continue;
      var x_out = M_BF * (p.x - Rz_BF * p.y + Ry_BF * p.z) + Dx_BF;
      var y_out = M_BF * (Rz_BF * p.x + p.y - Rx_BF * p.z) + Dy_BF;
      var z_out = M_BF * (-Ry_BF * p.x + Rx_BF * p.y + p.z) + Dz_BF;
      p.x = x_out;
      p.y = y_out;
      p.z = z_out;
    }
  }, // cs_geocentric_to_wgs84

  /****************************************************************/
  // pj_geocentic_from_wgs84()
  //  coordinate system definition,
  //  point to transform in geocentric coordinates (x,y,z)
  geocentric_from_wgs84: function(p) {

    if (this.datum_type === PJD_3PARAM) {
      //if( x[io] === HUGE_VAL )
      //    continue;
      p.x -= this.datum_params[0];
      p.y -= this.datum_params[1];
      p.z -= this.datum_params[2];

    }
    else if (this.datum_type === PJD_7PARAM) {
      var Dx_BF = this.datum_params[0];
      var Dy_BF = this.datum_params[1];
      var Dz_BF = this.datum_params[2];
      var Rx_BF = this.datum_params[3];
      var Ry_BF = this.datum_params[4];
      var Rz_BF = this.datum_params[5];
      var M_BF = this.datum_params[6];
      var x_tmp = (p.x - Dx_BF) / M_BF;
      var y_tmp = (p.y - Dy_BF) / M_BF;
      var z_tmp = (p.z - Dz_BF) / M_BF;
      //if( x[io] === HUGE_VAL )
      //    continue;

      p.x = x_tmp + Rz_BF * y_tmp - Ry_BF * z_tmp;
      p.y = -Rz_BF * x_tmp + y_tmp + Rx_BF * z_tmp;
      p.z = Ry_BF * x_tmp - Rx_BF * y_tmp + z_tmp;
    } //cs_geocentric_from_wgs84()
  }
};

/** point object, nothing fancy, just allows values to be
    passed back and forth by reference rather than by value.
    Other point classes may be used as long as they have
    x and y properties, which will get modified in the transform method.
* */
module.exports = datum;

},{}],31:[function(_dereq_,module,exports){
var PJD_3PARAM = 1;
var PJD_7PARAM = 2;
var PJD_GRIDSHIFT = 3;
var PJD_NODATUM = 5; // WGS84 or equivalent
var SRS_WGS84_SEMIMAJOR = 6378137; // only used in grid shift transforms
var SRS_WGS84_ESQUARED = 0.006694379990141316; //DGR: 2012-07-29
module.exports = function(source, dest, point) {
  var wp, i, l;

  function checkParams(fallback) {
    return (fallback === PJD_3PARAM || fallback === PJD_7PARAM);
  }
  // Short cut if the datums are identical.
  if (source.compare_datums(dest)) {
    return point; // in this case, zero is sucess,
    // whereas cs_compare_datums returns 1 to indicate TRUE
    // confusing, should fix this
  }

  // Explicitly skip datum transform by setting 'datum=none' as parameter for either source or dest
  if (source.datum_type === PJD_NODATUM || dest.datum_type === PJD_NODATUM) {
    return point;
  }

  //DGR: 2012-07-29 : add nadgrids support (begin)
  var src_a = source.a;
  var src_es = source.es;

  var dst_a = dest.a;
  var dst_es = dest.es;

  var fallback = source.datum_type;
  // If this datum requires grid shifts, then apply it to geodetic coordinates.
  if (fallback === PJD_GRIDSHIFT) {
    if (this.apply_gridshift(source, 0, point) === 0) {
      source.a = SRS_WGS84_SEMIMAJOR;
      source.es = SRS_WGS84_ESQUARED;
    }
    else {
      // try 3 or 7 params transformation or nothing ?
      if (!source.datum_params) {
        source.a = src_a;
        source.es = source.es;
        return point;
      }
      wp = 1;
      for (i = 0, l = source.datum_params.length; i < l; i++) {
        wp *= source.datum_params[i];
      }
      if (wp === 0) {
        source.a = src_a;
        source.es = source.es;
        return point;
      }
      if (source.datum_params.length > 3) {
        fallback = PJD_7PARAM;
      }
      else {
        fallback = PJD_3PARAM;
      }
    }
  }
  if (dest.datum_type === PJD_GRIDSHIFT) {
    dest.a = SRS_WGS84_SEMIMAJOR;
    dest.es = SRS_WGS84_ESQUARED;
  }
  // Do we need to go through geocentric coordinates?
  if (source.es !== dest.es || source.a !== dest.a || checkParams(fallback) || checkParams(dest.datum_type)) {
    //DGR: 2012-07-29 : add nadgrids support (end)
    // Convert to geocentric coordinates.
    source.geodetic_to_geocentric(point);
    // CHECK_RETURN;
    // Convert between datums
    if (checkParams(source.datum_type)) {
      source.geocentric_to_wgs84(point);
      // CHECK_RETURN;
    }
    if (checkParams(dest.datum_type)) {
      dest.geocentric_from_wgs84(point);
      // CHECK_RETURN;
    }
    // Convert back to geodetic coordinates
    dest.geocentric_to_geodetic(point);
    // CHECK_RETURN;
  }
  // Apply grid shift to destination if required
  if (dest.datum_type === PJD_GRIDSHIFT) {
    this.apply_gridshift(dest, 1, point);
    // CHECK_RETURN;
  }

  source.a = src_a;
  source.es = src_es;
  dest.a = dst_a;
  dest.es = dst_es;

  return point;
};


},{}],32:[function(_dereq_,module,exports){
var globals = _dereq_('./global');
var parseProj = _dereq_('./projString');
var wkt = _dereq_('./wkt');

function defs(name) {
  /*global console*/
  var that = this;
  if (arguments.length === 2) {
    var def = arguments[1];
    if (typeof def === 'string') {
      if (def.charAt(0) === '+') {
        defs[name] = parseProj(arguments[1]);
      }
      else {
        defs[name] = wkt(arguments[1]);
      }
    } else {
      defs[name] = def;
    }
  }
  else if (arguments.length === 1) {
    if (Array.isArray(name)) {
      return name.map(function(v) {
        if (Array.isArray(v)) {
          defs.apply(that, v);
        }
        else {
          defs(v);
        }
      });
    }
    else if (typeof name === 'string') {
      if (name in defs) {
        return defs[name];
      }
    }
    else if ('EPSG' in name) {
      defs['EPSG:' + name.EPSG] = name;
    }
    else if ('ESRI' in name) {
      defs['ESRI:' + name.ESRI] = name;
    }
    else if ('IAU2000' in name) {
      defs['IAU2000:' + name.IAU2000] = name;
    }
    else {
      console.log(name);
    }
    return;
  }


}
globals(defs);
module.exports = defs;

},{"./global":35,"./projString":38,"./wkt":66}],33:[function(_dereq_,module,exports){
var Datum = _dereq_('./constants/Datum');
var Ellipsoid = _dereq_('./constants/Ellipsoid');
var extend = _dereq_('./extend');
var datum = _dereq_('./datum');
var EPSLN = 1.0e-10;
// ellipoid pj_set_ell.c
var SIXTH = 0.1666666666666666667;
/* 1/6 */
var RA4 = 0.04722222222222222222;
/* 17/360 */
var RA6 = 0.02215608465608465608;
module.exports = function(json) {
  // DGR 2011-03-20 : nagrids -> nadgrids
  if (json.datumCode && json.datumCode !== 'none') {
    var datumDef = Datum[json.datumCode];
    if (datumDef) {
      json.datum_params = datumDef.towgs84 ? datumDef.towgs84.split(',') : null;
      json.ellps = datumDef.ellipse;
      json.datumName = datumDef.datumName ? datumDef.datumName : json.datumCode;
    }
  }
  if (!json.a) { // do we have an ellipsoid?
    var ellipse = Ellipsoid[json.ellps] ? Ellipsoid[json.ellps] : Ellipsoid.WGS84;
    extend(json, ellipse);
  }
  if (json.rf && !json.b) {
    json.b = (1.0 - 1.0 / json.rf) * json.a;
  }
  if (json.rf === 0 || Math.abs(json.a - json.b) < EPSLN) {
    json.sphere = true;
    json.b = json.a;
  }
  json.a2 = json.a * json.a; // used in geocentric
  json.b2 = json.b * json.b; // used in geocentric
  json.es = (json.a2 - json.b2) / json.a2; // e ^ 2
  json.e = Math.sqrt(json.es); // eccentricity
  if (json.R_A) {
    json.a *= 1 - json.es * (SIXTH + json.es * (RA4 + json.es * RA6));
    json.a2 = json.a * json.a;
    json.b2 = json.b * json.b;
    json.es = 0;
  }
  json.ep2 = (json.a2 - json.b2) / json.b2; // used in geocentric
  if (!json.k0) {
    json.k0 = 1.0; //default value
  }
  //DGR 2010-11-12: axis
  if (!json.axis) {
    json.axis = "enu";
  }

  if (!json.datum) {
    json.datum = datum(json);
  }
  return json;
};

},{"./constants/Datum":25,"./constants/Ellipsoid":26,"./datum":30,"./extend":34}],34:[function(_dereq_,module,exports){
module.exports = function(destination, source) {
  destination = destination || {};
  var value, property;
  if (!source) {
    return destination;
  }
  for (property in source) {
    value = source[property];
    if (value !== undefined) {
      destination[property] = value;
    }
  }
  return destination;
};

},{}],35:[function(_dereq_,module,exports){
module.exports = function(defs) {
  defs('EPSG:4326', "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees");
  defs('EPSG:4269', "+title=NAD83 (long/lat) +proj=longlat +a=6378137.0 +b=6356752.31414036 +ellps=GRS80 +datum=NAD83 +units=degrees");
  defs('EPSG:3857', "+title=WGS 84 / Pseudo-Mercator +proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs");

  defs.WGS84 = defs['EPSG:4326'];
  defs['EPSG:3785'] = defs['EPSG:3857']; // maintain backward compat, official code is 3857
  defs.GOOGLE = defs['EPSG:3857'];
  defs['EPSG:900913'] = defs['EPSG:3857'];
  defs['EPSG:102113'] = defs['EPSG:3857'];
};

},{}],36:[function(_dereq_,module,exports){
var proj4 = _dereq_('./core');
proj4.defaultDatum = 'WGS84'; //default datum
proj4.Proj = _dereq_('./Proj');
proj4.WGS84 = new proj4.Proj('WGS84');
proj4.Point = _dereq_('./Point');
proj4.toPoint = _dereq_("./common/toPoint");
proj4.defs = _dereq_('./defs');
proj4.transform = _dereq_('./transform');
proj4.mgrs = _dereq_('mgrs');
proj4.version = _dereq_('../package.json').version;
_dereq_('./includedProjections')(proj4);
module.exports = proj4;
},{"../package.json":68,"./Point":1,"./Proj":2,"./common/toPoint":23,"./core":29,"./defs":32,"./includedProjections":"hTEDpn","./transform":65,"mgrs":67}],37:[function(_dereq_,module,exports){
var defs = _dereq_('./defs');
var wkt = _dereq_('./wkt');
var projStr = _dereq_('./projString');
function testObj(code){
  return typeof code === 'string';
}
function testDef(code){
  return code in defs;
}
function testWKT(code){
  var codeWords = ['GEOGCS','GEOCCS','PROJCS','LOCAL_CS'];
  return codeWords.reduce(function(a,b){
    return a+1+code.indexOf(b);
  },0);
}
function testProj(code){
  return code[0] === '+';
}
function parse(code){
  if (testObj(code)) {
    //check to see if this is a WKT string
    if (testDef(code)) {
      return defs[code];
    }
    else if (testWKT(code)) {
      return wkt(code);
    }
    else if (testProj(code)) {
      return projStr(code);
    }
  }else{
    return code;
  }
}

module.exports = parse;
},{"./defs":32,"./projString":38,"./wkt":66}],38:[function(_dereq_,module,exports){
var D2R = 0.01745329251994329577;
var PrimeMeridian = _dereq_('./constants/PrimeMeridian');
var units = _dereq_('./constants/units');

module.exports = function(defData) {
  var self = {};
  var paramObj = {};
  defData.split("+").map(function(v) {
    return v.trim();
  }).filter(function(a) {
    return a;
  }).forEach(function(a) {
    var split = a.split("=");
    split.push(true);
    paramObj[split[0].toLowerCase()] = split[1];
  });
  var paramName, paramVal, paramOutname;
  var params = {
    proj: 'projName',
    datum: 'datumCode',
    rf: function(v) {
      self.rf = parseFloat(v);
    },
    lat_0: function(v) {
      self.lat0 = v * D2R;
    },
    lat_1: function(v) {
      self.lat1 = v * D2R;
    },
    lat_2: function(v) {
      self.lat2 = v * D2R;
    },
    lat_ts: function(v) {
      self.lat_ts = v * D2R;
    },
    lon_0: function(v) {
      self.long0 = v * D2R;
    },
    lon_1: function(v) {
      self.long1 = v * D2R;
    },
    lon_2: function(v) {
      self.long2 = v * D2R;
    },
    alpha: function(v) {
      self.alpha = parseFloat(v) * D2R;
    },
    lonc: function(v) {
      self.longc = v * D2R;
    },
    x_0: function(v) {
      self.x0 = parseFloat(v);
    },
    y_0: function(v) {
      self.y0 = parseFloat(v);
    },
    k_0: function(v) {
      self.k0 = parseFloat(v);
    },
    k: function(v) {
      self.k0 = parseFloat(v);
    },
    a: function(v) {
      self.a = parseFloat(v);
    },
    b: function(v) {
      self.b = parseFloat(v);
    },
    r_a: function() {
      self.R_A = true;
    },
    zone: function(v) {
      self.zone = parseInt(v, 10);
    },
    south: function() {
      self.utmSouth = true;
    },
    towgs84: function(v) {
      self.datum_params = v.split(",").map(function(a) {
        return parseFloat(a);
      });
    },
    to_meter: function(v) {
      self.to_meter = parseFloat(v);
    },
    units: function(v) {
      self.units = v;
      if (units[v]) {
        self.to_meter = units[v].to_meter;
      }
    },
    from_greenwich: function(v) {
      self.from_greenwich = v * D2R;
    },
    pm: function(v) {
      self.from_greenwich = (PrimeMeridian[v] ? PrimeMeridian[v] : parseFloat(v)) * D2R;
    },
    nadgrids: function(v) {
      if (v === '@null') {
        self.datumCode = 'none';
      }
      else {
        self.nadgrids = v;
      }
    },
    axis: function(v) {
      var legalAxis = "ewnsud";
      if (v.length === 3 && legalAxis.indexOf(v.substr(0, 1)) !== -1 && legalAxis.indexOf(v.substr(1, 1)) !== -1 && legalAxis.indexOf(v.substr(2, 1)) !== -1) {
        self.axis = v;
      }
    }
  };
  for (paramName in paramObj) {
    paramVal = paramObj[paramName];
    if (paramName in params) {
      paramOutname = params[paramName];
      if (typeof paramOutname === 'function') {
        paramOutname(paramVal);
      }
      else {
        self[paramOutname] = paramVal;
      }
    }
    else {
      self[paramName] = paramVal;
    }
  }
  if(typeof self.datumCode === 'string' && self.datumCode !== "WGS84"){
    self.datumCode = self.datumCode.toLowerCase();
  }
  return self;
};

},{"./constants/PrimeMeridian":27,"./constants/units":28}],39:[function(_dereq_,module,exports){
var projs = [
  _dereq_('./projections/merc'),
  _dereq_('./projections/longlat')
];
var names = {};
var projStore = [];

function add(proj, i) {
  var len = projStore.length;
  if (!proj.names) {
    console.log(i);
    return true;
  }
  projStore[len] = proj;
  proj.names.forEach(function(n) {
    names[n.toLowerCase()] = len;
  });
  return this;
}

exports.add = add;

exports.get = function(name) {
  if (!name) {
    return false;
  }
  var n = name.toLowerCase();
  if (typeof names[n] !== 'undefined' && projStore[names[n]]) {
    return projStore[names[n]];
  }
};
exports.start = function() {
  projs.forEach(add);
};

},{"./projections/longlat":51,"./projections/merc":52}],40:[function(_dereq_,module,exports){
var EPSLN = 1.0e-10;
var msfnz = _dereq_('../common/msfnz');
var qsfnz = _dereq_('../common/qsfnz');
var adjust_lon = _dereq_('../common/adjust_lon');
var asinz = _dereq_('../common/asinz');
exports.init = function() {

  if (Math.abs(this.lat1 + this.lat2) < EPSLN) {
    return;
  }
  this.temp = this.b / this.a;
  this.es = 1 - Math.pow(this.temp, 2);
  this.e3 = Math.sqrt(this.es);

  this.sin_po = Math.sin(this.lat1);
  this.cos_po = Math.cos(this.lat1);
  this.t1 = this.sin_po;
  this.con = this.sin_po;
  this.ms1 = msfnz(this.e3, this.sin_po, this.cos_po);
  this.qs1 = qsfnz(this.e3, this.sin_po, this.cos_po);

  this.sin_po = Math.sin(this.lat2);
  this.cos_po = Math.cos(this.lat2);
  this.t2 = this.sin_po;
  this.ms2 = msfnz(this.e3, this.sin_po, this.cos_po);
  this.qs2 = qsfnz(this.e3, this.sin_po, this.cos_po);

  this.sin_po = Math.sin(this.lat0);
  this.cos_po = Math.cos(this.lat0);
  this.t3 = this.sin_po;
  this.qs0 = qsfnz(this.e3, this.sin_po, this.cos_po);

  if (Math.abs(this.lat1 - this.lat2) > EPSLN) {
    this.ns0 = (this.ms1 * this.ms1 - this.ms2 * this.ms2) / (this.qs2 - this.qs1);
  }
  else {
    this.ns0 = this.con;
  }
  this.c = this.ms1 * this.ms1 + this.ns0 * this.qs1;
  this.rh = this.a * Math.sqrt(this.c - this.ns0 * this.qs0) / this.ns0;
};

/* Albers Conical Equal Area forward equations--mapping lat,long to x,y
  -------------------------------------------------------------------*/
exports.forward = function(p) {

  var lon = p.x;
  var lat = p.y;

  this.sin_phi = Math.sin(lat);
  this.cos_phi = Math.cos(lat);

  var qs = qsfnz(this.e3, this.sin_phi, this.cos_phi);
  var rh1 = this.a * Math.sqrt(this.c - this.ns0 * qs) / this.ns0;
  var theta = this.ns0 * adjust_lon(lon - this.long0);
  var x = rh1 * Math.sin(theta) + this.x0;
  var y = this.rh - rh1 * Math.cos(theta) + this.y0;

  p.x = x;
  p.y = y;
  return p;
};


exports.inverse = function(p) {
  var rh1, qs, con, theta, lon, lat;

  p.x -= this.x0;
  p.y = this.rh - p.y + this.y0;
  if (this.ns0 >= 0) {
    rh1 = Math.sqrt(p.x * p.x + p.y * p.y);
    con = 1;
  }
  else {
    rh1 = -Math.sqrt(p.x * p.x + p.y * p.y);
    con = -1;
  }
  theta = 0;
  if (rh1 !== 0) {
    theta = Math.atan2(con * p.x, con * p.y);
  }
  con = rh1 * this.ns0 / this.a;
  if (this.sphere) {
    lat = Math.asin((this.c - con * con) / (2 * this.ns0));
  }
  else {
    qs = (this.c - con * con) / this.ns0;
    lat = this.phi1z(this.e3, qs);
  }

  lon = adjust_lon(theta / this.ns0 + this.long0);
  p.x = lon;
  p.y = lat;
  return p;
};

/* Function to compute phi1, the latitude for the inverse of the
   Albers Conical Equal-Area projection.
-------------------------------------------*/
exports.phi1z = function(eccent, qs) {
  var sinphi, cosphi, con, com, dphi;
  var phi = asinz(0.5 * qs);
  if (eccent < EPSLN) {
    return phi;
  }

  var eccnts = eccent * eccent;
  for (var i = 1; i <= 25; i++) {
    sinphi = Math.sin(phi);
    cosphi = Math.cos(phi);
    con = eccent * sinphi;
    com = 1 - con * con;
    dphi = 0.5 * com * com / cosphi * (qs / (1 - eccnts) - sinphi / com + 0.5 / eccent * Math.log((1 - con) / (1 + con)));
    phi = phi + dphi;
    if (Math.abs(dphi) <= 1e-7) {
      return phi;
    }
  }
  return null;
};
exports.names = ["Albers_Conic_Equal_Area", "Albers", "aea"];

},{"../common/adjust_lon":5,"../common/asinz":6,"../common/msfnz":15,"../common/qsfnz":20}],41:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
var HALF_PI = Math.PI/2;
var EPSLN = 1.0e-10;
var mlfn = _dereq_('../common/mlfn');
var e0fn = _dereq_('../common/e0fn');
var e1fn = _dereq_('../common/e1fn');
var e2fn = _dereq_('../common/e2fn');
var e3fn = _dereq_('../common/e3fn');
var gN = _dereq_('../common/gN');
var asinz = _dereq_('../common/asinz');
var imlfn = _dereq_('../common/imlfn');
exports.init = function() {
  this.sin_p12 = Math.sin(this.lat0);
  this.cos_p12 = Math.cos(this.lat0);
};

exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  var sinphi = Math.sin(p.y);
  var cosphi = Math.cos(p.y);
  var dlon = adjust_lon(lon - this.long0);
  var e0, e1, e2, e3, Mlp, Ml, tanphi, Nl1, Nl, psi, Az, G, H, GH, Hs, c, kp, cos_c, s, s2, s3, s4, s5;
  if (this.sphere) {
    if (Math.abs(this.sin_p12 - 1) <= EPSLN) {
      //North Pole case
      p.x = this.x0 + this.a * (HALF_PI - lat) * Math.sin(dlon);
      p.y = this.y0 - this.a * (HALF_PI - lat) * Math.cos(dlon);
      return p;
    }
    else if (Math.abs(this.sin_p12 + 1) <= EPSLN) {
      //South Pole case
      p.x = this.x0 + this.a * (HALF_PI + lat) * Math.sin(dlon);
      p.y = this.y0 + this.a * (HALF_PI + lat) * Math.cos(dlon);
      return p;
    }
    else {
      //default case
      cos_c = this.sin_p12 * sinphi + this.cos_p12 * cosphi * Math.cos(dlon);
      c = Math.acos(cos_c);
      kp = c / Math.sin(c);
      p.x = this.x0 + this.a * kp * cosphi * Math.sin(dlon);
      p.y = this.y0 + this.a * kp * (this.cos_p12 * sinphi - this.sin_p12 * cosphi * Math.cos(dlon));
      return p;
    }
  }
  else {
    e0 = e0fn(this.es);
    e1 = e1fn(this.es);
    e2 = e2fn(this.es);
    e3 = e3fn(this.es);
    if (Math.abs(this.sin_p12 - 1) <= EPSLN) {
      //North Pole case
      Mlp = this.a * mlfn(e0, e1, e2, e3, HALF_PI);
      Ml = this.a * mlfn(e0, e1, e2, e3, lat);
      p.x = this.x0 + (Mlp - Ml) * Math.sin(dlon);
      p.y = this.y0 - (Mlp - Ml) * Math.cos(dlon);
      return p;
    }
    else if (Math.abs(this.sin_p12 + 1) <= EPSLN) {
      //South Pole case
      Mlp = this.a * mlfn(e0, e1, e2, e3, HALF_PI);
      Ml = this.a * mlfn(e0, e1, e2, e3, lat);
      p.x = this.x0 + (Mlp + Ml) * Math.sin(dlon);
      p.y = this.y0 + (Mlp + Ml) * Math.cos(dlon);
      return p;
    }
    else {
      //Default case
      tanphi = sinphi / cosphi;
      Nl1 = gN(this.a, this.e, this.sin_p12);
      Nl = gN(this.a, this.e, sinphi);
      psi = Math.atan((1 - this.es) * tanphi + this.es * Nl1 * this.sin_p12 / (Nl * cosphi));
      Az = Math.atan2(Math.sin(dlon), this.cos_p12 * Math.tan(psi) - this.sin_p12 * Math.cos(dlon));
      if (Az === 0) {
        s = Math.asin(this.cos_p12 * Math.sin(psi) - this.sin_p12 * Math.cos(psi));
      }
      else if (Math.abs(Math.abs(Az) - Math.PI) <= EPSLN) {
        s = -Math.asin(this.cos_p12 * Math.sin(psi) - this.sin_p12 * Math.cos(psi));
      }
      else {
        s = Math.asin(Math.sin(dlon) * Math.cos(psi) / Math.sin(Az));
      }
      G = this.e * this.sin_p12 / Math.sqrt(1 - this.es);
      H = this.e * this.cos_p12 * Math.cos(Az) / Math.sqrt(1 - this.es);
      GH = G * H;
      Hs = H * H;
      s2 = s * s;
      s3 = s2 * s;
      s4 = s3 * s;
      s5 = s4 * s;
      c = Nl1 * s * (1 - s2 * Hs * (1 - Hs) / 6 + s3 / 8 * GH * (1 - 2 * Hs) + s4 / 120 * (Hs * (4 - 7 * Hs) - 3 * G * G * (1 - 7 * Hs)) - s5 / 48 * GH);
      p.x = this.x0 + c * Math.sin(Az);
      p.y = this.y0 + c * Math.cos(Az);
      return p;
    }
  }


};

exports.inverse = function(p) {
  p.x -= this.x0;
  p.y -= this.y0;
  var rh, z, sinz, cosz, lon, lat, con, e0, e1, e2, e3, Mlp, M, N1, psi, Az, cosAz, tmp, A, B, D, Ee, F;
  if (this.sphere) {
    rh = Math.sqrt(p.x * p.x + p.y * p.y);
    if (rh > (2 * HALF_PI * this.a)) {
      return;
    }
    z = rh / this.a;

    sinz = Math.sin(z);
    cosz = Math.cos(z);

    lon = this.long0;
    if (Math.abs(rh) <= EPSLN) {
      lat = this.lat0;
    }
    else {
      lat = asinz(cosz * this.sin_p12 + (p.y * sinz * this.cos_p12) / rh);
      con = Math.abs(this.lat0) - HALF_PI;
      if (Math.abs(con) <= EPSLN) {
        if (this.lat0 >= 0) {
          lon = adjust_lon(this.long0 + Math.atan2(p.x, - p.y));
        }
        else {
          lon = adjust_lon(this.long0 - Math.atan2(-p.x, p.y));
        }
      }
      else {
        /*con = cosz - this.sin_p12 * Math.sin(lat);
        if ((Math.abs(con) < EPSLN) && (Math.abs(p.x) < EPSLN)) {
          //no-op, just keep the lon value as is
        } else {
          var temp = Math.atan2((p.x * sinz * this.cos_p12), (con * rh));
          lon = adjust_lon(this.long0 + Math.atan2((p.x * sinz * this.cos_p12), (con * rh)));
        }*/
        lon = adjust_lon(this.long0 + Math.atan2(p.x * sinz, rh * this.cos_p12 * cosz - p.y * this.sin_p12 * sinz));
      }
    }

    p.x = lon;
    p.y = lat;
    return p;
  }
  else {
    e0 = e0fn(this.es);
    e1 = e1fn(this.es);
    e2 = e2fn(this.es);
    e3 = e3fn(this.es);
    if (Math.abs(this.sin_p12 - 1) <= EPSLN) {
      //North pole case
      Mlp = this.a * mlfn(e0, e1, e2, e3, HALF_PI);
      rh = Math.sqrt(p.x * p.x + p.y * p.y);
      M = Mlp - rh;
      lat = imlfn(M / this.a, e0, e1, e2, e3);
      lon = adjust_lon(this.long0 + Math.atan2(p.x, - 1 * p.y));
      p.x = lon;
      p.y = lat;
      return p;
    }
    else if (Math.abs(this.sin_p12 + 1) <= EPSLN) {
      //South pole case
      Mlp = this.a * mlfn(e0, e1, e2, e3, HALF_PI);
      rh = Math.sqrt(p.x * p.x + p.y * p.y);
      M = rh - Mlp;

      lat = imlfn(M / this.a, e0, e1, e2, e3);
      lon = adjust_lon(this.long0 + Math.atan2(p.x, p.y));
      p.x = lon;
      p.y = lat;
      return p;
    }
    else {
      //default case
      rh = Math.sqrt(p.x * p.x + p.y * p.y);
      Az = Math.atan2(p.x, p.y);
      N1 = gN(this.a, this.e, this.sin_p12);
      cosAz = Math.cos(Az);
      tmp = this.e * this.cos_p12 * cosAz;
      A = -tmp * tmp / (1 - this.es);
      B = 3 * this.es * (1 - A) * this.sin_p12 * this.cos_p12 * cosAz / (1 - this.es);
      D = rh / N1;
      Ee = D - A * (1 + A) * Math.pow(D, 3) / 6 - B * (1 + 3 * A) * Math.pow(D, 4) / 24;
      F = 1 - A * Ee * Ee / 2 - D * Ee * Ee * Ee / 6;
      psi = Math.asin(this.sin_p12 * Math.cos(Ee) + this.cos_p12 * Math.sin(Ee) * cosAz);
      lon = adjust_lon(this.long0 + Math.asin(Math.sin(Az) * Math.sin(Ee) / Math.cos(psi)));
      lat = Math.atan((1 - this.es * F * this.sin_p12 / Math.sin(psi)) * Math.tan(psi) / (1 - this.es));
      p.x = lon;
      p.y = lat;
      return p;
    }
  }

};
exports.names = ["Azimuthal_Equidistant", "aeqd"];

},{"../common/adjust_lon":5,"../common/asinz":6,"../common/e0fn":7,"../common/e1fn":8,"../common/e2fn":9,"../common/e3fn":10,"../common/gN":11,"../common/imlfn":12,"../common/mlfn":14}],42:[function(_dereq_,module,exports){
var mlfn = _dereq_('../common/mlfn');
var e0fn = _dereq_('../common/e0fn');
var e1fn = _dereq_('../common/e1fn');
var e2fn = _dereq_('../common/e2fn');
var e3fn = _dereq_('../common/e3fn');
var gN = _dereq_('../common/gN');
var adjust_lon = _dereq_('../common/adjust_lon');
var adjust_lat = _dereq_('../common/adjust_lat');
var imlfn = _dereq_('../common/imlfn');
var HALF_PI = Math.PI/2;
var EPSLN = 1.0e-10;
exports.init = function() {
  if (!this.sphere) {
    this.e0 = e0fn(this.es);
    this.e1 = e1fn(this.es);
    this.e2 = e2fn(this.es);
    this.e3 = e3fn(this.es);
    this.ml0 = this.a * mlfn(this.e0, this.e1, this.e2, this.e3, this.lat0);
  }
};



/* Cassini forward equations--mapping lat,long to x,y
  -----------------------------------------------------------------------*/
exports.forward = function(p) {

  /* Forward equations
      -----------------*/
  var x, y;
  var lam = p.x;
  var phi = p.y;
  lam = adjust_lon(lam - this.long0);

  if (this.sphere) {
    x = this.a * Math.asin(Math.cos(phi) * Math.sin(lam));
    y = this.a * (Math.atan2(Math.tan(phi), Math.cos(lam)) - this.lat0);
  }
  else {
    //ellipsoid
    var sinphi = Math.sin(phi);
    var cosphi = Math.cos(phi);
    var nl = gN(this.a, this.e, sinphi);
    var tl = Math.tan(phi) * Math.tan(phi);
    var al = lam * Math.cos(phi);
    var asq = al * al;
    var cl = this.es * cosphi * cosphi / (1 - this.es);
    var ml = this.a * mlfn(this.e0, this.e1, this.e2, this.e3, phi);

    x = nl * al * (1 - asq * tl * (1 / 6 - (8 - tl + 8 * cl) * asq / 120));
    y = ml - this.ml0 + nl * sinphi / cosphi * asq * (0.5 + (5 - tl + 6 * cl) * asq / 24);


  }

  p.x = x + this.x0;
  p.y = y + this.y0;
  return p;
};

/* Inverse equations
  -----------------*/
exports.inverse = function(p) {
  p.x -= this.x0;
  p.y -= this.y0;
  var x = p.x / this.a;
  var y = p.y / this.a;
  var phi, lam;

  if (this.sphere) {
    var dd = y + this.lat0;
    phi = Math.asin(Math.sin(dd) * Math.cos(x));
    lam = Math.atan2(Math.tan(x), Math.cos(dd));
  }
  else {
    /* ellipsoid */
    var ml1 = this.ml0 / this.a + y;
    var phi1 = imlfn(ml1, this.e0, this.e1, this.e2, this.e3);
    if (Math.abs(Math.abs(phi1) - HALF_PI) <= EPSLN) {
      p.x = this.long0;
      p.y = HALF_PI;
      if (y < 0) {
        p.y *= -1;
      }
      return p;
    }
    var nl1 = gN(this.a, this.e, Math.sin(phi1));

    var rl1 = nl1 * nl1 * nl1 / this.a / this.a * (1 - this.es);
    var tl1 = Math.pow(Math.tan(phi1), 2);
    var dl = x * this.a / nl1;
    var dsq = dl * dl;
    phi = phi1 - nl1 * Math.tan(phi1) / rl1 * dl * dl * (0.5 - (1 + 3 * tl1) * dl * dl / 24);
    lam = dl * (1 - dsq * (tl1 / 3 + (1 + 3 * tl1) * tl1 * dsq / 15)) / Math.cos(phi1);

  }

  p.x = adjust_lon(lam + this.long0);
  p.y = adjust_lat(phi);
  return p;

};
exports.names = ["Cassini", "Cassini_Soldner", "cass"];
},{"../common/adjust_lat":4,"../common/adjust_lon":5,"../common/e0fn":7,"../common/e1fn":8,"../common/e2fn":9,"../common/e3fn":10,"../common/gN":11,"../common/imlfn":12,"../common/mlfn":14}],43:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
var qsfnz = _dereq_('../common/qsfnz');
var msfnz = _dereq_('../common/msfnz');
var iqsfnz = _dereq_('../common/iqsfnz');
/*
  reference:  
    "Cartographic Projection Procedures for the UNIX Environment-
    A User's Manual" by Gerald I. Evenden,
    USGS Open File Report 90-284and Release 4 Interim Reports (200
* 
*/
exports.init = function() {
  //no-op
  if (!this.sphere) {
    this.k0 = msfnz(this.e, Math.sin(this.lat_ts), Math.cos(this.lat_ts));
  }
};


/* Cylindrical Equal Area forward equations--mapping lat,long to x,y
    ------------------------------------------------------------*/
exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  var x, y;
  /* Forward equations
      -----------------*/
  var dlon = adjust_lon(lon - this.long0);
  if (this.sphere) {
    x = this.x0 + this.a * dlon * Math.cos(this.lat_ts);
    y = this.y0 + this.a * Math.sin(lat) / Math.cos(this.lat_ts);
  }
  else {
    var qs = qsfnz(this.e, Math.sin(lat));
    x = this.x0 + this.a * this.k0 * dlon;
    y = this.y0 + this.a * qs * 0.5 / this.k0;
  }

  p.x = x;
  p.y = y;
  return p;
};

/* Cylindrical Equal Area inverse equations--mapping x,y to lat/long
    ------------------------------------------------------------*/
exports.inverse = function(p) {
  p.x -= this.x0;
  p.y -= this.y0;
  var lon, lat;

  if (this.sphere) {
    lon = adjust_lon(this.long0 + (p.x / this.a) / Math.cos(this.lat_ts));
    lat = Math.asin((p.y / this.a) * Math.cos(this.lat_ts));
  }
  else {
    lat = iqsfnz(this.e, 2 * p.y * this.k0 / this.a);
    lon = adjust_lon(this.long0 + p.x / (this.a * this.k0));
  }

  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["cea"];

},{"../common/adjust_lon":5,"../common/iqsfnz":13,"../common/msfnz":15,"../common/qsfnz":20}],44:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
var adjust_lat = _dereq_('../common/adjust_lat');
exports.init = function() {

  this.x0 = this.x0 || 0;
  this.y0 = this.y0 || 0;
  this.lat0 = this.lat0 || 0;
  this.long0 = this.long0 || 0;
  this.lat_ts = this.lat_ts || 0;
  this.title = this.title || "Equidistant Cylindrical (Plate Carre)";

  this.rc = Math.cos(this.lat_ts);
};


// forward equations--mapping lat,long to x,y
// -----------------------------------------------------------------
exports.forward = function(p) {

  var lon = p.x;
  var lat = p.y;

  var dlon = adjust_lon(lon - this.long0);
  var dlat = adjust_lat(lat - this.lat0);
  p.x = this.x0 + (this.a * dlon * this.rc);
  p.y = this.y0 + (this.a * dlat);
  return p;
};

// inverse equations--mapping x,y to lat/long
// -----------------------------------------------------------------
exports.inverse = function(p) {

  var x = p.x;
  var y = p.y;

  p.x = adjust_lon(this.long0 + ((x - this.x0) / (this.a * this.rc)));
  p.y = adjust_lat(this.lat0 + ((y - this.y0) / (this.a)));
  return p;
};
exports.names = ["Equirectangular", "Equidistant_Cylindrical", "eqc"];

},{"../common/adjust_lat":4,"../common/adjust_lon":5}],45:[function(_dereq_,module,exports){
var e0fn = _dereq_('../common/e0fn');
var e1fn = _dereq_('../common/e1fn');
var e2fn = _dereq_('../common/e2fn');
var e3fn = _dereq_('../common/e3fn');
var msfnz = _dereq_('../common/msfnz');
var mlfn = _dereq_('../common/mlfn');
var adjust_lon = _dereq_('../common/adjust_lon');
var adjust_lat = _dereq_('../common/adjust_lat');
var imlfn = _dereq_('../common/imlfn');
var EPSLN = 1.0e-10;
exports.init = function() {

  /* Place parameters in static storage for common use
      -------------------------------------------------*/
  // Standard Parallels cannot be equal and on opposite sides of the equator
  if (Math.abs(this.lat1 + this.lat2) < EPSLN) {
    return;
  }
  this.lat2 = this.lat2 || this.lat1;
  this.temp = this.b / this.a;
  this.es = 1 - Math.pow(this.temp, 2);
  this.e = Math.sqrt(this.es);
  this.e0 = e0fn(this.es);
  this.e1 = e1fn(this.es);
  this.e2 = e2fn(this.es);
  this.e3 = e3fn(this.es);

  this.sinphi = Math.sin(this.lat1);
  this.cosphi = Math.cos(this.lat1);

  this.ms1 = msfnz(this.e, this.sinphi, this.cosphi);
  this.ml1 = mlfn(this.e0, this.e1, this.e2, this.e3, this.lat1);

  if (Math.abs(this.lat1 - this.lat2) < EPSLN) {
    this.ns = this.sinphi;
  }
  else {
    this.sinphi = Math.sin(this.lat2);
    this.cosphi = Math.cos(this.lat2);
    this.ms2 = msfnz(this.e, this.sinphi, this.cosphi);
    this.ml2 = mlfn(this.e0, this.e1, this.e2, this.e3, this.lat2);
    this.ns = (this.ms1 - this.ms2) / (this.ml2 - this.ml1);
  }
  this.g = this.ml1 + this.ms1 / this.ns;
  this.ml0 = mlfn(this.e0, this.e1, this.e2, this.e3, this.lat0);
  this.rh = this.a * (this.g - this.ml0);
};


/* Equidistant Conic forward equations--mapping lat,long to x,y
  -----------------------------------------------------------*/
exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  var rh1;

  /* Forward equations
      -----------------*/
  if (this.sphere) {
    rh1 = this.a * (this.g - lat);
  }
  else {
    var ml = mlfn(this.e0, this.e1, this.e2, this.e3, lat);
    rh1 = this.a * (this.g - ml);
  }
  var theta = this.ns * adjust_lon(lon - this.long0);
  var x = this.x0 + rh1 * Math.sin(theta);
  var y = this.y0 + this.rh - rh1 * Math.cos(theta);
  p.x = x;
  p.y = y;
  return p;
};

/* Inverse equations
  -----------------*/
exports.inverse = function(p) {
  p.x -= this.x0;
  p.y = this.rh - p.y + this.y0;
  var con, rh1, lat, lon;
  if (this.ns >= 0) {
    rh1 = Math.sqrt(p.x * p.x + p.y * p.y);
    con = 1;
  }
  else {
    rh1 = -Math.sqrt(p.x * p.x + p.y * p.y);
    con = -1;
  }
  var theta = 0;
  if (rh1 !== 0) {
    theta = Math.atan2(con * p.x, con * p.y);
  }

  if (this.sphere) {
    lon = adjust_lon(this.long0 + theta / this.ns);
    lat = adjust_lat(this.g - rh1 / this.a);
    p.x = lon;
    p.y = lat;
    return p;
  }
  else {
    var ml = this.g - rh1 / this.a;
    lat = imlfn(ml, this.e0, this.e1, this.e2, this.e3);
    lon = adjust_lon(this.long0 + theta / this.ns);
    p.x = lon;
    p.y = lat;
    return p;
  }

};
exports.names = ["Equidistant_Conic", "eqdc"];

},{"../common/adjust_lat":4,"../common/adjust_lon":5,"../common/e0fn":7,"../common/e1fn":8,"../common/e2fn":9,"../common/e3fn":10,"../common/imlfn":12,"../common/mlfn":14,"../common/msfnz":15}],46:[function(_dereq_,module,exports){
var FORTPI = Math.PI/4;
var srat = _dereq_('../common/srat');
var HALF_PI = Math.PI/2;
var MAX_ITER = 20;
exports.init = function() {
  var sphi = Math.sin(this.lat0);
  var cphi = Math.cos(this.lat0);
  cphi *= cphi;
  this.rc = Math.sqrt(1 - this.es) / (1 - this.es * sphi * sphi);
  this.C = Math.sqrt(1 + this.es * cphi * cphi / (1 - this.es));
  this.phic0 = Math.asin(sphi / this.C);
  this.ratexp = 0.5 * this.C * this.e;
  this.K = Math.tan(0.5 * this.phic0 + FORTPI) / (Math.pow(Math.tan(0.5 * this.lat0 + FORTPI), this.C) * srat(this.e * sphi, this.ratexp));
};

exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;

  p.y = 2 * Math.atan(this.K * Math.pow(Math.tan(0.5 * lat + FORTPI), this.C) * srat(this.e * Math.sin(lat), this.ratexp)) - HALF_PI;
  p.x = this.C * lon;
  return p;
};

exports.inverse = function(p) {
  var DEL_TOL = 1e-14;
  var lon = p.x / this.C;
  var lat = p.y;
  var num = Math.pow(Math.tan(0.5 * lat + FORTPI) / this.K, 1 / this.C);
  for (var i = MAX_ITER; i > 0; --i) {
    lat = 2 * Math.atan(num * srat(this.e * Math.sin(p.y), - 0.5 * this.e)) - HALF_PI;
    if (Math.abs(lat - p.y) < DEL_TOL) {
      break;
    }
    p.y = lat;
  }
  /* convergence failed */
  if (!i) {
    return null;
  }
  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["gauss"];

},{"../common/srat":22}],47:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
var EPSLN = 1.0e-10;
var asinz = _dereq_('../common/asinz');

/*
  reference:
    Wolfram Mathworld "Gnomonic Projection"
    http://mathworld.wolfram.com/GnomonicProjection.html
    Accessed: 12th November 2009
  */
exports.init = function() {

  /* Place parameters in static storage for common use
      -------------------------------------------------*/
  this.sin_p14 = Math.sin(this.lat0);
  this.cos_p14 = Math.cos(this.lat0);
  // Approximation for projecting points to the horizon (infinity)
  this.infinity_dist = 1000 * this.a;
  this.rc = 1;
};


/* Gnomonic forward equations--mapping lat,long to x,y
    ---------------------------------------------------*/
exports.forward = function(p) {
  var sinphi, cosphi; /* sin and cos value        */
  var dlon; /* delta longitude value      */
  var coslon; /* cos of longitude        */
  var ksp; /* scale factor          */
  var g;
  var x, y;
  var lon = p.x;
  var lat = p.y;
  /* Forward equations
      -----------------*/
  dlon = adjust_lon(lon - this.long0);

  sinphi = Math.sin(lat);
  cosphi = Math.cos(lat);

  coslon = Math.cos(dlon);
  g = this.sin_p14 * sinphi + this.cos_p14 * cosphi * coslon;
  ksp = 1;
  if ((g > 0) || (Math.abs(g) <= EPSLN)) {
    x = this.x0 + this.a * ksp * cosphi * Math.sin(dlon) / g;
    y = this.y0 + this.a * ksp * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon) / g;
  }
  else {

    // Point is in the opposing hemisphere and is unprojectable
    // We still need to return a reasonable point, so we project 
    // to infinity, on a bearing 
    // equivalent to the northern hemisphere equivalent
    // This is a reasonable approximation for short shapes and lines that 
    // straddle the horizon.

    x = this.x0 + this.infinity_dist * cosphi * Math.sin(dlon);
    y = this.y0 + this.infinity_dist * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon);

  }
  p.x = x;
  p.y = y;
  return p;
};


exports.inverse = function(p) {
  var rh; /* Rho */
  var sinc, cosc;
  var c;
  var lon, lat;

  /* Inverse equations
      -----------------*/
  p.x = (p.x - this.x0) / this.a;
  p.y = (p.y - this.y0) / this.a;

  p.x /= this.k0;
  p.y /= this.k0;

  if ((rh = Math.sqrt(p.x * p.x + p.y * p.y))) {
    c = Math.atan2(rh, this.rc);
    sinc = Math.sin(c);
    cosc = Math.cos(c);

    lat = asinz(cosc * this.sin_p14 + (p.y * sinc * this.cos_p14) / rh);
    lon = Math.atan2(p.x * sinc, rh * this.cos_p14 * cosc - p.y * this.sin_p14 * sinc);
    lon = adjust_lon(this.long0 + lon);
  }
  else {
    lat = this.phic0;
    lon = 0;
  }

  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["gnom"];

},{"../common/adjust_lon":5,"../common/asinz":6}],48:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
exports.init = function() {
  this.a = 6377397.155;
  this.es = 0.006674372230614;
  this.e = Math.sqrt(this.es);
  if (!this.lat0) {
    this.lat0 = 0.863937979737193;
  }
  if (!this.long0) {
    this.long0 = 0.7417649320975901 - 0.308341501185665;
  }
  /* if scale not set default to 0.9999 */
  if (!this.k0) {
    this.k0 = 0.9999;
  }
  this.s45 = 0.785398163397448; /* 45 */
  this.s90 = 2 * this.s45;
  this.fi0 = this.lat0;
  this.e2 = this.es;
  this.e = Math.sqrt(this.e2);
  this.alfa = Math.sqrt(1 + (this.e2 * Math.pow(Math.cos(this.fi0), 4)) / (1 - this.e2));
  this.uq = 1.04216856380474;
  this.u0 = Math.asin(Math.sin(this.fi0) / this.alfa);
  this.g = Math.pow((1 + this.e * Math.sin(this.fi0)) / (1 - this.e * Math.sin(this.fi0)), this.alfa * this.e / 2);
  this.k = Math.tan(this.u0 / 2 + this.s45) / Math.pow(Math.tan(this.fi0 / 2 + this.s45), this.alfa) * this.g;
  this.k1 = this.k0;
  this.n0 = this.a * Math.sqrt(1 - this.e2) / (1 - this.e2 * Math.pow(Math.sin(this.fi0), 2));
  this.s0 = 1.37008346281555;
  this.n = Math.sin(this.s0);
  this.ro0 = this.k1 * this.n0 / Math.tan(this.s0);
  this.ad = this.s90 - this.uq;
};

/* ellipsoid */
/* calculate xy from lat/lon */
/* Constants, identical to inverse transform function */
exports.forward = function(p) {
  var gfi, u, deltav, s, d, eps, ro;
  var lon = p.x;
  var lat = p.y;
  var delta_lon = adjust_lon(lon - this.long0);
  /* Transformation */
  gfi = Math.pow(((1 + this.e * Math.sin(lat)) / (1 - this.e * Math.sin(lat))), (this.alfa * this.e / 2));
  u = 2 * (Math.atan(this.k * Math.pow(Math.tan(lat / 2 + this.s45), this.alfa) / gfi) - this.s45);
  deltav = -delta_lon * this.alfa;
  s = Math.asin(Math.cos(this.ad) * Math.sin(u) + Math.sin(this.ad) * Math.cos(u) * Math.cos(deltav));
  d = Math.asin(Math.cos(u) * Math.sin(deltav) / Math.cos(s));
  eps = this.n * d;
  ro = this.ro0 * Math.pow(Math.tan(this.s0 / 2 + this.s45), this.n) / Math.pow(Math.tan(s / 2 + this.s45), this.n);
  p.y = ro * Math.cos(eps) / 1;
  p.x = ro * Math.sin(eps) / 1;

  if (!this.czech) {
    p.y *= -1;
    p.x *= -1;
  }
  return (p);
};

/* calculate lat/lon from xy */
exports.inverse = function(p) {
  var u, deltav, s, d, eps, ro, fi1;
  var ok;

  /* Transformation */
  /* revert y, x*/
  var tmp = p.x;
  p.x = p.y;
  p.y = tmp;
  if (!this.czech) {
    p.y *= -1;
    p.x *= -1;
  }
  ro = Math.sqrt(p.x * p.x + p.y * p.y);
  eps = Math.atan2(p.y, p.x);
  d = eps / Math.sin(this.s0);
  s = 2 * (Math.atan(Math.pow(this.ro0 / ro, 1 / this.n) * Math.tan(this.s0 / 2 + this.s45)) - this.s45);
  u = Math.asin(Math.cos(this.ad) * Math.sin(s) - Math.sin(this.ad) * Math.cos(s) * Math.cos(d));
  deltav = Math.asin(Math.cos(s) * Math.sin(d) / Math.cos(u));
  p.x = this.long0 - deltav / this.alfa;
  fi1 = u;
  ok = 0;
  var iter = 0;
  do {
    p.y = 2 * (Math.atan(Math.pow(this.k, - 1 / this.alfa) * Math.pow(Math.tan(u / 2 + this.s45), 1 / this.alfa) * Math.pow((1 + this.e * Math.sin(fi1)) / (1 - this.e * Math.sin(fi1)), this.e / 2)) - this.s45);
    if (Math.abs(fi1 - p.y) < 0.0000000001) {
      ok = 1;
    }
    fi1 = p.y;
    iter += 1;
  } while (ok === 0 && iter < 15);
  if (iter >= 15) {
    return null;
  }

  return (p);
};
exports.names = ["Krovak", "krovak"];

},{"../common/adjust_lon":5}],49:[function(_dereq_,module,exports){
var HALF_PI = Math.PI/2;
var FORTPI = Math.PI/4;
var EPSLN = 1.0e-10;
var qsfnz = _dereq_('../common/qsfnz');
var adjust_lon = _dereq_('../common/adjust_lon');
/*
  reference
    "New Equal-Area Map Projections for Noncircular Regions", John P. Snyder,
    The American Cartographer, Vol 15, No. 4, October 1988, pp. 341-355.
  */

exports.S_POLE = 1;
exports.N_POLE = 2;
exports.EQUIT = 3;
exports.OBLIQ = 4;


/* Initialize the Lambert Azimuthal Equal Area projection
  ------------------------------------------------------*/
exports.init = function() {
  var t = Math.abs(this.lat0);
  if (Math.abs(t - HALF_PI) < EPSLN) {
    this.mode = this.lat0 < 0 ? this.S_POLE : this.N_POLE;
  }
  else if (Math.abs(t) < EPSLN) {
    this.mode = this.EQUIT;
  }
  else {
    this.mode = this.OBLIQ;
  }
  if (this.es > 0) {
    var sinphi;

    this.qp = qsfnz(this.e, 1);
    this.mmf = 0.5 / (1 - this.es);
    this.apa = this.authset(this.es);
    switch (this.mode) {
    case this.N_POLE:
      this.dd = 1;
      break;
    case this.S_POLE:
      this.dd = 1;
      break;
    case this.EQUIT:
      this.rq = Math.sqrt(0.5 * this.qp);
      this.dd = 1 / this.rq;
      this.xmf = 1;
      this.ymf = 0.5 * this.qp;
      break;
    case this.OBLIQ:
      this.rq = Math.sqrt(0.5 * this.qp);
      sinphi = Math.sin(this.lat0);
      this.sinb1 = qsfnz(this.e, sinphi) / this.qp;
      this.cosb1 = Math.sqrt(1 - this.sinb1 * this.sinb1);
      this.dd = Math.cos(this.lat0) / (Math.sqrt(1 - this.es * sinphi * sinphi) * this.rq * this.cosb1);
      this.ymf = (this.xmf = this.rq) / this.dd;
      this.xmf *= this.dd;
      break;
    }
  }
  else {
    if (this.mode === this.OBLIQ) {
      this.sinph0 = Math.sin(this.lat0);
      this.cosph0 = Math.cos(this.lat0);
    }
  }
};

/* Lambert Azimuthal Equal Area forward equations--mapping lat,long to x,y
  -----------------------------------------------------------------------*/
exports.forward = function(p) {

  /* Forward equations
      -----------------*/
  var x, y, coslam, sinlam, sinphi, q, sinb, cosb, b, cosphi;
  var lam = p.x;
  var phi = p.y;

  lam = adjust_lon(lam - this.long0);

  if (this.sphere) {
    sinphi = Math.sin(phi);
    cosphi = Math.cos(phi);
    coslam = Math.cos(lam);
    if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
      y = (this.mode === this.EQUIT) ? 1 + cosphi * coslam : 1 + this.sinph0 * sinphi + this.cosph0 * cosphi * coslam;
      if (y <= EPSLN) {
        return null;
      }
      y = Math.sqrt(2 / y);
      x = y * cosphi * Math.sin(lam);
      y *= (this.mode === this.EQUIT) ? sinphi : this.cosph0 * sinphi - this.sinph0 * cosphi * coslam;
    }
    else if (this.mode === this.N_POLE || this.mode === this.S_POLE) {
      if (this.mode === this.N_POLE) {
        coslam = -coslam;
      }
      if (Math.abs(phi + this.phi0) < EPSLN) {
        return null;
      }
      y = FORTPI - phi * 0.5;
      y = 2 * ((this.mode === this.S_POLE) ? Math.cos(y) : Math.sin(y));
      x = y * Math.sin(lam);
      y *= coslam;
    }
  }
  else {
    sinb = 0;
    cosb = 0;
    b = 0;
    coslam = Math.cos(lam);
    sinlam = Math.sin(lam);
    sinphi = Math.sin(phi);
    q = qsfnz(this.e, sinphi);
    if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
      sinb = q / this.qp;
      cosb = Math.sqrt(1 - sinb * sinb);
    }
    switch (this.mode) {
    case this.OBLIQ:
      b = 1 + this.sinb1 * sinb + this.cosb1 * cosb * coslam;
      break;
    case this.EQUIT:
      b = 1 + cosb * coslam;
      break;
    case this.N_POLE:
      b = HALF_PI + phi;
      q = this.qp - q;
      break;
    case this.S_POLE:
      b = phi - HALF_PI;
      q = this.qp + q;
      break;
    }
    if (Math.abs(b) < EPSLN) {
      return null;
    }
    switch (this.mode) {
    case this.OBLIQ:
    case this.EQUIT:
      b = Math.sqrt(2 / b);
      if (this.mode === this.OBLIQ) {
        y = this.ymf * b * (this.cosb1 * sinb - this.sinb1 * cosb * coslam);
      }
      else {
        y = (b = Math.sqrt(2 / (1 + cosb * coslam))) * sinb * this.ymf;
      }
      x = this.xmf * b * cosb * sinlam;
      break;
    case this.N_POLE:
    case this.S_POLE:
      if (q >= 0) {
        x = (b = Math.sqrt(q)) * sinlam;
        y = coslam * ((this.mode === this.S_POLE) ? b : -b);
      }
      else {
        x = y = 0;
      }
      break;
    }
  }

  p.x = this.a * x + this.x0;
  p.y = this.a * y + this.y0;
  return p;
};

/* Inverse equations
  -----------------*/
exports.inverse = function(p) {
  p.x -= this.x0;
  p.y -= this.y0;
  var x = p.x / this.a;
  var y = p.y / this.a;
  var lam, phi, cCe, sCe, q, rho, ab;

  if (this.sphere) {
    var cosz = 0,
      rh, sinz = 0;

    rh = Math.sqrt(x * x + y * y);
    phi = rh * 0.5;
    if (phi > 1) {
      return null;
    }
    phi = 2 * Math.asin(phi);
    if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
      sinz = Math.sin(phi);
      cosz = Math.cos(phi);
    }
    switch (this.mode) {
    case this.EQUIT:
      phi = (Math.abs(rh) <= EPSLN) ? 0 : Math.asin(y * sinz / rh);
      x *= sinz;
      y = cosz * rh;
      break;
    case this.OBLIQ:
      phi = (Math.abs(rh) <= EPSLN) ? this.phi0 : Math.asin(cosz * this.sinph0 + y * sinz * this.cosph0 / rh);
      x *= sinz * this.cosph0;
      y = (cosz - Math.sin(phi) * this.sinph0) * rh;
      break;
    case this.N_POLE:
      y = -y;
      phi = HALF_PI - phi;
      break;
    case this.S_POLE:
      phi -= HALF_PI;
      break;
    }
    lam = (y === 0 && (this.mode === this.EQUIT || this.mode === this.OBLIQ)) ? 0 : Math.atan2(x, y);
  }
  else {
    ab = 0;
    if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
      x /= this.dd;
      y *= this.dd;
      rho = Math.sqrt(x * x + y * y);
      if (rho < EPSLN) {
        p.x = 0;
        p.y = this.phi0;
        return p;
      }
      sCe = 2 * Math.asin(0.5 * rho / this.rq);
      cCe = Math.cos(sCe);
      x *= (sCe = Math.sin(sCe));
      if (this.mode === this.OBLIQ) {
        ab = cCe * this.sinb1 + y * sCe * this.cosb1 / rho;
        q = this.qp * ab;
        y = rho * this.cosb1 * cCe - y * this.sinb1 * sCe;
      }
      else {
        ab = y * sCe / rho;
        q = this.qp * ab;
        y = rho * cCe;
      }
    }
    else if (this.mode === this.N_POLE || this.mode === this.S_POLE) {
      if (this.mode === this.N_POLE) {
        y = -y;
      }
      q = (x * x + y * y);
      if (!q) {
        p.x = 0;
        p.y = this.phi0;
        return p;
      }
      ab = 1 - q / this.qp;
      if (this.mode === this.S_POLE) {
        ab = -ab;
      }
    }
    lam = Math.atan2(x, y);
    phi = this.authlat(Math.asin(ab), this.apa);
  }


  p.x = adjust_lon(this.long0 + lam);
  p.y = phi;
  return p;
};

/* determine latitude from authalic latitude */
exports.P00 = 0.33333333333333333333;
exports.P01 = 0.17222222222222222222;
exports.P02 = 0.10257936507936507936;
exports.P10 = 0.06388888888888888888;
exports.P11 = 0.06640211640211640211;
exports.P20 = 0.01641501294219154443;

exports.authset = function(es) {
  var t;
  var APA = [];
  APA[0] = es * this.P00;
  t = es * es;
  APA[0] += t * this.P01;
  APA[1] = t * this.P10;
  t *= es;
  APA[0] += t * this.P02;
  APA[1] += t * this.P11;
  APA[2] = t * this.P20;
  return APA;
};

exports.authlat = function(beta, APA) {
  var t = beta + beta;
  return (beta + APA[0] * Math.sin(t) + APA[1] * Math.sin(t + t) + APA[2] * Math.sin(t + t + t));
};
exports.names = ["Lambert Azimuthal Equal Area", "Lambert_Azimuthal_Equal_Area", "laea"];

},{"../common/adjust_lon":5,"../common/qsfnz":20}],50:[function(_dereq_,module,exports){
var EPSLN = 1.0e-10;
var msfnz = _dereq_('../common/msfnz');
var tsfnz = _dereq_('../common/tsfnz');
var HALF_PI = Math.PI/2;
var sign = _dereq_('../common/sign');
var adjust_lon = _dereq_('../common/adjust_lon');
var phi2z = _dereq_('../common/phi2z');
exports.init = function() {

  // array of:  r_maj,r_min,lat1,lat2,c_lon,c_lat,false_east,false_north
  //double c_lat;                   /* center latitude                      */
  //double c_lon;                   /* center longitude                     */
  //double lat1;                    /* first standard parallel              */
  //double lat2;                    /* second standard parallel             */
  //double r_maj;                   /* major axis                           */
  //double r_min;                   /* minor axis                           */
  //double false_east;              /* x offset in meters                   */
  //double false_north;             /* y offset in meters                   */

  if (!this.lat2) {
    this.lat2 = this.lat1;
  } //if lat2 is not defined
  if (!this.k0) {
    this.k0 = 1;
  }
  this.x0 = this.x0 || 0;
  this.y0 = this.y0 || 0;
  // Standard Parallels cannot be equal and on opposite sides of the equator
  if (Math.abs(this.lat1 + this.lat2) < EPSLN) {
    return;
  }

  var temp = this.b / this.a;
  this.e = Math.sqrt(1 - temp * temp);

  var sin1 = Math.sin(this.lat1);
  var cos1 = Math.cos(this.lat1);
  var ms1 = msfnz(this.e, sin1, cos1);
  var ts1 = tsfnz(this.e, this.lat1, sin1);

  var sin2 = Math.sin(this.lat2);
  var cos2 = Math.cos(this.lat2);
  var ms2 = msfnz(this.e, sin2, cos2);
  var ts2 = tsfnz(this.e, this.lat2, sin2);

  var ts0 = tsfnz(this.e, this.lat0, Math.sin(this.lat0));

  if (Math.abs(this.lat1 - this.lat2) > EPSLN) {
    this.ns = Math.log(ms1 / ms2) / Math.log(ts1 / ts2);
  }
  else {
    this.ns = sin1;
  }
  if (isNaN(this.ns)) {
    this.ns = sin1;
  }
  this.f0 = ms1 / (this.ns * Math.pow(ts1, this.ns));
  this.rh = this.a * this.f0 * Math.pow(ts0, this.ns);
  if (!this.title) {
    this.title = "Lambert Conformal Conic";
  }
};


// Lambert Conformal conic forward equations--mapping lat,long to x,y
// -----------------------------------------------------------------
exports.forward = function(p) {

  var lon = p.x;
  var lat = p.y;

  // singular cases :
  if (Math.abs(2 * Math.abs(lat) - Math.PI) <= EPSLN) {
    lat = sign(lat) * (HALF_PI - 2 * EPSLN);
  }

  var con = Math.abs(Math.abs(lat) - HALF_PI);
  var ts, rh1;
  if (con > EPSLN) {
    ts = tsfnz(this.e, lat, Math.sin(lat));
    rh1 = this.a * this.f0 * Math.pow(ts, this.ns);
  }
  else {
    con = lat * this.ns;
    if (con <= 0) {
      return null;
    }
    rh1 = 0;
  }
  var theta = this.ns * adjust_lon(lon - this.long0);
  p.x = this.k0 * (rh1 * Math.sin(theta)) + this.x0;
  p.y = this.k0 * (this.rh - rh1 * Math.cos(theta)) + this.y0;

  return p;
};

// Lambert Conformal Conic inverse equations--mapping x,y to lat/long
// -----------------------------------------------------------------
exports.inverse = function(p) {

  var rh1, con, ts;
  var lat, lon;
  var x = (p.x - this.x0) / this.k0;
  var y = (this.rh - (p.y - this.y0) / this.k0);
  if (this.ns > 0) {
    rh1 = Math.sqrt(x * x + y * y);
    con = 1;
  }
  else {
    rh1 = -Math.sqrt(x * x + y * y);
    con = -1;
  }
  var theta = 0;
  if (rh1 !== 0) {
    theta = Math.atan2((con * x), (con * y));
  }
  if ((rh1 !== 0) || (this.ns > 0)) {
    con = 1 / this.ns;
    ts = Math.pow((rh1 / (this.a * this.f0)), con);
    lat = phi2z(this.e, ts);
    if (lat === -9999) {
      return null;
    }
  }
  else {
    lat = -HALF_PI;
  }
  lon = adjust_lon(theta / this.ns + this.long0);

  p.x = lon;
  p.y = lat;
  return p;
};

exports.names = ["Lambert Tangential Conformal Conic Projection", "Lambert_Conformal_Conic", "Lambert_Conformal_Conic_2SP", "lcc"];

},{"../common/adjust_lon":5,"../common/msfnz":15,"../common/phi2z":16,"../common/sign":21,"../common/tsfnz":24}],51:[function(_dereq_,module,exports){
exports.init = function() {
  //no-op for longlat
};

function identity(pt) {
  return pt;
}
exports.forward = identity;
exports.inverse = identity;
exports.names = ["longlat", "identity"];

},{}],52:[function(_dereq_,module,exports){
var msfnz = _dereq_('../common/msfnz');
var HALF_PI = Math.PI/2;
var EPSLN = 1.0e-10;
var R2D = 57.29577951308232088;
var adjust_lon = _dereq_('../common/adjust_lon');
var FORTPI = Math.PI/4;
var tsfnz = _dereq_('../common/tsfnz');
var phi2z = _dereq_('../common/phi2z');
exports.init = function() {
  var con = this.b / this.a;
  this.es = 1 - con * con;
  if(!('x0' in this)){
    this.x0 = 0;
  }
  if(!('y0' in this)){
    this.y0 = 0;
  }
  this.e = Math.sqrt(this.es);
  if (this.lat_ts) {
    if (this.sphere) {
      this.k0 = Math.cos(this.lat_ts);
    }
    else {
      this.k0 = msfnz(this.e, Math.sin(this.lat_ts), Math.cos(this.lat_ts));
    }
  }
  else {
    if (!this.k0) {
      if (this.k) {
        this.k0 = this.k;
      }
      else {
        this.k0 = 1;
      }
    }
  }
};

/* Mercator forward equations--mapping lat,long to x,y
  --------------------------------------------------*/

exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  // convert to radians
  if (lat * R2D > 90 && lat * R2D < -90 && lon * R2D > 180 && lon * R2D < -180) {
    return null;
  }

  var x, y;
  if (Math.abs(Math.abs(lat) - HALF_PI) <= EPSLN) {
    return null;
  }
  else {
    if (this.sphere) {
      x = this.x0 + this.a * this.k0 * adjust_lon(lon - this.long0);
      y = this.y0 + this.a * this.k0 * Math.log(Math.tan(FORTPI + 0.5 * lat));
    }
    else {
      var sinphi = Math.sin(lat);
      var ts = tsfnz(this.e, lat, sinphi);
      x = this.x0 + this.a * this.k0 * adjust_lon(lon - this.long0);
      y = this.y0 - this.a * this.k0 * Math.log(ts);
    }
    p.x = x;
    p.y = y;
    return p;
  }
};


/* Mercator inverse equations--mapping x,y to lat/long
  --------------------------------------------------*/
exports.inverse = function(p) {

  var x = p.x - this.x0;
  var y = p.y - this.y0;
  var lon, lat;

  if (this.sphere) {
    lat = HALF_PI - 2 * Math.atan(Math.exp(-y / (this.a * this.k0)));
  }
  else {
    var ts = Math.exp(-y / (this.a * this.k0));
    lat = phi2z(this.e, ts);
    if (lat === -9999) {
      return null;
    }
  }
  lon = adjust_lon(this.long0 + x / (this.a * this.k0));

  p.x = lon;
  p.y = lat;
  return p;
};

exports.names = ["Mercator", "Popular Visualisation Pseudo Mercator", "Mercator_1SP", "Mercator_Auxiliary_Sphere", "merc"];

},{"../common/adjust_lon":5,"../common/msfnz":15,"../common/phi2z":16,"../common/tsfnz":24}],53:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
/*
  reference
    "New Equal-Area Map Projections for Noncircular Regions", John P. Snyder,
    The American Cartographer, Vol 15, No. 4, October 1988, pp. 341-355.
  */


/* Initialize the Miller Cylindrical projection
  -------------------------------------------*/
exports.init = function() {
  //no-op
};


/* Miller Cylindrical forward equations--mapping lat,long to x,y
    ------------------------------------------------------------*/
exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  /* Forward equations
      -----------------*/
  var dlon = adjust_lon(lon - this.long0);
  var x = this.x0 + this.a * dlon;
  var y = this.y0 + this.a * Math.log(Math.tan((Math.PI / 4) + (lat / 2.5))) * 1.25;

  p.x = x;
  p.y = y;
  return p;
};

/* Miller Cylindrical inverse equations--mapping x,y to lat/long
    ------------------------------------------------------------*/
exports.inverse = function(p) {
  p.x -= this.x0;
  p.y -= this.y0;

  var lon = adjust_lon(this.long0 + p.x / this.a);
  var lat = 2.5 * (Math.atan(Math.exp(0.8 * p.y / this.a)) - Math.PI / 4);

  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["Miller_Cylindrical", "mill"];

},{"../common/adjust_lon":5}],54:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
var EPSLN = 1.0e-10;
exports.init = function() {};

/* Mollweide forward equations--mapping lat,long to x,y
    ----------------------------------------------------*/
exports.forward = function(p) {

  /* Forward equations
      -----------------*/
  var lon = p.x;
  var lat = p.y;

  var delta_lon = adjust_lon(lon - this.long0);
  var theta = lat;
  var con = Math.PI * Math.sin(lat);

  /* Iterate using the Newton-Raphson method to find theta
      -----------------------------------------------------*/
  for (var i = 0; true; i++) {
    var delta_theta = -(theta + Math.sin(theta) - con) / (1 + Math.cos(theta));
    theta += delta_theta;
    if (Math.abs(delta_theta) < EPSLN) {
      break;
    }
  }
  theta /= 2;

  /* If the latitude is 90 deg, force the x coordinate to be "0 + false easting"
       this is done here because of precision problems with "cos(theta)"
       --------------------------------------------------------------------------*/
  if (Math.PI / 2 - Math.abs(lat) < EPSLN) {
    delta_lon = 0;
  }
  var x = 0.900316316158 * this.a * delta_lon * Math.cos(theta) + this.x0;
  var y = 1.4142135623731 * this.a * Math.sin(theta) + this.y0;

  p.x = x;
  p.y = y;
  return p;
};

exports.inverse = function(p) {
  var theta;
  var arg;

  /* Inverse equations
      -----------------*/
  p.x -= this.x0;
  p.y -= this.y0;
  arg = p.y / (1.4142135623731 * this.a);

  /* Because of division by zero problems, 'arg' can not be 1.  Therefore
       a number very close to one is used instead.
       -------------------------------------------------------------------*/
  if (Math.abs(arg) > 0.999999999999) {
    arg = 0.999999999999;
  }
  theta = Math.asin(arg);
  var lon = adjust_lon(this.long0 + (p.x / (0.900316316158 * this.a * Math.cos(theta))));
  if (lon < (-Math.PI)) {
    lon = -Math.PI;
  }
  if (lon > Math.PI) {
    lon = Math.PI;
  }
  arg = (2 * theta + Math.sin(2 * theta)) / Math.PI;
  if (Math.abs(arg) > 1) {
    arg = 1;
  }
  var lat = Math.asin(arg);

  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["Mollweide", "moll"];

},{"../common/adjust_lon":5}],55:[function(_dereq_,module,exports){
var SEC_TO_RAD = 4.84813681109535993589914102357e-6;
/*
  reference
    Department of Land and Survey Technical Circular 1973/32
      http://www.linz.govt.nz/docs/miscellaneous/nz-map-definition.pdf
    OSG Technical Report 4.1
      http://www.linz.govt.nz/docs/miscellaneous/nzmg.pdf
  */

/**
 * iterations: Number of iterations to refine inverse transform.
 *     0 -> km accuracy
 *     1 -> m accuracy -- suitable for most mapping applications
 *     2 -> mm accuracy
 */
exports.iterations = 1;

exports.init = function() {
  this.A = [];
  this.A[1] = 0.6399175073;
  this.A[2] = -0.1358797613;
  this.A[3] = 0.063294409;
  this.A[4] = -0.02526853;
  this.A[5] = 0.0117879;
  this.A[6] = -0.0055161;
  this.A[7] = 0.0026906;
  this.A[8] = -0.001333;
  this.A[9] = 0.00067;
  this.A[10] = -0.00034;

  this.B_re = [];
  this.B_im = [];
  this.B_re[1] = 0.7557853228;
  this.B_im[1] = 0;
  this.B_re[2] = 0.249204646;
  this.B_im[2] = 0.003371507;
  this.B_re[3] = -0.001541739;
  this.B_im[3] = 0.041058560;
  this.B_re[4] = -0.10162907;
  this.B_im[4] = 0.01727609;
  this.B_re[5] = -0.26623489;
  this.B_im[5] = -0.36249218;
  this.B_re[6] = -0.6870983;
  this.B_im[6] = -1.1651967;

  this.C_re = [];
  this.C_im = [];
  this.C_re[1] = 1.3231270439;
  this.C_im[1] = 0;
  this.C_re[2] = -0.577245789;
  this.C_im[2] = -0.007809598;
  this.C_re[3] = 0.508307513;
  this.C_im[3] = -0.112208952;
  this.C_re[4] = -0.15094762;
  this.C_im[4] = 0.18200602;
  this.C_re[5] = 1.01418179;
  this.C_im[5] = 1.64497696;
  this.C_re[6] = 1.9660549;
  this.C_im[6] = 2.5127645;

  this.D = [];
  this.D[1] = 1.5627014243;
  this.D[2] = 0.5185406398;
  this.D[3] = -0.03333098;
  this.D[4] = -0.1052906;
  this.D[5] = -0.0368594;
  this.D[6] = 0.007317;
  this.D[7] = 0.01220;
  this.D[8] = 0.00394;
  this.D[9] = -0.0013;
};

/**
    New Zealand Map Grid Forward  - long/lat to x/y
    long/lat in radians
  */
exports.forward = function(p) {
  var n;
  var lon = p.x;
  var lat = p.y;

  var delta_lat = lat - this.lat0;
  var delta_lon = lon - this.long0;

  // 1. Calculate d_phi and d_psi    ...                          // and d_lambda
  // For this algorithm, delta_latitude is in seconds of arc x 10-5, so we need to scale to those units. Longitude is radians.
  var d_phi = delta_lat / SEC_TO_RAD * 1E-5;
  var d_lambda = delta_lon;
  var d_phi_n = 1; // d_phi^0

  var d_psi = 0;
  for (n = 1; n <= 10; n++) {
    d_phi_n = d_phi_n * d_phi;
    d_psi = d_psi + this.A[n] * d_phi_n;
  }

  // 2. Calculate theta
  var th_re = d_psi;
  var th_im = d_lambda;

  // 3. Calculate z
  var th_n_re = 1;
  var th_n_im = 0; // theta^0
  var th_n_re1;
  var th_n_im1;

  var z_re = 0;
  var z_im = 0;
  for (n = 1; n <= 6; n++) {
    th_n_re1 = th_n_re * th_re - th_n_im * th_im;
    th_n_im1 = th_n_im * th_re + th_n_re * th_im;
    th_n_re = th_n_re1;
    th_n_im = th_n_im1;
    z_re = z_re + this.B_re[n] * th_n_re - this.B_im[n] * th_n_im;
    z_im = z_im + this.B_im[n] * th_n_re + this.B_re[n] * th_n_im;
  }

  // 4. Calculate easting and northing
  p.x = (z_im * this.a) + this.x0;
  p.y = (z_re * this.a) + this.y0;

  return p;
};


/**
    New Zealand Map Grid Inverse  -  x/y to long/lat
  */
exports.inverse = function(p) {
  var n;
  var x = p.x;
  var y = p.y;

  var delta_x = x - this.x0;
  var delta_y = y - this.y0;

  // 1. Calculate z
  var z_re = delta_y / this.a;
  var z_im = delta_x / this.a;

  // 2a. Calculate theta - first approximation gives km accuracy
  var z_n_re = 1;
  var z_n_im = 0; // z^0
  var z_n_re1;
  var z_n_im1;

  var th_re = 0;
  var th_im = 0;
  for (n = 1; n <= 6; n++) {
    z_n_re1 = z_n_re * z_re - z_n_im * z_im;
    z_n_im1 = z_n_im * z_re + z_n_re * z_im;
    z_n_re = z_n_re1;
    z_n_im = z_n_im1;
    th_re = th_re + this.C_re[n] * z_n_re - this.C_im[n] * z_n_im;
    th_im = th_im + this.C_im[n] * z_n_re + this.C_re[n] * z_n_im;
  }

  // 2b. Iterate to refine the accuracy of the calculation
  //        0 iterations gives km accuracy
  //        1 iteration gives m accuracy -- good enough for most mapping applications
  //        2 iterations bives mm accuracy
  for (var i = 0; i < this.iterations; i++) {
    var th_n_re = th_re;
    var th_n_im = th_im;
    var th_n_re1;
    var th_n_im1;

    var num_re = z_re;
    var num_im = z_im;
    for (n = 2; n <= 6; n++) {
      th_n_re1 = th_n_re * th_re - th_n_im * th_im;
      th_n_im1 = th_n_im * th_re + th_n_re * th_im;
      th_n_re = th_n_re1;
      th_n_im = th_n_im1;
      num_re = num_re + (n - 1) * (this.B_re[n] * th_n_re - this.B_im[n] * th_n_im);
      num_im = num_im + (n - 1) * (this.B_im[n] * th_n_re + this.B_re[n] * th_n_im);
    }

    th_n_re = 1;
    th_n_im = 0;
    var den_re = this.B_re[1];
    var den_im = this.B_im[1];
    for (n = 2; n <= 6; n++) {
      th_n_re1 = th_n_re * th_re - th_n_im * th_im;
      th_n_im1 = th_n_im * th_re + th_n_re * th_im;
      th_n_re = th_n_re1;
      th_n_im = th_n_im1;
      den_re = den_re + n * (this.B_re[n] * th_n_re - this.B_im[n] * th_n_im);
      den_im = den_im + n * (this.B_im[n] * th_n_re + this.B_re[n] * th_n_im);
    }

    // Complex division
    var den2 = den_re * den_re + den_im * den_im;
    th_re = (num_re * den_re + num_im * den_im) / den2;
    th_im = (num_im * den_re - num_re * den_im) / den2;
  }

  // 3. Calculate d_phi              ...                                    // and d_lambda
  var d_psi = th_re;
  var d_lambda = th_im;
  var d_psi_n = 1; // d_psi^0

  var d_phi = 0;
  for (n = 1; n <= 9; n++) {
    d_psi_n = d_psi_n * d_psi;
    d_phi = d_phi + this.D[n] * d_psi_n;
  }

  // 4. Calculate latitude and longitude
  // d_phi is calcuated in second of arc * 10^-5, so we need to scale back to radians. d_lambda is in radians.
  var lat = this.lat0 + (d_phi * SEC_TO_RAD * 1E5);
  var lon = this.long0 + d_lambda;

  p.x = lon;
  p.y = lat;

  return p;
};
exports.names = ["New_Zealand_Map_Grid", "nzmg"];
},{}],56:[function(_dereq_,module,exports){
var tsfnz = _dereq_('../common/tsfnz');
var adjust_lon = _dereq_('../common/adjust_lon');
var phi2z = _dereq_('../common/phi2z');
var HALF_PI = Math.PI/2;
var FORTPI = Math.PI/4;
var EPSLN = 1.0e-10;

/* Initialize the Oblique Mercator  projection
    ------------------------------------------*/
exports.init = function() {
  this.no_off = this.no_off || false;
  this.no_rot = this.no_rot || false;

  if (isNaN(this.k0)) {
    this.k0 = 1;
  }
  var sinlat = Math.sin(this.lat0);
  var coslat = Math.cos(this.lat0);
  var con = this.e * sinlat;

  this.bl = Math.sqrt(1 + this.es / (1 - this.es) * Math.pow(coslat, 4));
  this.al = this.a * this.bl * this.k0 * Math.sqrt(1 - this.es) / (1 - con * con);
  var t0 = tsfnz(this.e, this.lat0, sinlat);
  var dl = this.bl / coslat * Math.sqrt((1 - this.es) / (1 - con * con));
  if (dl * dl < 1) {
    dl = 1;
  }
  var fl;
  var gl;
  if (!isNaN(this.longc)) {
    //Central point and azimuth method

    if (this.lat0 >= 0) {
      fl = dl + Math.sqrt(dl * dl - 1);
    }
    else {
      fl = dl - Math.sqrt(dl * dl - 1);
    }
    this.el = fl * Math.pow(t0, this.bl);
    gl = 0.5 * (fl - 1 / fl);
    this.gamma0 = Math.asin(Math.sin(this.alpha) / dl);
    this.long0 = this.longc - Math.asin(gl * Math.tan(this.gamma0)) / this.bl;

  }
  else {
    //2 points method
    var t1 = tsfnz(this.e, this.lat1, Math.sin(this.lat1));
    var t2 = tsfnz(this.e, this.lat2, Math.sin(this.lat2));
    if (this.lat0 >= 0) {
      this.el = (dl + Math.sqrt(dl * dl - 1)) * Math.pow(t0, this.bl);
    }
    else {
      this.el = (dl - Math.sqrt(dl * dl - 1)) * Math.pow(t0, this.bl);
    }
    var hl = Math.pow(t1, this.bl);
    var ll = Math.pow(t2, this.bl);
    fl = this.el / hl;
    gl = 0.5 * (fl - 1 / fl);
    var jl = (this.el * this.el - ll * hl) / (this.el * this.el + ll * hl);
    var pl = (ll - hl) / (ll + hl);
    var dlon12 = adjust_lon(this.long1 - this.long2);
    this.long0 = 0.5 * (this.long1 + this.long2) - Math.atan(jl * Math.tan(0.5 * this.bl * (dlon12)) / pl) / this.bl;
    this.long0 = adjust_lon(this.long0);
    var dlon10 = adjust_lon(this.long1 - this.long0);
    this.gamma0 = Math.atan(Math.sin(this.bl * (dlon10)) / gl);
    this.alpha = Math.asin(dl * Math.sin(this.gamma0));
  }

  if (this.no_off) {
    this.uc = 0;
  }
  else {
    if (this.lat0 >= 0) {
      this.uc = this.al / this.bl * Math.atan2(Math.sqrt(dl * dl - 1), Math.cos(this.alpha));
    }
    else {
      this.uc = -1 * this.al / this.bl * Math.atan2(Math.sqrt(dl * dl - 1), Math.cos(this.alpha));
    }
  }

};


/* Oblique Mercator forward equations--mapping lat,long to x,y
    ----------------------------------------------------------*/
exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  var dlon = adjust_lon(lon - this.long0);
  var us, vs;
  var con;
  if (Math.abs(Math.abs(lat) - HALF_PI) <= EPSLN) {
    if (lat > 0) {
      con = -1;
    }
    else {
      con = 1;
    }
    vs = this.al / this.bl * Math.log(Math.tan(FORTPI + con * this.gamma0 * 0.5));
    us = -1 * con * HALF_PI * this.al / this.bl;
  }
  else {
    var t = tsfnz(this.e, lat, Math.sin(lat));
    var ql = this.el / Math.pow(t, this.bl);
    var sl = 0.5 * (ql - 1 / ql);
    var tl = 0.5 * (ql + 1 / ql);
    var vl = Math.sin(this.bl * (dlon));
    var ul = (sl * Math.sin(this.gamma0) - vl * Math.cos(this.gamma0)) / tl;
    if (Math.abs(Math.abs(ul) - 1) <= EPSLN) {
      vs = Number.POSITIVE_INFINITY;
    }
    else {
      vs = 0.5 * this.al * Math.log((1 - ul) / (1 + ul)) / this.bl;
    }
    if (Math.abs(Math.cos(this.bl * (dlon))) <= EPSLN) {
      us = this.al * this.bl * (dlon);
    }
    else {
      us = this.al * Math.atan2(sl * Math.cos(this.gamma0) + vl * Math.sin(this.gamma0), Math.cos(this.bl * dlon)) / this.bl;
    }
  }

  if (this.no_rot) {
    p.x = this.x0 + us;
    p.y = this.y0 + vs;
  }
  else {

    us -= this.uc;
    p.x = this.x0 + vs * Math.cos(this.alpha) + us * Math.sin(this.alpha);
    p.y = this.y0 + us * Math.cos(this.alpha) - vs * Math.sin(this.alpha);
  }
  return p;
};

exports.inverse = function(p) {
  var us, vs;
  if (this.no_rot) {
    vs = p.y - this.y0;
    us = p.x - this.x0;
  }
  else {
    vs = (p.x - this.x0) * Math.cos(this.alpha) - (p.y - this.y0) * Math.sin(this.alpha);
    us = (p.y - this.y0) * Math.cos(this.alpha) + (p.x - this.x0) * Math.sin(this.alpha);
    us += this.uc;
  }
  var qp = Math.exp(-1 * this.bl * vs / this.al);
  var sp = 0.5 * (qp - 1 / qp);
  var tp = 0.5 * (qp + 1 / qp);
  var vp = Math.sin(this.bl * us / this.al);
  var up = (vp * Math.cos(this.gamma0) + sp * Math.sin(this.gamma0)) / tp;
  var ts = Math.pow(this.el / Math.sqrt((1 + up) / (1 - up)), 1 / this.bl);
  if (Math.abs(up - 1) < EPSLN) {
    p.x = this.long0;
    p.y = HALF_PI;
  }
  else if (Math.abs(up + 1) < EPSLN) {
    p.x = this.long0;
    p.y = -1 * HALF_PI;
  }
  else {
    p.y = phi2z(this.e, ts);
    p.x = adjust_lon(this.long0 - Math.atan2(sp * Math.cos(this.gamma0) - vp * Math.sin(this.gamma0), Math.cos(this.bl * us / this.al)) / this.bl);
  }
  return p;
};

exports.names = ["Hotine_Oblique_Mercator", "Hotine Oblique Mercator", "Hotine_Oblique_Mercator_Azimuth_Natural_Origin", "Hotine_Oblique_Mercator_Azimuth_Center", "omerc"];
},{"../common/adjust_lon":5,"../common/phi2z":16,"../common/tsfnz":24}],57:[function(_dereq_,module,exports){
var e0fn = _dereq_('../common/e0fn');
var e1fn = _dereq_('../common/e1fn');
var e2fn = _dereq_('../common/e2fn');
var e3fn = _dereq_('../common/e3fn');
var adjust_lon = _dereq_('../common/adjust_lon');
var adjust_lat = _dereq_('../common/adjust_lat');
var mlfn = _dereq_('../common/mlfn');
var EPSLN = 1.0e-10;
var gN = _dereq_('../common/gN');
var MAX_ITER = 20;
exports.init = function() {
  /* Place parameters in static storage for common use
      -------------------------------------------------*/
  this.temp = this.b / this.a;
  this.es = 1 - Math.pow(this.temp, 2); // devait etre dans tmerc.js mais n y est pas donc je commente sinon retour de valeurs nulles
  this.e = Math.sqrt(this.es);
  this.e0 = e0fn(this.es);
  this.e1 = e1fn(this.es);
  this.e2 = e2fn(this.es);
  this.e3 = e3fn(this.es);
  this.ml0 = this.a * mlfn(this.e0, this.e1, this.e2, this.e3, this.lat0); //si que des zeros le calcul ne se fait pas
};


/* Polyconic forward equations--mapping lat,long to x,y
    ---------------------------------------------------*/
exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  var x, y, el;
  var dlon = adjust_lon(lon - this.long0);
  el = dlon * Math.sin(lat);
  if (this.sphere) {
    if (Math.abs(lat) <= EPSLN) {
      x = this.a * dlon;
      y = -1 * this.a * this.lat0;
    }
    else {
      x = this.a * Math.sin(el) / Math.tan(lat);
      y = this.a * (adjust_lat(lat - this.lat0) + (1 - Math.cos(el)) / Math.tan(lat));
    }
  }
  else {
    if (Math.abs(lat) <= EPSLN) {
      x = this.a * dlon;
      y = -1 * this.ml0;
    }
    else {
      var nl = gN(this.a, this.e, Math.sin(lat)) / Math.tan(lat);
      x = nl * Math.sin(el);
      y = this.a * mlfn(this.e0, this.e1, this.e2, this.e3, lat) - this.ml0 + nl * (1 - Math.cos(el));
    }

  }
  p.x = x + this.x0;
  p.y = y + this.y0;
  return p;
};


/* Inverse equations
  -----------------*/
exports.inverse = function(p) {
  var lon, lat, x, y, i;
  var al, bl;
  var phi, dphi;
  x = p.x - this.x0;
  y = p.y - this.y0;

  if (this.sphere) {
    if (Math.abs(y + this.a * this.lat0) <= EPSLN) {
      lon = adjust_lon(x / this.a + this.long0);
      lat = 0;
    }
    else {
      al = this.lat0 + y / this.a;
      bl = x * x / this.a / this.a + al * al;
      phi = al;
      var tanphi;
      for (i = MAX_ITER; i; --i) {
        tanphi = Math.tan(phi);
        dphi = -1 * (al * (phi * tanphi + 1) - phi - 0.5 * (phi * phi + bl) * tanphi) / ((phi - al) / tanphi - 1);
        phi += dphi;
        if (Math.abs(dphi) <= EPSLN) {
          lat = phi;
          break;
        }
      }
      lon = adjust_lon(this.long0 + (Math.asin(x * Math.tan(phi) / this.a)) / Math.sin(lat));
    }
  }
  else {
    if (Math.abs(y + this.ml0) <= EPSLN) {
      lat = 0;
      lon = adjust_lon(this.long0 + x / this.a);
    }
    else {

      al = (this.ml0 + y) / this.a;
      bl = x * x / this.a / this.a + al * al;
      phi = al;
      var cl, mln, mlnp, ma;
      var con;
      for (i = MAX_ITER; i; --i) {
        con = this.e * Math.sin(phi);
        cl = Math.sqrt(1 - con * con) * Math.tan(phi);
        mln = this.a * mlfn(this.e0, this.e1, this.e2, this.e3, phi);
        mlnp = this.e0 - 2 * this.e1 * Math.cos(2 * phi) + 4 * this.e2 * Math.cos(4 * phi) - 6 * this.e3 * Math.cos(6 * phi);
        ma = mln / this.a;
        dphi = (al * (cl * ma + 1) - ma - 0.5 * cl * (ma * ma + bl)) / (this.es * Math.sin(2 * phi) * (ma * ma + bl - 2 * al * ma) / (4 * cl) + (al - ma) * (cl * mlnp - 2 / Math.sin(2 * phi)) - mlnp);
        phi -= dphi;
        if (Math.abs(dphi) <= EPSLN) {
          lat = phi;
          break;
        }
      }

      //lat=phi4z(this.e,this.e0,this.e1,this.e2,this.e3,al,bl,0,0);
      cl = Math.sqrt(1 - this.es * Math.pow(Math.sin(lat), 2)) * Math.tan(lat);
      lon = adjust_lon(this.long0 + Math.asin(x * cl / this.a) / Math.sin(lat));
    }
  }

  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["Polyconic", "poly"];
},{"../common/adjust_lat":4,"../common/adjust_lon":5,"../common/e0fn":7,"../common/e1fn":8,"../common/e2fn":9,"../common/e3fn":10,"../common/gN":11,"../common/mlfn":14}],58:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
var adjust_lat = _dereq_('../common/adjust_lat');
var pj_enfn = _dereq_('../common/pj_enfn');
var MAX_ITER = 20;
var pj_mlfn = _dereq_('../common/pj_mlfn');
var pj_inv_mlfn = _dereq_('../common/pj_inv_mlfn');
var HALF_PI = Math.PI/2;
var EPSLN = 1.0e-10;
var asinz = _dereq_('../common/asinz');
exports.init = function() {
  /* Place parameters in static storage for common use
    -------------------------------------------------*/


  if (!this.sphere) {
    this.en = pj_enfn(this.es);
  }
  else {
    this.n = 1;
    this.m = 0;
    this.es = 0;
    this.C_y = Math.sqrt((this.m + 1) / this.n);
    this.C_x = this.C_y / (this.m + 1);
  }

};

/* Sinusoidal forward equations--mapping lat,long to x,y
  -----------------------------------------------------*/
exports.forward = function(p) {
  var x, y;
  var lon = p.x;
  var lat = p.y;
  /* Forward equations
    -----------------*/
  lon = adjust_lon(lon - this.long0);

  if (this.sphere) {
    if (!this.m) {
      lat = this.n !== 1 ? Math.asin(this.n * Math.sin(lat)) : lat;
    }
    else {
      var k = this.n * Math.sin(lat);
      for (var i = MAX_ITER; i; --i) {
        var V = (this.m * lat + Math.sin(lat) - k) / (this.m + Math.cos(lat));
        lat -= V;
        if (Math.abs(V) < EPSLN) {
          break;
        }
      }
    }
    x = this.a * this.C_x * lon * (this.m + Math.cos(lat));
    y = this.a * this.C_y * lat;

  }
  else {

    var s = Math.sin(lat);
    var c = Math.cos(lat);
    y = this.a * pj_mlfn(lat, s, c, this.en);
    x = this.a * lon * c / Math.sqrt(1 - this.es * s * s);
  }

  p.x = x;
  p.y = y;
  return p;
};

exports.inverse = function(p) {
  var lat, temp, lon, s;

  p.x -= this.x0;
  lon = p.x / this.a;
  p.y -= this.y0;
  lat = p.y / this.a;

  if (this.sphere) {
    lat /= this.C_y;
    lon = lon / (this.C_x * (this.m + Math.cos(lat)));
    if (this.m) {
      lat = asinz((this.m * lat + Math.sin(lat)) / this.n);
    }
    else if (this.n !== 1) {
      lat = asinz(Math.sin(lat) / this.n);
    }
    lon = adjust_lon(lon + this.long0);
    lat = adjust_lat(lat);
  }
  else {
    lat = pj_inv_mlfn(p.y / this.a, this.es, this.en);
    s = Math.abs(lat);
    if (s < HALF_PI) {
      s = Math.sin(lat);
      temp = this.long0 + p.x * Math.sqrt(1 - this.es * s * s) / (this.a * Math.cos(lat));
      //temp = this.long0 + p.x / (this.a * Math.cos(lat));
      lon = adjust_lon(temp);
    }
    else if ((s - EPSLN) < HALF_PI) {
      lon = this.long0;
    }
  }
  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["Sinusoidal", "sinu"];
},{"../common/adjust_lat":4,"../common/adjust_lon":5,"../common/asinz":6,"../common/pj_enfn":17,"../common/pj_inv_mlfn":18,"../common/pj_mlfn":19}],59:[function(_dereq_,module,exports){
/*
  references:
    Formules et constantes pour le Calcul pour la
    projection cylindrique conforme à axe oblique et pour la transformation entre
    des systèmes de référence.
    http://www.swisstopo.admin.ch/internet/swisstopo/fr/home/topics/survey/sys/refsys/switzerland.parsysrelated1.31216.downloadList.77004.DownloadFile.tmp/swissprojectionfr.pdf
  */
exports.init = function() {
  var phy0 = this.lat0;
  this.lambda0 = this.long0;
  var sinPhy0 = Math.sin(phy0);
  var semiMajorAxis = this.a;
  var invF = this.rf;
  var flattening = 1 / invF;
  var e2 = 2 * flattening - Math.pow(flattening, 2);
  var e = this.e = Math.sqrt(e2);
  this.R = this.k0 * semiMajorAxis * Math.sqrt(1 - e2) / (1 - e2 * Math.pow(sinPhy0, 2));
  this.alpha = Math.sqrt(1 + e2 / (1 - e2) * Math.pow(Math.cos(phy0), 4));
  this.b0 = Math.asin(sinPhy0 / this.alpha);
  var k1 = Math.log(Math.tan(Math.PI / 4 + this.b0 / 2));
  var k2 = Math.log(Math.tan(Math.PI / 4 + phy0 / 2));
  var k3 = Math.log((1 + e * sinPhy0) / (1 - e * sinPhy0));
  this.K = k1 - this.alpha * k2 + this.alpha * e / 2 * k3;
};


exports.forward = function(p) {
  var Sa1 = Math.log(Math.tan(Math.PI / 4 - p.y / 2));
  var Sa2 = this.e / 2 * Math.log((1 + this.e * Math.sin(p.y)) / (1 - this.e * Math.sin(p.y)));
  var S = -this.alpha * (Sa1 + Sa2) + this.K;

  // spheric latitude
  var b = 2 * (Math.atan(Math.exp(S)) - Math.PI / 4);

  // spheric longitude
  var I = this.alpha * (p.x - this.lambda0);

  // psoeudo equatorial rotation
  var rotI = Math.atan(Math.sin(I) / (Math.sin(this.b0) * Math.tan(b) + Math.cos(this.b0) * Math.cos(I)));

  var rotB = Math.asin(Math.cos(this.b0) * Math.sin(b) - Math.sin(this.b0) * Math.cos(b) * Math.cos(I));

  p.y = this.R / 2 * Math.log((1 + Math.sin(rotB)) / (1 - Math.sin(rotB))) + this.y0;
  p.x = this.R * rotI + this.x0;
  return p;
};

exports.inverse = function(p) {
  var Y = p.x - this.x0;
  var X = p.y - this.y0;

  var rotI = Y / this.R;
  var rotB = 2 * (Math.atan(Math.exp(X / this.R)) - Math.PI / 4);

  var b = Math.asin(Math.cos(this.b0) * Math.sin(rotB) + Math.sin(this.b0) * Math.cos(rotB) * Math.cos(rotI));
  var I = Math.atan(Math.sin(rotI) / (Math.cos(this.b0) * Math.cos(rotI) - Math.sin(this.b0) * Math.tan(rotB)));

  var lambda = this.lambda0 + I / this.alpha;

  var S = 0;
  var phy = b;
  var prevPhy = -1000;
  var iteration = 0;
  while (Math.abs(phy - prevPhy) > 0.0000001) {
    if (++iteration > 20) {
      //...reportError("omercFwdInfinity");
      return;
    }
    //S = Math.log(Math.tan(Math.PI / 4 + phy / 2));
    S = 1 / this.alpha * (Math.log(Math.tan(Math.PI / 4 + b / 2)) - this.K) + this.e * Math.log(Math.tan(Math.PI / 4 + Math.asin(this.e * Math.sin(phy)) / 2));
    prevPhy = phy;
    phy = 2 * Math.atan(Math.exp(S)) - Math.PI / 2;
  }

  p.x = lambda;
  p.y = phy;
  return p;
};

exports.names = ["somerc"];

},{}],60:[function(_dereq_,module,exports){
var HALF_PI = Math.PI/2;
var EPSLN = 1.0e-10;
var sign = _dereq_('../common/sign');
var msfnz = _dereq_('../common/msfnz');
var tsfnz = _dereq_('../common/tsfnz');
var phi2z = _dereq_('../common/phi2z');
var adjust_lon = _dereq_('../common/adjust_lon');
exports.ssfn_ = function(phit, sinphi, eccen) {
  sinphi *= eccen;
  return (Math.tan(0.5 * (HALF_PI + phit)) * Math.pow((1 - sinphi) / (1 + sinphi), 0.5 * eccen));
};

exports.init = function() {
  this.coslat0 = Math.cos(this.lat0);
  this.sinlat0 = Math.sin(this.lat0);
  if (this.sphere) {
    if (this.k0 === 1 && !isNaN(this.lat_ts) && Math.abs(this.coslat0) <= EPSLN) {
      this.k0 = 0.5 * (1 + sign(this.lat0) * Math.sin(this.lat_ts));
    }
  }
  else {
    if (Math.abs(this.coslat0) <= EPSLN) {
      if (this.lat0 > 0) {
        //North pole
        //trace('stere:north pole');
        this.con = 1;
      }
      else {
        //South pole
        //trace('stere:south pole');
        this.con = -1;
      }
    }
    this.cons = Math.sqrt(Math.pow(1 + this.e, 1 + this.e) * Math.pow(1 - this.e, 1 - this.e));
    if (this.k0 === 1 && !isNaN(this.lat_ts) && Math.abs(this.coslat0) <= EPSLN) {
      this.k0 = 0.5 * this.cons * msfnz(this.e, Math.sin(this.lat_ts), Math.cos(this.lat_ts)) / tsfnz(this.e, this.con * this.lat_ts, this.con * Math.sin(this.lat_ts));
    }
    this.ms1 = msfnz(this.e, this.sinlat0, this.coslat0);
    this.X0 = 2 * Math.atan(this.ssfn_(this.lat0, this.sinlat0, this.e)) - HALF_PI;
    this.cosX0 = Math.cos(this.X0);
    this.sinX0 = Math.sin(this.X0);
  }
};

// Stereographic forward equations--mapping lat,long to x,y
exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;
  var sinlat = Math.sin(lat);
  var coslat = Math.cos(lat);
  var A, X, sinX, cosX, ts, rh;
  var dlon = adjust_lon(lon - this.long0);

  if (Math.abs(Math.abs(lon - this.long0) - Math.PI) <= EPSLN && Math.abs(lat + this.lat0) <= EPSLN) {
    //case of the origine point
    //trace('stere:this is the origin point');
    p.x = NaN;
    p.y = NaN;
    return p;
  }
  if (this.sphere) {
    //trace('stere:sphere case');
    A = 2 * this.k0 / (1 + this.sinlat0 * sinlat + this.coslat0 * coslat * Math.cos(dlon));
    p.x = this.a * A * coslat * Math.sin(dlon) + this.x0;
    p.y = this.a * A * (this.coslat0 * sinlat - this.sinlat0 * coslat * Math.cos(dlon)) + this.y0;
    return p;
  }
  else {
    X = 2 * Math.atan(this.ssfn_(lat, sinlat, this.e)) - HALF_PI;
    cosX = Math.cos(X);
    sinX = Math.sin(X);
    if (Math.abs(this.coslat0) <= EPSLN) {
      ts = tsfnz(this.e, lat * this.con, this.con * sinlat);
      rh = 2 * this.a * this.k0 * ts / this.cons;
      p.x = this.x0 + rh * Math.sin(lon - this.long0);
      p.y = this.y0 - this.con * rh * Math.cos(lon - this.long0);
      //trace(p.toString());
      return p;
    }
    else if (Math.abs(this.sinlat0) < EPSLN) {
      //Eq
      //trace('stere:equateur');
      A = 2 * this.a * this.k0 / (1 + cosX * Math.cos(dlon));
      p.y = A * sinX;
    }
    else {
      //other case
      //trace('stere:normal case');
      A = 2 * this.a * this.k0 * this.ms1 / (this.cosX0 * (1 + this.sinX0 * sinX + this.cosX0 * cosX * Math.cos(dlon)));
      p.y = A * (this.cosX0 * sinX - this.sinX0 * cosX * Math.cos(dlon)) + this.y0;
    }
    p.x = A * cosX * Math.sin(dlon) + this.x0;
  }
  //trace(p.toString());
  return p;
};


//* Stereographic inverse equations--mapping x,y to lat/long
exports.inverse = function(p) {
  p.x -= this.x0;
  p.y -= this.y0;
  var lon, lat, ts, ce, Chi;
  var rh = Math.sqrt(p.x * p.x + p.y * p.y);
  if (this.sphere) {
    var c = 2 * Math.atan(rh / (0.5 * this.a * this.k0));
    lon = this.long0;
    lat = this.lat0;
    if (rh <= EPSLN) {
      p.x = lon;
      p.y = lat;
      return p;
    }
    lat = Math.asin(Math.cos(c) * this.sinlat0 + p.y * Math.sin(c) * this.coslat0 / rh);
    if (Math.abs(this.coslat0) < EPSLN) {
      if (this.lat0 > 0) {
        lon = adjust_lon(this.long0 + Math.atan2(p.x, - 1 * p.y));
      }
      else {
        lon = adjust_lon(this.long0 + Math.atan2(p.x, p.y));
      }
    }
    else {
      lon = adjust_lon(this.long0 + Math.atan2(p.x * Math.sin(c), rh * this.coslat0 * Math.cos(c) - p.y * this.sinlat0 * Math.sin(c)));
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  else {
    if (Math.abs(this.coslat0) <= EPSLN) {
      if (rh <= EPSLN) {
        lat = this.lat0;
        lon = this.long0;
        p.x = lon;
        p.y = lat;
        //trace(p.toString());
        return p;
      }
      p.x *= this.con;
      p.y *= this.con;
      ts = rh * this.cons / (2 * this.a * this.k0);
      lat = this.con * phi2z(this.e, ts);
      lon = this.con * adjust_lon(this.con * this.long0 + Math.atan2(p.x, - 1 * p.y));
    }
    else {
      ce = 2 * Math.atan(rh * this.cosX0 / (2 * this.a * this.k0 * this.ms1));
      lon = this.long0;
      if (rh <= EPSLN) {
        Chi = this.X0;
      }
      else {
        Chi = Math.asin(Math.cos(ce) * this.sinX0 + p.y * Math.sin(ce) * this.cosX0 / rh);
        lon = adjust_lon(this.long0 + Math.atan2(p.x * Math.sin(ce), rh * this.cosX0 * Math.cos(ce) - p.y * this.sinX0 * Math.sin(ce)));
      }
      lat = -1 * phi2z(this.e, Math.tan(0.5 * (HALF_PI + Chi)));
    }
  }
  p.x = lon;
  p.y = lat;

  //trace(p.toString());
  return p;

};
exports.names = ["stere", "Stereographic_South_Pole", "Polar Stereographic (variant B)"];

},{"../common/adjust_lon":5,"../common/msfnz":15,"../common/phi2z":16,"../common/sign":21,"../common/tsfnz":24}],61:[function(_dereq_,module,exports){
var gauss = _dereq_('./gauss');
var adjust_lon = _dereq_('../common/adjust_lon');
exports.init = function() {
  gauss.init.apply(this);
  if (!this.rc) {
    return;
  }
  this.sinc0 = Math.sin(this.phic0);
  this.cosc0 = Math.cos(this.phic0);
  this.R2 = 2 * this.rc;
  if (!this.title) {
    this.title = "Oblique Stereographic Alternative";
  }
};

exports.forward = function(p) {
  var sinc, cosc, cosl, k;
  p.x = adjust_lon(p.x - this.long0);
  gauss.forward.apply(this, [p]);
  sinc = Math.sin(p.y);
  cosc = Math.cos(p.y);
  cosl = Math.cos(p.x);
  k = this.k0 * this.R2 / (1 + this.sinc0 * sinc + this.cosc0 * cosc * cosl);
  p.x = k * cosc * Math.sin(p.x);
  p.y = k * (this.cosc0 * sinc - this.sinc0 * cosc * cosl);
  p.x = this.a * p.x + this.x0;
  p.y = this.a * p.y + this.y0;
  return p;
};

exports.inverse = function(p) {
  var sinc, cosc, lon, lat, rho;
  p.x = (p.x - this.x0) / this.a;
  p.y = (p.y - this.y0) / this.a;

  p.x /= this.k0;
  p.y /= this.k0;
  if ((rho = Math.sqrt(p.x * p.x + p.y * p.y))) {
    var c = 2 * Math.atan2(rho, this.R2);
    sinc = Math.sin(c);
    cosc = Math.cos(c);
    lat = Math.asin(cosc * this.sinc0 + p.y * sinc * this.cosc0 / rho);
    lon = Math.atan2(p.x * sinc, rho * this.cosc0 * cosc - p.y * this.sinc0 * sinc);
  }
  else {
    lat = this.phic0;
    lon = 0;
  }

  p.x = lon;
  p.y = lat;
  gauss.inverse.apply(this, [p]);
  p.x = adjust_lon(p.x + this.long0);
  return p;
};

exports.names = ["Stereographic_North_Pole", "Oblique_Stereographic", "Polar_Stereographic", "sterea","Oblique Stereographic Alternative"];

},{"../common/adjust_lon":5,"./gauss":46}],62:[function(_dereq_,module,exports){
var e0fn = _dereq_('../common/e0fn');
var e1fn = _dereq_('../common/e1fn');
var e2fn = _dereq_('../common/e2fn');
var e3fn = _dereq_('../common/e3fn');
var mlfn = _dereq_('../common/mlfn');
var adjust_lon = _dereq_('../common/adjust_lon');
var HALF_PI = Math.PI/2;
var EPSLN = 1.0e-10;
var sign = _dereq_('../common/sign');
var asinz = _dereq_('../common/asinz');

exports.init = function() {
  this.e0 = e0fn(this.es);
  this.e1 = e1fn(this.es);
  this.e2 = e2fn(this.es);
  this.e3 = e3fn(this.es);
  this.ml0 = this.a * mlfn(this.e0, this.e1, this.e2, this.e3, this.lat0);
};

/**
    Transverse Mercator Forward  - long/lat to x/y
    long/lat in radians
  */
exports.forward = function(p) {
  var lon = p.x;
  var lat = p.y;

  var delta_lon = adjust_lon(lon - this.long0);
  var con;
  var x, y;
  var sin_phi = Math.sin(lat);
  var cos_phi = Math.cos(lat);

  if (this.sphere) {
    var b = cos_phi * Math.sin(delta_lon);
    if ((Math.abs(Math.abs(b) - 1)) < 0.0000000001) {
      return (93);
    }
    else {
      x = 0.5 * this.a * this.k0 * Math.log((1 + b) / (1 - b));
      con = Math.acos(cos_phi * Math.cos(delta_lon) / Math.sqrt(1 - b * b));
      if (lat < 0) {
        con = -con;
      }
      y = this.a * this.k0 * (con - this.lat0);
    }
  }
  else {
    var al = cos_phi * delta_lon;
    var als = Math.pow(al, 2);
    var c = this.ep2 * Math.pow(cos_phi, 2);
    var tq = Math.tan(lat);
    var t = Math.pow(tq, 2);
    con = 1 - this.es * Math.pow(sin_phi, 2);
    var n = this.a / Math.sqrt(con);
    var ml = this.a * mlfn(this.e0, this.e1, this.e2, this.e3, lat);

    x = this.k0 * n * al * (1 + als / 6 * (1 - t + c + als / 20 * (5 - 18 * t + Math.pow(t, 2) + 72 * c - 58 * this.ep2))) + this.x0;
    y = this.k0 * (ml - this.ml0 + n * tq * (als * (0.5 + als / 24 * (5 - t + 9 * c + 4 * Math.pow(c, 2) + als / 30 * (61 - 58 * t + Math.pow(t, 2) + 600 * c - 330 * this.ep2))))) + this.y0;

  }
  p.x = x;
  p.y = y;
  return p;
};

/**
    Transverse Mercator Inverse  -  x/y to long/lat
  */
exports.inverse = function(p) {
  var con, phi;
  var delta_phi;
  var i;
  var max_iter = 6;
  var lat, lon;

  if (this.sphere) {
    var f = Math.exp(p.x / (this.a * this.k0));
    var g = 0.5 * (f - 1 / f);
    var temp = this.lat0 + p.y / (this.a * this.k0);
    var h = Math.cos(temp);
    con = Math.sqrt((1 - h * h) / (1 + g * g));
    lat = asinz(con);
    if (temp < 0) {
      lat = -lat;
    }
    if ((g === 0) && (h === 0)) {
      lon = this.long0;
    }
    else {
      lon = adjust_lon(Math.atan2(g, h) + this.long0);
    }
  }
  else { // ellipsoidal form
    var x = p.x - this.x0;
    var y = p.y - this.y0;

    con = (this.ml0 + y / this.k0) / this.a;
    phi = con;
    for (i = 0; true; i++) {
      delta_phi = ((con + this.e1 * Math.sin(2 * phi) - this.e2 * Math.sin(4 * phi) + this.e3 * Math.sin(6 * phi)) / this.e0) - phi;
      phi += delta_phi;
      if (Math.abs(delta_phi) <= EPSLN) {
        break;
      }
      if (i >= max_iter) {
        return (95);
      }
    } // for()
    if (Math.abs(phi) < HALF_PI) {
      var sin_phi = Math.sin(phi);
      var cos_phi = Math.cos(phi);
      var tan_phi = Math.tan(phi);
      var c = this.ep2 * Math.pow(cos_phi, 2);
      var cs = Math.pow(c, 2);
      var t = Math.pow(tan_phi, 2);
      var ts = Math.pow(t, 2);
      con = 1 - this.es * Math.pow(sin_phi, 2);
      var n = this.a / Math.sqrt(con);
      var r = n * (1 - this.es) / con;
      var d = x / (n * this.k0);
      var ds = Math.pow(d, 2);
      lat = phi - (n * tan_phi * ds / r) * (0.5 - ds / 24 * (5 + 3 * t + 10 * c - 4 * cs - 9 * this.ep2 - ds / 30 * (61 + 90 * t + 298 * c + 45 * ts - 252 * this.ep2 - 3 * cs)));
      lon = adjust_lon(this.long0 + (d * (1 - ds / 6 * (1 + 2 * t + c - ds / 20 * (5 - 2 * c + 28 * t - 3 * cs + 8 * this.ep2 + 24 * ts))) / cos_phi));
    }
    else {
      lat = HALF_PI * sign(y);
      lon = this.long0;
    }
  }
  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["Transverse_Mercator", "Transverse Mercator", "tmerc"];

},{"../common/adjust_lon":5,"../common/asinz":6,"../common/e0fn":7,"../common/e1fn":8,"../common/e2fn":9,"../common/e3fn":10,"../common/mlfn":14,"../common/sign":21}],63:[function(_dereq_,module,exports){
var D2R = 0.01745329251994329577;
var tmerc = _dereq_('./tmerc');
exports.dependsOn = 'tmerc';
exports.init = function() {
  if (!this.zone) {
    return;
  }
  this.lat0 = 0;
  this.long0 = ((6 * Math.abs(this.zone)) - 183) * D2R;
  this.x0 = 500000;
  this.y0 = this.utmSouth ? 10000000 : 0;
  this.k0 = 0.9996;

  tmerc.init.apply(this);
  this.forward = tmerc.forward;
  this.inverse = tmerc.inverse;
};
exports.names = ["Universal Transverse Mercator System", "utm"];

},{"./tmerc":62}],64:[function(_dereq_,module,exports){
var adjust_lon = _dereq_('../common/adjust_lon');
var HALF_PI = Math.PI/2;
var EPSLN = 1.0e-10;
var asinz = _dereq_('../common/asinz');
/* Initialize the Van Der Grinten projection
  ----------------------------------------*/
exports.init = function() {
  //this.R = 6370997; //Radius of earth
  this.R = this.a;
};

exports.forward = function(p) {

  var lon = p.x;
  var lat = p.y;

  /* Forward equations
    -----------------*/
  var dlon = adjust_lon(lon - this.long0);
  var x, y;

  if (Math.abs(lat) <= EPSLN) {
    x = this.x0 + this.R * dlon;
    y = this.y0;
  }
  var theta = asinz(2 * Math.abs(lat / Math.PI));
  if ((Math.abs(dlon) <= EPSLN) || (Math.abs(Math.abs(lat) - HALF_PI) <= EPSLN)) {
    x = this.x0;
    if (lat >= 0) {
      y = this.y0 + Math.PI * this.R * Math.tan(0.5 * theta);
    }
    else {
      y = this.y0 + Math.PI * this.R * -Math.tan(0.5 * theta);
    }
    //  return(OK);
  }
  var al = 0.5 * Math.abs((Math.PI / dlon) - (dlon / Math.PI));
  var asq = al * al;
  var sinth = Math.sin(theta);
  var costh = Math.cos(theta);

  var g = costh / (sinth + costh - 1);
  var gsq = g * g;
  var m = g * (2 / sinth - 1);
  var msq = m * m;
  var con = Math.PI * this.R * (al * (g - msq) + Math.sqrt(asq * (g - msq) * (g - msq) - (msq + asq) * (gsq - msq))) / (msq + asq);
  if (dlon < 0) {
    con = -con;
  }
  x = this.x0 + con;
  //con = Math.abs(con / (Math.PI * this.R));
  var q = asq + g;
  con = Math.PI * this.R * (m * q - al * Math.sqrt((msq + asq) * (asq + 1) - q * q)) / (msq + asq);
  if (lat >= 0) {
    //y = this.y0 + Math.PI * this.R * Math.sqrt(1 - con * con - 2 * al * con);
    y = this.y0 + con;
  }
  else {
    //y = this.y0 - Math.PI * this.R * Math.sqrt(1 - con * con - 2 * al * con);
    y = this.y0 - con;
  }
  p.x = x;
  p.y = y;
  return p;
};

/* Van Der Grinten inverse equations--mapping x,y to lat/long
  ---------------------------------------------------------*/
exports.inverse = function(p) {
  var lon, lat;
  var xx, yy, xys, c1, c2, c3;
  var a1;
  var m1;
  var con;
  var th1;
  var d;

  /* inverse equations
    -----------------*/
  p.x -= this.x0;
  p.y -= this.y0;
  con = Math.PI * this.R;
  xx = p.x / con;
  yy = p.y / con;
  xys = xx * xx + yy * yy;
  c1 = -Math.abs(yy) * (1 + xys);
  c2 = c1 - 2 * yy * yy + xx * xx;
  c3 = -2 * c1 + 1 + 2 * yy * yy + xys * xys;
  d = yy * yy / c3 + (2 * c2 * c2 * c2 / c3 / c3 / c3 - 9 * c1 * c2 / c3 / c3) / 27;
  a1 = (c1 - c2 * c2 / 3 / c3) / c3;
  m1 = 2 * Math.sqrt(-a1 / 3);
  con = ((3 * d) / a1) / m1;
  if (Math.abs(con) > 1) {
    if (con >= 0) {
      con = 1;
    }
    else {
      con = -1;
    }
  }
  th1 = Math.acos(con) / 3;
  if (p.y >= 0) {
    lat = (-m1 * Math.cos(th1 + Math.PI / 3) - c2 / 3 / c3) * Math.PI;
  }
  else {
    lat = -(-m1 * Math.cos(th1 + Math.PI / 3) - c2 / 3 / c3) * Math.PI;
  }

  if (Math.abs(xx) < EPSLN) {
    lon = this.long0;
  }
  else {
    lon = adjust_lon(this.long0 + Math.PI * (xys - 1 + Math.sqrt(1 + 2 * (xx * xx - yy * yy) + xys * xys)) / 2 / xx);
  }

  p.x = lon;
  p.y = lat;
  return p;
};
exports.names = ["Van_der_Grinten_I", "VanDerGrinten", "vandg"];
},{"../common/adjust_lon":5,"../common/asinz":6}],65:[function(_dereq_,module,exports){
var D2R = 0.01745329251994329577;
var R2D = 57.29577951308232088;
var PJD_3PARAM = 1;
var PJD_7PARAM = 2;
var datum_transform = _dereq_('./datum_transform');
var adjust_axis = _dereq_('./adjust_axis');
var proj = _dereq_('./Proj');
var toPoint = _dereq_('./common/toPoint');
module.exports = function transform(source, dest, point) {
  var wgs84;
  if (Array.isArray(point)) {
    point = toPoint(point);
  }
  function checkNotWGS(source, dest) {
    return ((source.datum.datum_type === PJD_3PARAM || source.datum.datum_type === PJD_7PARAM) && dest.datumCode !== "WGS84");
  }

  // Workaround for datum shifts towgs84, if either source or destination projection is not wgs84
  if (source.datum && dest.datum && (checkNotWGS(source, dest) || checkNotWGS(dest, source))) {
    wgs84 = new proj('WGS84');
    transform(source, wgs84, point);
    source = wgs84;
  }
  // DGR, 2010/11/12
  if (source.axis !== "enu") {
    adjust_axis(source, false, point);
  }
  // Transform source points to long/lat, if they aren't already.
  if (source.projName === "longlat") {
    point.x *= D2R; // convert degrees to radians
    point.y *= D2R;
  }
  else {
    if (source.to_meter) {
      point.x *= source.to_meter;
      point.y *= source.to_meter;
    }
    source.inverse(point); // Convert Cartesian to longlat
  }
  // Adjust for the prime meridian if necessary
  if (source.from_greenwich) {
    point.x += source.from_greenwich;
  }

  // Convert datums if needed, and if possible.
  point = datum_transform(source.datum, dest.datum, point);

  // Adjust for the prime meridian if necessary
  if (dest.from_greenwich) {
    point.x -= dest.from_greenwich;
  }

  if (dest.projName === "longlat") {
    // convert radians to decimal degrees
    point.x *= R2D;
    point.y *= R2D;
  }
  else { // else project
    dest.forward(point);
    if (dest.to_meter) {
      point.x /= dest.to_meter;
      point.y /= dest.to_meter;
    }
  }

  // DGR, 2010/11/12
  if (dest.axis !== "enu") {
    adjust_axis(dest, true, point);
  }

  return point;
};
},{"./Proj":2,"./adjust_axis":3,"./common/toPoint":23,"./datum_transform":31}],66:[function(_dereq_,module,exports){
var D2R = 0.01745329251994329577;
var extend = _dereq_('./extend');

function mapit(obj, key, v) {
  obj[key] = v.map(function(aa) {
    var o = {};
    sExpr(aa, o);
    return o;
  }).reduce(function(a, b) {
    return extend(a, b);
  }, {});
}

function sExpr(v, obj) {
  var key;
  if (!Array.isArray(v)) {
    obj[v] = true;
    return;
  }
  else {
    key = v.shift();
    if (key === 'PARAMETER') {
      key = v.shift();
    }
    if (v.length === 1) {
      if (Array.isArray(v[0])) {
        obj[key] = {};
        sExpr(v[0], obj[key]);
      }
      else {
        obj[key] = v[0];
      }
    }
    else if (!v.length) {
      obj[key] = true;
    }
    else if (key === 'TOWGS84') {
      obj[key] = v;
    }
    else {
      obj[key] = {};
      if (['UNIT', 'PRIMEM', 'VERT_DATUM'].indexOf(key) > -1) {
        obj[key] = {
          name: v[0].toLowerCase(),
          convert: v[1]
        };
        if (v.length === 3) {
          obj[key].auth = v[2];
        }
      }
      else if (key === 'SPHEROID') {
        obj[key] = {
          name: v[0],
          a: v[1],
          rf: v[2]
        };
        if (v.length === 4) {
          obj[key].auth = v[3];
        }
      }
      else if (['GEOGCS', 'GEOCCS', 'DATUM', 'VERT_CS', 'COMPD_CS', 'LOCAL_CS', 'FITTED_CS', 'LOCAL_DATUM'].indexOf(key) > -1) {
        v[0] = ['name', v[0]];
        mapit(obj, key, v);
      }
      else if (v.every(function(aa) {
        return Array.isArray(aa);
      })) {
        mapit(obj, key, v);
      }
      else {
        sExpr(v, obj[key]);
      }
    }
  }
}

function rename(obj, params) {
  var outName = params[0];
  var inName = params[1];
  if (!(outName in obj) && (inName in obj)) {
    obj[outName] = obj[inName];
    if (params.length === 3) {
      obj[outName] = params[2](obj[outName]);
    }
  }
}

function d2r(input) {
  return input * D2R;
}

function cleanWKT(wkt) {
  if (wkt.type === 'GEOGCS') {
    wkt.projName = 'longlat';
  }
  else if (wkt.type === 'LOCAL_CS') {
    wkt.projName = 'identity';
    wkt.local = true;
  }
  else {
    if (typeof wkt.PROJECTION === "object") {
      wkt.projName = Object.keys(wkt.PROJECTION)[0];
    }
    else {
      wkt.projName = wkt.PROJECTION;
    }
  }
  if (wkt.UNIT) {
    wkt.units = wkt.UNIT.name.toLowerCase();
    if (wkt.units === 'metre') {
      wkt.units = 'meter';
    }
    if (wkt.UNIT.convert) {
      if (wkt.type === 'GEOGCS') {
        if (wkt.DATUM && wkt.DATUM.SPHEROID) {
          wkt.to_meter = parseFloat(wkt.UNIT.convert, 10)*wkt.DATUM.SPHEROID.a;
        }
      } else {
        wkt.to_meter = parseFloat(wkt.UNIT.convert, 10);
      }
    }
  }

  if (wkt.GEOGCS) {
    //if(wkt.GEOGCS.PRIMEM&&wkt.GEOGCS.PRIMEM.convert){
    //  wkt.from_greenwich=wkt.GEOGCS.PRIMEM.convert*D2R;
    //}
    if (wkt.GEOGCS.DATUM) {
      wkt.datumCode = wkt.GEOGCS.DATUM.name.toLowerCase();
    }
    else {
      wkt.datumCode = wkt.GEOGCS.name.toLowerCase();
    }
    if (wkt.datumCode.slice(0, 2) === 'd_') {
      wkt.datumCode = wkt.datumCode.slice(2);
    }
    if (wkt.datumCode === 'new_zealand_geodetic_datum_1949' || wkt.datumCode === 'new_zealand_1949') {
      wkt.datumCode = 'nzgd49';
    }
    if (wkt.datumCode === "wgs_1984") {
      if (wkt.PROJECTION === 'Mercator_Auxiliary_Sphere') {
        wkt.sphere = true;
      }
      wkt.datumCode = 'wgs84';
    }
    if (wkt.datumCode.slice(-6) === '_ferro') {
      wkt.datumCode = wkt.datumCode.slice(0, - 6);
    }
    if (wkt.datumCode.slice(-8) === '_jakarta') {
      wkt.datumCode = wkt.datumCode.slice(0, - 8);
    }
    if (~wkt.datumCode.indexOf('belge')) {
      wkt.datumCode = "rnb72";
    }
    if (wkt.GEOGCS.DATUM && wkt.GEOGCS.DATUM.SPHEROID) {
      wkt.ellps = wkt.GEOGCS.DATUM.SPHEROID.name.replace('_19', '').replace(/[Cc]larke\_18/, 'clrk');
      if (wkt.ellps.toLowerCase().slice(0, 13) === "international") {
        wkt.ellps = 'intl';
      }

      wkt.a = wkt.GEOGCS.DATUM.SPHEROID.a;
      wkt.rf = parseFloat(wkt.GEOGCS.DATUM.SPHEROID.rf, 10);
    }
    if (~wkt.datumCode.indexOf('osgb_1936')) {
      wkt.datumCode = "osgb36";
    }
  }
  if (wkt.b && !isFinite(wkt.b)) {
    wkt.b = wkt.a;
  }

  function toMeter(input) {
    var ratio = wkt.to_meter || 1;
    return parseFloat(input, 10) * ratio;
  }
  var renamer = function(a) {
    return rename(wkt, a);
  };
  var list = [
    ['standard_parallel_1', 'Standard_Parallel_1'],
    ['standard_parallel_2', 'Standard_Parallel_2'],
    ['false_easting', 'False_Easting'],
    ['false_northing', 'False_Northing'],
    ['central_meridian', 'Central_Meridian'],
    ['latitude_of_origin', 'Latitude_Of_Origin'],
    ['latitude_of_origin', 'Central_Parallel'],
    ['scale_factor', 'Scale_Factor'],
    ['k0', 'scale_factor'],
    ['latitude_of_center', 'Latitude_of_center'],
    ['lat0', 'latitude_of_center', d2r],
    ['longitude_of_center', 'Longitude_Of_Center'],
    ['longc', 'longitude_of_center', d2r],
    ['x0', 'false_easting', toMeter],
    ['y0', 'false_northing', toMeter],
    ['long0', 'central_meridian', d2r],
    ['lat0', 'latitude_of_origin', d2r],
    ['lat0', 'standard_parallel_1', d2r],
    ['lat1', 'standard_parallel_1', d2r],
    ['lat2', 'standard_parallel_2', d2r],
    ['alpha', 'azimuth', d2r],
    ['srsCode', 'name']
  ];
  list.forEach(renamer);
  if (!wkt.long0 && wkt.longc && (wkt.projName === 'Albers_Conic_Equal_Area' || wkt.projName === "Lambert_Azimuthal_Equal_Area")) {
    wkt.long0 = wkt.longc;
  }
  if (!wkt.lat_ts && wkt.lat1 && (wkt.projName === 'Stereographic_South_Pole' || wkt.projName === 'Polar Stereographic (variant B)')) {
    wkt.lat0 = d2r(wkt.lat1 > 0 ? 90 : -90);
    wkt.lat_ts = wkt.lat1;
  }
}
module.exports = function(wkt, self) {
  var lisp = JSON.parse(("," + wkt).replace(/\s*\,\s*([A-Z_0-9]+?)(\[)/g, ',["$1",').slice(1).replace(/\s*\,\s*([A-Z_0-9]+?)\]/g, ',"$1"]').replace(/,\["VERTCS".+/,''));
  var type = lisp.shift();
  var name = lisp.shift();
  lisp.unshift(['name', name]);
  lisp.unshift(['type', type]);
  lisp.unshift('output');
  var obj = {};
  sExpr(lisp, obj);
  cleanWKT(obj.output);
  return extend(self, obj.output);
};

},{"./extend":34}],67:[function(_dereq_,module,exports){



/**
 * UTM zones are grouped, and assigned to one of a group of 6
 * sets.
 *
 * {int} @private
 */
var NUM_100K_SETS = 6;

/**
 * The column letters (for easting) of the lower left value, per
 * set.
 *
 * {string} @private
 */
var SET_ORIGIN_COLUMN_LETTERS = 'AJSAJS';

/**
 * The row letters (for northing) of the lower left value, per
 * set.
 *
 * {string} @private
 */
var SET_ORIGIN_ROW_LETTERS = 'AFAFAF';

var A = 65; // A
var I = 73; // I
var O = 79; // O
var V = 86; // V
var Z = 90; // Z

/**
 * Conversion of lat/lon to MGRS.
 *
 * @param {object} ll Object literal with lat and lon properties on a
 *     WGS84 ellipsoid.
 * @param {int} accuracy Accuracy in digits (5 for 1 m, 4 for 10 m, 3 for
 *      100 m, 2 for 1000 m or 1 for 10000 m). Optional, default is 5.
 * @return {string} the MGRS string for the given location and accuracy.
 */
exports.forward = function(ll, accuracy) {
  accuracy = accuracy || 5; // default accuracy 1m
  return encode(LLtoUTM({
    lat: ll[1],
    lon: ll[0]
  }), accuracy);
};

/**
 * Conversion of MGRS to lat/lon.
 *
 * @param {string} mgrs MGRS string.
 * @return {array} An array with left (longitude), bottom (latitude), right
 *     (longitude) and top (latitude) values in WGS84, representing the
 *     bounding box for the provided MGRS reference.
 */
exports.inverse = function(mgrs) {
  var bbox = UTMtoLL(decode(mgrs.toUpperCase()));
  if (bbox.lat && bbox.lon) {
    return [bbox.lon, bbox.lat, bbox.lon, bbox.lat];
  }
  return [bbox.left, bbox.bottom, bbox.right, bbox.top];
};

exports.toPoint = function(mgrs) {
  var bbox = UTMtoLL(decode(mgrs.toUpperCase()));
  if (bbox.lat && bbox.lon) {
    return [bbox.lon, bbox.lat];
  }
  return [(bbox.left + bbox.right) / 2, (bbox.top + bbox.bottom) / 2];
};
/**
 * Conversion from degrees to radians.
 *
 * @private
 * @param {number} deg the angle in degrees.
 * @return {number} the angle in radians.
 */
function degToRad(deg) {
  return (deg * (Math.PI / 180.0));
}

/**
 * Conversion from radians to degrees.
 *
 * @private
 * @param {number} rad the angle in radians.
 * @return {number} the angle in degrees.
 */
function radToDeg(rad) {
  return (180.0 * (rad / Math.PI));
}

/**
 * Converts a set of Longitude and Latitude co-ordinates to UTM
 * using the WGS84 ellipsoid.
 *
 * @private
 * @param {object} ll Object literal with lat and lon properties
 *     representing the WGS84 coordinate to be converted.
 * @return {object} Object literal containing the UTM value with easting,
 *     northing, zoneNumber and zoneLetter properties, and an optional
 *     accuracy property in digits. Returns null if the conversion failed.
 */
function LLtoUTM(ll) {
  var Lat = ll.lat;
  var Long = ll.lon;
  var a = 6378137.0; //ellip.radius;
  var eccSquared = 0.00669438; //ellip.eccsq;
  var k0 = 0.9996;
  var LongOrigin;
  var eccPrimeSquared;
  var N, T, C, A, M;
  var LatRad = degToRad(Lat);
  var LongRad = degToRad(Long);
  var LongOriginRad;
  var ZoneNumber;
  // (int)
  ZoneNumber = Math.floor((Long + 180) / 6) + 1;

  //Make sure the longitude 180.00 is in Zone 60
  if (Long === 180) {
    ZoneNumber = 60;
  }

  // Special zone for Norway
  if (Lat >= 56.0 && Lat < 64.0 && Long >= 3.0 && Long < 12.0) {
    ZoneNumber = 32;
  }

  // Special zones for Svalbard
  if (Lat >= 72.0 && Lat < 84.0) {
    if (Long >= 0.0 && Long < 9.0) {
      ZoneNumber = 31;
    }
    else if (Long >= 9.0 && Long < 21.0) {
      ZoneNumber = 33;
    }
    else if (Long >= 21.0 && Long < 33.0) {
      ZoneNumber = 35;
    }
    else if (Long >= 33.0 && Long < 42.0) {
      ZoneNumber = 37;
    }
  }

  LongOrigin = (ZoneNumber - 1) * 6 - 180 + 3; //+3 puts origin
  // in middle of
  // zone
  LongOriginRad = degToRad(LongOrigin);

  eccPrimeSquared = (eccSquared) / (1 - eccSquared);

  N = a / Math.sqrt(1 - eccSquared * Math.sin(LatRad) * Math.sin(LatRad));
  T = Math.tan(LatRad) * Math.tan(LatRad);
  C = eccPrimeSquared * Math.cos(LatRad) * Math.cos(LatRad);
  A = Math.cos(LatRad) * (LongRad - LongOriginRad);

  M = a * ((1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256) * LatRad - (3 * eccSquared / 8 + 3 * eccSquared * eccSquared / 32 + 45 * eccSquared * eccSquared * eccSquared / 1024) * Math.sin(2 * LatRad) + (15 * eccSquared * eccSquared / 256 + 45 * eccSquared * eccSquared * eccSquared / 1024) * Math.sin(4 * LatRad) - (35 * eccSquared * eccSquared * eccSquared / 3072) * Math.sin(6 * LatRad));

  var UTMEasting = (k0 * N * (A + (1 - T + C) * A * A * A / 6.0 + (5 - 18 * T + T * T + 72 * C - 58 * eccPrimeSquared) * A * A * A * A * A / 120.0) + 500000.0);

  var UTMNorthing = (k0 * (M + N * Math.tan(LatRad) * (A * A / 2 + (5 - T + 9 * C + 4 * C * C) * A * A * A * A / 24.0 + (61 - 58 * T + T * T + 600 * C - 330 * eccPrimeSquared) * A * A * A * A * A * A / 720.0)));
  if (Lat < 0.0) {
    UTMNorthing += 10000000.0; //10000000 meter offset for
    // southern hemisphere
  }

  return {
    northing: Math.round(UTMNorthing),
    easting: Math.round(UTMEasting),
    zoneNumber: ZoneNumber,
    zoneLetter: getLetterDesignator(Lat)
  };
}

/**
 * Converts UTM coords to lat/long, using the WGS84 ellipsoid. This is a convenience
 * class where the Zone can be specified as a single string eg."60N" which
 * is then broken down into the ZoneNumber and ZoneLetter.
 *
 * @private
 * @param {object} utm An object literal with northing, easting, zoneNumber
 *     and zoneLetter properties. If an optional accuracy property is
 *     provided (in meters), a bounding box will be returned instead of
 *     latitude and longitude.
 * @return {object} An object literal containing either lat and lon values
 *     (if no accuracy was provided), or top, right, bottom and left values
 *     for the bounding box calculated according to the provided accuracy.
 *     Returns null if the conversion failed.
 */
function UTMtoLL(utm) {

  var UTMNorthing = utm.northing;
  var UTMEasting = utm.easting;
  var zoneLetter = utm.zoneLetter;
  var zoneNumber = utm.zoneNumber;
  // check the ZoneNummber is valid
  if (zoneNumber < 0 || zoneNumber > 60) {
    return null;
  }

  var k0 = 0.9996;
  var a = 6378137.0; //ellip.radius;
  var eccSquared = 0.00669438; //ellip.eccsq;
  var eccPrimeSquared;
  var e1 = (1 - Math.sqrt(1 - eccSquared)) / (1 + Math.sqrt(1 - eccSquared));
  var N1, T1, C1, R1, D, M;
  var LongOrigin;
  var mu, phi1Rad;

  // remove 500,000 meter offset for longitude
  var x = UTMEasting - 500000.0;
  var y = UTMNorthing;

  // We must know somehow if we are in the Northern or Southern
  // hemisphere, this is the only time we use the letter So even
  // if the Zone letter isn't exactly correct it should indicate
  // the hemisphere correctly
  if (zoneLetter < 'N') {
    y -= 10000000.0; // remove 10,000,000 meter offset used
    // for southern hemisphere
  }

  // There are 60 zones with zone 1 being at West -180 to -174
  LongOrigin = (zoneNumber - 1) * 6 - 180 + 3; // +3 puts origin
  // in middle of
  // zone

  eccPrimeSquared = (eccSquared) / (1 - eccSquared);

  M = y / k0;
  mu = M / (a * (1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256));

  phi1Rad = mu + (3 * e1 / 2 - 27 * e1 * e1 * e1 / 32) * Math.sin(2 * mu) + (21 * e1 * e1 / 16 - 55 * e1 * e1 * e1 * e1 / 32) * Math.sin(4 * mu) + (151 * e1 * e1 * e1 / 96) * Math.sin(6 * mu);
  // double phi1 = ProjMath.radToDeg(phi1Rad);

  N1 = a / Math.sqrt(1 - eccSquared * Math.sin(phi1Rad) * Math.sin(phi1Rad));
  T1 = Math.tan(phi1Rad) * Math.tan(phi1Rad);
  C1 = eccPrimeSquared * Math.cos(phi1Rad) * Math.cos(phi1Rad);
  R1 = a * (1 - eccSquared) / Math.pow(1 - eccSquared * Math.sin(phi1Rad) * Math.sin(phi1Rad), 1.5);
  D = x / (N1 * k0);

  var lat = phi1Rad - (N1 * Math.tan(phi1Rad) / R1) * (D * D / 2 - (5 + 3 * T1 + 10 * C1 - 4 * C1 * C1 - 9 * eccPrimeSquared) * D * D * D * D / 24 + (61 + 90 * T1 + 298 * C1 + 45 * T1 * T1 - 252 * eccPrimeSquared - 3 * C1 * C1) * D * D * D * D * D * D / 720);
  lat = radToDeg(lat);

  var lon = (D - (1 + 2 * T1 + C1) * D * D * D / 6 + (5 - 2 * C1 + 28 * T1 - 3 * C1 * C1 + 8 * eccPrimeSquared + 24 * T1 * T1) * D * D * D * D * D / 120) / Math.cos(phi1Rad);
  lon = LongOrigin + radToDeg(lon);

  var result;
  if (utm.accuracy) {
    var topRight = UTMtoLL({
      northing: utm.northing + utm.accuracy,
      easting: utm.easting + utm.accuracy,
      zoneLetter: utm.zoneLetter,
      zoneNumber: utm.zoneNumber
    });
    result = {
      top: topRight.lat,
      right: topRight.lon,
      bottom: lat,
      left: lon
    };
  }
  else {
    result = {
      lat: lat,
      lon: lon
    };
  }
  return result;
}

/**
 * Calculates the MGRS letter designator for the given latitude.
 *
 * @private
 * @param {number} lat The latitude in WGS84 to get the letter designator
 *     for.
 * @return {char} The letter designator.
 */
function getLetterDesignator(lat) {
  //This is here as an error flag to show that the Latitude is
  //outside MGRS limits
  var LetterDesignator = 'Z';

  if ((84 >= lat) && (lat >= 72)) {
    LetterDesignator = 'X';
  }
  else if ((72 > lat) && (lat >= 64)) {
    LetterDesignator = 'W';
  }
  else if ((64 > lat) && (lat >= 56)) {
    LetterDesignator = 'V';
  }
  else if ((56 > lat) && (lat >= 48)) {
    LetterDesignator = 'U';
  }
  else if ((48 > lat) && (lat >= 40)) {
    LetterDesignator = 'T';
  }
  else if ((40 > lat) && (lat >= 32)) {
    LetterDesignator = 'S';
  }
  else if ((32 > lat) && (lat >= 24)) {
    LetterDesignator = 'R';
  }
  else if ((24 > lat) && (lat >= 16)) {
    LetterDesignator = 'Q';
  }
  else if ((16 > lat) && (lat >= 8)) {
    LetterDesignator = 'P';
  }
  else if ((8 > lat) && (lat >= 0)) {
    LetterDesignator = 'N';
  }
  else if ((0 > lat) && (lat >= -8)) {
    LetterDesignator = 'M';
  }
  else if ((-8 > lat) && (lat >= -16)) {
    LetterDesignator = 'L';
  }
  else if ((-16 > lat) && (lat >= -24)) {
    LetterDesignator = 'K';
  }
  else if ((-24 > lat) && (lat >= -32)) {
    LetterDesignator = 'J';
  }
  else if ((-32 > lat) && (lat >= -40)) {
    LetterDesignator = 'H';
  }
  else if ((-40 > lat) && (lat >= -48)) {
    LetterDesignator = 'G';
  }
  else if ((-48 > lat) && (lat >= -56)) {
    LetterDesignator = 'F';
  }
  else if ((-56 > lat) && (lat >= -64)) {
    LetterDesignator = 'E';
  }
  else if ((-64 > lat) && (lat >= -72)) {
    LetterDesignator = 'D';
  }
  else if ((-72 > lat) && (lat >= -80)) {
    LetterDesignator = 'C';
  }
  return LetterDesignator;
}

/**
 * Encodes a UTM location as MGRS string.
 *
 * @private
 * @param {object} utm An object literal with easting, northing,
 *     zoneLetter, zoneNumber
 * @param {number} accuracy Accuracy in digits (1-5).
 * @return {string} MGRS string for the given UTM location.
 */
function encode(utm, accuracy) {
  // prepend with leading zeroes
  var seasting = "00000" + utm.easting,
    snorthing = "00000" + utm.northing;

  return utm.zoneNumber + utm.zoneLetter + get100kID(utm.easting, utm.northing, utm.zoneNumber) + seasting.substr(seasting.length - 5, accuracy) + snorthing.substr(snorthing.length - 5, accuracy);
}

/**
 * Get the two letter 100k designator for a given UTM easting,
 * northing and zone number value.
 *
 * @private
 * @param {number} easting
 * @param {number} northing
 * @param {number} zoneNumber
 * @return the two letter 100k designator for the given UTM location.
 */
function get100kID(easting, northing, zoneNumber) {
  var setParm = get100kSetForZone(zoneNumber);
  var setColumn = Math.floor(easting / 100000);
  var setRow = Math.floor(northing / 100000) % 20;
  return getLetter100kID(setColumn, setRow, setParm);
}

/**
 * Given a UTM zone number, figure out the MGRS 100K set it is in.
 *
 * @private
 * @param {number} i An UTM zone number.
 * @return {number} the 100k set the UTM zone is in.
 */
function get100kSetForZone(i) {
  var setParm = i % NUM_100K_SETS;
  if (setParm === 0) {
    setParm = NUM_100K_SETS;
  }

  return setParm;
}

/**
 * Get the two-letter MGRS 100k designator given information
 * translated from the UTM northing, easting and zone number.
 *
 * @private
 * @param {number} column the column index as it relates to the MGRS
 *        100k set spreadsheet, created from the UTM easting.
 *        Values are 1-8.
 * @param {number} row the row index as it relates to the MGRS 100k set
 *        spreadsheet, created from the UTM northing value. Values
 *        are from 0-19.
 * @param {number} parm the set block, as it relates to the MGRS 100k set
 *        spreadsheet, created from the UTM zone. Values are from
 *        1-60.
 * @return two letter MGRS 100k code.
 */
function getLetter100kID(column, row, parm) {
  // colOrigin and rowOrigin are the letters at the origin of the set
  var index = parm - 1;
  var colOrigin = SET_ORIGIN_COLUMN_LETTERS.charCodeAt(index);
  var rowOrigin = SET_ORIGIN_ROW_LETTERS.charCodeAt(index);

  // colInt and rowInt are the letters to build to return
  var colInt = colOrigin + column - 1;
  var rowInt = rowOrigin + row;
  var rollover = false;

  if (colInt > Z) {
    colInt = colInt - Z + A - 1;
    rollover = true;
  }

  if (colInt === I || (colOrigin < I && colInt > I) || ((colInt > I || colOrigin < I) && rollover)) {
    colInt++;
  }

  if (colInt === O || (colOrigin < O && colInt > O) || ((colInt > O || colOrigin < O) && rollover)) {
    colInt++;

    if (colInt === I) {
      colInt++;
    }
  }

  if (colInt > Z) {
    colInt = colInt - Z + A - 1;
  }

  if (rowInt > V) {
    rowInt = rowInt - V + A - 1;
    rollover = true;
  }
  else {
    rollover = false;
  }

  if (((rowInt === I) || ((rowOrigin < I) && (rowInt > I))) || (((rowInt > I) || (rowOrigin < I)) && rollover)) {
    rowInt++;
  }

  if (((rowInt === O) || ((rowOrigin < O) && (rowInt > O))) || (((rowInt > O) || (rowOrigin < O)) && rollover)) {
    rowInt++;

    if (rowInt === I) {
      rowInt++;
    }
  }

  if (rowInt > V) {
    rowInt = rowInt - V + A - 1;
  }

  var twoLetter = String.fromCharCode(colInt) + String.fromCharCode(rowInt);
  return twoLetter;
}

/**
 * Decode the UTM parameters from a MGRS string.
 *
 * @private
 * @param {string} mgrsString an UPPERCASE coordinate string is expected.
 * @return {object} An object literal with easting, northing, zoneLetter,
 *     zoneNumber and accuracy (in meters) properties.
 */
function decode(mgrsString) {

  if (mgrsString && mgrsString.length === 0) {
    throw ("MGRSPoint coverting from nothing");
  }

  var length = mgrsString.length;

  var hunK = null;
  var sb = "";
  var testChar;
  var i = 0;

  // get Zone number
  while (!(/[A-Z]/).test(testChar = mgrsString.charAt(i))) {
    if (i >= 2) {
      throw ("MGRSPoint bad conversion from: " + mgrsString);
    }
    sb += testChar;
    i++;
  }

  var zoneNumber = parseInt(sb, 10);

  if (i === 0 || i + 3 > length) {
    // A good MGRS string has to be 4-5 digits long,
    // ##AAA/#AAA at least.
    throw ("MGRSPoint bad conversion from: " + mgrsString);
  }

  var zoneLetter = mgrsString.charAt(i++);

  // Should we check the zone letter here? Why not.
  if (zoneLetter <= 'A' || zoneLetter === 'B' || zoneLetter === 'Y' || zoneLetter >= 'Z' || zoneLetter === 'I' || zoneLetter === 'O') {
    throw ("MGRSPoint zone letter " + zoneLetter + " not handled: " + mgrsString);
  }

  hunK = mgrsString.substring(i, i += 2);

  var set = get100kSetForZone(zoneNumber);

  var east100k = getEastingFromChar(hunK.charAt(0), set);
  var north100k = getNorthingFromChar(hunK.charAt(1), set);

  // We have a bug where the northing may be 2000000 too low.
  // How
  // do we know when to roll over?

  while (north100k < getMinNorthing(zoneLetter)) {
    north100k += 2000000;
  }

  // calculate the char index for easting/northing separator
  var remainder = length - i;

  if (remainder % 2 !== 0) {
    throw ("MGRSPoint has to have an even number \nof digits after the zone letter and two 100km letters - front \nhalf for easting meters, second half for \nnorthing meters" + mgrsString);
  }

  var sep = remainder / 2;

  var sepEasting = 0.0;
  var sepNorthing = 0.0;
  var accuracyBonus, sepEastingString, sepNorthingString, easting, northing;
  if (sep > 0) {
    accuracyBonus = 100000.0 / Math.pow(10, sep);
    sepEastingString = mgrsString.substring(i, i + sep);
    sepEasting = parseFloat(sepEastingString) * accuracyBonus;
    sepNorthingString = mgrsString.substring(i + sep);
    sepNorthing = parseFloat(sepNorthingString) * accuracyBonus;
  }

  easting = sepEasting + east100k;
  northing = sepNorthing + north100k;

  return {
    easting: easting,
    northing: northing,
    zoneLetter: zoneLetter,
    zoneNumber: zoneNumber,
    accuracy: accuracyBonus
  };
}

/**
 * Given the first letter from a two-letter MGRS 100k zone, and given the
 * MGRS table set for the zone number, figure out the easting value that
 * should be added to the other, secondary easting value.
 *
 * @private
 * @param {char} e The first letter from a two-letter MGRS 100´k zone.
 * @param {number} set The MGRS table set for the zone number.
 * @return {number} The easting value for the given letter and set.
 */
function getEastingFromChar(e, set) {
  // colOrigin is the letter at the origin of the set for the
  // column
  var curCol = SET_ORIGIN_COLUMN_LETTERS.charCodeAt(set - 1);
  var eastingValue = 100000.0;
  var rewindMarker = false;

  while (curCol !== e.charCodeAt(0)) {
    curCol++;
    if (curCol === I) {
      curCol++;
    }
    if (curCol === O) {
      curCol++;
    }
    if (curCol > Z) {
      if (rewindMarker) {
        throw ("Bad character: " + e);
      }
      curCol = A;
      rewindMarker = true;
    }
    eastingValue += 100000.0;
  }

  return eastingValue;
}

/**
 * Given the second letter from a two-letter MGRS 100k zone, and given the
 * MGRS table set for the zone number, figure out the northing value that
 * should be added to the other, secondary northing value. You have to
 * remember that Northings are determined from the equator, and the vertical
 * cycle of letters mean a 2000000 additional northing meters. This happens
 * approx. every 18 degrees of latitude. This method does *NOT* count any
 * additional northings. You have to figure out how many 2000000 meters need
 * to be added for the zone letter of the MGRS coordinate.
 *
 * @private
 * @param {char} n Second letter of the MGRS 100k zone
 * @param {number} set The MGRS table set number, which is dependent on the
 *     UTM zone number.
 * @return {number} The northing value for the given letter and set.
 */
function getNorthingFromChar(n, set) {

  if (n > 'V') {
    throw ("MGRSPoint given invalid Northing " + n);
  }

  // rowOrigin is the letter at the origin of the set for the
  // column
  var curRow = SET_ORIGIN_ROW_LETTERS.charCodeAt(set - 1);
  var northingValue = 0.0;
  var rewindMarker = false;

  while (curRow !== n.charCodeAt(0)) {
    curRow++;
    if (curRow === I) {
      curRow++;
    }
    if (curRow === O) {
      curRow++;
    }
    // fixing a bug making whole application hang in this loop
    // when 'n' is a wrong character
    if (curRow > V) {
      if (rewindMarker) { // making sure that this loop ends
        throw ("Bad character: " + n);
      }
      curRow = A;
      rewindMarker = true;
    }
    northingValue += 100000.0;
  }

  return northingValue;
}

/**
 * The function getMinNorthing returns the minimum northing value of a MGRS
 * zone.
 *
 * Ported from Geotrans' c Lattitude_Band_Value structure table.
 *
 * @private
 * @param {char} zoneLetter The MGRS zone to get the min northing for.
 * @return {number}
 */
function getMinNorthing(zoneLetter) {
  var northing;
  switch (zoneLetter) {
  case 'C':
    northing = 1100000.0;
    break;
  case 'D':
    northing = 2000000.0;
    break;
  case 'E':
    northing = 2800000.0;
    break;
  case 'F':
    northing = 3700000.0;
    break;
  case 'G':
    northing = 4600000.0;
    break;
  case 'H':
    northing = 5500000.0;
    break;
  case 'J':
    northing = 6400000.0;
    break;
  case 'K':
    northing = 7300000.0;
    break;
  case 'L':
    northing = 8200000.0;
    break;
  case 'M':
    northing = 9100000.0;
    break;
  case 'N':
    northing = 0.0;
    break;
  case 'P':
    northing = 800000.0;
    break;
  case 'Q':
    northing = 1700000.0;
    break;
  case 'R':
    northing = 2600000.0;
    break;
  case 'S':
    northing = 3500000.0;
    break;
  case 'T':
    northing = 4400000.0;
    break;
  case 'U':
    northing = 5300000.0;
    break;
  case 'V':
    northing = 6200000.0;
    break;
  case 'W':
    northing = 7000000.0;
    break;
  case 'X':
    northing = 7900000.0;
    break;
  default:
    northing = -1.0;
  }
  if (northing >= 0.0) {
    return northing;
  }
  else {
    throw ("Invalid zone letter: " + zoneLetter);
  }

}

},{}],68:[function(_dereq_,module,exports){
module.exports={
  "name": "proj4",
  "version": "2.3.12",
  "description": "Proj4js is a JavaScript library to transform point coordinates from one coordinate system to another, including datum transformations.",
  "main": "lib/index.js",
  "directories": {
    "test": "test",
    "doc": "docs"
  },
  "scripts": {
    "test": "./node_modules/istanbul/lib/cli.js test ./node_modules/mocha/bin/_mocha test/test.js"
  },
  "repository": {
    "type": "git",
    "url": "git://github.com/proj4js/proj4js.git"
  },
  "author": "",
  "license": "MIT",
  "jam": {
    "main": "dist/proj4.js",
    "include": [
      "dist/proj4.js",
      "README.md",
      "AUTHORS",
      "LICENSE.md"
    ]
  },
  "devDependencies": {
    "grunt-cli": "~0.1.13",
    "grunt": "~0.4.2",
    "grunt-contrib-connect": "~0.6.0",
    "grunt-contrib-jshint": "~0.8.0",
    "chai": "~1.8.1",
    "mocha": "~1.17.1",
    "grunt-mocha-phantomjs": "~0.4.0",
    "browserify": "~3.24.5",
    "grunt-browserify": "~1.3.0",
    "grunt-contrib-uglify": "~0.3.2",
    "curl": "git://github.com/cujojs/curl.git",
    "istanbul": "~0.2.4",
    "tin": "~0.4.0"
  },
  "dependencies": {
    "mgrs": "~0.0.2"
  }
}

},{}],"./includedProjections":[function(_dereq_,module,exports){
module.exports=_dereq_('hTEDpn');
},{}],"hTEDpn":[function(_dereq_,module,exports){
var projs = [
 _dereq_('./lib/projections/tmerc'),
	_dereq_('./lib/projections/utm'),
	_dereq_('./lib/projections/sterea'),
	_dereq_('./lib/projections/stere'),
	_dereq_('./lib/projections/somerc'),
	_dereq_('./lib/projections/omerc'),
	_dereq_('./lib/projections/lcc'),
	_dereq_('./lib/projections/krovak'),
	_dereq_('./lib/projections/cass'),
	_dereq_('./lib/projections/laea'),
	_dereq_('./lib/projections/aea'),
	_dereq_('./lib/projections/gnom'),
	_dereq_('./lib/projections/cea'),
	_dereq_('./lib/projections/eqc'),
	_dereq_('./lib/projections/poly'),
	_dereq_('./lib/projections/nzmg'),
	_dereq_('./lib/projections/mill'),
	_dereq_('./lib/projections/sinu'),
	_dereq_('./lib/projections/moll'),
	_dereq_('./lib/projections/eqdc'),
	_dereq_('./lib/projections/vandg'),
	_dereq_('./lib/projections/aeqd')
];
module.exports = function(proj4){
 projs.forEach(function(proj){
   proj4.Proj.projections.add(proj);
 });
}
},{"./lib/projections/aea":40,"./lib/projections/aeqd":41,"./lib/projections/cass":42,"./lib/projections/cea":43,"./lib/projections/eqc":44,"./lib/projections/eqdc":45,"./lib/projections/gnom":47,"./lib/projections/krovak":48,"./lib/projections/laea":49,"./lib/projections/lcc":50,"./lib/projections/mill":53,"./lib/projections/moll":54,"./lib/projections/nzmg":55,"./lib/projections/omerc":56,"./lib/projections/poly":57,"./lib/projections/sinu":58,"./lib/projections/somerc":59,"./lib/projections/stere":60,"./lib/projections/sterea":61,"./lib/projections/tmerc":62,"./lib/projections/utm":63,"./lib/projections/vandg":64}]},{},[36])
(36)
});
//require(["proj4"]);

if (!Mapzania)
    Mapzania = {};

Mapzania = (function (Mapzania) {

    proj4.defs("LATLONG", proj4.defs("WGS84"));
    proj4.defs("GoogleCoordinateSystem", proj4.defs("EPSG:900913"));
    proj4.defs("LO25", "PROJCS[\"LO25WGS84\",GEOGCS[\"GCS_Hartebeesthoek_1994\",DATUM[\"D_Hartebeesthoek_1994\",SPHEROID[\"WGS_1984\",6378137.0,298.257223563]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",25.0],PARAMETER[\"Scale_Factor\",1.0],PARAMETER[\"Latitude_Of_Origin\",0.0],UNIT[\"Meter\",1.0]]");

    var projectExtent = function (extent, fromSrid, toSrid ) {

        var min = { x: extent.minX, y: extent.minY };
        var max = { x: extent.maxX, y: extent.maxY };

        proj4(fromSrid, toSrid, min);
        proj4(fromSrid, toSrid, max);

        extent.minX = min.x;
        extent.minY = min.y;
        extent.maxX = max.x;
        extent.maxY = max.y;
    };

    var projectFeature = function (feature, fromSrid, toSrid) {
        projectGeometry(feature.geometry, fromSrid, toSrid)
    }

    var projectFeatureArray = function (features, fromSrid, toSrid) {
        
        for (var i = 0; i < features.length; i++) 
            projectFeature(features[i], fromSrid, toSrid);
    }

    var projectGeometry = function (geometry, fromSrid, toSrid) {
        
        switch (geometry.getType()) {
            case 1:
                projectPoint(geometry, fromSrid, toSrid);
                break;
            case 2:
                projectLine(geometry, fromSrid, toSrid);
                break;
            case 3:
                projectPolygon(geometry, fromSrid, toSrid);
                break;
            case 4:
                projectMultiPoint(geometry, fromSrid, toSrid);
                break;
            case 5:
                projectMultiLine(geometry, fromSrid, toSrid);
                break;
            case 6:
                projectMultiPolygon(geometry, fromSrid, toSrid);
                break;
            default:
                throw "Cannot project unknown geometry type."
        
        }
    }

    var projectPoint = function (geom, fromSrid, toSrid) {
        //NOTE: This function changed from cloning and then projecting to projecting passed object
        proj4(fromSrid, toSrid, geom);
    };

    var projectLine = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.points.length; i++)
            projectPoint(geom.points[i], fromSrid, toSrid);
    }

    var projectPolygon = function (geom, fromSrid, toSrid) {

        projectLine(geom.shell, fromSrid, toSrid);

        for (var i = 0; i < geom.holes.length; i++)
            projectLine(geom.holes[i], fromSrid, toSrid);
    }

    var projectMultiPoint = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.points.length; i++)
            projectPoint(geom.points[i], fromSrid, toSrid);
    }

    var projectMultiLine = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.lines.length; i++)
            projectLine(geom.lines[i], fromSrid, toSrid);
    }

    var projectMultiPolygon = function (geom, fromSrid, toSrid) {

        for (var i = 0; i < geom.polygons.length; i++)
            projectPolygon(geom.polygons[i], fromSrid, toSrid);
    }


    Mapzania.Projection = {
        projectExtent: projectExtent,
        projectFeature: projectFeature,
        projectFeatureArray: projectFeatureArray,
        projectGeometry:projectGeometry,
        projectPoint: projectPoint,
        projectLine: projectLine,
        projectPolygon: projectPolygon,
        projectMultiPoint: projectMultiPoint,
        projectMultiLine: projectMultiLine,
        projectMultiPolygon: projectMultiPolygon,
    };

    return Mapzania;

})(Mapzania || {});