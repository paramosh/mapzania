﻿(function () {
    'use strict';

    angular
        .module('app')
        .config(config);

    function config($routeProvider) {
        $routeProvider
            .when('/list', {
                templateUrl: 'listView.html',
                controller: 'listController',
                controllerAs: 'listVm'
            });
    }

})();