﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('sourcesDialog', controller);

    function controller(data, events) {
        var vm = this;

        vm.sources = data.sources;

        vm.cancel = cancel;
        vm.select = select;

        /////////

        function cancel() {
            events.cancel();
        }

        function select(map) {
            events.complete(map);
        }
    }

})();