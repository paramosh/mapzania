﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('dialogs', service);

    service.$inject = ['$q', '$mdDialog', 'api'];

    function service($q, $mdDialog, api) {

        return {
            confirm: confirm,
            show: show,
            showHelp: showHelp
        };

        ////////////

        function confirm(text) {

            var confirm = $mdDialog.confirm()
                  .title('Please confirm')
                  .textContent(text)
                  .ariaLabel(text)
                  .ok('Yes')
                  .cancel('No');

            return $mdDialog.show(confirm);
        }

        function show(name, data, options) {

            var defaults = {
                showClose: true
            };

            var settings = Object.assign(defaults, options);

            var templateUrl = api.getTemplateUrl(
                "dialogs/" + name + "/"
                + name + ".html");

            return $q((resolve, reject) => {

                $mdDialog.show({
                    templateUrl: templateUrl,
                    controller: name + "Dialog",
                    controllerAs: "vm",
                    parent: angular.element(document.body),
                    locals: {
                        options: settings,
                        data: data,
                        events: {
                            cancel: function () {
                                $mdDialog.cancel();

                            },
                            complete: function (data) {
                                $mdDialog.cancel();
                                resolve(data);
                            }
                        },
                        api:api
                    }
                });
            });
        };

        function showHelp(event, title, text) {

            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title(title)
                .textContent(text)
                .ariaLabel(text)
                .ok('Done')
                .targetEvent(event)
            );
        }
    }

})();