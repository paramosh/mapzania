﻿(function () {
    'use strict';

    angular
        .module("app")
        .directive("inputField", directive);

    ////////////////////////////////////////

    directive.$inject = ['api'];

    function directive(api) {

        return {
            scope: {
                model: '=ngModel',
                type: '@',
                property: '@',
                title: '@',
                toolTip: '@'
            },
            templateUrl: api.getTemplateUrl( "widgets/inputfield/inputfield.html"),
            controller: controller,
            controllerAs: "vm",
            bindToController: true
        };

        controller.$inject = ['dialogs'];

        function controller(dialogs) {
            var vm = this;

            if (!vm.type)
                vm.type = "text";

            vm.showHelp = showHelp;

            //////////////////////////////////////

            function showHelp(ev) {
                dialogs.showHelp(ev, vm.title, vm.toolTip);
            }
        }
    }

})();
