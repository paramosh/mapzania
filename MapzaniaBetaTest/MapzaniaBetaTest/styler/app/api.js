﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('api', service);

    service.$inject = ['$http', '$q', 'logger', 'defaults', 'upgrader'];

    function service($http, $q, logger, defaults, upgrader) {

        return {
            createMap: createMap,
            createLayer: createLayer,
            createTheme: createTheme,
            createThemeValue: createThemeValue,

            getMap: getMap,
            getMaps: getMaps,
            getSources: getSources,
            getThemeTypes: getThemeTypes,

            getTemplateUrl: getTemplateUrl,

            saveMap: saveMap
        };

        ///////////////////////////////

        function createMap() {

            return $q((resolve, reject) => {
                var map = {
                    entity: defaults.LatLongMap(),
                    schema: defaults.GoogleMap()
                };
                resolve(map);
            });
        };

        function createLayer(sourceInfo) {

            var key = sourceInfo.key;
            var type = sourceInfo.type;

            return $q((resolve, reject) => {

                switch (type) {

                    case "VECTOR":
                        var layer = defaults.VectorLayer();
                        layer.sourceKey = key;
                        layer.key = key;
                        resolve(layer);
                        break;

                    case "TILED_RASTER":
                        var layer = defaults.TiledRasterLayer();
                        layer.sourceKey = key;
                        layer.key = key;
                        resolve(layer);
                        break;

                    default:
                        throw "Unknown sourceType";
                }
            });
        };

        function createVectorLayer() {
        }

        function createRasterLayer() {
        }

        function createTheme(themeType) {

            switch (themeType.type) {

                case "StandardTheme":
                    return createStandardTheme();

                case "RangeTheme":
                    return createRangeTheme();

                default:
                    throw "Unknown sourceType";
            };
        };

        function createStandardTheme() {

            return $http.get("/mz/schema/standardtheme/create")
                .then(success)
                .catch(fail);

            function success(data, status, headers, config) {

                var val = data.data;
                val.toString = function () {
                    return "ZOOM: " + this.minZoom + "-" + this.maxZoom;
                }
                return val;
            }

            function fail(e) {
                logger.error('createStandardTheme', e);
                return $q.reject(e);
            }
        };

        function createRangeTheme() {

            return $http.get("/mz/schema/rangetheme/create")
                .then(success)
                .catch(fail);

            function success(data, status, headers, config) {

                var val = data.data;
                val.toString = function () {
                    return "ZOOM: " + this.minZoom + "-" + this.maxZoom;
                }
                return val;
            }

            function fail(e) {
                logger.error('createStandardTheme', e);
                return $q.reject(e);
            }
        };

        function createThemeValue() {

            return $http.get("/mz/schema/themevalue/create")
                .then(success)
                .catch(fail);

            function success(data, status, headers, config) {

                var val = data.data;

                val.toString = function () {

                    if (this.value)
                        return this.value;

                    return this.min + " to " + this.max;
                }

                return val;
            }

            function fail(e) {
                logger.error('createThemeValue', e);
                return $q.reject(e);
            }
        };

        function getMap(key) {

            return $q((resolve, reject) => {
                var gets = [
                    $http.get("/mz/maps/" + key),
                    $http.get("/mz/maps/" + key + "?srid=GOOGLE")
                ];

                $q.all(gets)
                    .then(success)
                    .catch(fail);

                function success(results) {
                    resolve({
                        entity: upgrader.upgradeMap(results[0].data),
                        schema: upgrader.upgradeMap(results[1].data),
                    });
                }

                function fail(e) {
                    logger.error('getMap', e);
                    reject(e);
                }
            });
        };

        function getMaps() {
            return $http.get("/mz/maps")
                .then(success)
                .catch(fail);

            function success(data, status, headers, config) {
                return data.data;
            }

            function fail(e) {
                logger.error('getMaps', e);
                return $q.reject(e);
            }
        };

        function getSources() {
            return $http.get("/mz/sources?$orderby=Key")
                .then(success)
                .catch(fail);

            function success(data, status, headers, config) {
                var sources = data.data;

                for (var i = 0; i < sources.length; i++) {
                    var src = sources[i];
                    src.type = src.sourceType;
                }

                return sources;
            }

            function fail(e) {
                logger.error('getSources', e);
                return $q.reject(e);
            }
        };

        function getThemeTypes() {

            return $q((resolve, reject) => {

                var types = [
                    {
                        name: "Standard Theme",
                        description: "A theme that uses a single symbol for all features.",
                        type: "StandardTheme"
                    },
                    {
                        name: "Range Theme",
                        description: "A theme that uses different symbols based on data values of features.",
                        type: "RangeTheme"
                    },
                ];

                resolve(types);
            });
        };

        function getTemplateUrl(key) {
            var url = key
                .replace(/\//g, "_")
                .replace(/\./g, "_")

            return "/mz/styler/app_" + url;
        }

        function saveMap(map) {

            return $http.post("/mz/schema/save", map)
                .then(success)
                .catch(fail);

            function success(data, status, headers, config) {
                console.log("--------------SAVED------------------");
                console.log(data.data);
            }

            function fail(e) {
                logger.error('saveMap', e);
                return $q.reject(e);
            }
        }
    }

})();