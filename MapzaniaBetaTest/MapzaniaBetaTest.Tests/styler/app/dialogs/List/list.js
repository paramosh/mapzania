﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('listDialog', controller);

    function controller(data, events) {
        var vm = this;

        vm.title = data.title;
        vm.items = data.items;
        vm.icon = data.icon;
        vm.cancel = cancel;
        vm.select = select;

        /////////

        function cancel() {
            events.cancel();
        }

        function select(map) {
            events.complete(map);
        }
    }

})();