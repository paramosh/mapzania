﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('symbolDialog', controller);

    function controller(data, events, api) {
        var vm = this;

        console.log(data);
        console.log(data.symbol);

        vm.symbol = data.symbol;
        vm.templateUrl = api.getTemplateUrl(
            "dialogs/symbol/"
            + data.symbol.type
            + ".html"
            );

        vm.cancel = cancel;
        vm.apply = apply;

        /////////////////////

        function cancel() {
            events.cancel();
        }

        function apply() {
            events.complete(vm.symbol);
        }
    }

})();