﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('labellerDialog', controller);

    function controller(data, events) {
        var vm = this;

        console.log(data);
        console.log(data.current);

        vm.current = data.current;
        vm.templateUrl = "wwwroot/app/dialogs/labeller/labeller.html";

        vm.cancel = cancel;
        vm.apply = apply;

        /////////////////////

        function cancel() {
            events.cancel();
        }

        function apply() {
            events.complete(vm.current);
        }
    }

})();