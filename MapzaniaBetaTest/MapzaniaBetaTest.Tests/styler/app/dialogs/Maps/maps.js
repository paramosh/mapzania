﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('mapsDialog', controller);

    //controller.$inject = ['$scope'];

    function controller(options, data, events) {
        var vm = this;

        vm.showClose = options.showClose;
        vm.maps = data.maps;
        vm.cancel = cancel;
        vm.select = select;

        /////////

        function cancel() {
            events.cancel();
        }

        function select(map) {
            events.complete(map);
        }
    }

})();