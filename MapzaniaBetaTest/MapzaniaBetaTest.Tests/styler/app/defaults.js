﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('defaults', service);

    service.$inject = ['upgrader'];

    function service(upgrader) {

        return {
            LatLongMap: LatLongMap,
            GoogleMap: GoogleMap,
            StandardTheme: StandardTheme,
            RangeTheme: RangeTheme,
            TiledRasterLayer: TiledRasterLayer,
            VectorLayer: VectorLayer
        };

        //////////////////////////////

        function LatLongMap() {
            var map = {
                "name": "New Map",
                "key": "NEW_MAP",
                "backColor": "#FFFFFF",
                "layers": [
                  {
                      "opacity": 100,
                      "greyScale": true,
                      "type": "RasterLayer",
                      "key": "OSM_TILES_GREY",
                      "name": "OSM",
                      "ordinal": 100,
                      "visible": true,
                      "sourceKey": "OSM_TILES"
                  }
                ],
                "startExtent": {
                    "minX": -60.0,
                    "minY": -60.0,
                    "maxX": 90.0,
                    "maxY": 60.0
                },
                "useUnifiedZoomSizes": true,
                "unifiedZoomSizes": [
                      {
                          "zoom": 1,
                          "size": 135.0
                      },
            {
                "zoom": 2,
                "size": 67.5
            },
            {
                "zoom": 3,
                "size": 33.75
            },
            {
                "zoom": 4,
                "size": 16.875
            },
            {
                "zoom": 5,
                "size": 8.4375
            },
            {
                "zoom": 6,
                "size": 4.21875
            },
            {
                "zoom": 7,
                "size": 2.109375
            },
            {
                "zoom": 8,
                "size": 1.0546875
            },
            {
                "zoom": 9,
                "size": 0.52734375
            },
            {
                "zoom": 10,
                "size": 0.263671875
            },
            {
                "zoom": 11,
                "size": 0.1318359375
            },
            {
                "zoom": 12,
                "size": 0.06591796875
            },
            {
                "zoom": 13,
                "size": 0.032958984375
            },
            {
                "zoom": 14,
                "size": 0.0164794921875
            },
            {
                "zoom": 15,
                "size": 0.00823974609375
            },
            {
                "zoom": 16,
                "size": 0.004119873046875
            },
            {
                "zoom": 17,
                "size": 0.0020599365234375
            },
            {
                "zoom": 18,
                "size": 0.00102996826171875
            },
            {
                "zoom": 19,
                "size": 0.000514984130859375
            },
            {
                "zoom": 20,
                "size": 0.0002574920654296875
            },
            {
                "zoom": 21,
                "size": 0.00012874603271484375
            },
            {
                "zoom": 22,
                "size": 6.4373016357421875E-05
            }
                ],
                "snapUIToUnifiedZoomSizes": true
            };

            return upgrader.upgradeMap(map);
        };

        function GoogleMap() {
            var map = {
                "backColor": "#FFFFFF",
                "layers": [
                  {
                      "opacity": 100,
                      "greyScale": true,
                      "type": "RasterLayer",
                      "key": "OSM_TILES_GREY",
                      "name": "OSM",
                      "ordinal": 100,
                      "visible": true,
                      "sourceKey": "OSM_TILES"
                  }
                ],
                "startExtent": {
                    "minX": -6679169,
                    "minY": -8399737,
                    "maxX": 10018754,
                    "maxY": 8399737
                },
                "useUnifiedZoomSizes": true,
                "unifiedZoomSizes": [
                  {
                      "zoom": 1,
                      "size": 16588442.369218897
                  },
                  {
                      "zoom": 2,
                      "size": 8294221.1846094485
                  },
                  {
                      "zoom": 3,
                      "size": 4147110.5923047243
                  },
                  {
                      "zoom": 4,
                      "size": 2073555.2961523621
                  },
                  {
                      "zoom": 5,
                      "size": 1036777.6480761811
                  },
                  {
                      "zoom": 6,
                      "size": 518388.82403809053
                  },
                  {
                      "zoom": 7,
                      "size": 259194.41201904527
                  },
                  {
                      "zoom": 8,
                      "size": 129597.20600952263
                  },
                  {
                      "zoom": 9,
                      "size": 64798.603004761317
                  },
                  {
                      "zoom": 10,
                      "size": 32399.301502380658
                  },
                  {
                      "zoom": 11,
                      "size": 16199.650751190329
                  },
                  {
                      "zoom": 12,
                      "size": 8099.8253755951646
                  },
                  {
                      "zoom": 13,
                      "size": 4049.9126877975823
                  },
                  {
                      "zoom": 14,
                      "size": 2024.9563438987912
                  },
                  {
                      "zoom": 15,
                      "size": 1012.4781719493956
                  },
                  {
                      "zoom": 16,
                      "size": 506.23908597469779
                  },
                  {
                      "zoom": 17,
                      "size": 253.11954298734889
                  },
                  {
                      "zoom": 18,
                      "size": 126.55977149367445
                  },
                  {
                      "zoom": 19,
                      "size": 63.279885746837223
                  },
                  {
                      "zoom": 20,
                      "size": 31.639942873418612
                  },
                  {
                      "zoom": 21,
                      "size": 15.819971436709306
                  },
                  {
                      "zoom": 22,
                      "size": 7.9099857183546529
                  }
                ],
                "snapUIToUnifiedZoomSizes": true
            };

            return upgrader.upgradeMap(map);
        };

        function StandardTheme() {
            return {
                type: "StandardTheme",
                minZoom: 1,
                maxZoom: 30,
                symbol: DefaultSymbol(),
                scaleType: 0,
                toString: function () {
                    return "ZOOM: " + this.minZoom + "-" + this.maxZoom;
                }
            };
        };

        function RangeTheme() {
            return {
                type: "RangeTheme",
                field: null,
                minZoom: 1,
                maxZoom: 30,
                normalizeValues: false,
                values: [],
                defaultSymbol: DefaultSymbol(),
                scaleType: 0,
                toString: function () {
                    return "ZOOM: " + this.minZoom + "-" + this.maxZoom;
                }
            };
        };

        function DefaultSymbol() {
            return {
                type: "GenericSymbol",
                iconSize: 8,
                iconPath: null,
                fillColor: "#999999",
                strokeColor: "#000000",
                strokeWidth: 2
            };
        };

        function TiledRasterLayer() {
            return {
                type: "RasterLayer",
                ordinal: 0,
                visible: true,
                key: "NEW_LAYER",
                name: "New Layer",
                opacity: 100,
                greyScale: true
            };
        };

        function VectorLayer() {
            return {
                type: "VectorLayer",
                ordinal: 0,
                visible: true,
                key: "NEW_LAYER",
                name: "New Layer",
                themes: []
            };
        };
    }

})();