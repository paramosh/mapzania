﻿(function () {
    'use strict';

    angular
        .module("app")
        .directive("symbolField", directive);

    //////////////////////////////////

    directive.$inject = ['api'];

    function directive(api) {

        return {

            scope: {
                model: '=ngModel',
                property: '@',
                title: '@',
                toolTip: '@'
            },
            templateUrl: api.getTemplateUrl("widgets/SymbolField/symbolField.html"),
            controller: controller,
            controllerAs: "vm",
            bindToController: true
        };
    }

    controller.$inject = ['$q', 'dialogs'];

    function controller($q, dialogs) {
        var vm = this;

        vm.showDialog = showDialog;
        vm.showHelp = showHelp;

        //////////////////////////

        function showDialog() {

            return $q((resolve, reject) => {

                var symbol = angular.copy(vm.model[vm.property]);

                dialogs.show('symbol',
                    { symbol: symbol })
                    .then(data => {
                        vm.model[vm.property] = data;
                    });
            });
        }

        function showHelp(ev) {
            dialogs.showHelp(ev, vm.title, vm.toolTip);
        }
    }

})();