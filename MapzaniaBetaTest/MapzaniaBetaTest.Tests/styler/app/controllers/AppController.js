﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('appController', controller);

    controller.$inject = ['dialogs', 'api'];

    //////////////////////////////////

    function controller(dialogs, api) {

        var vm = this;

        vm.map = null;
        vm.context = [];

        vm.addLayer = addLayer;
        vm.deleteLayer = deleteLayer;

        vm.addTheme = addTheme;
        vm.deleteTheme = deleteTheme;

        vm.showMapsDialog = showMapsDialog;
        vm.setPanel = setPanel;

        vm.addThemeValue = addThemeValue;
        vm.setThemeValue = setThemeValue;
        vm.deleteThemeValue = deleteThemeValue;

        vm.setThemeValueLabeller = setThemeValueLabeller;

        showMapsDialog();

        //////////////////////

        function addLayer() {

            showSourcesDialog()
                .then(source => {

                    api.createLayer(source)
                        .then(layer => {
                            vm.map.layers.push(layer)
                            setPanel(layer);
                        });
                });
        }

        function deleteLayer(layer) {
            dialogs.confirm("Are you sure you want to delete this layer?")
                .then(m=> {
                    var idx = vm.map.layers.indexOf(layer);
                    vm.map.layers.splice(idx, 1);
                });
        }

        function showMapsDialog() {

            api.getMaps().then(maps => {

                dialogs.show('maps',
                    { maps: maps },
                    { showClose: vm.map })
                    .then(loadMap);
            });
        }

        function loadMap(mapInfo) {
            if (!mapInfo)
                var promise = api.createMap();
            else
                var promise = api.getMap(mapInfo.key);

            promise.then(activateMap);
        }

        function activateMap(mapDef) {
            vm.map = mapDef.entity;
            new MapController(mapDef, api);
            setPanel(vm.map);
        }

        function setPanel(item, isNew) {
            vm.current = item;
            vm.panelTemplateUrl = api.getTemplateUrl("panels/" + item.type + ".html");
        }

        function showSourcesDialog() {

            return new Promise((resolve, reject) => {
                api.getSources()
                    .then(sources => {
                        dialogs.show('sources', {
                            sources: sources
                        })
                        .then(source => resolve(source));
                    });
            });
        }

        function addTheme() {
            var layer = vm.current;

            showThemeTypeDialog()
                .then(themeType => {

                    api.createTheme(themeType)
                        .then(theme => {
                            layer.themes.push(theme)
                            setPanel(theme);
                        });
                });
        }

        function deleteTheme(theme) {
            dialogs.confirm("Are you sure you want to delete this theme?")
                .then(m=> {
                    var layer = vm.current;
                    var idx = layer.themes.indexOf(theme);
                    layer.themes.splice(idx, 1);
                });
        }

        function showThemeTypeDialog() {

            return new Promise((resolve, reject) => {
                api.getThemeTypes()
                    .then(themeTypes => {
                        dialogs.show('list', {
                            title: "Select Theme Type",
                            icon: "brush",
                            items: themeTypes
                        })
                        .then(item => resolve(item));
                    });
            });
        }

        function addThemeValue(theme) {

            api.createThemeValue()
                .then(themeValue => {
                    dialogs.show('themeValue',
                        {
                            value: themeValue
                        })
                    .then(data => {
                        theme.values.push(data);
                    });
                });
        }

        function setThemeValue(value) {

            dialogs.show('themeValue',
                {
                    value: angular.copy(value)
                })
                .then(data => {
                    Object.assign(value, data);
                });
        }

        function deleteThemeValue(value) {
            dialogs.confirm("Are you sure you want to delete this value?")
                .then(m=> {
                    var theme = vm.current;
                    var idx = theme.values.indexOf(value);
                    theme.values.splice(idx, 1);
                });
        }

        function setThemeValueLabeller(value) {

            dialogs.show('labeller',
                {
                    current: angular.copy(value.labeller)
                })
                .then(data => {
                    Object.assign(value.labeller, data);
                });
        }
    }

})();