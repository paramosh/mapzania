﻿/**
 * @doc Mapzania.Line.*ClassDescription*
 *
 * Encapsulates a single Line geometry. A line geometry defines a set of connected points. 
 */
var Mapzania = (function (ns) {

    /**
      * @doc Mapzania.Line.Line
      *
      * Constructs a Mapzania Geometry.
      *
      * @param points (Array) An array of Mapzania.Point instances.
      */
    function Line(points) {
        if (!points)
            this.points = [];
        else
            this.points = points;
    }

    /**
      * @doc Mapzania.Line.read
      *
      * Unpack the geometry from a Mapzania formatted 3-dimensional array of point data.
      *
      * @param data   (Array[][][])    A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Line.prototype.read = function (data) {
        for (var i = 1; i < data[0][0].length; i += 2)
            this.points.push(new ns.Point().read([[[data[0][0][i - 1], data[0][0][i]]]]));

        return this;
    }

    /**
      * @doc Mapzania.Line.getExtent
      *
      * Determines the extent for this geometry.
      *
      * @return (Mapzania.Extent) The extent for this geometry.
      */
    Line.prototype.getExtent = function () {
        var algo = new ns.Algo();
        return algo.getExtentFromPoints(this.points);
    }

    /**
      * @doc Mapzania.Line.distanceTo
      *
      * Determines the closest distance from this geometry to another geometry. NB: This method is 
      * not implemented for all the geometry types.
      *
      * @param geometry   (Mapzania.Geometry) A Geometry instance to calculate the closest distance to.
      *
      * @return (Number)    The distance between this geometry and geometry provided in the geometry parameter. 
      *                     The smallest value returned by this method is zero.
      */
    Line.prototype.distanceTo = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Line.intersects
      *
      * Determines if this geometry instance intersects with another geometry instance. This method throws an Exception if the operation is not yet
      * implemented or supported by the Mapzania SDK.
      *
      * @param geometry   (Mapzania.Geometry) A different Geometry instance.
      *
      * @return (Boolean)    true to indicate that this geometry instance and the provided geometry instance intersects. Otherwise false.
      */
    Line.prototype.intersects = function (geometry) {
        throw "Not implemented";
    }

    /**
      * @doc Mapzania.Line.getPath
      *
      * Pack this geometry into a Mapzania formatted 3-dimensional array of point data.
      *
      * @return (Array[][][]) A 3-dimensional array of point data (presented in the Mapzania format).
      */
    Line.prototype.getPath = function () {
        var line = [];
        for (var i = 0; i < this.points.length; i++) {
            var pnt = this.points[i];
            line.push(pnt.x);
            line.push(pnt.y);
        }

        return [[line]];
    }

    /**
      * @doc Mapzania.Line.getSize
      *
      * Determines the total number of points in this geometry.
      *
      * @return (Number) The number of points in this geometry.
      */
    Line.prototype.getSize = function () {
        return this.points.length;
    }

    /**
      * @doc Mapzania.Line.getCentroid
      *
      * Determines the centroid for this geometry.
      *
      * @return (Mapzania.Point) The centroid for this geometry.
      */
    Line.prototype.getCentroid = function () {
        //TODO:Better centroid algorithm
        return this.getExtent().getCenter();
    }

    /**
      * @doc Mapzania.Line.rotate
      *
      * Rotates a line around an origin, by a number of degrees (provided in radians). This method returns a new Line instance.
      *
      * @param origin (Mapzania.Point) The origin point.
      *
      * @param angleRandians (Number) The angle, in radians. A positive value indicates an anti-clockwise direction.
      *
      * @return (Mapzania.Line) A rotated line instance.
      */

    Line.prototype.rotate = function (origin, angleRandians) {
        var newPoints = [];
        this.points.forEach(function (cur) {
            newPoints.push(cur.rotate(origin, angleRandians));
        });
        return new Mapzania.Line(newPoints);
    }

    /**
      * @doc Mapzania.Line.getLineSegments
      *
      * Returns a list of line segments that makes up this line geometry.
      *
      * @param extent   (Mapzania.Extent) An optional extent. Only extents visible in this extent will be included in the result of this function.
      *                                     All segments are included when this parameter is set to null.
      *
      * @return (Mapzania.LineSegment []) A list of line segment instances.
      */

    Line.prototype.getLineSegments = function (extent) {
        var res = [];
        for (var i = 0; i < this.points.length - 1; i++) {
            var p1 = this.points[i];
            var p2 = this.points[i + 1];
            var ls = new Mapzania.LineSegment(p1, p2);
            if (extent) {
                if (ls.isVisibleInExtent(extent)) //TODO: Get the visible extent
                    res.push(ls);
            } else {
                res.push(ls);
            }
        }
        return res;
    }

    /**
      * @doc Mapzania.Line.fromLineSegments
      *
      * Populates this line geometry from an array of connected line segments.
      *
      * @param segments   (Mapzania.LineSegment []) An array of connected line segment instances. 
      *
      */

    Line.prototype.fromLineSegments = function (segments) {
        this.points = [];
        this.points.push(segments[0].p1);
        var me = this;
        segments.forEach(function (cur) {
            me.points.push(cur.p2);
        });
    }

    /**
      * @doc Mapzania.Line.getGeometryType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Line.prototype.getGeometryType = function () {
        return 2;
    }

    /**
      * @doc Mapzania.Line.getType
      *
      * Gets a geometry type indicator.
      *
      * @return (int) An integer value, indicating the geometry type.
      */

    Line.prototype.getType = function () {
        return 2;
    }

    /**
      * @doc Mapzania.Line.clone
      *
      * Clones this geometry instance.
      *
      * @return (Mapzania.Line) An exact deep clone of this instance.
      */

    Line.prototype.clone = function () {
        var result = new Mapzania.Line();
        result.read(this.getPath());
        return result;
    }

    /**
      * @doc Mapzania.Line.translate
      *
      * Translate this geometry instance.
      *
      * @param x   (Number) The units to translate this geometry in the X direction. Positive values typically indicates a direction to the right.
      *
      * @param y   (Number) The units to translate this geometry in the Y direction. Positive values typically indicates an upwards (i.e. North) direction.
      *
      */

    Line.prototype.translate = function (x, y) {
        for (var i = 0; i < this.points.length; i++) {
            this.points[i].translate(x, y);
        }
    }

    /**
      * @doc Mapzania.Line.getLength
      *
      * Calculates the length of the line.
      *
      * @return (Number) The line length.
      */

    Line.prototype.getLength = function () {
        var r = 0.0;
        for (var i = 0; i < this.points.length - 1; i++) {
            r += this.points[i].distanceTo(this.points[i + 1]);
        }
        return r;
    }

    /**
      * @doc Mapzania.Line.reverse
      *
      * Calculates a reversed instance of this line.
      *
      * @return (Mapzania.Line) The line instance.
      */

    Line.prototype.reverse = function () {
        var points = this.points.slice(0);
        points.reverse();
        return new Mapzania.Line(points);
    }

    /**
      * @doc Mapzania.Line.getPointAndSegmentAtLength
      *
      * Calculate the line segment and the point on the line that corresonds to the length of the line up to a provided length (starting at the first point on the line).
      *
      * @param length   (Number) A length. 
      *
      * @return (Object) An object in the form {segment: x, point: y}, where segment is an instance of Mapzania.LineSegment and point an instance of Mapzania.Point.
      *                  Applications may assume that the point is present on the corresponding line segment. 
      *                  The return value may be null if the length is greater than the length of this line.
      */

    Line.prototype.getPointAndSegmentAtLength = function (length) {
        var l = 0.0;
        var result = null;
        for (var i = 0; i < this.points.length - 1; i++) {
            var p1 = this.points[i];
            var p2 = this.points[i + 1];
            l += p1.distanceTo(p2);
            if (l >= length) {
                var delta = l - length;
                var d = p1.distanceTo(p2);
                var f = 1 - (delta / d);

                var pt = new Mapzania.Point(p1.x, p1.y);
                pt.translate((p2.x - p1.x) * f, (p2.y - p1.y) * f);

                result = { segment: new Mapzania.LineSegment(p1, p2), point: pt };
                return result;
            }
        }
        return result;
    }

    ns.Line = Line;

    if (!ns.FeatureTypes)
        ns.FeatureTypes = {};

    ns.FeatureTypes["2"] = Line;

    return ns;

})(Mapzania || {});
