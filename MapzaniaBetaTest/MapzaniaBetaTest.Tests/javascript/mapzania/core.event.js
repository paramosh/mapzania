﻿/**
 * @doc Mapzania.Event.*ClassDescription*
 *
 * An utility class for dispatching events to a set of listeners.
 */
var Mapzania = (function (ns) {
    /**
      * @doc Mapzania.Event.Event
      *
      * Constructs an empty event and set the list of listeners to an empty list.
      *
      */

	function Event() {
		this.listeners = []
	}

    /**
      * @doc Mapzania.Event.add
      *
      * Adds a listener to this event instance
      *
      * @param listener (Function) A handle to the listener function. This function should take as input a single object, containing a "source" and "data" property.
      *                             Duplicate listeners are not allowed.
      */
	Event.prototype.add = function (listener) {
		this.listeners.push(listener)
	}

    /**
      * @doc Mapzania.Event.remove
      *
      * Removes a listener from this event instance
      *
      * @param listener (Function) A handle to the listener function to remove. 
      */
	Event.prototype.remove = function (listener) {
	    for (var i = 0; i < this.listeners.length; i++) {
	        if (this.listeners[i] === listener) {
	            this.listeners.splice(i, 1);
	            return;
	        }
	    }
	}

    /**
      * @doc Mapzania.Event.fire
      *
      * Fires this event, by calling all the listeners with the source reference and data provided
      *
      * @param source (Object) A handle to the object that initiated the event fire. 
      *
      * @param data (Object) A handle to the event data. 
      */
	Event.prototype.fire = function (source, data) {
		for (var i = 0; i < this.listeners.length; i++) {
			this.listeners[i]({ source: source, data: data })
		}
	}

	ns.Event = Event;

	return ns;

})(Mapzania || {})
