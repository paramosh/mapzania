﻿var Mapzania = (function (ns) {

	function RangeThemer(theme) {
	    this.theme = theme
	    this.minValue = null;
	    this.maxValue = null;
	}

	RangeThemer.prototype.evalFeature = function (feature) {
	    if (this.theme.normalizeValues) {
	        var featVal = feature.attributes[this.theme.field];
	        if (this.minValue == null) {
	            this.minValue = featVal;
	            this.maxValue = featVal;
	        } else {
	            if (featVal < this.minValue) this.minValue = featVal;
	            if (featVal > this.maxValue) this.maxValue = featVal;
	        }
	    }
	}

	RangeThemer.prototype.doneWithEvalFeatures = function () {
	    
	}

	RangeThemer.prototype.getSymbol = function (feature) {
	    var sym;
	    var themeValue = this._getThemeValue(feature);
	    if (themeValue && themeValue.symbol) {
	        sym = themeValue.symbol;
	    } else {
	        sym = this.theme.defaultSymbol;
	    }
		return sym;
	}

	//RangeThemer.prototype.getLabeller = function (feature) {
	//    var labeller = null;
	//    var themeValue = this._getThemeValue(feature);
	//    if (themeValue && themeValue.labeller) {
	//        labeller = themeValue.labeller;
	//    } else {
	//        labeller = this.theme.labeller;
	//    }

	//    return labeller;
    //}

	RangeThemer.prototype.getLabelText = function (feature) {
	    var labeller = null;
	    var themeValue = this._getThemeValue(feature);
	    if (themeValue && themeValue.labeller) {
	        labeller = themeValue.labeller;
	    } else {
	        labeller = this.theme.labeller;
	    }

	    if (!labeller || !labeller.visible)
	        return null;

	    return feature.attributes[labeller.field];
	}

	RangeThemer.prototype.getLabelSymbol = function (feature) {
	    var labeller = null;
	    var themeValue = this._getThemeValue(feature);
	    if (themeValue && themeValue.labeller) {
	        labeller = themeValue.labeller;
	    } else {
	        labeller = this.theme.labeller;
	    }

	    if (!labeller || !labeller.visible)
	        return null;

	    return labeller.symbol;
	}

	RangeThemer.prototype._getThemeValue = function (feature) {
	    var sym;
	    var featVal = feature.attributes[this.theme.field];

	    if (this.theme.normalizeValues) {
	        var diff = this.maxValue - this.minValue;
	        var perc = 0;
	        if (diff != 0) {
	            perc = (featVal - this.minValue) / diff * 100.0;
	        }
	        if (perc > 100) perc = 100;
	        if (perc < 0) perc = 0;
	        featVal = perc;
	    }

	    var funcStrCmpIgnoreCase = function (s1, s2) {
	        s1 = s1 ? String(s1) : "";
	        s2 = s2 ? String(s2) : "";
	        return s1.toUpperCase().trim() == s2.toUpperCase().trim();
	    };

	    for (var i = 0; i < this.theme.values.length; i++) {
	        var themeVal = this.theme.values[i];

	        if (themeVal.value) { // unique range
	            if (funcStrCmpIgnoreCase(featVal, themeVal.value)) {
	                return themeVal;
	            }
	        } else { // range
	            if (featVal >= themeVal.min && featVal <= themeVal.max) {
	                return themeVal;
	            }
	        }
	    }

	    return null;
	}

	ns.RangeThemer = RangeThemer;

	return ns;

})(Mapzania || {})
