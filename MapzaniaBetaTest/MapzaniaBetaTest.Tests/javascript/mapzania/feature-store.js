﻿var Mapzania = (function (ns) {
    var me;

    function FeatureStore() {

        this.changed = new ns.Event();

        var me = this;

        var store = {};

        this.set = function (layerKey, features, suppressChangedEvent) {
            store[layerKey] = features;
            if (!suppressChangedEvent)
                me.changed.fire(me, layerKey);
        }

        this.add = function (layerKey, features, suppressChangedEvent) {

            if (!store[layerKey])
                store[layerKey] = features;
            else
                store[layerKey] = store[layerKey].concat(features);

            if (!suppressChangedEvent)
                me.changed.fire(me, layerKey);
        }

        this.get = function (layerKey) {
            return store[layerKey];
        }
    }

    ns.FeatureStore = FeatureStore;

    return ns;

})(Mapzania || {});