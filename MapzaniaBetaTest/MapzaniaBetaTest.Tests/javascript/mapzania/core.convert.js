﻿var Mapzania = (function (ns) {
    function Convert() {
    }

    Convert.prototype.fromDDToDMS = function (decimalDegrees) {
        var result = {};
        if (typeof (decimalDegrees) != "undefined" && decimalDegrees != null) {
            var frac = Math.abs(decimalDegrees - (decimalDegrees | 0));
            result.degrees = decimalDegrees | 0;
            result.minutes = (frac * 60) | 0;
            result.seconds = Math.round((frac * 3600 - result.minutes * 60) * 1000) / 1000;
        }
        return result;
    }

    Convert.prototype.fromDMSToDD = function (dms) {
        if (!dms)
            return null;

        var deg = parseFloat(dms.degrees);
        var min = parseFloat(dms.minutes);
        var sec = parseFloat(dms.seconds);
        var sign = (deg >= 0 ? 1 : -1);

        return sign * (Math.abs(deg) + min / 60.0 + sec / 3600.0);
    }

    ns.Convert = Convert;

    return ns;

})(Mapzania || {});

