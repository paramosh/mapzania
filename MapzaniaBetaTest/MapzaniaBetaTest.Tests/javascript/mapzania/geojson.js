﻿/// <reference path="mapzania.js"/>
var Mapzania = (function (ns) {

    //--------------------TO-------------------------

    var toFeatureCollection = function (features) {
        var res = {
            type: "FeatureCollection", 
            features: []
        };

        var bbox = [Number.MAX_VALUE, Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE];

        for (var i = 0; i < features.length; i++) {

            var ftr = toFeature(features[i]);
            res.features.push(ftr);

            bbox[0] = Math.min(bbox[0], ftr.bbox[0]);
            bbox[1] = Math.min(bbox[1], ftr.bbox[1]);
            bbox[2] = Math.max(bbox[2], ftr.bbox[2]);
            bbox[3] = Math.max(bbox[3], ftr.bbox[3]);
            //console.log(bbox[3]);
        }

        res.bbox = bbox; 

        return res;
    }

    var toFeature = function (feature) {
        var res = {
            type: "Feature",
            id: feature.id || feature.attributes["id"] || feature.attributes["Id"],
            geometry: toGeometry(feature.geometry),
            properties: feature.attributes,
            bbox: toBoundingBox(feature.geometry.getExtent()),
            style: feature.style
        };

        return res;
    }

    var toBoundingBox = function (extent) {
        return [extent.minX, extent.minY, extent.maxX, extent.maxY];
    }

    var toGeometry = function (geom) {

        var res = {
            //properties: ftr.attributes,
            type: "ERR",
            coordinates: []
        };

        var toPoint = function (point) {
            return [point.x, point.y];
        }

        var toPoints = function (points) {
            var res = [];
            for (var i = 0; i < points.length; i++)
                res.push(toPoint(points[i]));
            return res;
        }

        //Point
        if (geom.getType() == 1) {
            res.type = "Point";
            res.coordinates = toPoint(geom);
        }

        //MultiPoint
        if (geom.getType() == 4) {
            res.type = "MultiPoint";
            res.coordinates = toPoints(geom.points);
        }

        //Line
        if (geom.getType() == 2) {
            res.type = "LineString";
            res.coordinates = toPoints(geom.points);
        }

        //MultiLine
        if (geom.getType() == 5) {
            res.type = "MultiLineString";
            res.coordinates = [];
            for (var i = 0; i < geom.lines.length; i++)
                res.coordinates.push(toPoints(geom.lines[i].points));
        }

        var makeRing = function (line) {
            var pnts = line.points;

            //if (pnts.length == 0)
            //    throw "0 points";

            //while (pnts.length < 4) {
            //    var pnt = pnts[pnts.length - 1];
            //    var npnt = new Mapzania.Point(pnt.x + 0.000000000001, pnt.y + 0.000000000001);
            //    pnts.push(npnt);
            //}

            var first = pnts[0];
            var last = pnts[pnts.length - 1];

            if (first.x != last.x || first.y != last.y)
                pnts.push(first);

        }

        //Polygon
        if (geom.getType() == 3) {

            makeRing(geom.shell);
            for (var i = 0; i < geom.holes.length; i++) 
                makeRing(geom.holes[i]);


            if (geom.shell.points.length < 4)
                throw "broken polygon";

            res.type = "Polygon";
            res.coordinates = [];

            var lines = [geom.shell];
            lines = lines.concat(geom.holes);

            for (var i = 0; i < lines.length; i++) {
                if (lines[i].points.length < 4)
                    continue;
                res.coordinates.push(toPoints(lines[i].points));
            }
        }

        //MultiPolygon
        if (geom.getType() == 6) {
            res.type = "MultiPolygon";
            res.coordinates = [];

            for (var k = 0; k < geom.polygons.length; k++) {

                var poly = geom.polygons[k];

                makeRing(poly.shell);
                for (var i = 0; i < poly.holes.length; i++)
                    makeRing(poly.holes[i]);

                if (poly.shell.points.length < 4)
                    continue;

                var polyArr = [];
                res.coordinates.push(polyArr);

                var lines = [poly.shell];
                lines = lines.concat(poly.holes);

                for (var i = 0; i < lines.length; i++) {
                    if (lines[i].points.length < 4)
                        continue;
                    polyArr.push(toPoints(lines[i].points));
                }
            }
        }

        return res;
    }

    //--------------------FROM-------------------------

    var fromFeatureCollection = function (collection) {
        var res = [];

        var features = collection.features;

        for (var i = 0; i < features.length; i++) {
            res.push(fromFeature(features[i], i));
        }

        return res;
    }

    var fromFeature = function (feature, id) {
        var res = new Mapzania.Feature();

        if (feature.id)
            res.id = feature.id;
        else if (feature.properties) {
            if (feature.properties["id"])
                res.id = feature.properties["id"];
            if (feature.properties["Id"])
                res.id = feature.properties["Id"];
        }

        res.attributes = feature.properties;
        res.geometry = fromGeometry(feature.geometry);
        res.style = feature.style;

        return res;
    }

    var fromBoundingBox = function (bbox) {
        return new Mapzania.Extent({
            minX: bbox[0],
            minY: bbox[1],
            maxX: bbox[2],
            maxY: bbox[3]
        });
    }

    var fromGeometry = function (geom) {

        var coords = geom.coordinates;

        var res = {
            type: "ERR"
        };

        var fromPoint = function (coordArray) {
            return new Mapzania.Point(coordArray[0], coordArray[1]);
        }

        var fromLine = function (coordArray) {
            var line = new Mapzania.Line();

            for (var i = 0; i < coordArray.length; i++)
                line.points.push(fromPoint(coordArray[i]));

            return line;
        }

        //Point
        if (geom.type == "Point") {
            res = fromPoint(coords);
        }

        //MultiPoint
        if (geom.type == "MultiPoint") {
            var res = new Mapzania.MultiPoint();

            for (var i = 0; i < coords.length; i++)
                res.points.push(fromPoint(coords[i]));
        }

        //Line
        if (geom.type == "LineString") {
            res = fromLine(coords); 
        }

        //MultiLine
        if (geom.type == "MultiLineString") {
            var res = new Mapzania.MultiLine();

            for (var i = 0; i < coords.length; i++)
                res.lines.push(fromLine(coords[i]))
        }

        //Polygon
        if (geom.type == "Polygon") {

            var res = new Mapzania.Polygon();

            for (var i = 0; i < coords.length; i++)
                if (i == 0)
                    res.shell = fromLine(coords[i]);
                else
                    res.holes.push(fromLine(coords[i]));
        } 

        //MultiPolygon
        if (geom.type == "MultiPolygon") {

            var res = new Mapzania.MultiPolygon();

            for (var i = 0; i < coords.length; i++) {
                var poly = new Mapzania.Polygon();
                res.polygons.push(poly);

                for (var j = 0; j < coords[i].length; j++)
                    if (j == 0)
                        poly.shell = fromLine(coords[i][j]);
                    else
                        poly.holes.push(fromLine(coords[i][j]));
            }
        }

        return res;
    }

    ns.GeoJson = {
        toFeatureCollection: toFeatureCollection,
        toFeature: toFeature,
        toBoundingBox:toBoundingBox,
        toGeometry: toGeometry,
        fromFeatureCollection: fromFeatureCollection,
        fromFeature: fromFeature,
        fromBoundingBox:fromBoundingBox,
        fromGeometry: fromGeometry
    };

    return ns;

})(Mapzania || {});