﻿/// <reference path="../mapzania.js"/>

var Mapzania = (function (ns) {

    function Layer(key, map) {

        //-------------------- FLUENT ---------------------------------------
        this.usingFeature = function (id) {
            return new ns.MapFeature(id, key, map);
        }

        // --------------------------- LAYERS --------------------------------------

        this.update = function () {
            map.updateLayer(key);
            return this;
        }

        //---------------------LAYER MANIPULATION--------------------------------
        this.show = function () {
            map.showLayer(key);
            return this;
        };

        this.hide = function () {
            map.hideLayer(key);
            return this;
        };

        this.addFeatureCollection = function (featureCollection) {
            map.addFeatureCollection(key, featureCollection);
            return this;
        }

        this.addFeature = function (feature) {
            map.addFeature(key, feature);
            return this;
        }

        this.clearFeatures = function () {
            map.clearFeatures(key);
            return this;
        };

        //-------------------- FILTERS ------------------------------------------

        this.addFilter = function (filter, index) {
            map.addFilter(key, filter, index);
            return this;
        }

        this.removeFilter = function (index) {
            map.removeFilter(key, index);
            return this;
        }

        this.clearFilters = function () {
            map.clearFilters(key);
            return this;
        };

        this.filterByText = function (statement, passthru) {
            var filter = ns.Filters.filterByText(statement, passthru);
            map.addFilter(key, filter);
            return this;
        }

        this.filterByBoundingBox = function (bbox) {
            var filter = ns.Filters.filterByBoundingBox(bbox);
            map.addFilter(key, filter);
            return this;
        }

        this.filterByNearest = function (point, take, includeDistance) {
            var filter = ns.Filters.filterByNearest(point, take, includeDistance);
            map.addFilter(key, filter);
            return this;
        }

        this.filterByFeatureCollection = function (features, operation) {
            var filter = ns.Filters.filterByFeatureCollection(features, operation);
            map.addFilter(key, filter);
            return this;
        }

        this.filterByLayer = function (layerKey, layerFilters, operation) {
            var filter = ns.Filters.filterByLayer(layerKey, layerFilters, operation);
            map.addFilter(key, filter);
            return this;
        }

        //this.filterBy = function (param1, param2, param3) {

        //    if (!param1)
        //        throw "filterBy: Invalid parameter(s).";

        //    if (typeof (param) == "string" && param3 == null)
        //        return this.filterByText(param1, param2);
        //    else
        //        return this.filterByLayer(param1, param2, param3);

        //    if (param instanceof Array)
        //        return this.filterByBoundingBox(param1);

        //    if (param1.type == "FeatureCollection")
        //        return this.filterByFeatureCollection(param1, param2);

        //    if (param1.type == "Point")
        //        return this.filterByNearest(param1, param2, param3);

        //    return this.splitLineByFeatures(param, method);
        //}

        this.nearest = function (point, take, includeDistance) {
            return this.filterByNearest(point, take, includeDistance);
        }

        this.clipByFeatureCollection = function (featureCollection) {
            var filter = ns.Filters.clipByFeatureCollection(featureCollection);
            map.addFilter(key, filter);
            return this;
        }

        this.changeProperties = function (properties) {
            var filter = ns.Filters.changeProperties(properties);
            map.addFilter(key, filter);
            return this;
        }

        this.convertToCentroids = function () {
            var filter = ns.Filters.convertToCentroids();
            map.addFilter(key, filter);
            return this;
        }

        this.centroids = function () {
            return this.convertToCentroids();
        }

        this.bufferGeometry = function (factor, side, joins, ends, mitreLimit, srid) {
            var filter = ns.Filters.bufferGeometry(factor, side, joins, ends, mitreLimit, srid);
            map.addFilter(key, filter);
            return this;
        }

        this.buffer = function (factor, side, joins, ends, mitreLimit, srid) {
            return this.bufferGeometry(factor, side, joins, ends, mitreLimit, srid);
        }

        this.aggregateByFeatures = function (featuresCollection, aggregation) {
            var filter = ns.Filters.aggregateByFeatures(featuresCollection, aggregation);
            map.addFilter(key, filter);
            return this;
        }

        this.aggregateByLayer = function (layersource, aggregation) {
            var filter = ns.Filters.aggregateByLayer(layersource, aggregation);
            map.addFilter(key, filter);
            return this;
        }

        this.aggregateLinesByOriginAndDestination = function (gridSize, includeCount) {
            var filter = ns.Filters.aggregateLinesByOriginAndDestination(gridSize, includeCount);
            map.addFilter(key, filter);
            return this;
        }

        this.aggregate = function (param, aggregation) {

            if (typeof (param) == "string")
                return this.aggregateByLayer(param, aggregation);

            return this.aggregateByFeatures(param, aggregation);
        }

        this.clusterPoints = function (factor, properties) {
            var filter = ns.Filters.clusterPoints(factor, properties);
            map.addFilter(key, filter);
            return this;
        }

        this.cluster = function (factor, properties) {
            return this.clusterPoints(factor, properties);
        }

        this.splitLineByFeatures = function (featureCollection, method) {
            var filter = ns.Filters.splitLineByFeatures(featureCollection, method);
            map.addFilter(key, filter);
            return this;
        }

        this.splitLineByLayer = function (layerSource, method) {
            var filter = ns.Filters.splitLineByLayer(layerSource, method);
            map.addFilter(key, filter);
            return this;
        }

        this.split = function (param, method) {

            if (typeof (param) == "string")
                return this.splitLineByLayer(param, method);

            return this.splitLineByFeatures(param, method);
        }

        // ------------------------ STYLING --------------------------------------

        this.setLayerStyle = function (style) {
            map.setLayerStyle(key, style);
            return this;
        };

        this.addLayerStyle = function (style) {
            map.addLayerStyle(key, style);
            return this;
        };

        this.clearLayerStyles = function () {
            map.clearLayerStyles(key);
            return this;
        };

        this.clearStyle = function () {
            map.clearLayerStyles(key);
            return this;
        };

        this.style = function (style) {
            map.setLayerStyle(key, style);
            return this;
        };

        this.styleFeature = function (id, style) {
            map.setFeatureStyle(key, id, style);
            return this;
        };

        // ------------------------ QUERYING ----------------------------------

        this.getFeatureCollection = function () {
            return map.getFeatureCollection(key);
        };
    }

    ns.Layer = Layer;

    return ns;

})(Mapzania || {});