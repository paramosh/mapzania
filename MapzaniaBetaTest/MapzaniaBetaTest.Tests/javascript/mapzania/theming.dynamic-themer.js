﻿/// <reference path="../mapzania.js"/>
var Mapzania = (function (ns) {

    function DynamicThemer(style) {
        this._style = style;
    }

    DynamicThemer.prototype.getSymbol = function (feature) {
        var type = feature.geometry.getType();

        //var style = feature.style;

        //if (!style)
        style = this._style;

        if (Mapzania.Util.isFunction(style))
            style = style(feature.attributes);

        if (type == 1 || type == 4)
            return Mapzania.Symbols.toPointSymbol(style);

        if (type == 2 || type == 5)
            return Mapzania.Symbols.toLineSymbol(style);

        if (type == 3 || type == 6)
            return Mapzania.Symbols.toFillSymbol(style);

        throw "DynamicThemer: unsupported geometry type.";
    }

    DynamicThemer.prototype.getLabelText = function (feature) {
        //var style = feature.style;

        //if (!style)
        style = this._style;

        if (style.labelField)
            return feature.attributes[style.labelField];

        if (Mapzania.Util.isFunction(style))
            style = style(feature.attributes);

        if (!style.label)
            return null;

        var label;

        if (Mapzania.Util.isFunction(style))
            label = style.label(feature.attributes);
        else
            label = style.label;

        var text;
        if (Mapzania.Util.isFunction(style))
            text = label.text(feature.attributes);
        else
            text = label.text;

        return text;
    }

    DynamicThemer.prototype.getLabelSymbol = function (feature) {
        //var style = feature.style;

        //if (!style)
        style = this._style;

        if (style.labelField)
            return new ns.DefaultThemer().getLabelSymbol();

        if (Mapzania.Util.isFunction(style))
            style = style(feature.attributes);

        if (!style.label)
            return null;

        var label;

        if (Mapzania.Util.isFunction(style))
            label = style.label(feature.attributes);
        else
            label = style.label;

        var symbol = Mapzania.Symbols.toLabelSymbol(label);

        return symbol;
    }

    ns.DynamicThemer = DynamicThemer;

    return ns;

})(Mapzania || {})
