﻿/// <reference path="../mapzania.js"/>

var Mapzania = (function (ns) {

    function DrawingInstructionEngine(transform, scaleType) {

        var me = this;

        var passes = [[]];

        this.addLabel = function (label) {

            var lsym = label.symbol;

            var rotation = label.rotation;

            if (label.rotationFollowGeometry)
                rotation = transform.labelRotationTo(label.rotation);

            var instruction = {
                type: "draw-text",
                location: transform.to(1, label.center),
                text: label.text,
                font: lsym.fontFamily,
                size: calcSize(lsym.fontSize),
                color: lsym.color,
                backColor: lsym.backColor,
                opacity: lsym.opacity,
                align: lsym.horizontalAlignment,
                haloSize: calcSize(lsym.haloThickness),
                haloColor: lsym.haloColor,
                bold: lsym.bold,
                italic: lsym.italic,
                underline: lsym.underline,
                addMark: false,
                rotation: rotation,

                // The calcSize function assumes positive size values. We need to preserve negative sizes here.
                offset: {
                    x: funcSign(lsym.offset.x) * calcSize(Math.abs(lsym.offset.x)),
                    y: funcSign(lsym.offset.y) * calcSize(Math.abs(lsym.offset.y))
                },
                labelWeight: label.weight
            };

            addInstruction(0, instruction);
        }

        this.addGeometry = function (geometry, symbol) {

            switch (geometry.getGeometryType()) {
                case 1: //point
                    me.addPoint(geometry, symbol);
                    break;
                case 2: //line
                    me.addLine(geometry, symbol);
                    break;
                case 3: //polygon
                    me.addPolygon(geometry, symbol);
                    break;
                case 4: //multi point
                    me.addMultiPoint(geometry, symbol);
                    break;
                case 5: //multi line
                    me.addMultiLine(geometry, symbol);
                    break;
                case 6: //multi polygon
                    me.addMultiPolygon(geometry, symbol);
                    break;
                default:
                    throw "DrawingInstructionEngine.addGeometry: Invalid geometry type.";
            }
        }

        this.addPoint = function (geometry, symbol) {

            var cog;

            switch (symbol.type) {

                case "GenericSymbol":
                    cog = new GenericCog(symbol);
                    break;

                case "PointSymbol":
                    cog = new PointCog(symbol);
                    break;

                case "CompoundPointSymbol":
                    cog = new CompoundPointCog(symbol);
                    break;

                default:
                    throw "DrawingInstructionEngine.addPoint: Invalid style type."
                    + symbol.type;
            }

            cog.addPoint(geometry);
        }

        this.addLine = function (geometry, symbol) {

            var cog;

            switch (symbol.type) {

                case "GenericSymbol":
                    cog = new GenericCog(symbol);
                    break;

                case "LineSymbol":
                    cog = new LineCog(symbol);
                    break;

                case "CompoundLineSymbol":
                    cog = new CompoundLineCog(symbol);
                    break;

                default:
                    throw "DrawingInstructionEngine.addLine: Invalid style type."
                    + symbol.type;
            }

            cog.addLine(geometry);


            var sym = symbol;
            var trf = transform;

            if (sym.lines) {
                for (var j = 0; j < sym.lines.length; j++) {
                    var lsym = sym.lines[j];
                    var instructions = [];
                    var instr = { type: "draw-line", line: trf.to(2, geometry), size: calcSize(lsym.size), color: lsym.color, opacity: lsym.opacity, endCap: lsym.endCap, join: lsym.join, dashArray: getDashArray(lsym) };
                    instructions.push(instr);

                    // Start Arrow
                    if (lsym.startArrow) {
                        instructions = instructions.concat(generateLineArrowInstruction(geometry, lsym.startArrow, lsym.startArrowInterval));
                    }

                    if (lsym.endArrow) {
                        instructions = instructions.concat(generateLineArrowInstruction(geometry.reverse(), lsym.endArrow, lsym.endArrowInterval));
                    }

                    if (sym.drawByLayer) {
                        instructions.forEach(function (cur) {
                            addInstruction(j, cur);
                        });
                    }
                    else {
                        instructions.forEach(function (cur) {
                            addInstruction(0, cur);
                        });
                    }
                }
            }
            else {
                var instr = { type: "draw-line", line: trf.to(2, geometry), size: calcSize(sym.size), color: sym.color, opacity: sym.opacity, endCap: sym.endCap, join: sym.join, dashArray: getDashArray(sym) };
                addInstruction(0, instr);

                if (sym.startArrow) {
                    instructions = generateLineArrowInstruction(geometry, sym.startArrow, sym.startArrowInterval);
                    instructions.forEach(function (cur) {
                        addInstruction(0, cur);
                    });
                }

                if (sym.endArrow) {
                    instructions = generateLineArrowInstruction(geometry.reverse(), sym.endArrow, sym.endArrowInterval);
                    instructions.forEach(function (cur) {
                        addInstruction(0, cur);
                    });
                }
            }
        }

        this.addPolygon = function (geometry, symbol) {

            var cog;

            switch (symbol.type) {

                case "GenericSymbol":
                    cog = new GenericCog(symbol);
                    break;

                case "FillSymbol":
                    cog = new FillCog(symbol);
                    break;

                default:
                    throw "DrawingInstructionEngine.addPolygon: Invalid style type."
                    + symbol.type;
            }

            cog.addPolygon(geometry);
        }

        this.addMultiPoint = function (geometry, symbol) {
            geometry.points.forEach(function (cur) {
                me.addPoint(geometry, symbol);
            });
        }

        this.addMultiLine = function (geometry, symbol) {
            geometry.lines.forEach(function (cur) {
                me.addLine(cur, symbol);
            });
        }

        this.addMultiPolygon = function (geometry, symbol) {
            geometry.polygons.forEach(function (cur) {
                me.addPolygon(cur, symbol);
            });
        }

        this.getInstructions = function () {
            var res = [];

            for (var i = 0; i < passes.length; i++) {
                for (var j = 0; j < passes[i].length; j++) {
                    res.push(passes[i][j]);
                }
            }

            return res;
        }

        ///////////////////////////////////////////////

        var calcSize = function (size) {
            if (scaleType == 0)
                return size;

            var res = transform.toSize(size);
            if (res != 0 && res < 1)
                res = 1;
            return res;
        }

        var addInstruction = function (pass, inst) {
            if (pass > passes.length - 1)
                passes.push([]);

            passes[pass].push(inst);
        }

        var funcSign = function (n) {

            // A function to determine the sign of a number

            if (!n) return +1;
            if (n >= 0) {
                return +1;
            } else {
                return -1;
            }
        };

        var getDashArray = function (sym) {

            var size = calcSize(sym.size);

            if (size == 0)
                return [];

            var dashes = [];

            if (sym.lineType == 1)
                dashes = [4 * size, 2 * size];

            if (sym.lineType == 2)
                dashes = [2 * size, 2 * size];

            return dashes;
        }

        var generateLineArrowInstruction = function (geometry, psym, interval) {
            if (!interval) interval = 0;

            var result = [];
            var length = 0;
            while (true) {
                var r = geometry.getPointAndSegmentAtLength(length);
                if (r) {
                    var p0 = r.segment.p1;
                    var p1 = r.segment.p2;
                    var angleRadians = Math.PI / 2 - Math.atan2(p1.y - p0.y, p1.x - p0.x) + Math.PI;
                    var instrs = me._generatePointSymbolInstructions(r.point, psym, trf, calcSize, getDashArray, angleRadians);
                    result = result.concat(instrs);
                    if (!interval) {
                        break;
                    } else {
                        length += interval;
                    }
                } else {
                    break; // No segments found
                }
            }

            return result;
        };

        var generatePointInstructions = function (point, sym, trf, calcSize, getDashArray, angleRadians) {

            if (!angleRadians) angleRadians = 0.0;
            var res = [];
            var center = trf.to(1, point);
            var radius = calcSize(sym.size);
            if (!sym.pointType)
                sym.pointType = 0;

            switch (sym.pointType) {
                case 0:
                    // Point symbol
                    var instr = { type: "fill-circle", center: center, radius: radius, color: sym.color, opacity: sym.opacity };
                    res.push(instr);

                    if (sym.border && sym.border.size > 0) {
                        var instr = { type: "draw-circle", center: trf.to(1, point), radius: radius, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity };
                        res.push(instr);
                    }
                    break;
                case 1:
                    // Square
                    var points = [];
                    points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));
                    points.push(new Mapzania.Point(center.x + (radius * 1), center.y + (radius * 1)));
                    points.push(new Mapzania.Point(center.x + (radius * 1), center.y - (radius * 1)));
                    points.push(new Mapzania.Point(center.x - (radius * 1), center.y - (radius * 1)));
                    points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));

                    var lines = new Mapzania.Line(points);
                    lines = lines.rotate(center, angleRadians);

                    var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                    res.push(instr);
                    if (sym.border && sym.border.size > 0) {
                        var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                        res.push(instr);
                    }
                    break;
                case 2:
                    // Triangle
                    var points = [];
                    points.push(new Mapzania.Point(center.x, center.y - radius));
                    points.push(new Mapzania.Point(center.x + (radius * 1), center.y + (radius * 1)));
                    points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));
                    points.push(new Mapzania.Point(center.x, center.y - radius));

                    var lines = new Mapzania.Line(points);
                    lines = lines.rotate(center, angleRadians);

                    var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                    res.push(instr);
                    if (sym.border && sym.border.size > 0) {
                        var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                        res.push(instr);
                    }
                    break;
                case 3:
                    // Cross
                    var points = [];
                    points.push(new Mapzania.Point(center.x - (radius * 1), center.y + (radius * 1)));
                    points.push(new Mapzania.Point(center.x + (radius * 1), center.y - (radius * 1)));
                    var line1 = new Mapzania.Line(points);
                    line1 = line1.rotate(center, angleRadians);

                    points = [];
                    points.push(new Mapzania.Point(center.x + (radius * 1), center.y + (radius * 1)));
                    points.push(new Mapzania.Point(center.x - (radius * 1), center.y - (radius * 1)));
                    var line2 = new Mapzania.Line(points);
                    line2 = line2.rotate(center, angleRadians);

                    var instr1 = { type: "draw-line", line: line1, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    var instr2 = { type: "draw-line", line: line2, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    res.push(instr1);
                    res.push(instr2);
                    break;
                case 4:
                    // Diamand
                    var points = [];
                    points.push(new Mapzania.Point(center.x, center.y + radius));
                    points.push(new Mapzania.Point(center.x + (radius * 0.707), center.y));
                    points.push(new Mapzania.Point(center.x, center.y - radius));
                    points.push(new Mapzania.Point(center.x - (radius * 0.707), center.y));
                    points.push(new Mapzania.Point(center.x, center.y + radius));

                    var lines = new Mapzania.Line(points);
                    lines = lines.rotate(center, angleRadians);

                    var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                    res.push(instr);
                    if (sym.border && sym.border.size > 0) {
                        var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                        res.push(instr);
                    }
                    break;
                case 5:
                    // Crosshair
                    var points = [];
                    points.push(new Mapzania.Point(center.x - radius, center.y));
                    points.push(new Mapzania.Point(center.x + radius, center.y));
                    var line1 = new Mapzania.Line(points);
                    line1 = line1.rotate(center, angleRadians);

                    points = [];
                    points.push(new Mapzania.Point(center.x, center.y + radius));
                    points.push(new Mapzania.Point(center.x, center.y - radius));
                    var line2 = new Mapzania.Line(points);
                    line2 = line2.rotate(center, angleRadians);

                    var instr1 = { type: "draw-line", line: line1, size: calcSize(sym.border.size) / 2.0, color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    var instr2 = { type: "draw-line", line: line2, size: calcSize(sym.border.size) / 2.0, color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                    res.push(instr1);
                    res.push(instr2);

                    var instr3 = { type: "draw-circle", center: center, radius: radius, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity };
                    res.push(instr3);
                    break;
                case 6:
                    // Triangle for a Line Arrow
                    var points = [];
                    points.push(new Mapzania.Point(center.x, center.y - radius));
                    points.push(new Mapzania.Point(center.x + (radius * 0.866), center.y + (radius * 0.5)));
                    points.push(new Mapzania.Point(center.x - (radius * 0.866), center.y + (radius * 0.5)));
                    points.push(new Mapzania.Point(center.x, center.y - radius));

                    var lines = new Mapzania.Line(points);
                    lines = lines.rotate(center, angleRadians);

                    var instr = { type: "fill-polygon", lines: [lines], color: sym.color, opacity: sym.opacity, fillType: 1 };
                    res.push(instr);
                    if (sym.border && sym.border.size > 0) {
                        var instr = { type: "draw-line", line: lines, size: calcSize(sym.border.size), color: sym.border.color, opacity: sym.border.opacity, endCap: sym.border.endCap, join: sym.border.join, dashArray: getDashArray(sym.border) };
                        res.push(instr);
                    }
                    break;
                case 500:
                    // Image / Icon symbol
                    var p = new Mapzania.Point(center.x, center.y);
                    p.translate(sym.imageOffset.x, -sym.imageOffset.y);

                    var extent = new Mapzania.Extent({
                        minX: p.x - sym.imageSize.width / 2,
                        minY: p.y - sym.imageSize.height / 2,
                        maxX: p.x + sym.imageSize.width / 2,
                        maxY: p.y + sym.imageSize.height / 2
                    });
                    var instr = { type: "image", extent: extent, opacity: sym.opacity, src: sym.imageUrl };
                    res.push(instr);
                    break;
                default:
                    throw ("Unknown point symbol type: " + sym.pointType);
            }

            return res;
        }

        /////////////////////////////////////////////

        function CompoundPointCog(symbol) {

            this.addPoint = function (geometry) {

                var sym = symbol;

                for (var j = 0; j < sym.points.length; j++) {
                    var psym = sym.points[j];
                    var instrs = generatePointInstructions(geometry, psym, transform, calcSize, getDashArray);

                    if (sym.drawByLayer) {
                        instrs.forEach(function (cur) {
                            addInstruction(j, cur);
                        });
                    }
                    else {
                        instrs.forEach(function (cur) {
                            addInstruction(0, cur);
                        });
                    }
                }
            }
        }

        function PointCog(symbol) {

            this.addPoint = function (geometry) {

                var sym = symbol;

                var instrs = generatePointInstructions(geometry, sym, transform, calcSize, getDashArray);
                instrs.forEach(function (cur) {
                    addInstruction(0, cur);
                });
            }
        }

        function LineCog(symbol) {

            this.addLine = function (geometry) {

                var sym = symbol;
                var trf = transform;

                var instr = { type: "draw-line", line: trf.to(2, geometry), size: calcSize(sym.size), color: sym.color, opacity: sym.opacity, endCap: sym.endCap, join: sym.join, dashArray: getDashArray(sym) };
                addInstruction(0, instr);

                if (sym.startArrow) {
                    instructions = generateLineArrowInstruction(geometry, sym.startArrow, sym.startArrowInterval);
                    instructions.forEach(function (cur) {
                        addInstruction(0, cur);
                    });
                }

                if (sym.endArrow) {
                    instructions = generateLineArrowInstruction(geometry.reverse(), sym.endArrow, sym.endArrowInterval);
                    instructions.forEach(function (cur) {
                        addInstruction(0, cur);
                    });
                }
            }
        }

        function CompoundLineCog(symbol) {

            this.addLine = function (geometry) {

                var sym = symbol;
                var trf = transform;

                for (var j = 0; j < sym.lines.length; j++) {
                    var lsym = sym.lines[j];
                    var instructions = [];
                    var instr = { type: "draw-line", line: trf.to(2, geometry), size: calcSize(lsym.size), color: lsym.color, opacity: lsym.opacity, endCap: lsym.endCap, join: lsym.join, dashArray: getDashArray(lsym) };
                    instructions.push(instr);

                    // Start Arrow
                    if (lsym.startArrow) {
                        instructions = instructions.concat(generateLineArrowInstruction(geometry, lsym.startArrow, lsym.startArrowInterval));
                    }

                    if (lsym.endArrow) {
                        instructions = instructions.concat(generateLineArrowInstruction(geometry.reverse(), lsym.endArrow, lsym.endArrowInterval));
                    }

                    if (sym.drawByLayer) {
                        instructions.forEach(function (cur) {
                            addInstruction(j, cur);
                        });
                    }
                    else {
                        instructions.forEach(function (cur) {
                            addInstruction(0, cur);
                        });
                    }
                }
            }
        }

        function FillCog(symbol) {

            this.addPolygon = function (geometry) {
                var sym = symbol;
                var trf = transform;

                var g = trf.to(3, geometry);
                var lines = [g.shell].concat(g.holes);

                if (sym.color) {
                    var instr = { type: "fill-polygon", lines: lines, color: sym.color, opacity: sym.opacity, fillType: sym.fillType };
                    addInstruction(0, instr);
                }

                if (sym.outline) {
                    var osym = sym.outline;

                    for (var k = 0; k < lines.length; k++) {

                        var line = lines[k];

                        if (osym.lines) {
                            for (var j = 0; j < osym.lines.length; j++) {
                                var lsym = osym.lines[j];
                                var instr = { type: "draw-line", line: line, size: calcSize(lsym.size), color: lsym.color, opacity: lsym.opacity, endCap: lsym.endCap, join: lsym.join, dashArray: getDashArray(lsym) };

                                if (osym.drawByLayer)
                                    addInstruction(j, instr);
                                else
                                    addInstruction(0, instr);
                            }
                        }
                        else {
                            var instr = { type: "draw-line", line: line, size: calcSize(osym.size), color: osym.color, opacity: osym.opacity, endCap: osym.endCap, join: osym.join, dashArray: getDashArray(osym) };
                            addInstruction(0, instr);
                        }
                    }
                }
            }
        }

        function GenericCog(symbol) {

            this.addPolygon = function (geometry) {
                var sym = symbol;
                var trf = transform;

                var g = trf.to(3, geometry);
                var lines = [g.shell].concat(g.holes);

                if (sym.fillColor) {
                    var instr = {
                        type: "fill-polygon",
                        lines: lines,
                        color: sym.fillColor,
                        opacity: 100,
                        fillType: 1
                    };
                    addInstruction(0, instr);
                }

                if (sym.strokeColor && sym.strokeWidth > 0) {
                    var osym = sym.outline;

                    for (var k = 0; k < lines.length; k++) {

                        var line = lines[k];

                        var instr = {
                            type: "draw-line",
                            line: line,
                            size: calcSize(sym.strokeWidth),
                            color: sym.strokeColor,
                            opacity: 100
                        };
                        addInstruction(0, instr);
                    }
                }
            }

            this.addLine = function (geometry) {

                var instr = {
                    type: "draw-line",
                    line: geometry,
                    size: calcSize(symbol.strokeWidth),
                    color: symbol.strokeColor,
                    opacity: 100
                };

                addInstruction(0, instr);
            }

            this.addPoint = function (geometry) {

                var sym = symbol;
                var center = transform.to(1, geometry);
                var radius = calcSize(sym.iconSize);

                var instr = {
                    type: "fill-circle",
                    center: center,
                    radius: radius,
                    color: sym.fillColor,
                    opacity: 100
                };

                addInstruction(0, instr);

                if (sym.strokeColor && sym.strokeWidth > 0) {

                    var instr = {
                        type: "draw-circle",
                        center: transform.to(1, geometry),
                        radius: radius,
                        size: calcSize(sym.strokeWidth),
                        color: sym.strokeColor,
                        opacity: 100
                    };

                    addInstruction(0, instr);
                }
            }
        }
    }

    ns.DrawingInstructionEngine = DrawingInstructionEngine;

    return ns;

})(Mapzania || {});