﻿/// <reference path="../lib/swfobject-1.5.js" />

var Mapzania = (function (ns) {

    //====================Canvas Checker====================

    function CanvasCheck() { }

    CanvasCheck.browserSupportsCanvas = function () {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
    }

    //========================Toggler=======================

    function Toggler() {
        this.ctrlrs = [];
    }

    Toggler.prototype.add = function (key, ctrlr) {
        this.ctrlrs.push({ key: key, ctrlr: ctrlr });
    }

    Toggler.prototype.toggle = function (key) {
        // The Tools Manager only manages certain map states. The result may, in some cases, be null.
        var result = null;
        for (var i = 0; i < this.ctrlrs.length; i++) {
            var item = this.ctrlrs[i];
            item.ctrlr.enabled(item.key == key);
            if (item.key == key) {
                result = {
                    state: key,
                    control: item.ctrlr
                }
            }
        }
        return result;
    }

    //======================ToolsManager====================

    function ToolsManager(model, viewport, drawer) {

        this.model = model;
        this.viewport = viewport;
        this.drawer = drawer;

        // The leaflet viewport needs a handle to the tools manager. We needs this, as certain tools should not allow
        //  leaflet to capture events - i.e. certain tools should disable map pan and zoom interactions.
        this.viewport._toolsManager = this; 

        this.toggler = new Toggler();

        this.defaultMapState = Mapzania.MapState.pan;
        this.currentControlBag = null;

        var ctns = Mapzania.Controllers;

        ////-----------------Area Selector----------------------
        //var area = new ctns.MultiSelector(viewport, drawer, function (data) {
        //    var line = new ns.Line();
        //    line.points = data;

        //    var res = me._trf().from(2, line);

        //    model.getArea(res.points);
        //}, false, true);

        //this.toggler.add(ns.MapState.area, area);

        ////---------------Length Selector----------------------
        //var length = new ctns.MultiSelector(viewport, drawer, function (data) {
        //    var line = new ns.Line();
        //    line.points = data;

        //    var res = me._trf().from(2, line);

        //    model.getLength(res.points);
        //}, false, false);

        //this.toggler.add(ns.MapState.length, length);

        //--------------------Point Selector------------------
        var queryPoint = new ctns.MultiSelector(viewport, drawer, function (data) {
            if (data.length == 0)
                return;

            var pnt = me._trf().from(1, data[0]);
            model.queryAt({ type: 1, geom: pnt, pixel:data[0] });

        }, true, false);

        this.toggler.add(ns.MapState.queryPoint, queryPoint);

        //----------------------Line Selector------------------------        
        var queryLine = new ctns.MultiSelector(viewport, drawer, function (data) {

            if (data.length >= 2) {
                var line = new ns.Line();
                line.points = data;
                var res = me._trf().from(2, line);
                model.queryAt({ type: 2, geom: res });
            }
        }, false, false);

        this.toggler.add(ns.MapState.queryLine, queryLine);

        //---------------------Polygon Selector-----------------
        var queryPolygon = new ctns.MultiSelector(viewport, drawer, function (data) {

            if (data.length >= 3) {
                var line = new ns.Line();
                line.points = data;
                var poly = new ns.Polygon();
                poly.shell = line;

                var res = me._trf().from(3, poly);
                model.queryAt({ type: 3, geom: res, enclosed: true });
            }
        }, false, true);

        this.toggler.add(ns.MapState.queryPolygon, queryPolygon);

        //--------------Enclosed Polygon Selector----------
        var queryPolygonEnclosed = new ctns.MultiSelector(viewport, drawer, function (data) {
            var line = new ns.Line();
            line.points = data;
            //line.points.push(data[0]);

            var poly = new ns.Polygon();
            poly.shell = line;

            var res = me._trf().from(3, poly);
            model.queryAt({ type: 3, geom: res, enclosed: true });

        }, false, true);

        this.toggler.add(ns.MapState.queryPolygonEnclosed, queryPolygonEnclosed);

        //----------------------Pan------------------------
        this.toggler.add(ns.MapState.pan, new ctns.Pan(viewport, function (diffs) {
            // During a Pan, we Pan with a delta x and y value in pixels (input parameters).
            //  This gives us an angle. The delta x and y in world coordinates gives as a
            //  length in world coordinates (from pixel point (0, 0)). 
            //  All we do then, is to Pan the model's extent with the given angle in world coordinates. 

            var pDiffPixels = new Mapzania.Point(diffs.x, diffs.y);
            var pWorld = me._trf().from(1, pDiffPixels);
            var pPixelZero = new Mapzania.Point(0, 0);
            var pWorldZero = me._trf().from(1, pPixelZero);

            var diffXWorld = pWorld.x - pWorldZero.x;
            var diffYWorld = pWorld.y - pWorldZero.y;

            var lengthWorld = Math.sqrt(diffXWorld * diffXWorld + diffYWorld * diffYWorld);
            var angle = Math.atan2(-diffs.y, diffs.x);

            var diffX = lengthWorld * Math.cos(angle);
            var diffY = lengthWorld * Math.sin(angle);

            var extNew = {};
            $.extend(extNew, me.model.extent);

            extNew.minX -= diffXWorld;
            extNew.maxX -= diffXWorld;
            extNew.minY -= diffYWorld;
            extNew.maxY -= diffYWorld;

            // Instruct the model to zoomTo, indicating that this is comming from a PAN operation.
            model.zoomTo(new Mapzania.Extent(extNew), false, false, false, true);
        }));

        //----------------------Zoom In------------------------
        this.toggler.add(ns.MapState.zoomIn, new ctns.RectangleSelector(viewport, drawer, function (ext) {
            if (ext.width() < 10 || ext.height() < 10)
                return;
            var extNew = me._trf().from(-1, new ns.Extent(ext));
            model.zoomTo(extNew, false, true);
            model.changeState(me.defaultMapState);
        }));

        var me = this;

        model.stateChanged.add(function (e) {
            var result = me.toggler.toggle(e.data);
            if (result) {
                // We know it is a control that we manage
                if (result.control.isTempState()) {
                    if (me.currentControlBag && !me.currentControlBag.control.isTempState()) {
                        setTimeout(function () { me.model.changeState(me.defaultMapState); }, 0);
                    }
                } else {
                    me.currentControlBag = result;
                }
            } else {
                // A control (or state) that we do not manage, i.e. Print, Export or custom toolbar items. We assume these are temporary states
                if (me.currentControlBag && !me.currentControlBag.control.isTempState()) {
                    setTimeout(function () { me.model.changeState(me.defaultMapState); }, 0);
                }
            }
        });

        this.viewport.mouseWheelUp.add(function (e) {
            if (me.currentControlBag && me.currentControlBag.control.canZoomMap()) {

                //console.log("wheel-up", e);
                //console.log("wheel:" + e.data.x + "/" + e.data.y);

                //me.drawer.clearLayer("ts_interactive");
                //me.drawer.fillCircle("ts_interactive", e.data, 4, "#FFFF00", 60);
                //me.drawer.fillCircle("ts_interactive", e.data, 2, "#000000", 100);

                var pnt = me._trf().from(1, e.data);
                me.model.zoomIn(pnt);
            }
        });

        this.viewport.doubleClick.add(function (e) {
            if (me.currentControlBag && me.currentControlBag.control.canZoomMap()) {
                var pnt = me._trf().from(1, e.data);
                me.model.zoomIn(pnt);
            }
        });

        this.viewport.mouseWheelDown.add(function (e) {
            if (me.currentControlBag && me.currentControlBag.control.canZoomMap()) {

                //console.log("wheel-down", e);
                //console.log("wheel:" + e.data.x + "/" + e.data.y);

                //me.drawer.clearLayer("ts_interactive");
                //me.drawer.fillCircle("ts_interactive", e.data, 4, "#FFFF00", 60);
                //me.drawer.fillCircle("ts_interactive", e.data, 2, "#000000", 100);

                var pnt = me._trf().from(1, e.data);
                me.model.zoomOut(pnt);
            }
        });

        //
        // During map startup, we make the Pan control the default
        //
        this.model.schemaLoaded.add(function () {
            me.model.changeState(me.defaultMapState);
        });
    }

    ToolsManager.prototype._trf = function () {
        return this.viewport.getTransformer(this.model.extent);
    }

    ns.CanvasCheck = CanvasCheck;
    ns.ToolsManager = ToolsManager;

    return ns;

})(Mapzania || {});
