﻿var Mapzania = (function (ns) {
    QueryType = { point: 1, polygon: 2, polygonEnclosed: 3 };
    QueryGeometryType = { point: "Point", polygon: "Polygon" };

    //#region Constructor

    function QueryManager(model, viewport, bufferPerc, geoFenceLayerSourceKey, selectableLayerNames) {
        var me = this;

        this.model = model;
        this.viewport = viewport;
        this.bufferPerc = bufferPerc;
        this.geoFenceLayerSourceKey = geoFenceLayerSourceKey;
        this.selectableLayerNames = selectableLayerNames;

        //This object gets returned in the featuresFetched event
        this.featuresFetchedResultObject = {
            layerKey: "",
            sourceKey: "",
            queryType: null,
            features: []
        };

        this.geoFenceFilterObject = {
            OriginalLayerSourceKey: "",
            OriginalLayerSourceFilter: "",
            GeoFenceSRID: "",
            GeoFenceFeatures: [],
            GeoFenceEnclose: false,
            DisplaySRID: null
        };

        this.selectableLayersObject = {
            layerKey: null,
            sourceKey: null,
            layerName: null,
            layerOrdinal: null,
            layerVisible: false
        };

        this.selectableLayers = [];

        this.featuresFetched = new Mapzania.Event();

        me.model.schemaLoaded.add(function (evt) {
            me._buildQueryableLayers(evt.data, me.selectableLayerNames);
        });

        me.model.layerShown.add(function (evt) {
            for (var i = 0; i < me.selectableLayers.length; i++) {
                if (evt.data.lastIndexOf(me.selectableLayers[i].layerKey, 0) === 0) {
                    me.selectableLayers[i].layerVisible = true;
                    return false;
                }
            }
        });

        me.model.layerHidden.add(function (evt) {
            for (var i = 0; i < me.selectableLayers.length; i++) {
                if (evt.data.lastIndexOf(me.selectableLayers[i].layerKey, 0) === 0) {
                    me.selectableLayers[i].layerVisible = false;
                    return false;
                }
            }
        });
    }

    //#endregion

    //#region Public Methods

    QueryManager.prototype.updateQueryableLayers = function (newSelectableLayerNames) {
        var me = this;

        me.selectableLayerNames = newSelectableLayerNames;
        me._buildQueryableLayers(me.model.getSchema(), me.selectableLayerNames);
    }

    QueryManager.prototype.queryPoint = function (point, displaySRID) {
        var me = this;

        //Shows waiting div
        //me.viewport.setIsLoading(true);

        //Get map's current extent
        var currentExtent = me.model.extent;

        if (me.bufferPerc == null)
            me.bufferPerc = 2;

        //Set point buffer
        var buffer = Math.max((currentExtent.width() * me.bufferPerc * 0.01), (currentExtent.height() * me.bufferPerc * 0.01));

        //Fetch the closest feature
        me._fetchFeaturesForPoint(point, buffer, me.model.getSchema(), displaySRID);
    }

    QueryManager.prototype.queryPolygon = function (polygon, displaySRID, enclosing) {
        var me = this;

        //Shows waiting div
        me.viewport.setIsLoading(true);

        //This method checks whether the polyon is closed, if not it closes the polygon
        polygon = me._closePolygon(polygon);

        //Fetch the closest feature
        me._fetchFeaturesForPolygon(polygon, me.model.getSchema(), displaySRID, enclosing);
    }

    //#endregion

    //#region Private Methods

    QueryManager.prototype._closePolygon = function (polygon) {
        var me = this;

        if (polygon != null) {
            if (polygon.shell != null) {
                if (polygon.shell.points != null && polygon.shell.points.length > 0) {
                    var l = polygon.shell.points.length;
                    if (l == 1 || (polygon.shell.points[0].x != polygon.shell.points[l - 1].x || polygon.shell.points[0].y != polygon.shell.points[l - 1].y)) {
                        polygon.shell.points.push(new Mapzania.Point(polygon.shell.points[0].x, polygon.shell.points[0].y));
                    }
                }
            }
        }

        return polygon;
    }

    QueryManager.prototype._buildQueryableLayers = function (schema, selectableLayerNames) {
        var me = this;

        me.selectableLayers = [];

        if (selectableLayerNames == null)
            return;

        for (var i = 0; i < schema.layers.length; i++) {
            if (selectableLayerNames.length == 0 || selectableLayerNames.indexOf(schema.layers[i].key) >= 0) {
                if (!(typeof schema.layers[i].fields === "undefined") &&
                    (schema.layers[i].fields.length > 0)) {

                    me.selectableLayersObject = {
                        layerKey: schema.layers[i].key,
                        sourceKey: schema.layers[i].sourceKey,
                        layerName: schema.layers[i].name,
                        layerOrdinal: schema.layers[i].ordinal,
                        layerVisible: schema.layers[i].visible
                    };

                    me.selectableLayers.push(me.selectableLayersObject);
                }
            }
        }

        me.selectableLayers = me.selectableLayers.sort(function (a, b) {
            return a.layerOrdinal > b.layerOrdinal;
        });
    }

    QueryManager.prototype._fetchFeaturesForPoint = function (point, buffer, schema, displaySRID) {
        var me = this;

        var featuresFetchedResults = [];

        var featuresFetchLoop = function (i) {
            if (i >= me.selectableLayers.length) {
                //me.viewport.setIsLoading(false);
                me.featuresFetched.fire(me, featuresFetchedResults);
                return;
            }

            if (me.selectableLayers[i].layerVisible == true) {
                var orgFilter = "";

                var layer = $.grep(schema.layers, function (e) { return e.key === me.selectableLayers[i].layerKey; })[0];

                if (layer != null) {
                    orgFilter = layer.filter;
                }

                var polygon = new Mapzania.Polygon();
                polygon.shell = new Mapzania.Line([
                    new Mapzania.Point(point.x - buffer, point.y - buffer),
                    new Mapzania.Point(point.x - buffer, point.y + buffer),
                    new Mapzania.Point(point.x + buffer, point.y + buffer),
                    new Mapzania.Point(point.x + buffer, point.y - buffer),
                    new Mapzania.Point(point.x - buffer, point.y - buffer),
                ]);

                var geoFenceFeature = new Mapzania.Feature();
                geoFenceFeature.geometry = polygon;
                geoFenceFeature.attributes = null;

                me.geoFenceFilterObject = {
                    OriginalLayerSourceKey: me.selectableLayers[i].sourceKey,
                    OriginalLayerSourceFilter: orgFilter,
                    GeoFenceSRID: displaySRID,
                    GeoFenceFeatures: [geoFenceFeature],
                    GeoFenceEnclose: false,
                    DisplaySRID: displaySRID
                };

                me.model.api.getFeatures(
                    me.geoFenceLayerSourceKey,
                    JSON.stringify(me.geoFenceFilterObject),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    function (ftrs) {
                        me.featuresFetchedResultObject = {
                            layerKey: me.selectableLayers[i].layerKey,
                            sourceKey: me.selectableLayers[i].sourceKey,
                            queryType: QueryType.point,
                            features: ftrs,
                            queryGeometryType: QueryGeometryType.point,
                            queryGeometry: point,
                            querySRID: displaySRID
                        };

                        featuresFetchedResults.push(me.featuresFetchedResultObject);
                        featuresFetchLoop(i + 1);
                    });
            } else {
                featuresFetchLoop(i + 1);
            }
        };
        featuresFetchLoop(0);
    }

    QueryManager.prototype._fetchFeaturesForPolygon = function (polygon, schema, displaySRID, enclosing) {
        var me = this;

        var featuresFetchedResults = [];

        var featuresFetchLoop = function (i) {
            if (i >= me.selectableLayers.length) {
                me.viewport.setIsLoading(false);
                me.featuresFetched.fire(me, featuresFetchedResults);
                return;
            }

            if (me.selectableLayers[i].layerVisible == true) {
                var orgFilter = "";

                var layer = $.grep(schema.layers, function (e) { return e.key === me.selectableLayers[i].layerKey; })[0];

                if (layer != null) {
                    orgFilter = layer.filter;
                }

                var geoFenceFeature = new Mapzania.Feature();
                geoFenceFeature.geometry = polygon;
                geoFenceFeature.attributes = null;

                me.geoFenceFilterObject = {
                    OriginalLayerSourceKey: me.selectableLayers[i].sourceKey,
                    OriginalLayerSourceFilter: orgFilter,
                    GeoFenceSRID: displaySRID,
                    GeoFenceFeatures: [geoFenceFeature],
                    GeoFenceEnclose: enclosing,
                    DisplaySRID: displaySRID
                };

                me.model.api.getFeatures(
                    me.geoFenceLayerSourceKey,
                    JSON.stringify(me.geoFenceFilterObject),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    function (ftrs) {
                        me.featuresFetchedResultObject = {
                            layerKey: me.selectableLayers[i].layerKey,
                            sourceKey: me.selectableLayers[i].sourceKey,
                            queryType: (enclosing) ? QueryType.polygonEnclosed : QueryType.polygon,
                            features: ftrs,
                            queryGeometryType: QueryGeometryType.polygon,
                            queryGeometry: polygon,
                            querySRID: displaySRID
                        };

                        featuresFetchedResults.push(me.featuresFetchedResultObject);
                        featuresFetchLoop(i + 1);
                    });
            } else {
                featuresFetchLoop(i + 1);
            }
        };
        featuresFetchLoop(0);
    }

    //#endregion

    ns.QueryManager = QueryManager;

    return ns;

})(Mapzania || {})
