﻿var Mapzania = (function (ns) {

	function StandardThemer(theme) {
		this.theme = theme
	}

	StandardThemer.prototype.getSymbol = function (feature) {
	    return this.theme.symbol;
	}

	//StandardThemer.prototype.getLabeller = function (feature) {
	//    return this.theme.labeller;
	//}

	StandardThemer.prototype.getLabelText = function (feature) {

	    var labeller = this.theme.labeller;

	    if (!labeller || !labeller.visible)
	        return null;

	    return feature.attributes[labeller.field];
	}

	StandardThemer.prototype.getLabelSymbol = function (feature) {

	    var labeller = this.theme.labeller;

	    if (!labeller || !labeller.visible)
	        return null;

	    return labeller.symbol;
	}

	ns.StandardThemer = StandardThemer;

	return ns;

})(Mapzania || {})
