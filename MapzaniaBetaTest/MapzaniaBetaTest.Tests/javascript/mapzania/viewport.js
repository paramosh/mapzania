﻿/**
 * @doc Mapzania.Viewport.*ClassDescription*
 *
 * A class responsible for coordinating the web user interface and the Mapper (also known as the drawer, such as Canvas or Raphael).
 * Applications typically instantiate this class once, and does not make direct use of any functionality provided in this class.
 */
var Mapzania = (function (ns) {

    var ts = Mapzania;

    /**
      * @doc Mapzania.Viewport.Viewport
      *
      * Constructor.
      *
      * @param elId (String) A CSS selector for the element (typically a DIV) in which the map User Interface will be inject. 
      *                      An example value for this parameter is "#map", where the map containing DIV has an id of "map".
      *
      * @param mapper (Mapzania.Mapper) The map renderer instance to use. Valid class instances are Canvas.Mapper, Raphael.Mapper or Flash.Mapper.
      */
    function Viewport(elId, mapper) {

        this.mapper = mapper;
        this.elId = elId;
        this.$el = $(elId);

        this.mouseDown = new ts.Event();
        this.mouseUp = new ts.Event();
        this.mouseMove = new ts.Event();
        this.doubleClick = new ts.Event();
        this.mouseWheelUp = new ts.Event();
        this.mouseWheelDown = new ts.Event();
        this.resized = new ns.Event();

        this.buffer = 2;
        this.scale = 2;

        this.rotation = {
            originX: 0,
            originY: 0,
            rotateZ: 0
        };
    }

    /**
      * @doc Mapzania.Viewport.init
      *
      * Initializes the viewport by injecting the map HTML UI component(s) in the web application.
      *
      * @param callback (Function) A callback function to call when the operation completed.
      */
    Viewport.prototype.init = function (callback) {
        $el = this.$el;
        $el.css("overflow", "hidden");

        $el.html("<div></div><div class = 'ts-loading-progress'></div><div></div>");

        this.$waitEl = $(this.elId + " div:last-child");
        this.$waitEl.addClass("ts-viewport-loading");

        this.$progressLoading = $(this.elId + " div:nth-child(2)");

        this.$mapEl = $(this.elId + " div:first-child");
        this.$mapEl.height(this.scale * $el.height());
        this.$mapEl.width(this.scale * $el.width());
        this.$mapEl.css("position", "absolute");

        var w = this.$mapEl.width();
        var h = this.$mapEl.height();
        var left = -(w - w / this.buffer) / 2;
        var top = -(h - h / this.buffer) / 2;
        this.startPos = { left: left, top: top };
        this.setPos(this.startPos);

        var me = this;
        function calcOffset(event) {
            var w = me.$mapEl.width();
            var h = me.$mapEl.height();
            var left = -(w - w / me.buffer) / 2;
            var top = -(h - h / me.buffer) / 2;
            var res = {
                x: event.pageX - me.$el.offset().left - left,
                y: event.pageY - me.$el.offset().top - top
            };
            return res;
        }

        $el.mousedown(function (e) {
            var res = calcOffset(e);
            res.raw = { x: e.pageX, y: e.pageY };
            me.mouseDown.fire(me, res);
            $(this).css('cursor', 'default');
            return false;
        });

        $el.mouseup(function (e) {
            var res = calcOffset(e);
            res.raw = { x: e.pageX, y: e.pageY };
            me.mouseUp.fire(me, res);
            $(this).css('cursor', 'default');
            return false;
        });

        $el.mousemove(function (e) {
            me.mouseMove.fire(me, calcOffset(e));
            return false;
        });


        $el.on('wheel mousewheel DOMMouseScroll', function (e) {

            var evt = e.originalEvent;
            //console.log("WHEEL: " + evt.type);
            //console.log(evt);

            if (evt.deltaY > 0) { 
                me.mouseWheelDown.fire(me, calcOffset(evt));
            } else {
                me.mouseWheelUp.fire(me, calcOffset(evt));
            }
            return false; //prevent page fom scrolling
        });

        $el.dblclick(function (e) {
            var res = calcOffset(e);
            res.raw = { x: e.pageX, y: e.pageY };
            me.doubleClick.fire(me, res);
            $(this).css('cursor', 'default');
            return false;
        });

        this.mapper.init(this.$mapEl, callback);

        this._startMonitoringSize();
    }

    /**
      * @doc Mapzania.Viewport.getSize
      *
      * Returns the map viewport size, in pixels.
      *
      * @return (Object) An object in the form {width: 100, height: 100}, where both width and height are values in pixels.
      */
    Viewport.prototype.getSize = function () {
        return {
            height: this.$el.height(),
            width: this.$el.width()
        };
    }

    Viewport.prototype.onResized = function () {
        Mapzania.Log.debug("Detected that map was resized: "+ JSON.stringify(this.getSize()), "Mapzania.ViewPort");

        this.$mapEl = $(this.elId + " div:first-child");
        this.$mapEl.height(this.scale * $el.height());
        this.$mapEl.width(this.scale * $el.width());
        this.$mapEl.css("position", "absolute");

        var w = this.$mapEl.width();
        var h = this.$mapEl.height();
        var left = -(w - w / this.buffer) / 2;
        var top = -(h - h / this.buffer) / 2;
        this.startPos = { left: left, top: top };
        this.setPos(this.startPos);

        this.mapper.onResized();

        this.resized.fire(this);
    }

    Viewport.prototype._startMonitoringSize = function () {
        var currentSize = this.getSize();
        var me = this;
        var func = function () {
            var s = me.getSize();
            if (s.height != currentSize.height || s.width != currentSize.width) {
                if (s.width != 0 && s.height != 0) {
                    currentSize = s;
                    me.onResized();
                }
            }
            setTimeout(func, 500);
        };
        setTimeout(func, 500);
    }

    Viewport.prototype.getTransformer = function (extent) {
        var size = this.getSize();

        var wr = size.width / Math.abs(extent.maxX - extent.minX);
        var hr = size.height / Math.abs(extent.maxY - extent.minY);

        var ratio = Math.min(wr, hr);

        var scale = ratio * this.scale / this.buffer;

        var x = size.width / ratio;
        var y = size.height / ratio;

        var xoff = ratio != wr ? (size.width / wr - x) / 2 : 0;
        var yoff = ratio != hr ? (size.height / hr - y) / 2 : 0;

        var pointCenter = extent.getCenter();
        var pointCenterRotated = pointCenter.rotate(new Mapzania.Point(this.rotation.originX, this.rotation.originY), this.rotation.rotateZ);

        var params = {
            translateX: (pointCenterRotated.x - extent.width() / 2) + xoff - x * (this.buffer - 1) / 2,
            translateY: (pointCenterRotated.y + extent.height() / 2) - yoff + y * (this.buffer - 1) / 2,
            scaleX: scale,
            scaleY: -scale
        };

        $.extend(params, this.rotation);
        
        return new Mapzania.PixelTransformer(params);
    }

    Viewport.prototype.pan = function (deltaX, deltaY) {
        var diff = { x: deltaX, y: deltaY };
        //console.log("canvas pan");
        var start = this.startPos;

        var pos = this.$mapEl.position();
        this.setPos({
            left: start.left + deltaX,
            top: start.top + deltaY
        });
    }

    Viewport.prototype.reset = function () {
        this.setPos(this.startPos);
    }

    Viewport.prototype.panDone = function () {
        this.setPos(this.startPos);
    }

    Viewport.prototype.setIsLoading = function (isLoading) {
        if (isLoading)
            this.$waitEl.css("display", "block");
        else
            this.$waitEl.css("display", "none");
    }

    Viewport.prototype.setLoadingProgress = function (total, outstanding) {
        if (outstanding == 0 || total == 1) {
            this.$progressLoading.css("display", "none");
            this.$progressLoading.html("");
        } else {
            var perc = (total - outstanding) / total * 100;
            if (perc < 1) perc = 1;
            var html = "<table><tr><td style = 'background-color: black;' width='" + perc + "%'></td><td></td></tr></table>"
            this.$progressLoading.html(html);
            this.$progressLoading.css("display", "block");
        }
    }

    Viewport.prototype.zoomTo = function (extent) {
    }

    Viewport.prototype.setPos = function(pos){
        this.$mapEl.css("left", pos.left + "px");
        this.$mapEl.css("top", pos.top + "px");
    }

    ns.Viewport = Viewport;

    return ns;

})(Mapzania || {})
