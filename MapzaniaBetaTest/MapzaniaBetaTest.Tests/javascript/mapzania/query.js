﻿var Mapzania = (function (ns) {

    // EXAMPLE USAGE

  //  var map = new Mapzania.Map();

  //  var qry = new Mapzania.Query();

  //  qry.usingSource("MY_SRC")
  //          .filterByText("x==y")
  //          .filterByBoundingBox([1, 2, 3, 4])
  //          .fetch()
  //          .then(function (data) {
  //              //do stuff
  //          });

  //  qry.usingMapLayer(map, "LAYER_NAME")
  //.usingMapStateAndLayerFilters()
  //.filterByText("x==y")
  //.filterByBoundingBox([1, 2, 3, 4])
  //.fetch()
  //.then(function (data) {
  //    //do stuff
  //});

  //  qry.usingMapLayer(map, "LAYER_NAME")
  //      .usingMapStateOnly()
  //      .filterByText("x==y")
  //      .filterByBoundingBox([1, 2, 3, 4])
  //      .fetch()
  //      .then(function (data) {
  //          //do stuff
  //      });

  //  qry.usingMapLayer(map, "LAYER_NAME")
  //      .ignoringMapStateAndLayerFilters()
  //    .filterByText("x==y")
  //    .filterByBoundingBox([1, 2, 3, 4])
  //    .fetch()
  //    .then(function (data) {
  //        //do stuff
  //    });

    function Query(options) {

        var defaults = {
            apiRoot: "mz",
            isCompact: true
        };

        var settings = Object.assign(defaults, options);

        var l = window.location;
        var path = l.protocol + "//" + l.host + "/" + settings.apiRoot + "/";

        var api = new Mapzania.Api(path, settings.isCompact);

        this.usingSource = function (sourceKey) {
            return new SourceQuery(sourceKey);
        }

        this.usingMapLayer = function (map, layerKey) {
            return new MapLayerQuery(map, layerKey);
        }

        var SourceQuery = function (sourceKey, filters) {
            if (!filters)
                filters = [];

            this.filterByText = function (statement, passthru) {
                var filter = ns.Filters.filterByText(statement, passthru);
                filters.push(filter);
                return this;
            }

            this.filterByBoundingBox = function (bbox) {
                var filter = ns.Filters.filterByBoundingBox(bbox);
                filters.push(filter);
                return this;
            }

            this.filterByNearest = function (point, take, includeDistance) {
                var filter = ns.Filters.filterByNearest(point, take, includeDistance);
                filters.push(filter);
                return this;
            }

            this.filterByFeatureCollection = function (features, operation) {
                var filter = ns.Filters.filterByFeatureCollection(features, operation);
                filters.push(filter);
                return this;
            }

            this.filterByLayer = function (layerKey, layerFilters, operation) {
                var filter = ns.Filters.filterByLayer(layerKey, layerFilters, operation);
                filters.push(filter);
                return this;
            }

            this.clipByFeatureCollection = function (featureCollection) {
                var filter = ns.Filters.clipByFeatureCollection(featureCollection);
                filters.push(filter);
                return this;
            }

            this.changeProperties = function (properties) {
                var filter = ns.Filters.changeProperties(properties);
                filters.push(filter);
                return this;
            }

            this.convertToCentroids = function () {
                var filter = ns.Filters.convertToCentroids();
                filters.push(filter);
                return this;
            }

            this.bufferGeometry = function (factor, side, joins, ends, mitreLimit, srid) {
                var filter = ns.Filters.bufferGeometry(factor, side, joins, ends, mitreLimit, srid);
                filters.push(filter);
                return this;
            }

            this.fetch = function () {
                var res = new Promise(function (resolve, reject) {
                    try {
                        api.getFeaturesDirect(sourceKey, filters, settings, function (data) {
                            resolve(data);
                        });
                    }
                    catch (e) {
                        reject(e);
                    }
                });

                return res;
            }
        }

        var MapLayerQuery = function (map, layerKey) {
            var layer = map.getLayer(layerKey);
            var sourceKey = layer.sourceKey;

            this.usingMapStateAndLayerFilters = function () {
                var filters = [];

                Array.prototype.push.apply(filters, map.getStateFilters(layerKey));
                Array.prototype.push.apply(filters, map.getFilters(layerKey));

                return new SourceQuery(sourceKey, filters);
            }

            this.usingMapStateOnly = function () {
                var filters = [];

                Array.prototype.push.apply(filters, map.getStateFilters(layerKey));

                return new SourceQuery(sourceKey, filters);
            }

            this.usingLayerFiltersOnly = function () {
                var filters = [];

                Array.prototype.push.apply(filters, map.getFilters(layerKey));

                return new SourceQuery(sourceKey, filters);
            }

            this.ignoringMapStateAndLayerFilters = function () {
                return new SourceQuery(sourceKey);
            }
        }
    }

    ns.Query = Query;

    return ns;

})(Mapzania || {})
