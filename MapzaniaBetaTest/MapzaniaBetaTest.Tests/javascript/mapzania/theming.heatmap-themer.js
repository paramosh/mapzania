﻿var Mapzania = (function (ns) {

    function HeatMapThemer(theme) {
        this.theme = theme;
        this.minValue = null;
        this.maxValue = null;
        this.lookup = [];
    }

    HeatMapThemer.prototype.evalFeature = function (feature) {
        var featVal = feature.attributes[this.theme.field];
        if (this.minValue == null) {
            this.minValue = featVal;
            this.maxValue = featVal;
        } else {
            if (featVal < this.minValue) this.minValue = featVal;
            if (featVal > this.maxValue) this.maxValue = featVal;
        }
    }

    HeatMapThemer.prototype.doneWithEvalFeatures = function () {
        var step = (this.maxValue - this.minValue) / (this.theme.minToMaxSymbols.length - 1);
        for (var i = 0; i < this.theme.minToMaxSymbols.length - 1; i++) {
            var fromS = this.theme.minToMaxSymbols[i];
            var toS = this.theme.minToMaxSymbols[i + 1];
            var fromV = (this.minValue + (i * step));
            var toV = (this.minValue + (i * step)) + step;
            this.lookup.push({fromV: fromV, toV: toV, fromS: fromS, toS: toS});
        }
    }

    HeatMapThemer.prototype.getSymbol = function (feature) {
        var sym = null;
        var featVal = feature.attributes[this.theme.field];

        for (var i = 0; i < this.lookup.length; i++) {
            var cur = this.lookup[i];
            if (featVal >= cur.fromV && featVal <= cur.toV) {
                sym = this.buildSymbol(cur, featVal);
            }
        }

        return sym;
    }

    HeatMapThemer.prototype.getLabeller = function (feature) {
        return this.theme.labeller;
    }

    HeatMapThemer.prototype.buildSymbol = function (lookupEntry, featVal) {
        var sym = {};
        $.extend(true, sym, lookupEntry.fromS);

        var perc = (featVal - lookupEntry.fromV) / (lookupEntry.toV - lookupEntry.fromV);

        var fromRGB = this.hexToRgb(lookupEntry.fromS.color);
        var toRGB = this.hexToRgb(lookupEntry.toS.color);
        var colorR = Math.floor((toRGB.r - fromRGB.r) * perc + fromRGB.r);
        var colorG = Math.floor((toRGB.g - fromRGB.g) * perc + fromRGB.g);
        var colorB = Math.floor((toRGB.b - fromRGB.b) * perc + fromRGB.b);

        sym.color = this.rgbToHex(colorR, colorG, colorB);
        return sym;
    }

    HeatMapThemer.prototype.hexToRgb = function (hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function (m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    HeatMapThemer.prototype.rgbToHex = function (r, g, b) {
        var componentToHex = function (c) {
            var hex = c.toString(16);
            return hex.length == 1 ? "0" + hex : hex;
        }
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    ns.HeatMapThemer = HeatMapThemer;

    return ns;

})(Mapzania || {})
