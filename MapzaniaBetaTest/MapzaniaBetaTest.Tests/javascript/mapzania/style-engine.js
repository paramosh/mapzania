﻿/// <reference path="../mapzania.js"/>
var Mapzania = (function (ns) {

    function StyleEngine(model, layerKey) {

        var getTheme = function (layer) {
            var zoom = Math.max(model.extent.width(), model.extent.height());
            if (model.currentZoomSize) {
                zoom = model.currentZoomSize.zoom;
            }

            for (var i = 0; i < layer.themes.length; i++) {
                var theme = layer.themes[i];
                if (zoom >= theme.minZoom && zoom <= theme.maxZoom)
                    return theme;
            }
            return null;
        }

        var getStyle = function (layer) {
            if (layer.styles && layer.styles.length > 0)
                //TODO: This needs to cascade the styles
                return layer.styles[0];

            return null;
        }

        var theme;

        var getThemer = function (layer) {

            var style = getStyle(layer);

            //Is it a local layer without styling?
            if (layer.isTemp && !style)
                return new ns.DefaultThemer();

            //Does it have local styling?
            if (style)
                return new ns.DynamicThemer(style);

            //Does it have no server themes?
            if (!layer.themes || layer.themes.length == 0)
                return new ns.DefaultThemer();

            theme = getTheme(layer);

            //Is it missing a theme for the current zoom?
            if (!theme)
                return null;

            if (theme.type == "StandardTheme")
                return new ns.StandardThemer(theme)

            if (theme.type == "RangeTheme")
                return new ns.RangeThemer(theme)

            if (theme.type == "HeatMapTheme")
                return new ns.HeatMapThemer(theme);

            throw "Unknown themer";
        }

        var layer = model.getLayer(layerKey);

        var featureStyles = layer.featureStyles || {};

        var themer = getThemer(layer);

        this.process = function (features) {

            Mapzania.Log.debug("Processing Geometries[" + layer.key + "]", "StyleEngine", { themer: themer, layer: layer, features:features });

            var scaleType = layer.scaleType != null ? layer.scaleType : model.scaleType;

            if (features.length == 0 || themer == null) {

                var res =  {
                    scaleType: scaleType,
                    symbolizedGeometry: []
                };

                Mapzania.Log.debug("Cannot process geometries [" + layer.key + "]", "StyleEngine", { themer: themer, layer: layer, features: features, sgs: res });

                return res;
            }

            if (themer.evalFeature) {
                for (var i = 0; i < features.length; i++) {
                    var ftr = features[i];
                    themer.evalFeature(ftr);
                }
            }

            if (themer.doneWithEvalFeatures) {
                themer.doneWithEvalFeatures();
            }

            var symbolizedGeometry = [];

            if (theme && theme.scaleType) {
                scaleType = theme.scaleType;
            }

            var labelStrategy = new Mapzania.StandardLabelFormattingStrategy(model.extent.grow(1.0));

            for (var i = 0; i < features.length; i++) {
                var ftr = features[i];

                var ftrThemer = themer;

                //backward compatibility
                if (ftr.style)
                    ftrThemer = new ns.DynamicThemer(ftr.style);

                var ftrStyle = featureStyles[ftr.id];

                if (ftrStyle)
                    ftrThemer = new ns.DynamicThemer(ftrStyle[0]);

                var sym = ftrThemer.getSymbol(ftr);

                if (sym == null)
                    continue;

                var sg = {};
                sg.geometry = ftr.geometry;
                sg.symbol = sym;

                var labelInfo = labelStrategy.getLabelInstance(layer, ftrThemer, ftr);
                if (labelInfo) {
                    sg.label = labelInfo;
                }

                symbolizedGeometry.push(sg);
            }



            var res = {
                scaleType: scaleType,
                symbolizedGeometry: symbolizedGeometry
            };

            Mapzania.Log.debug("Processed Geometries[" + layer.key + "]", "StyleEngine", { themer: themer, layer: layer, features: features, sgs: res });

            return res;

        }
    }

    ns.StyleEngine = StyleEngine

    return ns;

})(Mapzania || {})