﻿/// <reference path="mapzania.js"/>

var Mapzania = (function (ns) {

    var Toolbar = function ($el, map) {
        this.$el = $el;

        $(".reset", $el).click(function () {
            map.reset();
        });

        $(".zoomIn", $el).click(function () {
            map.setMouseMode(Mapzania.MouseModes.dragToZoomIn);
        });

        $(".zoomOut", $el).click(function () {
            map._zoomOut();
        });

        $(".pan", $el).click(function () {
            map.setMouseMode(Mapzania.MouseModes.dragToPanAndWheelToZoom);
        });

        $(".queryPoint", $el).click(function () {
            map.setMouseMode(Mapzania.MouseModes.clickToDraw);
            map.setDrawingMode(Mapzania.DrawingModes.drawPoint);
        });

        $(".queryLine", $el).click(function () {
            map.setMouseMode(Mapzania.MouseModes.clickToDraw);
            map.setDrawingMode(Mapzania.DrawingModes.drawLine);
        });

        $(".queryPolygon", $el).click(function () {
            map.setMouseMode(Mapzania.MouseModes.clickToDraw);
            map.setDrawingMode(Mapzania.DrawingModes.drawPolygon);
        });

        //$(".length", $el).click(function () {
        //    model.changeState(Mapzania.MapState.length);
        //});

        //$(".area", $el).click(function () {
        //    model.changeState(Mapzania.MapState.area);
        //});

        //$(".queryPolygonEnclosed", $el).click(function (e) {
        //    model.changeState(Mapzania.MapState.queryPolygonEnclosed);
        //});

        //$(".printMap", $el).click(function () {
        //    model.changeState(Mapzania.MapState.printMap);
        //});

        //$(".exportMap", $el).click(function () {
        //    model.changeState(Mapzania.MapState.exportMap);
        //});
    }

    Toolbar.buttons = {
        reset: { key: "reset", text: "Reset" },
        zoomIn: { key: "zoomIn", text: "Zoom In" },
        zoomOut: { key: "zoomOut", text: "Zoom Out" },
        pan: { key: "pan", text: "Pan" },
        queryPoint: { key: "queryPoint", text: "Query Point" },
        queryLine: { key: "queryLine", text: "Query Line" },
        queryPolygon: { key: "queryPolygon", text: "Query Polygon" },
    };

    var btns = Toolbar.buttons;

    Toolbar.Layouts = {
        simple: ["reset", "zoomIn", "zoomOut", "pan"],
        standard: ["reset", "zoomIn", "zoomOut", "pan", "queryPoint", "queryLine", "queryPolygon"]
    };

    ns.Toolbar = Toolbar;

    return ns;

})(Mapzania || {});
