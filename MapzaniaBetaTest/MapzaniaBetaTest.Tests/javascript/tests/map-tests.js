﻿/// <reference path="../jquery/jquery.min.js"/>
/// <reference path="../jasmine/jasmine.js"/>
/// <reference path="../jasmine/jasmine-jquery.js"/>
/// <reference path="../mapzania.js"/>
describe("Map object", function () {

    var map = new Mapzania.Map();

    it("fire an event multiple times", function () {
        var i = 0;

        var iterate = function(){
            i++;
        };

        map.on("click", iterate);

        map.fire("click");
        map.fire("click");
        map.fire("click");
        map.fire("notclick");
        map.fire("notclick");

        expect(i).toBe(3);
    })

    it("fire an once-only event once", function () {
        var i = 0;

        var iterate = function () {
            i++;
        };

        map.once("click", iterate);

        map.fire("click");
        map.fire("click");
        map.fire("click");
        map.fire("notclick");
        map.fire("notclick");

        expect(i).toBe(1);
    })

    it("fire multiple event", function () {
        var i = 0;

        var iterate = function () {
            i++;
        };

        map.on("click", iterate);
        map.on("click", iterate);
        map.on("click", iterate);
        map.fire("notclick");
        map.fire("notclick");

        map.fire("click");

        expect(i).toBe(3);
    })

    it("fire event and receive data", function () {

        var data = { foo: "bar" };
        var newData;

        var iterate = function (data) {
            newData = data;
        };

        map.on("click", iterate);

        map.fire("click", data);
        map.fire("notclick");
        map.fire("notclick");

        expect(newData).toBe(data);
    })
});