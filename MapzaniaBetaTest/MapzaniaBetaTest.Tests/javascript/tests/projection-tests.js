﻿/// <reference path="../jasmine/jasmine.js"/>
/// <reference path="../mapzania.js"/>
describe("Projection", function () {

    it("should project from LatLong to Google", function () {
        var point = new Mapzania.Point();
        point.x = 10;
        point.y = 20;

        var ftr = { geometry: point };

        Mapzania.Projection.projectFeatureArray([ftr], "LATLONG", "GOOGLE");

        expect(point.x).toBeCloseTo(1113194.908, 3);
        expect(point.y).toBeCloseTo(2273030.927, 3);
    })
});

describe("Projection", function () {

    it("should project from Google to Latlong", function () {
        var point = new Mapzania.Point();
        point.x = 1113194.908;
        point.y = 2273030.927;

        var ftr = { geometry: point };

        Mapzania.Projection.projectFeatureArray([ftr], "GOOGLE", "LATLONG");

        expect(point.x).toBeCloseTo(10, 0);
        expect(point.y).toBeCloseTo(20, 0);
    })
});